// SPDX-License-Identifier: Apache-2.0

package acorebase

import chisel3._
import chiseltest._
import org.scalatest.flatspec.AnyFlatSpec
import a_core_common._

class DebugSpec extends AnyFlatSpec with ChiselScalatestTester {
  it should "convert Instruction.Type to correct ASCII string" in {
    test(new DebugInstrText) { c => 
      assert(c.toSimpleString(Instruction.ADD) === "ADD")
      assert(c.toSimpleString(Instruction.BEQ) === "BEQ")
      assert(c.toSimpleString(Instruction.AUIPC) === "AUIPC")
    }
  }
}