// SPDX-License-Identifier: Apache-2.0

package acorebase

import chisel3._
import chiseltest._
import org.scalatest.flatspec.AnyFlatSpec
import a_core_common._
import amba.common._

/*
class DataCacheSpec extends AnyFlatSpec with ChiselScalatestTester {

  lazy val memory_map = Map[MemoryRange, AnyRef] (
    MemoryRange(begin="h0000_1000", depth=14, mode=MemoryMode.R, cached=false) -> None,
    MemoryRange(begin="h2000_0000", depth=14, mode=MemoryMode.RW, cached=true) -> None,
    MemoryRange(begin="h3000_0000", depth=5, mode=MemoryMode.RW, cached=false) -> None,
    MemoryRange(begin="h3000_1000", depth=4, mode=MemoryMode.W, cached=false) -> None
  )
  
  it should "write through" in {
    val config = ACoreConfig.default
    config.dCache.option = CacheOption.ALWAYS_ON
    config.dCache.write_policy = WritePolicy.WRITE_THROUGH
    test(new DataCache(memory_map, config)).withAnnotations(Seq(WriteVcdAnnotation)) { c =>

      def readRequest(raddr: UInt) = {
        c.io.core.mem_if.req.poke(true.B)
        c.io.core.mem_if.info.ren.poke(true.B)
        c.io.core.mem_if.info.addr.poke(raddr)
        c.clock.step()
        c.io.core.mem_if.req.poke(false.B)
        c.io.core.mem_if.info.ren.poke(false.B)
        c.io.core.mem_if.info.addr.poke(0.U)
      }

      def writeRequest(waddr: UInt, wdata: UInt, op_width: MemOpWidth.Type, op_unsigned: Bool) = {
        c.io.core.mem_if.req.poke(true.B)
        c.io.core.mem_if.info.wen.poke(true.B)
        c.io.core.mem_if.info.addr.poke(waddr)
        c.io.core.mem_if.info.wdata.poke(wdata)
        c.io.core.mem_if.info.op_width.poke(op_width)
        c.io.core.mem_if.info.op_unsigned.poke(op_unsigned)
        c.clock.step()
        c.io.core.mem_if.req.poke(false.B)
        c.io.core.mem_if.info.wen.poke(false.B)
      }

      def memResponse(data: UInt) = {
        c.io.mem.mem_if.rdata.valid.poke(true.B)
        c.io.mem.mem_if.rdata.bits.poke(data)
        c.clock.step()
        c.io.mem.mem_if.rdata.valid.poke(false.B)
        c.io.mem.mem_if.rdata.bits.poke(0.U)
      }

      c.io.core.mem_if.req.poke(false.B)
      c.io.core.mem_if.info.ren.poke(false.B)
      c.io.core.mem_if.info.wen.poke(false.B)
      c.io.core.mem_if.info.op_width.poke(MemOpWidth.WORD)
      c.io.core.mem_if.info.op_unsigned.poke(false.B)
      c.io.core.mem_if.info.addr.poke(0.U)
      c.io.core.mem_if.info.wdata.poke(0.U)
      c.io.core.mem_if.info.wmask.poke("b1111".U)
      c.io.mem.mem_if.gnt.poke(false.B)
      c.io.mem.mem_if.rd_fault.poke(false.B)
      c.io.mem.mem_if.rd_misalig.poke(false.B)
      c.io.mem.mem_if.wr_fault.poke(false.B)
      c.io.mem.mem_if.wr_misalig.poke(false.B)
      c.io.mem.mem_if.fault.poke(false.B)
      c.io.mem.mem_if.rdata.bits.poke(0.U)
      c.io.mem.mem_if.rdata.valid.poke(false.B)

      writeRequest("h3000_1000".U, "hFAFABABA".U, MemOpWidth.WORD, true.B)

      c.clock.step(5)
    }
  }
  //it should "test dcache" in {
  //  val config = ACoreConfig()
  //  config.dCache_depth = 16
  //  config.dCache_line_depth = 4
  //  println(f"Cache size: ${config.getCacheSize()} bits.")

  //  test(new DataCache(config)).withAnnotations(Seq(WriteVcdAnnotation)) { c =>
  //    println(f"${c.tagHighIdx}")

  //    def readRequest(raddr: UInt) = {
  //      c.io.core.mem_if.req.poke(true.B)
  //      c.io.core.mem_if.info.ren.poke(true.B)
  //      c.io.core.mem_if.info.addr.poke(raddr)
  //      c.clock.step()
  //      c.io.core.mem_if.req.poke(false.B)
  //      c.io.core.mem_if.info.ren.poke(false.B)
  //      c.io.core.mem_if.info.addr.poke(0.U)
  //    }

  //    def memResponse(data: UInt) = {
  //      c.io.mem.mem_if.rdata.valid.poke(true.B)
  //      c.io.mem.mem_if.rdata.bits.poke(data)
  //      c.clock.step()
  //      c.io.mem.mem_if.rdata.valid.poke(false.B)
  //      c.io.mem.mem_if.rdata.bits.poke(0.U)
  //    }

  //    c.io.core.mem_if.req.poke(false.B)
  //    c.io.core.mem_if.info.ren.poke(false.B)
  //    c.io.core.mem_if.info.wen.poke(false.B)
  //    c.io.core.mem_if.info.op_width.poke(MemOpWidth.WORD)
  //    c.io.core.mem_if.info.op_unsigned.poke(false.B)
  //    c.io.core.mem_if.info.addr.poke(0.U)
  //    c.io.core.mem_if.info.wdata.poke(0.U)
  //    c.io.core.mem_if.info.wmask.poke("b1111".U)
  //    c.io.mem.mem_if.gnt.poke(false.B)
  //    c.io.mem.mem_if.rd_fault.poke(false.B)
  //    c.io.mem.mem_if.rd_misalig.poke(false.B)
  //    c.io.mem.mem_if.wr_fault.poke(false.B)
  //    c.io.mem.mem_if.wr_misalig.poke(false.B)
  //    c.io.mem.mem_if.fault.poke(false.B)
  //    c.io.mem.mem_if.rdata.bits.poke(0.U)
  //    c.io.mem.mem_if.rdata.valid.poke(false.B)
  //    
  //    c.clock.step()

  //    // Create request
  //    readRequest("b11001100".U)

  //    c.clock.step(5)

  //    for (i <- (0 until config.dCache_line_depth)) {
  //      memResponse(i.U)
  //    }

  //    c.clock.step(5)

  //    readRequest("b11001100".U)

  //    c.clock.step(5)

  //  }
  //}
}
*/

