// SPDX-License-Identifier: Apache-2.0

// Initially written by Aleksi Korsman (aleksi.korsman@aalto.fi), 2023-03-21
package acorebase

import chisel3._
import chisel3.util._
import chiseltest._
import org.scalatest.flatspec.AnyFlatSpec
import a_core_common._
import amba.common._
import scala.util.Random
import scala.math

/*

class TestMemory(config: ACoreConfig, 
                 mem_size: Int, 
                 read_delay: Int = 1, 
                 burst: Boolean = false) extends Module {
  val io = IO(new Bundle {
    val req = Input(Bool())
    val ren = Input(Bool())
    val raddr = Input(UInt(config.addr_width.W))
    val waddr = Input(UInt(config.addr_width.W))
    val gnt = Output(Bool())
    val rdata = ValidIO(UInt(config.data_width.W))
    val wen = Input(Bool())
    val wdata = Input(UInt(config.data_width.W))
  })

  val delayChain = RegInit(VecInit.fill(read_delay) {0.U(config.data_width.W)})
  val validChain = RegInit(VecInit.fill(read_delay) {false.B})

  val busy = RegInit(false.B)
  val mem = Mem(mem_size, UInt(config.data_width.W))
  val finish = !(busy && !validChain(read_delay-1))
  val gnt = {
    if (burst) io.req
    else io.req && finish
  }
  io.gnt := gnt
  
  val renReg = RegNext(io.ren && gnt)

  when (io.ren && gnt) {
    delayChain(0) := mem.read(io.raddr)
    validChain(0) := true.B
    busy := true.B
  } .otherwise {
    delayChain(0) := 0.U
    validChain(0) := false.B
    busy := !finish
  }

  if (read_delay > 1) {
    for (i <- (1 until read_delay)) {
      delayChain(i) := delayChain(i-1)
      validChain(i) := validChain(i-1)
    }
  }
  io.rdata.bits  := delayChain(read_delay-1)
  io.rdata.valid := validChain(read_delay-1)

  when (io.wen) {
    mem.write(io.waddr, io.wdata)
  }

}

class DUTWithTestMemory(val config: ACoreConfig, mem_size: Int, read_delay: Int = 1, burst: Boolean = false) extends Module {
  val dut = Module(new SeqMemRequest(config))
  val mem = Module(new TestMemory(config, mem_size, read_delay, burst))
  val dut_io = IO(dut.io.cloneType)
  val mem_io = IO(mem.io.cloneType)
  dut.io <> dut_io
  mem.io <> mem_io
  mem.io.req := dut.io.req
  mem.io.ren := dut.io.ren
  mem.io.raddr := dut.io.addr(config.addr_width-1,2)
  dut.io.gnt := mem.io.gnt
  dut.io.rdata <> mem.io.rdata
}

class SeqMemRequestSpec extends AnyFlatSpec with ChiselScalatestTester {

  val config = ACoreConfig.default
  config.dCache.line_depth = 1
  val memSize = 128
  val randgen = new Random(666)
  val writeData = Seq.tabulate(memSize){i => randgen.nextInt(math.pow(2, config.data_width).toInt).U(config.data_width.W)}

  def loadDefaults(c: DUTWithTestMemory) = {
    c.dut_io.cache_addr.poke(0.U)
    c.dut_io.start.poke(false.B)
    c.mem_io.wen.poke(false.B)
    c.mem_io.wdata.poke(0.U)
  }

  def loadMemory(c: DUTWithTestMemory) = {
    for (i <- 0 until memSize) {
      c.mem_io.waddr.poke(i.U)
      c.mem_io.wen.poke(true.B)
      c.mem_io.wdata.poke(writeData(i))
      c.clock.step()
    }
    c.mem_io.wen.poke(false.B)
  }

  def startReadRequest(c: DUTWithTestMemory, addr: Int) = {
    c.dut_io.cache_addr poke addr.U
    c.dut_io.start      poke true.B
    c.clock.step()
    c.dut_io.start      poke false.B
  }

  def checkReadRequest(c: DUTWithTestMemory, addr: Int, read_delay: Int = 1, burst: Boolean = false) = {
    if (burst) c.clock.step(read_delay - 1)
    for (i <- (0 until config.dCache.line_depth)) {
      if (burst) c.clock.step()
      else c.clock.step(read_delay)
      c.dut_io.cache_wen expect true.B
      c.dut_io.cache_widx expect i.U
      c.dut_io.cache_wdata expect writeData(addr*config.dCache.line_depth+i)
    }
  }

  it should "be busy after start" in {
    test(new SeqMemRequest(config)).withAnnotations(Seq(WriteVcdAnnotation)) { c =>
      c.io.cache_addr.poke(0.U)
      c.io.start.poke(true.B)
      c.io.gnt.poke(false.B)
      c.io.rdata.bits.poke(0.U)
      c.io.rdata.valid.poke(false.B)
      c.io.busy expect false.B
      c.clock.step()
      c.io.busy expect true.B
    }
  }

  it should "test read request sequence" in {
    test(new DUTWithTestMemory(config, memSize)).withAnnotations(Seq(WriteVcdAnnotation)) { c =>
      loadDefaults(c)
      loadMemory(c)
      c.clock.step()
      fork {
        for (cacheAddr <- (0 until memSize/config.dCache.line_depth)) {
          startReadRequest(c, cacheAddr)
          c.clock.step(config.dCache.line_depth)
        }
      } .fork {
        for (cacheAddr <- (0 until memSize/config.dCache.line_depth)) {
          c.clock.step()
          checkReadRequest(c, cacheAddr)
        }
      } .join()
      c.clock.step(10)
    }
  }

  it should "test read request sequence with multiple cycle delay" in {
    val read_delay = 3
    test(new DUTWithTestMemory(config, memSize, read_delay)).withAnnotations(Seq(WriteVcdAnnotation)) { c =>
      loadDefaults(c)
      loadMemory(c)
      c.clock.step()
      fork {
        for (cacheAddr <- (0 until memSize/config.dCache.line_depth)) {
          startReadRequest(c, cacheAddr)
          c.clock.step(config.dCache.line_depth*read_delay)
        }
      } .fork {
        for (cacheAddr <- (0 until memSize/config.dCache.line_depth)) {
          c.clock.step()
          checkReadRequest(c, cacheAddr, read_delay)
        }
      } .join()
      c.clock.step(10)
    }
  }
}
*/
