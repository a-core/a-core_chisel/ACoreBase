// SPDX-License-Identifier: Apache-2.0

package acorebase

import chisel3._
import chiseltest._
import org.scalatest.flatspec.AnyFlatSpec
import a_core_common._

import scala.math.BigInt
import scala.util.Random

class IntRegFileSpec extends AnyFlatSpec with ChiselScalatestTester {

  it should "read zero from x0" in {
    test(new IntRegFile(XLEN=32)) { c =>
      // x0 should read zero
      c.io.read_a_addr  poke    0.U
      c.io.read_a_data  expect  0.U
    }
  }
  
  it should "read zero from x0 after attempted write" in {
    test(new IntRegFile(XLEN=32)) { c =>
      c.io.write_en   poke true.B
      c.io.write_addr poke 0.U
      c.io.write_data poke 1234.U
      c.clock.step()
      c.io.read_a_addr poke   0.U
      c.io.read_a_data expect 0.U
    }
  }

  it should "should write to x1-x31 successfully" in {
    test(new IntRegFile(XLEN=32)) { c =>
      val rand = new Random()
      for (i <- 1 until 32) {
        val testValue = BigInt(c.XLEN, rand)
        // write random value to register x[i]
        c.io.write_en   poke true.B
        c.io.write_addr poke i.U
        c.io.write_data poke testValue.U
        // step simulation
        c.clock.step()
        c.io.write_en poke false.B
        // read value from read port A
        c.io.read_a_addr poke   i.U
        c.io.read_a_data expect testValue.U
        // read value from read port B
        c.io.read_b_addr poke   i.U
        c.io.read_b_data expect testValue.U
      }
    }
  }

}