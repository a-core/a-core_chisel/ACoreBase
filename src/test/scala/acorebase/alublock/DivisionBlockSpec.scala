// The test was prepared for A-Core Division Block

package acorebase

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chiseltest._
import chisel3.tester.RawTester.test
import chisel3.testers.BasicTester
import org.scalatest.flatspec.AnyFlatSpec
import a_core_common._
import scala.math.{ BigInt, pow }
import scala.util.Random

trait DivisionWrapperTester {
  this: AnyFlatSpec with ChiselScalatestTester =>

  /**
    * Converts a Scala BigInt into Chisel UInt representation
    * Does not modify bits
    * For example, -1 BigInt would be turned into "hFFFFFFFF".U
    *
    * @param num BigInt input
    * @param XLEN Width of the data 
    * @return Chisel unsigned type
    */
  def BigIntToUInt(num: BigInt, XLEN: Int): UInt  = {
    if (num == 0) 0.U(XLEN.W)
    else if (num < 0) (num+pow(2,XLEN).toLong).U(XLEN.W)
    else num.U(XLEN.W)
  }

  def testDivisionWrapper(XLEN: Int, DIVCOUNT: Int, signed: Boolean): Unit = {
    test(new DivisionWrapper(XLEN=XLEN, DIVCOUNT=DIVCOUNT)){ c =>

      // Long integers should be converted to BigInts to support 64-bit unsigned tests
      // The Division Module itself should support 64 or any other bit width 
      assert(XLEN < 64, "This test does not support 64 bit inputs")

      var clockCount = 0
      val extraCalcs = 2 // How many more calculations are generated than DIVCOUNT
      val totalCalcs = c.DIVCOUNT + extraCalcs
      val testDividends:  Array[BigInt] = new Array[BigInt](totalCalcs)
      val testDivisors:   Array[BigInt] = new Array[BigInt](totalCalcs)
      val testQuotiens:   Array[BigInt] = new Array[BigInt](totalCalcs)
      val testRemainders: Array[BigInt] = new Array[BigInt](totalCalcs)
      val rand = new Random()

      // Generate test data and golden model outputs
      for (i <- 0 until totalCalcs) {
        var dividend = BigInt(c.XLEN, rand)
        var divisor  = BigInt(c.XLEN, rand)
        if (signed) {
          dividend -= pow(2,  c.XLEN-1).toLong
          divisor  -= pow(2,  c.XLEN-1).toLong 
        }

        // Increase chance of producing a special case
        if (rand.nextInt(c.DIVCOUNT) == 0) {
          divisor = 0
        }
        if (signed && (rand.nextInt(c.DIVCOUNT) == 0)) {
          dividend = -1
          divisor = -pow(2, c.XLEN-1).toLong
        }

        var quotient = math.pow(2, c.XLEN).toLong - 1 
        if (divisor != 0) quotient = dividend.toLong / divisor.toLong
        var remainder = dividend
        if (divisor != 0) remainder = dividend % divisor
        testDividends(i)  = dividend
        testDivisors(i)   = divisor 
        
        testQuotiens(i)   = quotient
        testRemainders(i) = remainder
      }

      // The division wrapper may produce outputs already during
      // input writing phase (e.g. division by zero), therefore
      // the test needs to be forked into two processes
      fork {
        // This fork writes inputs
        for (i <- 0 until totalCalcs) {

          // Wait if the wrapper is busy
          while(c.out.division_busy.peek().litToBoolean) c.clock.step()

          c.in.sign_en            poke    signed.B
          c.in.dividend           poke    BigIntToUInt(testDividends(i), c.XLEN)
          c.in.divisor            poke    BigIntToUInt(testDivisors(i), c.XLEN)
          c.in.division_en        poke    true.B
          // Insert random pause between writes
          if (rand.nextBoolean()) {
            c.clock.step()
            c.in.division_en        poke    false.B
            val delay = rand.nextInt(5)
            c.clock.step(delay)
          } else {
            c.clock.step()
          }
        }
        // Disable the divison block
        c.in.sign_en            poke    false.B
        c.in.dividend           poke    0.U
        c.in.divisor            poke    0.U
        c.in.division_en        poke    false.B
      } .fork {
        // This fork checks outputs
        for (i <- 0 until totalCalcs) {

          // Wait until there is a valid result
          while(!c.out.result_valid.peek().litToBoolean) c.clock.step()

          if (testDivisors(i) == 0) { 
            c.out.quotient          expect  BigIntToUInt(testQuotiens(i), c.XLEN)
            c.out.remainder         expect  BigIntToUInt(testRemainders(i), c.XLEN)
            c.out.result_valid      expect  true.B
            c.out.division_error    expect  true.B
            c.out.division_busy     expect  false.B
            c.out.block_num         expect  pow(2,i%c.DIVCOUNT).toLong.U(c.DIVCOUNT-1,0)
          } else if (testDivisors(i) == -1 && testDividends(i) == -pow(2, c.XLEN-1)) {
            c.out.quotient          expect  BigIntToUInt(-1, c.XLEN)
            c.out.remainder         expect  0.U
            c.out.result_valid      expect  true.B
            c.out.division_error    expect  true.B
            c.out.division_busy     expect  false.B
            c.out.block_num         expect  pow(2,i%c.DIVCOUNT).toLong.U(c.DIVCOUNT-1,0)
          } else { 
            c.out.quotient          expect  BigIntToUInt(testQuotiens(i), c.XLEN)
            c.out.remainder         expect  BigIntToUInt(testRemainders(i), c.XLEN)
            c.out.result_valid      expect  true.B
            c.out.division_error    expect  false.B
            c.out.division_busy     expect  false.B
            c.out.block_num         expect  pow(2,i%c.DIVCOUNT).toLong.U(c.DIVCOUNT-1,0)
          }
          c.clock.step()
        }
      } .join()
    }
  }
}

class DivisionBlockSpec extends AnyFlatSpec with ChiselScalatestTester with DivisionWrapperTester {

 

  it should "divide two signed 32-bit nums" in {

    test(new DivisionTopBlock(WIDTH=32)){ c =>

      c.in.sign_en            poke    true.B
      c.in.dividend           poke    1.U
      c.in.divisor            poke    1.U
      c.in.division_en        poke    true.B
      c.clock.step(1)

      // 33 clock cycle
      for(j <- 0 until c.WIDTH + 2){ // busy signal expections
        c.out.division_busy     expect  true.B
        c.out.result_valid      expect  false.B
        c.clock.step(1)
      }

      c.out.remainder         expect  0.U
      c.out.quotient          expect  1.U
      c.out.result_valid      expect  true.B
      c.out.division_error    expect  false.B
      c.out.division_busy     expect  false.B

      c.in.sign_en            poke    true.B
      c.in.dividend           poke    "hFFFFFFFF".U
      c.in.divisor            poke    1.U
      c.in.division_en        poke    true.B
      c.clock.step(c.WIDTH + 3)
      c.out.quotient          expect  "hFFFFFFFF".U
      c.out.remainder         expect  0.U
      c.out.result_valid      expect  true.B
      c.out.division_error    expect  false.B

      c.in.sign_en            poke    true.B
      c.in.dividend           poke    1.U
      c.in.divisor            poke    "hFFFFFFFF".U
      c.in.division_en        poke    true.B
      c.clock.step(c.WIDTH + 3)
      c.out.quotient          expect  "hFFFFFFFF".U
      c.out.remainder         expect  0.U
      c.out.result_valid      expect  true.B
      c.out.division_error    expect  false.B

      c.in.dividend           poke    12.U
      c.in.divisor            poke    3.U
      c.in.division_en        poke    true.B
      c.clock.step(c.WIDTH + 3)
      c.out.quotient          expect  4.U
      c.out.remainder         expect  0.U
      c.out.result_valid      expect  true.B
      c.out.division_error    expect  false.B

      c.in.dividend           poke    50.U
      c.in.divisor            poke    15.U
      c.in.division_en        poke    true.B
      c.clock.step(c.WIDTH + 3)
      c.out.quotient          expect  3.U
      c.out.remainder         expect  5.U
      c.out.result_valid      expect  true.B
      c.out.division_error    expect  false.B

      // gives error:
      c.in.dividend           poke    102.U
      c.in.divisor            poke    0.B
      c.in.division_en        poke    true.B
      c.clock.step(2)
      c.out.quotient          expect  "hFFFFFFFF".U
      c.out.remainder         expect  102.U
      c.out.result_valid      expect  true.B
      c.out.division_error    expect  true.B
      c.in.dividend           poke    BigIntToUInt(-5, c.WIDTH)
      c.in.divisor            poke    0.B
      c.in.division_en        poke    true.B
      c.clock.step(2)
      c.out.quotient          expect  "hFFFFFFFF".U
      c.out.remainder         expect  BigIntToUInt(-5, c.WIDTH)
      c.out.result_valid      expect  true.B
      c.out.division_error    expect  true.B
      c.in.dividend           poke    "h80000000".U
      c.in.divisor            poke    "hFFFFFFFF".U
      c.in.division_en        poke    true.B
      c.clock.step(2)
      c.out.quotient          expect  "h80000000".U
      c.out.remainder         expect  0.U
      c.out.result_valid      expect  true.B
      c.out.division_error    expect  true.B

    }
  }
  

  it should "divide two unsigned 32-bit nums" in {
      
    test(new DivisionTopBlock(WIDTH=32)){ c =>

      c.in.sign_en            poke    false.B

      c.in.dividend           poke    1.U
      c.in.divisor            poke    1.U
      c.in.division_en        poke    true.B
      c.clock.step()
      c.in.division_en        poke    false.B
      c.clock.step(c.WIDTH + 2)
      c.out.quotient          expect  1.U
      c.out.remainder         expect  0.U
      c.out.result_valid      expect  true.B
      c.out.division_error    expect  false.B

      c.in.dividend           poke    "hFFFFFFFF".U
      c.in.divisor            poke    1.U
      c.in.division_en        poke    true.B
      c.clock.step()
      c.in.division_en        poke    false.B
      c.clock.step(c.WIDTH + 2)
      c.out.quotient          expect  "hFFFFFFFF".U
      c.out.remainder         expect  0.U
      c.out.result_valid      expect  true.B
      c.out.division_error    expect  false.B

      c.in.dividend           poke    12.U
      c.in.divisor            poke    3.U
      c.in.division_en        poke    true.B
      c.clock.step()
      c.in.division_en        poke    false.B
      c.clock.step(c.WIDTH + 2)
      c.out.quotient          expect  4.U
      c.out.remainder         expect  0.U
      c.out.result_valid      expect  true.B
      c.out.division_error    expect  false.B

      c.in.dividend           poke    50.U
      c.in.divisor            poke    15.U
      c.in.division_en         poke    true.B
      c.clock.step()
      c.in.division_en        poke    false.B
      c.clock.step(c.WIDTH + 2)
      c.out.quotient          expect  3.U
      c.out.remainder         expect  5.U
      c.out.result_valid      expect  true.B
      c.out.division_error    expect  false.B

      c.in.dividend           poke    102.U
      c.in.divisor            poke    0.B
      c.in.division_en         poke    true.B
      c.clock.step()
      c.in.division_en        poke    false.B
      c.clock.step(c.WIDTH + 3)
      c.out.quotient          expect  "hFFFFFFFF".U
      c.out.remainder         expect  102.U
      c.out.result_valid      expect  true.B
      c.out.division_error    expect  true.B
      
      c.in.sign_en            poke    false.B
      // loop test
      val rand = new Random()
      for (i <- 1 to 100) {
        //...
        val test0 = BigInt(c.WIDTH, rand)
        val test1 = BigInt(c.WIDTH, rand)
        var testQuo_o = (pow(2,c.WIDTH)-1).toLong
        var testRem_o = test0
        if (test1 != 0) testQuo_o = (test0 / test1).toLong // Qutient Side
        if (test1 != 0) testRem_o = test0 % test1 // Remainder Side

        c.in.dividend         poke    test0.U
        c.in.divisor          poke    test1.U
        c.in.division_en      poke    true.B
        c.clock.step()
        c.in.division_en      poke    false.B

        if (test1 == 0) {
          //...
          c.clock.step(1)  
          c.out.quotient          expect  0.U
          c.out.remainder         expect  0.U
          c.out.result_valid      expect  false.B
          c.out.division_error    expect  true.B

        } else {
          //...
          c.clock.step(c.WIDTH + 2)
          c.out.quotient          expect  (testQuo_o).U
          c.out.remainder         expect  (testRem_o).U
          c.out.result_valid      expect  true.B
          c.out.division_error    expect  false.B
        }
      } 
    }
  }

  it should "divide multiple two signed 32-bit nums" in {
    it should behave like testDivisionWrapper(XLEN = 32, DIVCOUNT = 5, signed  = true)
    it should behave like testDivisionWrapper(XLEN = 32, DIVCOUNT = 2, signed  = true)
    it should behave like testDivisionWrapper(XLEN = 32, DIVCOUNT = 32, signed = true)
  }
  
  it should "divide multiple two unsigned 32-bit nums" in {
    it should behave like testDivisionWrapper(XLEN = 32, DIVCOUNT = 5, signed = false)
    it should behave like testDivisionWrapper(XLEN = 32, DIVCOUNT = 2, signed = false)
    it should behave like testDivisionWrapper(XLEN = 32, DIVCOUNT = 32, signed = false)
  }
    
}