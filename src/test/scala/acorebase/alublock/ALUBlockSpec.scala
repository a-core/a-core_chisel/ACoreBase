// SPDX-License-Identifier: Apache-2.0

package acorebase

import chisel3._
import chiseltest._
import org.scalatest.flatspec.AnyFlatSpec
import a_core_common._

class ALUBlockSpec extends AnyFlatSpec with ChiselScalatestTester {
  
  it should "work with all input combinations (testing on ADD only)" in {
    test(new ALUBlock(ACoreConfig.default)) { c =>
      // set inputs values to constants
      c.io.in.data.read_a_data  poke 1.U
      c.io.in.data.read_b_data  poke 2.U
      c.io.in.data.imm          poke 3.U
      c.io.in.data.pc           poke 4.U

      c.io.in.control.operation    poke ALUOp.ADD

      // a = rs1
      // b = rs1
      c.io.in.control.operand_a_src poke AluOperandSrc.RS1
      c.io.in.control.operand_b_src poke AluOperandSrc.RS1
      c.io.out.data.result expect 2.U
      // a = rs1
      // b = rs2
      c.io.in.control.operand_a_src poke AluOperandSrc.RS1
      c.io.in.control.operand_b_src poke AluOperandSrc.RS2
      c.io.out.data.result expect 3.U
      // a = rs1
      // b = imm
      c.io.in.control.operand_a_src poke AluOperandSrc.RS1
      c.io.in.control.operand_b_src poke AluOperandSrc.IMM
      c.io.out.data.result expect 4.U
      // a = rs1
      // b = pc
      c.io.in.control.operand_a_src poke AluOperandSrc.RS1
      c.io.in.control.operand_b_src poke AluOperandSrc.PC
      c.io.out.data.result expect 5.U

      // a = rs2
      // b = rs1
      c.io.in.control.operand_a_src poke AluOperandSrc.RS2
      c.io.in.control.operand_b_src poke AluOperandSrc.RS1
      c.io.out.data.result expect 3.U
      // a = rs1
      // b = rs2
      c.io.in.control.operand_a_src poke AluOperandSrc.RS2
      c.io.in.control.operand_b_src poke AluOperandSrc.RS2
      c.io.out.data.result expect 4.U
      // a = rs1
      // b = imm
      c.io.in.control.operand_a_src poke AluOperandSrc.RS2
      c.io.in.control.operand_b_src poke AluOperandSrc.IMM
      c.io.out.data.result expect 5.U
      // a = rs1
      // b = pc
      c.io.in.control.operand_a_src poke AluOperandSrc.RS2
      c.io.in.control.operand_b_src poke AluOperandSrc.PC
      c.io.out.data.result expect 6.U

      // a = imm
      // b = rs1
      c.io.in.control.operand_a_src poke AluOperandSrc.IMM
      c.io.in.control.operand_b_src poke AluOperandSrc.RS1
      c.io.out.data.result expect 4.U
      // a = rs1
      // b = rs2
      c.io.in.control.operand_a_src poke AluOperandSrc.IMM
      c.io.in.control.operand_b_src poke AluOperandSrc.RS2
      c.io.out.data.result expect 5.U
      // a = rs1
      // b = imm
      c.io.in.control.operand_a_src poke AluOperandSrc.IMM
      c.io.in.control.operand_b_src poke AluOperandSrc.IMM
      c.io.out.data.result expect 6.U
      // a = rs1
      // b = pc
      c.io.in.control.operand_a_src poke AluOperandSrc.IMM
      c.io.in.control.operand_b_src poke AluOperandSrc.PC
      c.io.out.data.result expect 7.U

      // a = pc
      // b = rs1
      c.io.in.control.operand_a_src poke AluOperandSrc.PC
      c.io.in.control.operand_b_src poke AluOperandSrc.RS1
      c.io.out.data.result expect 5.U
      // a = rs1
      // b = rs2
      c.io.in.control.operand_a_src poke AluOperandSrc.PC
      c.io.in.control.operand_b_src poke AluOperandSrc.RS2
      c.io.out.data.result expect 6.U
      // a = rs1
      // b = imm
      c.io.in.control.operand_a_src poke AluOperandSrc.PC
      c.io.in.control.operand_b_src poke AluOperandSrc.IMM
      c.io.out.data.result expect 7.U
      // a = rs1
      // b = pc
      c.io.in.control.operand_a_src poke AluOperandSrc.PC
      c.io.in.control.operand_b_src poke AluOperandSrc.PC
      c.io.out.data.result expect 8.U
    }
  }

  it should "set zero flag when result is zero (testing on SUB only)" in {
    test(new ALUBlock(ACoreConfig.default)) { c =>
      // set inputs values to constants
      c.io.in.data.read_a_data  poke 1.U
      c.io.in.data.read_b_data  poke 2.U
      c.io.in.data.imm          poke 3.U
      c.io.in.data.pc           poke 4.U

      c.io.in.control.operation    poke ALUOp.SUB

      c.io.in.control.operand_a_src poke AluOperandSrc.RS1
      c.io.in.control.operand_b_src poke AluOperandSrc.RS1
      c.io.out.data.result expect 0.U
      c.io.out.data.zero expect true.B
      c.io.in.control.operand_a_src poke AluOperandSrc.RS1
      c.io.in.control.operand_b_src poke AluOperandSrc.RS2
      c.io.out.data.result expect "hFFFFFFFF".U
      c.io.out.data.zero expect false.B
      c.io.in.control.operand_a_src poke AluOperandSrc.RS1
      c.io.in.control.operand_b_src poke AluOperandSrc.IMM
      c.io.out.data.result expect "hFFFFFFFE".U
      c.io.out.data.zero expect false.B
      c.io.in.control.operand_a_src poke AluOperandSrc.RS1
      c.io.in.control.operand_b_src poke AluOperandSrc.PC
      c.io.out.data.result expect "hFFFFFFFD".U
      c.io.out.data.zero expect false.B

      c.io.in.control.operand_a_src poke AluOperandSrc.RS2
      c.io.in.control.operand_b_src poke AluOperandSrc.RS1
      c.io.out.data.result expect 1.U
      c.io.out.data.zero expect false.B
      c.io.in.control.operand_a_src poke AluOperandSrc.RS2
      c.io.in.control.operand_b_src poke AluOperandSrc.RS2
      c.io.out.data.result expect 0.U
      c.io.out.data.zero expect true.B
      c.io.in.control.operand_a_src poke AluOperandSrc.RS2
      c.io.in.control.operand_b_src poke AluOperandSrc.IMM
      c.io.out.data.result expect "hFFFFFFFF".U
      c.io.out.data.zero expect false.B
      c.io.in.control.operand_a_src poke AluOperandSrc.RS2
      c.io.in.control.operand_b_src poke AluOperandSrc.PC
      c.io.out.data.result expect "hFFFFFFFE".U
      c.io.out.data.zero expect false.B

      c.io.in.control.operand_a_src poke AluOperandSrc.IMM
      c.io.in.control.operand_b_src poke AluOperandSrc.RS1
      c.io.out.data.result expect 2.U
      c.io.out.data.zero expect false.B
      c.io.in.control.operand_a_src poke AluOperandSrc.IMM
      c.io.in.control.operand_b_src poke AluOperandSrc.RS2
      c.io.out.data.result expect 1.U
      c.io.out.data.zero expect false.B
      c.io.in.control.operand_a_src poke AluOperandSrc.IMM
      c.io.in.control.operand_b_src poke AluOperandSrc.IMM
      c.io.out.data.result expect 0.U
      c.io.out.data.zero expect true.B
      c.io.in.control.operand_a_src poke AluOperandSrc.IMM
      c.io.in.control.operand_b_src poke AluOperandSrc.PC
      c.io.out.data.result expect "hFFFFFFFF".U
      c.io.out.data.zero expect false.B

      c.io.in.control.operand_a_src poke AluOperandSrc.PC
      c.io.in.control.operand_b_src poke AluOperandSrc.RS1
      c.io.out.data.result expect 3.U
      c.io.out.data.zero expect false.B
      c.io.in.control.operand_a_src poke AluOperandSrc.PC
      c.io.in.control.operand_b_src poke AluOperandSrc.RS2
      c.io.out.data.result expect 2.U
      c.io.out.data.zero expect false.B
      c.io.in.control.operand_a_src poke AluOperandSrc.PC
      c.io.in.control.operand_b_src poke AluOperandSrc.IMM
      c.io.out.data.result expect 1.U
      c.io.out.data.zero expect false.B
      c.io.in.control.operand_a_src poke AluOperandSrc.PC
      c.io.in.control.operand_b_src poke AluOperandSrc.PC
      c.io.out.data.result expect 0.U
      c.io.out.data.zero expect true.B
    }
  }

}
