export STARTDIR=$(pwd)
echo "\n\nDownloading SoftFloat\n\n"
wget "http://www.jhauser.us/arithmetic/SoftFloat-3e.zip"
echo "\n\nDownloading TestFloat\n\n"
wget "http://www.jhauser.us/arithmetic/TestFloat-3e.zip"
echo "\n\nUnpacking SoftFloat\n\n"
unzip -q SoftFloat-3e.zip
echo "\n\nUnpacking TestFloat\n\n"
unzip -q TestFloat-3e.zip
echo "\n\nInstalling SoftFloat\n\n"
cd $STARTDIR/SoftFloat-3e/build/Linux-x86_64-GCC && make
echo "\n\nInstalling TestFloat\n\n"
cd $STARTDIR/TestFloat-3e/build/Linux-x86_64-GCC && make
echo "\n\nRemoving zip files\n\n"
rm -rf $STARTDIR/SoftFloat-3e.zip
rm -rf $STARTDIR/TestFloat-3e.zip
echo "\n\nSuccess\n\n"
