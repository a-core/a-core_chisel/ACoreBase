// SPDX-License-Identifier: Apache-2.0

package acorebase

import sys.process._
import scala.language.postfixOps
import scala.util.control.Breaks._
import chisel3._
import chiseltest._
import org.scalatest.flatspec.AnyFlatSpec
import a_core_common._

class FPUBlockSpec_Manual extends AnyFlatSpec with ChiselScalatestTester {

  def sqrtModel(float: Float) : String = {
    val sqrtRes = math.sqrt(float).toFloat
    val floatToInt = floatToIEEEBits(sqrtRes)
    floatToInt
  }

  def divModel(fracA: Float, fracB: Float) : String = {
    val divRes = fracA / fracB
    val floatToInt = floatToIEEEBits(divRes)
    floatToInt
  }

  def floatToIEEEBits(float: Float) : String = {
    val hexStr = Integer.toHexString(java.lang.Float.floatToIntBits(float)).toUpperCase()
    "h" + hexStr
  }

  it should "add two positive floating-point numbers" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(VerilatorBackendAnnotation)) { c =>
      c.io.control.op           poke      FPUOp.FADD_S
      c.io.control.rm           poke      RoundingMode.RNE

      // 3.75 + 5.125 = 8.875
      c.io.data.a            poke      "h40700000".U
      c.io.data.b            poke      "h40a40000".U
      c.io.data.out          expect    "h410e0000".U
      
      // 4.45 + 10.975 = 15.425
      c.io.data.a            poke      "h408e6666".U
      c.io.data.b            poke      "h412f999a".U
      c.io.data.out          expect    "h4176cccd".U

      // 120.00032 + 0.200942 = 120.201126
      c.io.data.a            poke      "h42f0002a".U
      c.io.data.b            poke      "h3e4dc3bd".U
      c.io.data.out          expect    "h42f0670c".U

      c.io.data.a            poke      "h40000000".U
      c.io.data.b            poke      "h00000000".U
      c.io.data.out          expect    "h40000000".U
    }
  }

  it should "add negative floating-point numbers" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(VerilatorBackendAnnotation)) { c =>
      c.io.control.op           poke      FPUOp.FADD_S
      c.io.control.rm           poke      RoundingMode.RNE

      // 234.345 + -34.322 = 200.02301
      c.io.data.a            poke      "h436a5852".U
      c.io.data.b            poke      "hc20949ba".U
      c.io.data.out          expect    "h434805e4".U

      // -98.435 + 729.3 = 630.865
      c.io.data.a            poke      "hc2c4deb8".U
      c.io.data.b            poke      "h44365333".U
      c.io.data.out          expect    "h441db75c".U
      
      // -7.37 + -0.43 = -7.7999997
      c.io.data.a            poke      "hc0ebd70a".U
      c.io.data.b            poke      "hbedc28f6".U
      c.io.data.out          expect    "hc0f99999".U
    }
  }

  

  it should "add zeros" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(VerilatorBackendAnnotation)) { c =>
      c.io.control.op           poke      FPUOp.FADD_S
      c.io.control.rm           poke      RoundingMode.RNE
      
      // 1.0 + -1.0 = 0.0
      c.io.data.a            poke      "h3f800000".U
      c.io.data.b            poke      "hbf800000".U
      c.io.data.b            poke      "h00000000".U
      
      // 0.0 + 0.0 = 0.0
      c.io.data.a            poke      "h00000000".U
      c.io.data.b            poke      "h00000000".U
      c.io.data.out          expect    "h00000000".U
      
      // -0.0 + 0.0 = 0.0
      c.io.data.a            poke      "h80000000".U
      c.io.data.b            poke      "h00000000".U
      c.io.data.out          expect    "h00000000".U
    }
  }


  it should "subtract two positive floating-point numbers when A > B" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(VerilatorBackendAnnotation)) { c =>
      c.io.control.op           poke      FPUOp.FSUB_S
      c.io.control.rm           poke      RoundingMode.RNE

      // 2.0 - 1.0 = 1.0
      c.io.data.a            poke      "h40000000".U
      c.io.data.b            poke      "h3f800000".U
      c.io.data.out          expect    "h3f800000".U

      // 3.55 - 1.33 = 2.2199998
      c.io.data.a            poke      "h40633333".U
      c.io.data.b            poke      "h3faa3d71".U
      c.io.data.out          expect    "h400e147a".U
      
      // 10.1234 - 4.6543 = 5.4690995
      c.io.data.a            poke      "h4121f972".U
      c.io.data.b            poke      "h4094f007".U
      c.io.data.out          expect    "h40af02dd".U
    }
  }

  it should "subtract two positive floating-point numbers when A < B" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(VerilatorBackendAnnotation)) { c =>
      c.io.control.op           poke      FPUOp.FSUB_S
      c.io.control.rm           poke      RoundingMode.RNE

      // 1.0 - 2.0 = -1.0
      c.io.data.a            poke      "h3f800000".U
      c.io.data.b            poke      "h40000000".U
      c.io.data.out          expect    "hbf800000".U

      // 2.567 - 6.31 = -3.743
      c.io.data.a            poke      "h402449ba".U
      c.io.data.b            poke      "h40c9eb85".U
      c.io.data.out          expect    "hc06f8d50".U

      // 7.375 - 84.23 = -76.855
      c.io.data.a            poke      "h40ec0000".U
      c.io.data.b            poke      "h42a875c3".U
      c.io.data.out          expect    "hc299b5c3".U
    }
  }

  it should "subtract negative floating-point numbers" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(VerilatorBackendAnnotation)) { c =>
      c.io.control.op           poke      FPUOp.FSUB_S
      c.io.control.rm           poke      RoundingMode.RNE

      // 13.6798 - -4.57 = 18.2498
      c.io.data.a            poke      "h415ae076".U
      c.io.data.b            poke      "hc0923d71".U
      c.io.data.out          expect    "h4191ff97".U

      // -1.3267 - 80.30024 = -80.30024
      c.io.data.a            poke      "hbfa9d14e".U
      c.io.data.b            poke      "h429df274".U
      c.io.data.out          expect    "hc2a099b9".U
      
      // -44.436 - -234.325 = 189.88899
      c.io.data.a            poke      "hc231be77".U
      c.io.data.b            poke      "hc36a5333".U
      c.io.data.out          expect    "h433de395".U
    }
  }
  

  it should "subtract zeros" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(VerilatorBackendAnnotation)) { c =>
      c.io.control.op           poke      FPUOp.FSUB_S
      c.io.control.rm           poke      RoundingMode.RNE

      c.io.data.a            poke      "h00000000".U
      c.io.data.b            poke      "h00000000".U
      c.io.data.out          expect    "h00000000".U

      c.io.data.a            poke      "h80000000".U
      c.io.data.b            poke      "h00000000".U
      c.io.data.out          expect    "h80000000".U
    }
  }
 

  it should "multiply two positive floating-point numbers" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(VerilatorBackendAnnotation)) { c =>
      c.io.control.op           poke      FPUOp.FMUL_S
      c.io.control.rm           poke      RoundingMode.RNE

      // 1.0 * 1.0 = 1.0
      c.io.data.a            poke      "h3f800000".U
      c.io.data.b            poke      "h3f800000".U
      c.io.data.out          expect    "h3f800000".U

      // 0.5 * 0.25 = 0.125
      c.io.data.a            poke      "h3f000000".U
      c.io.data.b            poke      "h3e800000".U
      c.io.data.out          expect    "h3e000000".U

      // 7.3 * 1.5 = 10.950001
      c.io.data.a            poke      "h40e9999a".U
      c.io.data.b            poke      "h3fc00000".U
      c.io.data.out          expect    "h412f3334".U

      // 15.39 * 2.76 = 42.476402
      c.io.data.a            poke      "h41763d71".U
      c.io.data.b            poke      "h4030a3d7".U
      c.io.data.out          expect    "h4229e7d6".U
    }
  }

  it should "multiply negative floating-point numbers" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(VerilatorBackendAnnotation)) { c =>
      c.io.control.op           poke      FPUOp.FMUL_S
      c.io.control.rm           poke      RoundingMode.RNE

      // -6.47 * 0.39 = -2.5233
      c.io.data.a            poke      "hc0cf0a3d".U
      c.io.data.b            poke      "h3ec7ae14".U
      c.io.data.out          expect    "hc0217dbf".U

      // 38.76 * -10.111 = -391.90234
      c.io.data.a            poke      "h421b0a3d".U
      c.io.data.b            poke      "hc121c6a8".U
      c.io.data.out          expect    "hc3c3f380".U

      // -5.0002 * -7.0254 = 35.128403
      c.io.data.a            poke      "hc0a001a3".U
      c.io.data.b            poke      "hc0e0d014".U
      c.io.data.out          expect    "h420c837c".U

      // 1.265625 * -3.1911316 = -4.038776
      c.io.data.a            poke      "h3fa20000".U
      c.io.data.b            poke      "hc04c3b80".U
      c.io.data.out          expect    "hc0813da7".U
    }
  }

  it should "multiply zeros" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(VerilatorBackendAnnotation)) { c =>
      c.io.control.op           poke      FPUOp.FMUL_S
      c.io.control.rm           poke      RoundingMode.RNE

      // 0.0 * 0.0 = 0.0
      c.io.data.a            poke      "h00000000".U
      c.io.data.b            poke      "h00000000".U
      c.io.data.out          expect    "h00000000".U

      // -0.0 * 1.0 = -0.0
      c.io.data.a            poke      "h80000000".U
      c.io.data.b            poke      "h3f800000".U
      c.io.data.out          expect    "h80000000".U

      // -5.76 * -0.0 = 0.0
      c.io.data.a            poke      "hc0b851ec".U
      c.io.data.b            poke      "h80000000".U
      c.io.data.out          expect    "h00000000".U
    }
  }

  it should "multiply inf" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(VerilatorBackendAnnotation)) { c =>
      c.io.control.op           poke      FPUOp.FMUL_S
      c.io.control.rm           poke      RoundingMode.RNE

      // inf * 1.0 = inf
      c.io.data.a            poke      "h7f800000".U
      c.io.data.b            poke      "h3f800000".U
      c.io.data.out          expect    "h7f800000".U

      // -inf * 1.0 = -inf
      c.io.data.a            poke      "hff800000".U
      c.io.data.b            poke      "h3f800000".U
      c.io.data.out          expect    "hff800000".U

      // inf * inf = inf
      c.io.data.a            poke      "h7f800000".U
      c.io.data.b            poke      "h7f800000".U
      c.io.data.out          expect    "h7f800000".U

      // inf * 0.0 = NaN
      c.io.data.a            poke      "h7f800000".U
      c.io.data.b            poke      "h00000000".U
      c.io.data.out          expect    "hffc00000".U

      // 1.2312313E20 * 1.2321123E20 = inf
      c.io.data.a            poke      "h60d595a7".U
      c.io.data.b            poke      "h60d5bcc7".U
      c.io.data.out          expect    "h7f800000".U
    }
  }


  it should "divide two positive floating-point numbers" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(VerilatorBackendAnnotation)) { c =>
      c.io.control.op           poke      FPUOp.FDIV_S
      c.io.control.rm           poke      RoundingMode.RNE

      // 1.0 / 1.0 = 1.0
      c.io.data.a            poke      "h3f800000".U
      c.io.data.b            poke      "h3f800000".U
      c.io.data.out          expect    "h3f800000".U

      // Some tests
      for (i <- -100 to 100; j <- -100 to 100) {
        breakable {
          if (i == 0 || j == 0)
            break()
          else {
            val fracA = 10.0/i
            val fracB = 10.0/j
            c.io.data.a            poke      floatToIEEEBits(fracA.toFloat).U
            c.io.data.b            poke      floatToIEEEBits(fracB.toFloat).U
            c.io.data.out          expect    divModel(fracA.toFloat,fracB.toFloat).U
          }
        }
      }
    }
  }


it should "take square root" in {
  test(new FPUBlock(XLEN=32)).withAnnotations(Seq(VerilatorBackendAnnotation)) { c =>
    c.io.control.op           poke      FPUOp.FSQRT_S
    c.io.control.rm           poke      RoundingMode.RNE

    var errCount = 0
    for (i <- 1 to 1000) {
      val mant = 10.0/i
      c.io.data.a          poke      floatToIEEEBits(0.0f + mant.toFloat).U

      // 1 bit error is allowed for now
      if (c.io.data.out.peek().litValue != sqrtModel(0.0f + mant.toFloat).U.litValue) {
        val err = sqrtModel(0.0f + mant.toFloat).U.litValue - c.io.data.out.peek().litValue
        assert(math.abs(err.toFloat) <= 1, "\nAbsolute bit error was more than 1 for input: " + c.io.data.a.peek().litValue + " with output: " + c.io.data.out.peek().litValue)
        // print("Err of " + {sqrtModel(0.0f + mant.toFloat).U.litValue - c.io.data.out.peek().litValue} + " at " + mant + ": ")
        // println(c.io.data.out.peek().litValue, sqrtModel(0.0f + mant.toFloat).U.litValue)
        errCount += 1
      }
    }
    // println("Error count was: " + errCount)

  }
}

  it should "multiply two values and add third" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(VerilatorBackendAnnotation)) { c =>
      c.io.control.op           poke      FPUOp.FMADD_S
      c.io.control.rm           poke      RoundingMode.RNE

      // (2.5 * 3.0) + 2.5 = 10.0
      c.io.data.a            poke      "h40200000".U
      c.io.data.b            poke      "h40400000".U
      c.io.data.c            poke      "h40200000".U
      c.io.data.out          expect    "h41200000".U

      // (3.74 * -43.6) + 10.6 = -152.46399
      c.io.data.a            poke      "h406f5c29".U
      c.io.data.b            poke      "hc22e6666".U
      c.io.data.c            poke      "h4129999a".U
      c.io.data.out          expect    "hc31876c7".U

      // (0.6324562 * -1574.324) + -323.3432 = -1319.0342
      c.io.data.a            poke      "h3f21e8a6".U
      c.io.data.b            poke      "hc4c4ca5e".U
      c.io.data.c            poke      "hc3a1abee".U
      c.io.data.out          expect    "hc4a4e118".U
    }
  }

  it should "multiply two values and sub third" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(VerilatorBackendAnnotation)) { c =>
      c.io.control.op           poke      FPUOp.FMSUB_S
      c.io.control.rm           poke      RoundingMode.RNE

      // (2.5 * 3.0) - 2.5 = 5.0
      c.io.data.a            poke      "h40200000".U
      c.io.data.b            poke      "h40400000".U
      c.io.data.c            poke      "h40200000".U
      c.io.data.out          expect    "h40a00000".U

      // (3.74 * -43.6) - 10.6 = -173.664
      c.io.data.a            poke      "h406f5c29".U
      c.io.data.b            poke      "hc22e6666".U
      c.io.data.c            poke      "h4129999a".U
      c.io.data.out          expect    "hc32da9fb".U

      // (0.6324562 * -1574.324) - -323.3432 = -672.3477
      c.io.data.a            poke      "h3f21e8a6".U
      c.io.data.b            poke      "hc4c4ca5e".U
      c.io.data.c            poke      "hc3a1abee".U
      c.io.data.out          expect    "hc4281641".U
    }
  }

  it should "take negative multiplication of two values and add third" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(VerilatorBackendAnnotation)) { c =>
      c.io.control.op           poke      FPUOp.FNMSUB_S
      c.io.control.rm           poke      RoundingMode.RNE

      // -(2.5 * 3.0) + 2.5 = -5.0
      c.io.data.a            poke      "h40200000".U
      c.io.data.b            poke      "h40400000".U
      c.io.data.c            poke      "h40200000".U
      c.io.data.out          expect    "hc0a00000".U

      // -(3.74 * -43.6) + 10.6 = 173.664
      c.io.data.a            poke      "h406f5c29".U
      c.io.data.b            poke      "hc22e6666".U
      c.io.data.c            poke      "h4129999a".U
      c.io.data.out          expect    "h432da9fb".U

      // -(0.6324562 * -1574.324) + -323.3432 = 672.3477
      c.io.data.a            poke      "h3f21e8a6".U
      c.io.data.b            poke      "hc4c4ca5e".U
      c.io.data.c            poke      "hc3a1abee".U
      c.io.data.out          expect    "h44281641".U
    }
  }

  it should "take negative multiplication of two values and sub third" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(VerilatorBackendAnnotation)) { c =>
      c.io.control.op           poke      FPUOp.FNMADD_S
      c.io.control.rm           poke      RoundingMode.RNE

      // -(2.5 * 3.0) - 2.5 = -10.0
      c.io.data.a            poke      "h40200000".U
      c.io.data.b            poke      "h40400000".U
      c.io.data.c            poke      "h40200000".U
      c.io.data.out          expect    "hc1200000".U

      // -(3.74 * -43.6) - 10.6 = 152.46399
      c.io.data.a            poke      "h406f5c29".U
      c.io.data.b            poke      "hc22e6666".U
      c.io.data.c            poke      "h4129999a".U
      c.io.data.out          expect    "h431876c7".U

      // -(0.6324562 * -1574.324) + -323.3432 = 1319.0342
      c.io.data.a            poke      "h3f21e8a6".U
      c.io.data.b            poke      "hc4c4ca5e".U
      c.io.data.c            poke      "hc3a1abee".U
      c.io.data.out          expect    "h44a4e118".U
    }
  }

  it should "Find the minimum and maximum of 2 values" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(VerilatorBackendAnnotation)) { c =>
      c.io.control.op           poke      FPUOp.FMIN_S

      // MIN(6.47, 0.39) = 0.39
      c.io.data.a            poke      "b01000000110011110000101000111101".U
      c.io.data.b            poke      "b00111110110001111010111000010100".U
      c.io.data.out          expect    "b00111110110001111010111000010100".U

      // MIN(-6.47, 0.39) = -6.47
      c.io.data.a            poke      "b11000000110011110000101000111101".U
      c.io.data.b            poke      "b00111110110001111010111000010100".U
      c.io.data.out          expect    "b11000000110011110000101000111101".U

      // MIN(-6.47, -6.47) = -6.47
      c.io.data.a            poke      "b11000000110011110000101000111101".U
      c.io.data.b            poke      "b11000000110011110000101000111101".U
      c.io.data.out          expect    "b11000000110011110000101000111101".U

      // MIN(6.47, qNaN) = 6.47
      c.io.data.a            poke      "b01000000110011110000101000111101".U
      c.io.data.b            poke      "b01111111110000000000000000000000".U
      c.io.data.out          expect    "b01000000110011110000101000111101".U

      // MIN(sNaN, 0.39) = 0.39
      c.io.data.a            poke      "b01111111100000000000000000000001".U
      c.io.data.b            poke      "b00111110110001111010111000010100".U
      c.io.data.out          expect    "b00111110110001111010111000010100".U
      c.io.data.fault_data   expect    "b10000".U //FaultFlag.NV

      // MIN(sNaN, qNaN) = cNaN
      c.io.data.a            poke      "b01111111100000000000000000000001".U
      c.io.data.b            poke      "b01111111110000000000000000000000".U
      c.io.data.out          expect    "b01111111110000000000000000000000".U


      c.io.control.op           poke      FPUOp.FMAX_S

      // MAX(6.47, 0.39) = 6.47
      c.io.data.a            poke      "b01000000110011110000101000111101".U
      c.io.data.b            poke      "b00111110110001111010111000010100".U
      c.io.data.out          expect    "b01000000110011110000101000111101".U

      // MAX(-6.47, 0.39) = 0.39
      c.io.data.a            poke      "b11000000110011110000101000111101".U
      c.io.data.b            poke      "b00111110110001111010111000010100".U
      c.io.data.out          expect    "b00111110110001111010111000010100".U

      // MAX(6.47, qNaN) = 6.47
      c.io.data.a            poke      "b01000000110011110000101000111101".U
      c.io.data.b            poke      "b01111111110000000000000000000000".U
      c.io.data.out          expect    "b01000000110011110000101000111101".U

      // MAX(sNaN, 0.39) = 0.39
      c.io.data.a            poke      "b01111111100000000000000000000001".U
      c.io.data.b            poke      "b00111110110001111010111000010100".U
      c.io.data.out          expect    "b00111110110001111010111000010100".U
      c.io.data.fault_data   expect    "b10000".U //FaultFlag.NV

      // MAX(sNaN, qNaN) = cNaN
      c.io.data.a            poke      "b01111111100000000000000000000001".U
      c.io.data.b            poke      "b01111111110000000000000000000000".U
      c.io.data.out          expect    "b01111111110000000000000000000000".U

    }
  }

  it should "convert floating-point numbers to singed integers" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(VerilatorBackendAnnotation)) { c =>
      c.io.control.op           poke      FPUOp.FCVT_W_S
      c.io.control.rm           poke      RoundingMode.RNE

      // +Inf should be all ones (except for sign) 
      c.io.data.a            poke      "h7f800000".U
      c.io.data.out          expect    "h7fffffff".U
      
      c.io.data.fault_data   expect    "b10000".U //FaultFlag.NV

      // -Inf should be max negative
      c.io.data.a            poke      "hff800000".U
      c.io.data.out          expect    "h80000000".U
      
      c.io.data.fault_data   expect    "b10000".U //FaultFlag.NV

      // Nan should be all ones (except for sign) 
      c.io.data.a            poke      "h7f849000".U
      c.io.data.out          expect    "h7fffffff".U
      
      c.io.data.fault_data   expect    "b10000".U //FaultFlag.NV

      // Nan should be positive all ones even if negative 
      c.io.data.a            poke      "hff849000".U
      c.io.data.out          expect    "h7fffffff".U
      
      c.io.data.fault_data   expect    "b10000".U //FaultFlag.NV

      // 6.5 should be 6
      c.io.data.a            poke      "h40d00000".U
      c.io.data.out          expect    "h00000006".U
      

      // 7.5 should be 8
      c.io.data.a            poke      "h40f00000".U
      c.io.data.out          expect    "h00000008".U
      

      // -6.5 should be -6
      c.io.data.a            poke      "hc0c00000".U
      c.io.data.out          expect    "hfffffffa".U
      

      // 0.5 should be 0
      c.io.data.a            poke      "h3f000000".U
      c.io.data.out          expect    "h00000000".U
      

      // 0x4f0000ff should clip to highest possible value
      c.io.data.a            poke      "h4f0000ff".U
      c.io.data.out          expect    "h7fffffff".U
      
      c.io.data.fault_data   expect    "b10000".U //FaultFlag.NV

      // 0xhcf0000 should clip to lowest possible value
      c.io.data.a            poke      "hcf000000".U
      c.io.data.out          expect    "h80000000".U
      
      c.io.data.fault_data   expect    "b10000".U //FaultFlag.NV

      // +0 should be 0
      c.io.data.a            poke      "h00000000".U
      c.io.data.out          expect    "h00000000".U
      

      // -0 should be 0
      c.io.data.a            poke      "h80000000".U
      c.io.data.out          expect    "h00000000".U
      

      // 0.4 should be 0
      c.io.data.a            poke      "h3ecccccd".U
      c.io.data.out          expect    "h00000000".U
      

      // -0.4 should be 0
      c.io.data.a            poke      "hbecccccd".U
      c.io.data.out          expect    "h00000000".U
      

      // 13000000 (requires zero padding)
      c.io.data.a            poke      "h4cf7f490".U
      c.io.data.out          expect    "h07bfa480".U
      

      // exponent 24
      c.io.data.a            poke      "h4bf7f490".U
      c.io.data.out          expect    "h01efe920".U
      

      // exponent 23
      c.io.data.a            poke      "h4b77f490".U
      c.io.data.out          expect    "h00f7f490".U
      

    }
  }

  it should "convert singed integers to floating-point numbers" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(VerilatorBackendAnnotation)) { c =>
      c.io.control.op           poke      FPUOp.FCVT_S_W
      c.io.control.rm           poke      RoundingMode.RNE
      
      // 15
      c.io.data.a            poke      "h0000000f".U
      c.io.data.out          expect    "h41700000".U

      // -30
      c.io.data.a            poke      "hffffffe2".U
      c.io.data.out          expect    "hc1f00000".U

      // -2^31
      c.io.data.a            poke      "h80000000".U
      c.io.data.out          expect    "hcf000000".U

      // 2^31 - 1
      c.io.data.a            poke      "h7fffffff".U
      c.io.data.out          expect    "h4f000000".U

      // Rounding - the last 1 causes sticky to become one
      // Thus, the result is rounded up to 1
      c.io.data.a            poke      "b00100000000000000000000000100001".U
      c.io.data.out          expect    "h4e000001".U

      // G is 1, but R and S are 0 -> rounded towards even
      c.io.data.a            poke      "b00100000000000000000000000100000".U
      c.io.data.out          expect    "h4e000000".U

    }
  }

  it should "convert floating-point numbers to unsinged integers" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(VerilatorBackendAnnotation)) { c =>
      c.io.control.op           poke      FPUOp.FCVT_WU_S
      c.io.control.rm           poke      RoundingMode.RNE

      // +Inf should be all ones 
      c.io.data.a            poke      "h7f800000".U
      c.io.data.out          expect    "hffffffff".U
      
      c.io.data.fault_data   expect    "b10000".U //FaultFlag.NV

      // -Inf should be all zeros
      c.io.data.a            poke      "hff800000".U
      c.io.data.out          expect    "h00000000".U
      
      c.io.data.fault_data   expect    "b10000".U //FaultFlag.NV

      // Nan should be all ones 
      c.io.data.a            poke      "h7f849000".U
      c.io.data.out          expect    "hffffffff".U
      
      c.io.data.fault_data   expect    "b10000".U //FaultFlag.NV

      //// Nan should be positive all ones even if negative 
      c.io.data.a            poke      "hff849000".U
      c.io.data.out          expect    "hffffffff".U
      
      c.io.data.fault_data   expect    "b10000".U //FaultFlag.NV

       // 6.5 should be 6
      c.io.data.a            poke      "h40d00000".U
      c.io.data.out          expect    "h00000006".U
      

      // 7.5 should be 8
      c.io.data.a            poke      "h40f00000".U
      c.io.data.out          expect    "h00000008".U
      

      // -6.5 should be 0
      c.io.data.a            poke      "hc0c00000".U
      c.io.data.out          expect    "h00000000".U
      
      c.io.data.fault_data   expect    "b10000".U //FaultFlag.NV

      // 0.5 should be 0
      c.io.data.a            poke      "h3f000000".U
      c.io.data.out          expect    "h00000000".U
      

      // 0 should be 0 (and no flags)
      c.io.data.a            poke      "h3f000000".U
      c.io.data.out          expect    "h00000000".U
      

      // exponent 24
      c.io.data.a            poke      "h4bf7f490".U
      c.io.data.out          expect    "h01efe920".U
      

      // exponent 25
      c.io.data.a            poke      "h4c77f490".U
      c.io.data.out          expect    "h03dfd240".U
       

      // First to overflow
      c.io.data.a            poke      "h4f800000".U
      c.io.data.out          expect    "hffffffff".U
       
      c.io.data.fault_data   expect    "b10000".U //FaultFlag.NV

      // Last to not overflow
      c.io.data.a            poke      "h4f7fffff".U
      c.io.data.out          expect    "hffffff00".U
       
    }
  }

  it should "convert unsinged integers to floating-point numbers" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(VerilatorBackendAnnotation)) { c =>
      c.io.control.op           poke      FPUOp.FCVT_S_WU
      c.io.control.rm           poke      RoundingMode.RNE
      
      // 15
      c.io.data.a            poke      "h0000000f".U
      c.io.data.out          expect    "h41700000".U

      // 2^32 - 1
      c.io.data.a            poke      "hffffffff".U
      c.io.data.out          expect    "h4f800000".U

      // Rounding - the last 1 causes sticky to become one
      // Thus, the result is rounded up to 1
      c.io.data.a            poke      "b00100000000000000000000000100001".U
      c.io.data.out          expect    "h4e000001".U

      // G is 1, but R and S are 0 -> rounded towards even
      c.io.data.a            poke      "b00100000000000000000000000100000".U
      c.io.data.out          expect    "h4e000000".U
    }
  }

  it should "Inject signs correctly" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(VerilatorBackendAnnotation)) { c =>
      c.io.control.op           poke      FPUOp.FSGNJ_S
      
      c.io.data.a            poke      "b00000000000000000000000000000001".U
      c.io.data.b            poke      "b10000000000000000000000000000010".U
      c.io.data.out          expect    "b10000000000000000000000000000001".U

      c.io.data.a            poke      "b10000000000000000000000000000001".U
      c.io.data.b            poke      "b00000000000000000000000000000010".U
      c.io.data.out          expect    "b00000000000000000000000000000001".U


      c.io.control.op           poke      FPUOp.FSGNJN_S
      
      c.io.data.a            poke      "b10000000000000000000000000000001".U
      c.io.data.b            poke      "b10000000000000000000000000000010".U
      c.io.data.out          expect    "b00000000000000000000000000000001".U

      c.io.data.a            poke      "b00000000000000000000000000000001".U
      c.io.data.b            poke      "b00000000000000000000000000000010".U
      c.io.data.out          expect    "b10000000000000000000000000000001".U


      c.io.control.op           poke      FPUOp.FSGNJX_S
      
      c.io.data.a            poke      "b00000000000000000000000000000001".U
      c.io.data.b            poke      "b00000000000000000000000000000010".U
      c.io.data.out          expect    "b00000000000000000000000000000001".U

      c.io.data.a            poke      "b10000000000000000000000000000001".U
      c.io.data.b            poke      "b10000000000000000000000000000010".U
      c.io.data.out          expect    "b00000000000000000000000000000001".U

      c.io.data.a            poke      "b00000000000000000000000000000001".U
      c.io.data.b            poke      "b10000000000000000000000000000010".U
      c.io.data.out          expect    "b10000000000000000000000000000001".U

      c.io.data.a            poke      "b10000000000000000000000000000001".U
      c.io.data.b            poke      "b00000000000000000000000000000010".U
      c.io.data.out          expect    "b10000000000000000000000000000001".U

    }
  }

  it should "Compare values correctly" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(VerilatorBackendAnnotation)) { c =>
      c.io.control.op           poke      FPUOp.FCMP_FEQ_S
      
      // sNaN < b = ERROR
      c.io.data.a            poke      "b01111111101000000000000000000000".U
      c.io.data.b            poke      "b01000000101100000000000000000000".U
      c.io.data.out          expect    "b00000000000000000000000000000000".U
      
      c.io.data.fault_data   expect    "b10000".U //FaultFlag.NV

      // a < sNaN = ERROR
      c.io.data.a            poke      "b00111111110000000000000000000000".U
      c.io.data.b            poke      "b01111111101000000000000000000000".U
      c.io.data.out          expect    "b00000000000000000000000000000000".U
      
      c.io.data.fault_data   expect    "b10000".U //FaultFlag.NV

      // a < b = 0
      c.io.data.a            poke      "b00111111110000000000000000000000".U
      c.io.data.b            poke      "b01000000101100000000000000000000".U
      c.io.data.out          expect    "b00000000000000000000000000000000".U

      // a > b = 0
      c.io.data.a            poke      "b01000000101100000000000000000000".U
      c.io.data.b            poke      "b00111111110000000000000000000000".U
      c.io.data.out          expect    "b00000000000000000000000000000000".U

      // a == b = 1
      c.io.data.a            poke      "b00111111110000000000000000000000".U
      c.io.data.b            poke      "b00111111110000000000000000000000".U
      c.io.data.out          expect    "b00000000000000000000000000000001".U

      // +0 == -0 = 1
      c.io.data.a            poke      "h00000000".U
      c.io.data.b            poke      "h80000000".U
      c.io.data.out          expect    "h00000001".U


      c.io.control.op           poke      FPUOp.FCMP_FLT_S
      
      // NaN < b = ERROR
      c.io.data.a            poke      "b01111111100000000000000000000001".U
      c.io.data.b            poke      "b01000000101100000000000000000000".U
      c.io.data.out          expect    "b00000000000000000000000000000000".U
      c.io.data.fault_data   expect    "b10000".U //FaultFlag.NV

      // a < NaN = ERROR
      c.io.data.a            poke      "b00111111110000000000000000000000".U
      c.io.data.b            poke      "b01111111110000000000000000000000".U
      c.io.data.out          expect    "b00000000000000000000000000000000".U
      c.io.data.fault_data   expect    "b10000".U //FaultFlag.NV

      // a < b = 1      
      c.io.data.a            poke      "b00111111110000000000000000000000".U
      c.io.data.b            poke      "b01000000101100000000000000000000".U
      c.io.data.out          expect    "b00000000000000000000000000000001".U

      // -a < b = 1      
      c.io.data.a            poke      "b10111111110000000000000000000000".U
      c.io.data.b            poke      "b01000000101100000000000000000000".U
      c.io.data.out          expect    "b00000000000000000000000000000001".U

      // a > b = 0
      c.io.data.a            poke      "b01000000101100000000000000000000".U
      c.io.data.b            poke      "b00111111110000000000000000000000".U
      c.io.data.out          expect    "b00000000000000000000000000000000".U

      // a == b = 0
      c.io.data.a            poke      "b00111111110000000000000000000000".U
      c.io.data.b            poke      "b00111111110000000000000000000000".U
      c.io.data.out          expect    "b00000000000000000000000000000000".U

      // -2 < -1 = 1
      c.io.data.a            poke      "hc0000000".U // -2
      c.io.data.b            poke      "hbf800000".U // -1
      c.io.data.out          expect    "b00000000000000000000000000000001".U

      // -0 < +0 = 0
      c.io.data.a            poke      "h80000000".U
      c.io.data.b            poke      "h00000000".U
      c.io.data.out          expect    "h00000000".U

      // 123123.3459 < 13371337.1337 = 1
      c.io.data.a            poke      "b01000111111100000111100110101100".U
      c.io.data.b            poke      "b01001011010011000000011111001001".U
      c.io.data.out          expect    "b00000000000000000000000000000001".U

      // 13371337.1337 > 123123.3459 = 0
      c.io.data.a            poke      "b01001011010011000000011111001001".U 
      c.io.data.b            poke      "b01000111111100000111100110101100".U
      c.io.data.out          expect    "b00000000000000000000000000000000".U


      c.io.control.op           poke      FPUOp.FCMP_FLE_S

      c.io.data.a            poke      "hc0000000".U // -2
      c.io.data.b            poke      "hbf800000".U // -1
      c.io.data.out          expect    "b00000000000000000000000000000001".U
  
      // NaN < b = ERROR
      c.io.data.a            poke      "b01111111100000000000000000000001".U
      c.io.data.b            poke      "b01000000101100000000000000000000".U
      c.io.data.out          expect    "b00000000000000000000000000000000".U
      c.io.data.fault_data   expect    "b10000".U //FaultFlag.NV

      // a < NaN = ERROR
      c.io.data.a            poke      "b00111111110000000000000000000000".U
      c.io.data.b            poke      "b01111111110000000000000000000000".U
      c.io.data.out          expect    "b00000000000000000000000000000000".U
      c.io.data.fault_data   expect    "b10000".U //FaultFlag.NV
    
      // a < b = 1      
      c.io.data.a            poke      "b00111111110000000000000000000000".U
      c.io.data.b            poke      "b01000000101100000000000000000000".U
      c.io.data.out          expect    "b00000000000000000000000000000001".U

      // -a < b = 1      
      c.io.data.a            poke      "b10111111110000000000000000000000".U
      c.io.data.b            poke      "b01000000101100000000000000000000".U
      c.io.data.out          expect    "b00000000000000000000000000000001".U

      // a > b = 0
      c.io.data.a            poke      "b01000000101100000000000000000000".U
      c.io.data.b            poke      "b00111111110000000000000000000000".U
      c.io.data.out          expect    "b00000000000000000000000000000000".U

      // a == b = 1
      c.io.data.a            poke      "b00111111110000000000000000000000".U
      c.io.data.b            poke      "b00111111110000000000000000000000".U
      c.io.data.out          expect    "b00000000000000000000000000000001".U

      // 123123.3459 < 13371337.1337 = 1
      c.io.data.a            poke      "b01000111111100000111100110101100".U
      c.io.data.b            poke      "b01001011010011000000011111001001".U
      c.io.data.out          expect    "b00000000000000000000000000000001".U

      // 13371337.1337 > 123123.3459 = 0
      c.io.data.a            poke      "b01001011010011000000011111001001".U 
      c.io.data.b            poke      "b01000111111100000111100110101100".U
      c.io.data.out          expect    "b00000000000000000000000000000000".U

    }
  }

  it should "Classify floating-point numbers correctly" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(VerilatorBackendAnnotation)) { c =>
      c.io.control.op           poke      FPUOp.FCLASS_S

      // -Inf
      c.io.data.a            poke      "b11111111100000000000000000000000".U
      c.io.data.out          expect    "b00000000000000000000000000000001".U

      // negative normal number 
      c.io.data.a            poke      "b10000000110000000000000000000000".U
      c.io.data.out          expect    "b00000000000000000000000000000010".U

      // negative subnormal number
      c.io.data.a            poke      "b10000000010000000000000000000000".U
      c.io.data.out          expect    "b00000000000000000000000000000100".U

      // -0
      c.io.data.a            poke      "b10000000000000000000000000000000".U
      c.io.data.out          expect    "b00000000000000000000000000001000".U

      // +0
      c.io.data.a            poke      "b00000000000000000000000000000000".U
      c.io.data.out          expect    "b00000000000000000000000000010000".U

      // positive subnormal number
      c.io.data.a            poke      "b00000000010000000000000000000000".U
      c.io.data.out          expect    "b00000000000000000000000000100000".U

      // positive normal number 
      c.io.data.a            poke      "b00000000110000000000000000000000".U
      c.io.data.out          expect    "b00000000000000000000000001000000".U

      // +Inf
      c.io.data.a            poke      "b01111111100000000000000000000000".U
      c.io.data.out          expect    "b00000000000000000000000010000000".U

      // sNaN
      c.io.data.a            poke      "b01111111100000000000000000000001".U
      c.io.data.out          expect    "b00000000000000000000000100000000".U

      // qNaN
      c.io.data.a            poke      "b01111111110000000000000000000000".U
      c.io.data.out          expect    "b00000000000000000000001000000000".U

    }
  }

}
