package acorebase

import sys.process._
import scala.language.postfixOps
import scala.util.control.Breaks._
import scala.io.Source
import java.io.{BufferedWriter, FileWriter, File}
import java.nio.file.{Paths, Files}
import scala.sys.process._
import chisel3._
import chiseltest._
import org.scalatest.flatspec.AnyFlatSpec
import a_core_common._

class FPUBlockSpec_TestFloat extends AnyFlatSpec with ChiselScalatestTester {

  // Custom path to TestFloat can be provided with TESTFLOAT_PATH environmental variable
  val path_to_testfloat = sys.env.get("TESTFLOAT_PATH") match {
    case Some(x) => x
    case None    => "src/test/scala/acorebase/alublock/fpublock/TestFloat-3e/build/Linux-x86_64-GCC/"
  }

  /**
    * Generates a test vector with TestFloat program. TestFloat must be installed
    * locally in the path stored in path_to_testfloat
    *
    * @param operands number of operands (inputs + output)
    * @param command command in TestFloat syntax (e.g. f32_add)
    * @param tests number of tests generated
    * @return test vectors in format (inputs, output, faultflags)
    */
  def genTestVector(operands: Int, command: String, tests: Int) = {
    val cmd_str = path_to_testfloat + "testfloat_gen " + command + " -n " + tests.toString()
    val test_vec = Process(cmd_str).!!
    val result = test_vec.split("\\s+").grouped(operands)
    result
  }

  /**
    * Generates a test vector with TestFloat program and writes the vectors into a file. TestFloat must be installed
    * locally in the path stored in path_to_testfloat
    *
    * @param command command in TestFloat syntax (e.g. f32_add)
    * @param tests number of tests generated
    * @return test vectors in format (inputs, output, faultflags)
    */
  def genTestVectorFile(command: String, tests: Int) = {
    val cmd_str = path_to_testfloat + "testfloat_gen " + command + " -n " + tests.toString()
    val writer = new FileWriter(new File("src/test/scala/acorebase/alublock/fpublock/" + command + "_testvector.txt"))
    val lines = Process(cmd_str).lazyLines
    for (line <- lines) {writer.write(line + "\n")}
    writer.close()
  }

  it should "f32_add test" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(WriteVcdAnnotation, VerilatorBackendAnnotation)) { c => 
      c.io.control.op poke FPUOp.FADD_S
      c.io.control.rm poke RoundingMode.RNE

      val test_vecs = genTestVector(4, "f32_add", 46464)
      var count = 0
      for (vec <- test_vecs) {
        count = count + 1
        c.io.data.a           poke    {"h" + vec(0)}.U        
        c.io.data.b           poke    {"h" + vec(1)}.U
        c.clock.step()
        c.io.data.out         expect  {"h" + vec(2)}.U
        c.io.data.fault_data  expect  {"h" + vec(3)}.U

        //println("Test vector " + count + ": " + vec(0) + "," + vec(1) + "," + vec(2) + "," + vec(3).reverse + 
        //  " Result: " + c.io.data.out.peek().litValue.toInt.toHexString.toUpperCase())
      }
    }
  }

  it should "f32_sub test" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(WriteVcdAnnotation, VerilatorBackendAnnotation)) { c => 
      c.io.control.op poke FPUOp.FSUB_S
      c.io.control.rm poke RoundingMode.RNE

      val test_vecs = genTestVector(4, "f32_sub", 46464)
      var count = 0

      for (vec <- test_vecs) {
        count = count + 1
        c.io.data.a           poke    {"h" + vec(0)}.U        
        c.io.data.b           poke    {"h" + vec(1)}.U
        c.clock.step()

        if (vec(2) == "FFC00000"){
          c.io.data.out         expect  "h7fc00000".U
          c.io.data.fault_data  expect  {"h" + vec(3)}.U

          //println("Test vector " + count + ": " + vec(0) + "," + vec(1) + ", 7FC00000," + vec(3).reverse + 
          //  " Result: " + c.io.data.out.peek().litValue.toInt.toHexString.toUpperCase())
        } else {
          c.io.data.out         expect  {"h" + vec(2)}.U
          c.io.data.fault_data  expect  {"h" + vec(3)}.U

          //println("Test vector " + count + ": " + vec(0) + "," + vec(1) + "," + vec(2) + "," + vec(3).reverse + 
          //  " Result: " + c.io.data.out.peek().litValue.toInt.toHexString.toUpperCase())
        }
      }
    }
  }

  it should "f32_mul test" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(WriteVcdAnnotation, VerilatorBackendAnnotation)) { c => 
      c.io.control.op poke FPUOp.FMUL_S
      c.io.control.rm poke RoundingMode.RNE

      val test_vecs = genTestVector(4, "f32_mul", 46464)
      var count = 0
      for (vec <- test_vecs) {
        count = count + 1
        c.io.data.a           poke    {"h" + vec(0)}.U        
        c.io.data.b           poke    {"h" + vec(1)}.U
        c.clock.step()
        c.io.data.out         expect  {"h" + vec(2)}.U
        c.io.data.fault_data  expect  {"h" + vec(3)}.U

        //println("Test vector " + count + ": " + vec(0) + "," + vec(1) + "," + vec(2) + "," + vec(3).reverse + 
        //  " Result: " + c.io.data.out.peek().litValue.toInt.toHexString.toUpperCase())
      }
    }
  }

  it should "f32_div test" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(WriteVcdAnnotation, VerilatorBackendAnnotation)) { c => 
      c.io.control.op poke FPUOp.FDIV_S
      c.io.control.rm poke RoundingMode.RNE

      val test_vecs = genTestVector(4, "f32_div", 46464)
      var count = 0
      for (vec <- test_vecs) {
        count = count + 1
        c.io.data.a           poke    {"h" + vec(0)}.U        
        c.io.data.b           poke    {"h" + vec(1)}.U
        c.clock.step()
        c.io.data.out         expect  {"h" + vec(2)}.U
        c.io.data.fault_data  expect  {"h" + vec(3)}.U

        //println("Test vector " + count + ": " + vec(0) + "," + vec(1) + "," + vec(2) + "," + vec(3).reverse + 
        //  " Result: " + c.io.data.out.peek().litValue.toInt.toHexString.toUpperCase())
      }
    }
  }

  it should "f32_sqrt test" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(WriteVcdAnnotation, VerilatorBackendAnnotation)) { c => 
      c.io.control.op poke FPUOp.FSQRT_S
      c.io.control.rm poke RoundingMode.RNE

      val test_vecs = genTestVector(3, "f32_sqrt", 46464)
      var count = 0
      for (vec <- test_vecs) {
        count = count + 1
        c.io.data.a           poke    {"h" + vec(0)}.U
        c.clock.step()
        c.io.data.fault_data  expect  {"h" + vec(2)}.U
        /*
        println("Test vector " + count + ": " + vec(0) + "," + vec(1) + "," + vec(2).reverse + 
          " Result: " + c.io.data.out.peek().litValue.toInt.toHexString.toUpperCase())

        // 1 bit error is allowed for now
        if (c.io.data.out.peek().litValue != {"h" + vec(1)}.U.litValue) {
          val err = {"h" + vec(1)}.U.litValue - c.io.data.out.peek().litValue
          assert(math.abs(err.toFloat) <= 1, "\nAbsolute bit error was more than 1 for input: " + 
            c.io.data.a.peek().litValue + " with output: " + c.io.data.out.peek().litValue)
        }
        */
      }
    }
  }

  // Run with sbt -mem 3072 'testOnly acorebase.FPUBlockSpec_TestFloat'
  it should "f32_mulAdd test" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(WriteVcdAnnotation, VerilatorBackendAnnotation)) { c => 
      c.io.control.op poke FPUOp.FMADD_S
      c.io.control.rm poke RoundingMode.RNE

      if (!Files.exists(Paths.get("src/test/scala/acorebase/alublock/fpublock/f32_mulAdd_testvector.txt"))) {
        val test_vecs = genTestVectorFile("f32_mulAdd", 6133248)
      }

      val source = Source.fromFile("src/test/scala/acorebase/alublock/fpublock/f32_mulAdd_testvector.txt")
      var count = 0
      
      for (line <- source.getLines()){
        count = count + 1
        var vec = line.split("\\s+")

        c.io.data.a           poke    {"h" + vec(0)}.U        
        c.io.data.b           poke    {"h" + vec(1)}.U
        c.io.data.c           poke    {"h" + vec(2)}.U
        c.clock.step()
        c.io.data.out         expect  {"h" + vec(3)}.U
        c.io.data.fault_data  expect  {"h" + vec(4)}.U
        /*
        println("Test vector " + count + ": " + vec(0) + "," + vec(1) + "," + vec(2) + "," + vec(3) + "," + vec(4).reverse + 
          " Result: " + c.io.data.out.peek().litValue.toInt.toHexString.toUpperCase())
        */
      }
      source.close()
    }
  }

  it should "ui32_to_f32 test" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(WriteVcdAnnotation, VerilatorBackendAnnotation)) { c => 
      c.io.control.op poke FPUOp.FCVT_S_WU
      c.io.control.rm poke RoundingMode.RNE

      val test_vecs = genTestVector(3, "ui32_to_f32", 46464)
      var count = 0
      for (vec <- test_vecs) {
        count = count + 1
        c.io.data.a           poke    {"h" + vec(0)}.U
        c.clock.step()
        c.io.data.out         expect  {"h" + vec(1)}.U
        c.io.data.fault_data  expect  {"h" + vec(2)}.U

        //println("Test vector " + count + ": " + vec(0) + "," + vec(1) + "," + vec(2).reverse + 
        //  " Result: " + c.io.data.out.peek().litValue.toInt.toHexString.toUpperCase())
      }
    }
  }

  it should "i32_to_f32 test" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(WriteVcdAnnotation, VerilatorBackendAnnotation)) { c => 
      c.io.control.op poke FPUOp.FCVT_S_W
      c.io.control.rm poke RoundingMode.RNE

      val test_vecs = genTestVector(3, "i32_to_f32", 46464)
      var count = 0
      for (vec <- test_vecs) {
        count = count + 1
        c.io.data.a           poke    {"h" + vec(0)}.U
        c.clock.step()
        c.io.data.out         expect  {"h" + vec(1)}.U
        c.io.data.fault_data  expect  {"h" + vec(2)}.U

        //println("Test vector " + count + ": " + vec(0) + "," + vec(1) + "," + vec(2).reverse + 
        //  " Result: " + c.io.data.out.peek().litValue.toInt.toHexString.toUpperCase())
      }
    }
  }

  it should "f32_to_ui32 test" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(WriteVcdAnnotation, VerilatorBackendAnnotation)) { c => 
      c.io.control.op poke FPUOp.FCVT_WU_S
      c.io.control.rm poke RoundingMode.RNE

      val test_vecs = genTestVector(3, "f32_to_ui32", 46464)
      var count = 0
      for (vec <- test_vecs) {
        count = count + 1
        c.io.data.a           poke    {"h" + vec(0)}.U
        c.clock.step()
        c.io.data.out         expect  {"h" + vec(1)}.U
        c.io.data.fault_data  expect  {"h" + vec(2)}.U

        /*
        println("Test vector " + count + ": " + vec(0) + "," + vec(1) + "," + vec(2).reverse + 
          " Result: " + c.io.data.out.peek().litValue.toInt.toHexString.toUpperCase())
        */
      }
    }
  }

  it should "f32_to_i32 test" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(WriteVcdAnnotation, VerilatorBackendAnnotation)) { c => 
      c.io.control.op poke FPUOp.FCVT_W_S
      c.io.control.rm poke RoundingMode.RNE

      val test_vecs = genTestVector(3, "f32_to_i32", 46464)
      var count = 0
      for (vec <- test_vecs) {
        count = count + 1
        c.io.data.a           poke    {"h" + vec(0)}.U
        c.clock.step()
        c.io.data.out         expect  {"h" + vec(1)}.U
        c.io.data.fault_data  expect  {"h" + vec(2)}.U
        /*
        println("Test vector " + count + ": " + vec(0) + "," + vec(1) + "," + vec(2).reverse + 
          " Result: " + c.io.data.out.peek().litValue.toInt.toHexString.toUpperCase())
        */
      }
    }
  }

  it should "f32_eq test" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(WriteVcdAnnotation, VerilatorBackendAnnotation)) { c => 
      c.io.control.op poke FPUOp.FCMP_FEQ_S
      c.io.control.rm poke RoundingMode.RNE

      val test_vecs = genTestVector(4, "f32_eq", 46464)
      var count = 0
      for (vec <- test_vecs) {
        count = count + 1
        c.io.data.a           poke    {"h" + vec(0)}.U        
        c.io.data.b           poke    {"h" + vec(1)}.U
        c.clock.step()
        c.io.data.out         expect  {"h" + vec(2)}.U
        c.io.data.fault_data  expect  {"h" + vec(3)}.U

        //println("Test vector " + count + ": " + vec(0) + "," + vec(1) + "," + vec(2) + "," + vec(3).reverse + 
        //  " Result: " + c.io.data.out.peek().litValue.toInt.toHexString.toUpperCase())
      }
    }
  }

  it should "f32_le test" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(WriteVcdAnnotation, VerilatorBackendAnnotation)) { c => 
      c.io.control.op poke FPUOp.FCMP_FLE_S
      c.io.control.rm poke RoundingMode.RNE

      val test_vecs = genTestVector(4, "f32_le", 46464)
      var count = 0
      for (vec <- test_vecs) {
        count = count + 1
        c.io.data.a           poke    {"h" + vec(0)}.U        
        c.io.data.b           poke    {"h" + vec(1)}.U
        c.clock.step()
        c.io.data.out         expect  {"h" + vec(2)}.U
        c.io.data.fault_data  expect  {"h" + vec(3)}.U

        //println("Test vector " + count + ": " + vec(0) + "," + vec(1) + "," + vec(2) + "," + vec(3).reverse + 
        //  " Result: " + c.io.data.out.peek().litValue.toInt.toHexString.toUpperCase())
      }
    }
  }

  it should "f32_lt test" in {
    test(new FPUBlock(XLEN=32)).withAnnotations(Seq(WriteVcdAnnotation, VerilatorBackendAnnotation)) { c => 
      c.io.control.op poke FPUOp.FCMP_FLT_S
      c.io.control.rm poke RoundingMode.RNE

      val test_vecs = genTestVector(4, "f32_lt", 46464)
      var count = 0
      for (vec <- test_vecs) {
        count = count + 1
        c.io.data.a           poke    {"h" + vec(0)}.U        
        c.io.data.b           poke    {"h" + vec(1)}.U
        c.clock.step()
        c.io.data.out         expect  {"h" + vec(2)}.U
        c.io.data.fault_data  expect  {"h" + vec(3)}.U

        //println("Test vector " + count + ": " + vec(0) + "," + vec(1) + "," + vec(2) + "," + vec(3).reverse + 
        //  " Result: " + c.io.data.out.peek().litValue.toInt.toHexString.toUpperCase())
      }
    }
  }
}
