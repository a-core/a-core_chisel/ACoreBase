// SPDX-License-Identifier: Apache-2.0

package acorebase

import chisel3._
import chiseltest._
import org.scalatest.flatspec.AnyFlatSpec
import a_core_common._

class ALUSpec extends AnyFlatSpec with ChiselScalatestTester {

  //////////////////////////////////////////////////////////
  // Addition
  //////////////////////////////////////////////////////////
  it should "add with nonzero result" in {
    test(new ALU(XLEN=32)) { c =>
      c.io.op   poke    ALUOp.ADD
      c.io.a    poke    13.U
      c.io.b    poke    8.U
      c.io.out  expect  21.U
      c.io.zero expect  false.B
    }
  }

  it should "add with zero result" in {
    test(new ALU(XLEN=32)) { c =>
      c.io.op   poke    ALUOp.ADD
      c.io.a    poke    "hFFFFFFFF".U // -1 in 2's complement
      c.io.b    poke    "h00000001".U
      c.io.out  expect  0.U
      c.io.zero expect  true.B
    }
  }

  it should "add with overflow" in {
    test(new ALU(XLEN=32)) { c =>
      c.io.op   poke    ALUOp.ADD
      c.io.a    poke    "h80000000".U
      c.io.b    poke    "h80000000".U
      c.io.out  expect  0.U
      c.io.zero expect  true.B
    }
  }

  //////////////////////////////////////////////////////////
  // Subtraction
  //////////////////////////////////////////////////////////
  it should "subtract with nonzero result" in {
    test(new ALU(XLEN=32)) { c =>
      c.io.op   poke    ALUOp.SUB
      c.io.a    poke    1234.U
      c.io.b    poke    1001.U
      c.io.out  expect  233.U
      c.io.zero expect  false.B
    }
  }

  it should "subtract with zero result" in {
    test(new ALU(XLEN=32)) { c =>
      c.io.op   poke    ALUOp.SUB
      c.io.a    poke    17734.U
      c.io.b    poke    17734.U
      c.io.out  expect  0.U
      c.io.zero expect  true.B
    }
  }

  it should "subtract with negative result" in {
    test(new ALU(XLEN=32)) { c =>
      c.io.op   poke    ALUOp.SUB
      c.io.a    poke    1000.U
      c.io.b    poke    1002.U
      c.io.out  expect  "hFFFFFFFE".U // -2 in 2's complement
      c.io.zero expect  false.B
    }
  }

  //////////////////////////////////////////////////////////
  // Bitwise AND
  //////////////////////////////////////////////////////////
  it should "zero bytes with AND operation" in {
    test(new ALU(XLEN=32)) { c =>
      c.io.op   poke    ALUOp.AND
      c.io.a    poke    "hFFFFFFFF".U
      c.io.b    poke    "hAAAAAAAA".U
      c.io.out  expect  "hAAAAAAAA".U
      c.io.zero expect  false.B
    }
  }

  it should "not set any bits with AND operation" in {
    test(new ALU(XLEN=32)) { c =>
      c.io.op   poke    ALUOp.AND
      c.io.a    poke    0.U
      c.io.b    poke    "hFFFFFFFF".U
      c.io.out  expect  0.U
      c.io.zero expect  true.B
    }
  }

  //////////////////////////////////////////////////////////
  // Bitwise OR
  //////////////////////////////////////////////////////////
  it should "set bits with OR operator" in {
    test(new ALU(XLEN=32)) { c =>
      c.io.op   poke    ALUOp.OR
      c.io.a    poke    0.U
      c.io.b    poke    "hABCD1234".U
      c.io.out  expect  "hABCD1234".U
      c.io.zero expect  false.B
    }
  }

  it should "not clear any bits with OR operation" in {
    test(new ALU(XLEN=32)) { c =>
      c.io.op   poke    ALUOp.OR
      c.io.a    poke    "hFFFFFFFF".U
      c.io.b    poke    0.U
      c.io.out  expect  "hFFFFFFFF".U
      c.io.zero expect  false.B
    }
  }

  //////////////////////////////////////////////////////////
  // Bitwise XOR
  //////////////////////////////////////////////////////////
  it should "flip bits with XOR operator" in {
    test(new ALU(XLEN=32)) { c =>
      c.io.op   poke    ALUOp.XOR
      c.io.a    poke    "hFFFFFFFF".U
      c.io.b    poke    "hAAAAAAAA".U
      c.io.out  expect  "h55555555".U
      c.io.zero expect  false.B
    }
  }

  it should "not flib zero bits with XOR operator" in {
    test(new ALU(XLEN=32)) { c =>
      c.io.op   poke    ALUOp.XOR
      c.io.a    poke    "hFFFFFFFF".U
      c.io.b    poke    0.U
      c.io.out  expect  "hFFFFFFFF".U
      c.io.zero expect  false.B
    }
  }

  //////////////////////////////////////////////////////////
  // Less than (signed), assert output if A < B
  //////////////////////////////////////////////////////////
  it should "compare positive signed numbers correctly 1" in {
    test(new ALU(XLEN=32)) { c =>
      c.io.op   poke    ALUOp.LT
      c.io.a    poke    "h71234532".U
      c.io.b    poke    "h78123432".U
      c.io.out  expect  1.U
      c.io.zero expect  false.B
    }
  }

  it should "compare positive signed numbers correctly 2" in {
    test(new ALU(XLEN=32)) { c =>
      c.io.op   poke    ALUOp.LT
      c.io.a    poke    "h12345678".U
      c.io.b    poke    "h00345532".U
      c.io.out  expect  0.U
      c.io.zero expect  true.B
    }
  }

  it should "compare signed numbers with different signs correctly 1" in {
    test(new ALU(XLEN=32)) { c =>
      c.io.op   poke    ALUOp.LT
      c.io.a    poke    "h80000000".U
      c.io.b    poke    "h25341235".U
      c.io.out  expect  1.U
      c.io.zero expect  false.B
    }
  }

  it should "compare signed numbers with different signs correctly 2" in {
    test(new ALU(XLEN=32)) { c =>
      c.io.op   poke    ALUOp.LT
      c.io.a    poke    "h12345678".U
      c.io.b    poke    "hFFFFFFFF".U
      c.io.out  expect  0.U
      c.io.zero expect  true.B
    }
  }

  it should "compare negative signed numbers correctly 1" in {
    test(new ALU(XLEN=32)) { c =>
      c.io.op   poke    ALUOp.LT
      c.io.a    poke    "h82345678".U
      c.io.b    poke    "hFFFFFFFF".U
      c.io.out  expect  1.U
      c.io.zero expect  false.B
    }
  }

  it should "compare negative signed numbers correctly 2" in {
    test(new ALU(XLEN=32)) { c =>
      c.io.op   poke    ALUOp.LT
      c.io.a    poke    "hFFFF1234".U
      c.io.b    poke    "hEEEE1234".U
      c.io.out  expect  0.U
      c.io.zero expect  true.B
    }
  }

  //////////////////////////////////////////////////////////
  // Less than (unsigned), assert output if A < B
  //////////////////////////////////////////////////////////
  it should "compare unsigned numbers correctly 1" in {
    test(new ALU(XLEN=32)) { c =>
      c.io.op   poke    ALUOp.LTU
      c.io.a    poke    "h31234532".U
      c.io.b    poke    "h78123432".U
      c.io.out  expect  1.U
      c.io.zero expect  false.B
    }
  }

  it should "compare unsigned numbers correctly 2" in {
    test(new ALU(XLEN=32)) { c =>
      c.io.op   poke    ALUOp.LTU
      c.io.a    poke    "h12345678".U
      c.io.b    poke    "h00345532".U
      c.io.out  expect  0.U
      c.io.zero expect  true.B
    }
  }

  //////////////////////////////////////////////////////////
  // Logical Shift Left
  //////////////////////////////////////////////////////////
  it should "logically shift left correctly" in {
    test(new ALU(XLEN=32)) { c =>
      c.io.op   poke    ALUOp.SLL
      c.io.a    poke    "h0000A00C".U
      c.io.b    poke    0.U
      c.io.out  expect  "h0000A00C".U
      c.io.zero expect  false.B
      c.io.b    poke    4.U
      c.io.out  expect  "h000A00C0".U
      c.io.zero expect  false.B
      c.io.b    poke    5.U
      c.io.out  expect  "h00140180".U
      c.io.zero expect  false.B
      c.io.b    poke    8.U
      c.io.out  expect  "h00A00C00".U
      c.io.zero expect  false.B
      c.io.b    poke    12.U
      c.io.out  expect  "h0A00C000".U
      c.io.zero expect  false.B
      c.io.b    poke    20.U
      c.io.out  expect  "h00C00000".U
      c.io.zero expect  false.B
      c.io.b    poke    31.U
      c.io.out  expect  "h00000000".U
      c.io.zero expect  true.B
    }
  }

  //////////////////////////////////////////////////////////
  // Logical Shift Right
  //////////////////////////////////////////////////////////
  it should "logically shift right correctly" in {
    test(new ALU(XLEN=32)) { c =>
      c.io.op   poke    ALUOp.SRL
      c.io.a    poke    "h12345678".U
      c.io.b    poke    0.U
      c.io.out  expect  "h12345678".U
      c.io.zero expect  false.B
      c.io.b    poke    4.U
      c.io.out  expect  "h01234567".U
      c.io.zero expect  false.B
      c.io.b    poke    8.U
      c.io.out  expect  "h00123456".U
      c.io.zero expect  false.B
      c.io.b    poke    12.U
      c.io.out  expect  "h00012345".U
      c.io.zero expect  false.B
      c.io.b    poke    20.U
      c.io.out  expect  "h00000123".U
      c.io.zero expect  false.B
      c.io.b    poke    31.U
      c.io.out  expect  "h00000000".U
      c.io.zero expect  true.B
    }
  }

  //////////////////////////////////////////////////////////
  // Arithmetic Shift Right
  //////////////////////////////////////////////////////////
  it should "arithmetically shift right correctly (unsigned)" in {
    test(new ALU(XLEN=32)) { c =>
      c.io.op   poke    ALUOp.SRA
      c.io.a    poke    "h12345678".U
      c.io.b    poke    0.U
      c.io.out  expect  "h12345678".U
      c.io.zero expect  false.B
      c.io.b    poke    4.U
      c.io.out  expect  "h01234567".U
      c.io.zero expect  false.B
      c.io.b    poke    8.U
      c.io.out  expect  "h00123456".U
      c.io.zero expect  false.B
      c.io.b    poke    12.U
      c.io.out  expect  "h00012345".U
      c.io.zero expect  false.B
      c.io.b    poke    20.U
      c.io.out  expect  "h00000123".U
      c.io.zero expect  false.B
      c.io.b    poke    31.U
      c.io.out  expect  "h00000000".U
      c.io.zero expect  true.B
    }
  }

  it should "arithmetically shift right correctly (signed)" in {
    test(new ALU(XLEN=32)) { c =>
      c.io.op   poke    ALUOp.SRA
      c.io.a    poke    "h82345678".U
      c.io.b    poke    0.U
      c.io.out  expect  "h82345678".U
      c.io.zero expect  false.B
      c.io.b    poke    4.U
      c.io.out  expect  "hF8234567".U
      c.io.zero expect  false.B
      c.io.b    poke    8.U
      c.io.out  expect  "hFF823456".U
      c.io.zero expect  false.B
      c.io.b    poke    12.U
      c.io.out  expect  "hFFF82345".U
      c.io.zero expect  false.B
      c.io.b    poke    20.U
      c.io.out  expect  "hFFFFF823".U
      c.io.zero expect  false.B
      c.io.b    poke    31.U
      c.io.out  expect  "hFFFFFFFF".U
      c.io.zero expect  false.B
    }
  }

  //////////////////////////////////////////////////////////
  // Addition for JARL instruction
  //////////////////////////////////////////////////////////
  it should "add two numbers and clear only least significant bit" in {
    test(new ALU(XLEN=32)) { c =>
      c.io.op   poke    ALUOp.ADD_JALR
      c.io.a    poke    "hffff0000".U
      c.io.b    poke    "h0000ffff".U
      c.io.out  expect  "hfffffffe".U
      c.io.zero expect  false.B
    }
  }

  it should "add with overflow and clear least significant bit" in {
    test(new ALU(XLEN=32)) { c =>
      c.io.op   poke    ALUOp.ADD_JALR
      c.io.a    poke    "h80000001".U
      c.io.b    poke    "h80000000".U
      c.io.out  expect  0.U
      c.io.zero expect  true.B
    }
  }

}

