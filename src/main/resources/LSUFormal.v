`default_nettype none

module LSUFormal #(
    parameter ADDR_WIDTH=32,
    parameter DATA_WIDTH=32
) (
    /* Core Interface */
    input  wire                    core_req,
    input  wire                    core_gnt,
    input  wire                    core_info_ren,
    input  wire                    core_info_wen,
    input  wire                    core_info_op_unsigned,
    input  wire                    core_info_atomic,
    input  wire [  ADDR_WIDTH-1:0] core_info_addr,
    input  wire [  DATA_WIDTH-1:0] core_info_wdata,
    input  wire [DATA_WIDTH/8-1:0] core_info_wmask,
    input  wire                    core_rresp_valid,
    input  wire                    core_rresp_bits_fault,
    input  wire [  DATA_WIDTH-1:0] core_rresp_bits_data,
    input  wire                    core_wresp_valid,
    input  wire                    core_wresp_bits_fault,

    // last request
    input  wire                    last_info_wen,
    input  wire                    last_info_op_unsigned,
    input  wire                    last_info_atomic,
    input  wire [  ADDR_WIDTH-1:0] last_info_addr,
    input  wire [  DATA_WIDTH-1:0] last_info_wdata,
    input  wire [DATA_WIDTH/8-1:0] last_info_wmask,

    /* AXI4-Lite Interface */
    // Clock and Reset
    input  wire                    axi4l_aclk,
    input  wire                    axi4l_aresetn,
    // Read Address Channel
    input  wire                    axi4l_arready,
    input  wire                    axi4l_arvalid,
    input  wire [  ADDR_WIDTH-1:0] axi4l_araddr,
    input  wire [             2:0] axi4l_arprot,
    // Read Data Channel
    input  wire                    axi4l_rready,
    input  wire                    axi4l_rvalid,
    input  wire [  DATA_WIDTH-1:0] axi4l_rdata,
    input  wire [             1:0] axi4l_rresp,
    // Write Address Channel
    input  wire                    axi4l_awready,
    input  wire                    axi4l_awvalid,
    input  wire [  ADDR_WIDTH-1:0] axi4l_awaddr,
    input  wire [             2:0] axi4l_awprot,
    // Write Data Channel
    input  wire                    axi4l_wready,
    input  wire                    axi4l_wvalid,
    input  wire [  DATA_WIDTH-1:0] axi4l_wdata,
    input  wire [DATA_WIDTH/8-1:0] axi4l_wstrb,
    // Write Response Channel
    input  wire                    axi4l_bready,
    input  wire                    axi4l_bvalid,
    input  wire [             1:0] axi4l_bresp
);

    /* Core Formal Assertions */
    reg f_past_valid = 1'b0;
    always @(posedge axi4l_aclk)
	    f_past_valid <= 1'b1;

    // assert reset on first cycle
    always @*
        if (!f_past_valid)
            assume(!axi4l_aresetn);
        else
            assume(axi4l_aresetn);
    
    reg not_in_reset = 1'b0;
    always @(posedge axi4l_aclk)
        not_in_reset <= $past(f_past_valid) && f_past_valid
                        && $past(axi4l_aresetn, 2) && $past(axi4l_aresetn) && axi4l_aresetn;

    reg misaligned = 1'b0;
    // always @(*) begin
    //     misaligned <= 1'b0;
    //     if (op_width == 2'd1) begin // op_width == HWORD
    //         if (core_info_addr[0] != 1'd0)
    //             misaligned <= 1'b1;
    //     end
    //     else if ((op_width == 2'd2) || (op_width == 2'd3)) begin // op_width == WORD or DWORD
    //         if (core_info_addr[1:0] != 2'd0)
    //             misaligned <= 1'b1;
    //     end
    // end

    
    wire read_req = core_req && core_gnt && !core_info_wen;
    wire not_read_req = !read_req;
    wire write_req = core_req && core_gnt && core_info_wen;
    wire not_write_req = !write_req;

    // There can be only one request at a time
    always @(*)
        assume(core_info_ren + core_info_wen < 2);

    // read / write lines are only asserted together with req
    always @(*)
        if (core_info_wen)
            assume(core_req);

    // request lines should only go down after grant
    always @(posedge axi4l_aclk) begin
        if ($past(core_req) && $past(!core_gnt)) begin
            assume(core_req);
            assume(core_info_wen == $past(core_info_wen));
        end
    end

    // request stays stable until grant
    always @(posedge axi4l_aclk)
        if ($past(core_req) && !$past(core_gnt)) begin
            assume($stable(core_info_ren));
            assume($stable(core_info_wen));
            assume($stable(core_info_op_unsigned));
            assume($stable(core_info_addr));
            assume($stable(core_info_wdata));
        end

    // only assert *VALID lines on request
    always @(*)
        if (!core_req) begin
            assert(!axi4l_arvalid);
            assert(!axi4l_awvalid);
            assert(!axi4l_wvalid);
        end

    // inactive channel *VALID lines shold stay deasserted
    // active *VALID lines should be asserted
    always @(posedge axi4l_aclk) begin
        if (not_in_reset && core_req && core_info_ren && core_gnt) begin
            assert(axi4l_arvalid);
            assert(!axi4l_awvalid);
            assert(!axi4l_wvalid);
        end
        if (not_in_reset && core_req && core_info_wen && core_gnt) begin
            assert(!axi4l_arvalid);
            assert(axi4l_awvalid);
            assert(axi4l_wvalid);
        end
    end

    /* cover properties */
    // single read request, subordinate is always ready
    always @(posedge axi4l_aclk)
        if (not_in_reset) begin
            assume(axi4l_aresetn);
            assume(axi4l_arready);
            cover($past(not_read_req, 2) && $past(read_req) && $past(core_info_addr == 32'h12345678) && !$past(core_rresp_valid)
                && $past(axi4l_araddr == 32'h12345678) && $past(axi4l_arvalid)
                && !read_req && core_rresp_valid);
        end
    
    // single slow read request blocked by subordinate
    always @(posedge axi4l_aclk)
        if (not_in_reset) begin
            assume(axi4l_aresetn);
            assume(!core_info_wen);
            cover($past(core_req && !core_gnt && core_info_ren, 2)
                  && $past(axi4l_arvalid && axi4l_araddr == 32'h12345678, 2)
                  && $past(read_req) && !$past(core_rresp_valid)
                  //&& $past(axi4l_araddr == 32'h12345678) && $past(axi4l_arvalid)
                  && !read_req && core_rresp_valid);
        end
    
    // single slow read response followed by fast read
    always @(posedge axi4l_aclk)
        if (not_in_reset) begin
            assume(axi4l_aresetn);
            assume(!core_info_wen);
            cover($past(core_req && core_info_ren && core_gnt, 3) //&& !$past(core_rresp_valid, 3)
                  && $past(axi4l_araddr == 32'h12345678, 3) && $past(axi4l_arvalid, 3)
                  && $past(!core_req && !core_rresp_valid, 2)
                  && $past(read_req && core_rresp_valid && core_rresp_bits_data == 32'h100)
                  && core_rresp_valid);
        end
    
    // pipelined reads
    always @(posedge axi4l_aclk)
        if (not_in_reset) begin
            assume(axi4l_aresetn);
            cover($past(read_req, 2) && $past(read_req) &&
                  $past(core_rresp_valid && core_rresp_valid));
        end

    /* Use right byte lanes */
    // load byte
    // always @(*)
    //     if (axi4l_rvalid && last_op_width == 2'b00) begin
    //         assume(axi4l_aresetn);
    //         if (last_addr[1:0] == 2'b00) assert(axi4l_rdata[7:0] == core_rresp_bits_data[7:0]);
    //         if (last_addr[1:0] == 2'b01) assert(axi4l_rdata[15:8] == core_rresp_bits_data[7:0]);
    //         if (last_addr[1:0] == 2'b10) assert(axi4l_rdata[23:16] == core_rresp_bits_data[7:0]);
    //         if (last_addr[1:0] == 2'b11) assert(axi4l_rdata[31:24] == core_rresp_bits_data[7:0]);
    //     end
    // // load half word
    // always @(*)
    //     if (axi4l_rvalid && last_op_width == 2'b01) begin
    //         assume(axi4l_aresetn);
    //         if (last_addr[1:0] == 2'b00) assert(axi4l_rdata[15:0] == core_rresp_bits_data[15:0]);
    //         if (last_addr[1:0] == 2'b10) assert(axi4l_rdata[31:16] == core_rresp_bits_data[15:0]);
    //     end
    // load word
    always @(*)
        if (axi4l_rvalid) begin
            assume(axi4l_aresetn);
            if (last_info_addr[1:0] == 2'b00) begin 
                assert(axi4l_rdata[31:0] == core_rresp_bits_data[31:0]);
                assert(core_rresp_valid);
            end
        end
    // // store byte
    // always @(*)
    //     if (axi4l_awvalid && axi4l_wvalid && op_width == 2'b00) begin
    //         assume(axi4l_aresetn);
    //         if (core_info_addr[1:0] == 2'b00) assert(axi4l_wdata[7:0] == core_info_wdata[7:0] && axi4l_wstrb == 4'b0001);
    //         if (core_info_addr[1:0] == 2'b01) assert(axi4l_wdata[15:8] == core_info_wdata[7:0] && axi4l_wstrb == 4'b0010);
    //         if (core_info_addr[1:0] == 2'b10) assert(axi4l_wdata[23:16] == core_info_wdata[7:0] && axi4l_wstrb == 4'b0100);
    //         if (core_info_addr[1:0] == 2'b11) assert(axi4l_wdata[31:24] == core_info_wdata[7:0] && axi4l_wstrb == 4'b1000);
    //         assert(axi4l_awaddr[1:0] == 2'b00);
    //     end
    // // store hword
    // always @(*)
    // if (axi4l_awvalid && axi4l_wvalid && op_width == 2'b01) begin
    //     assume(axi4l_aresetn);
    //     if (core_info_addr[1:0] == 2'b00) assert(axi4l_wdata[15:0] == core_info_wdata[15:0] && axi4l_wstrb == 4'b0011);
    //     if (core_info_addr[1:0] == 2'b10) assert(axi4l_wdata[31:16] == core_info_wdata[15:0] && axi4l_wstrb == 4'b1100);
    //     assert(axi4l_awaddr[1:0] == 2'b00);
    // end

    // store word
    always @(*)
        if (axi4l_awvalid && axi4l_wvalid) begin
            assume(axi4l_aresetn);
            assert(axi4l_wdata[31:0] == core_info_wdata[31:0] && axi4l_wstrb == core_info_wmask);
            assert(axi4l_awaddr[1:0] == 2'b00);
        end
    
    /* Sign extension and zero extension */
    // Byte Loads
    // wire byte_sign_extended = &core_rresp_bits_data[DATA_WIDTH-1:8];
    // wire byte_zero_extended = !(|core_rresp_bits_data[DATA_WIDTH-1:8]);
    // always @(posedge axi4l_aclk)
    //     if (f_past_valid && $past(read_req) && (axi4l_rvalid && axi4l_rready) && last_op_width == 2'b00) begin
    //         assume(axi4l_aresetn);
    //         if (!last_op_unsigned && core_rresp_bits_data[7])
    //             assert(byte_sign_extended);
    //         else 
    //             assert(byte_zero_extended);
    //     end

    // // Halfword Loads
    // wire hword_sign_extended = &core_rresp_bits_data[DATA_WIDTH-1:16];
    // wire hword_zero_extended = !(|core_rresp_bits_data[DATA_WIDTH-1:16]);
    // always @(posedge axi4l_aclk)
    //     if (f_past_valid && $past(read_req) && (axi4l_rvalid && axi4l_rready) && last_op_width == 2'b01) begin
    //         assume(axi4l_aresetn);
    //         if (!last_op_unsigned && core_rresp_bits_data[15])
    //             assert(hword_sign_extended);
    //         else 
    //             assert(hword_zero_extended);
    //     end

    // // Ensure that misaligned flag is raised and not raised
    // always @(posedge axi4l_aclk) begin
    //     if (misaligned && core_info_ren)
    //         assert(rd_misalig); 
    //     else
    //         assert(!rd_misalig); 
    //     if (misaligned && core_info_wen)
    //         assert(wr_misalig);
    //     else
    //         assert(!wr_misalig);
    // end
    

    /* AXI4-Lite Formal Assertions */

    // During reset a manager interface must drive axi4l_arvalid, axi4l_awvalid, and axi4l_wvalid low. (A3-40)
    // FIXME: it seems like chisel doesn't yet support active low async resets
    // always @(*)
    //     if (!axi4l_aresetn) begin
    //         assert(!axi4l_arvalid);
    //         assert(!axi4l_awvalid);
    //         assert(!axi4l_wvalid);
    //     end

    // The earliest point after reset that a Manager is permitted to begin driving axi4l_arvalid, axi4l_awvalid,
    // or axi4l_wvalid high is at a rising axi4l_aclk edge after axi4l_aresetn is high (example figure). (A3-40)
    always @(posedge axi4l_aclk)
        // assert that valid doesn't go high at the edge axi4l_aresetn is deasserted
        if (f_past_valid && axi4l_aresetn && $past(!axi4l_aresetn)) begin
            assert(!axi4l_arvalid);
            assert(!axi4l_awvalid);
            assert(!axi4l_wvalid);
        end
    
    // When asserted, axi4l_awvalid must remain asserted until the rising clock edge after the Subordinate
    // asserts axi4l_awready. (A3-42)
    always @(posedge axi4l_aclk)
        if (f_past_valid && $past(axi4l_aresetn) && $past(axi4l_awvalid) && !axi4l_awvalid)
            assert($past(axi4l_awready));
    
    // When asserted, axi4l_wvalid must remain asserted until the rising clock edge after the Subordinate
    // asserts axi4l_wready. (A3-43)
    always @(posedge axi4l_aclk)
    if (f_past_valid && $past(axi4l_aresetn) && $past(axi4l_wvalid) && !axi4l_wvalid)
        assert($past(axi4l_wready));

    // When asserted, axi4l_arvalid must remain asserted until the rising clock edge after the Subordinate
    // asserts the axi4l_arready signal. (A3-43)
    always @(posedge axi4l_aclk)
    if (f_past_valid && $past(axi4l_aresetn) && $past(axi4l_arvalid) && !axi4l_arvalid)
        assert($past(axi4l_arready));

endmodule
