package acorebase

import chisel3._
import chisel3.util._
import chisel3.experimental._
import amba.axi4l._
import a_core_common._

case class IFetchIO(config: ACoreConfig) extends Bundle {
  val ifetch = new SRAMIf(config.addr_width, config.data_width).read
  val in = Input(new Bundle {
    val jump   = Valid(UInt(config.addr_width.W))
    val branch = Valid(UInt(config.addr_width.W))
    val trap   = Valid(UInt(config.addr_width.W))
    val mret   = Valid(UInt(config.addr_width.W))
    val flush  = Bool()
    val stall  = Bool()
  })
  val out = Decoupled(new Bundle {
    val instr_bits  = UInt(config.data_width.W)
    val pc          = UInt(config.addr_width.W)
    val trap_cause  = Valid(ExceptionCauses())
  })
}

/**
  * Instruction fetch stage. This stage generates the address for a new instruction and fetches it.
  * Instruction fetch is sequential so this stage takes two clock cycles.
  * Address for the fetch comes from AddrGen which holds an internal PC register.
  * Pipeline events (jump, branch, trap, etc.) load a new value to the PC register.
  *
  * @param config
  */
class IFetch(config: ACoreConfig) extends Module {
  val io = IO(IFetchIO(config))
  val addr_gen = Module(new AddrGen(config))

  // How large is our instruction queue
  val bufSize = 1
  // Queue to store outstanding request addresses
  // Entry is stored at first `req`
  // All requests will be completed
  val reqQueue     = Module(new Queue(UInt(config.addr_width.W), bufSize, pipe = true))
  // Buffer to store fetched instructions in case of stalls
  // Essentially a skid buffer
  val ifetchBuffer = Module(new Queue(new Bundle {
    val data = UInt(config.data_width.W)
    val fault = Bool()
  }, bufSize, pipe = true, flow = true))

  object IFetchControlFSMState extends chisel3.ChiselEnum {
    val NORMAL = Value
    val FLUSH  = Value
  }
  val control_fsm_reg = RegInit(IFetchControlFSMState.NORMAL)
  val req_not_gnt_reg = RegInit(false.B)
  val req             = WireDefault(false.B)

  // New program counter (PC) is loaded and moved to Program Memory input
  io.ifetch.addr := addr_gen.io.out.pc

  addr_gen.io.in.branch := io.in.branch
  addr_gen.io.in.jump   := io.in.jump
  addr_gen.io.in.trap   := io.in.trap
  addr_gen.io.in.mret   := io.in.mret
  addr_gen.io.in.grant  := io.ifetch.gnt

  reqQueue.io.enq.bits  := addr_gen.io.out.pc
  reqQueue.io.enq.valid := io.ifetch.req
  reqQueue.io.deq.ready := io.out.ready && ifetchBuffer.io.deq.valid

  ifetchBuffer.io.enq.bits.data  := io.ifetch.data.bits
  ifetchBuffer.io.enq.bits.fault := io.ifetch.fault
  ifetchBuffer.io.enq.valid      := io.ifetch.data.valid

  io.out.bits.instr_bits       := ifetchBuffer.io.deq.bits.data
  io.out.bits.pc               := reqQueue.io.deq.bits
  io.out.bits.trap_cause.bits  := ExceptionCauses.INSTR_ACCESS_FAULT
  io.out.bits.trap_cause.valid := ifetchBuffer.io.deq.bits.fault

  // Instruction fetch controller
  // Handles that all requests are received and ignored in case of a flush
  req                       := reqQueue.io.enq.ready || req_not_gnt_reg
  io.ifetch.req             := false.B
  io.out.valid              := false.B
  ifetchBuffer.io.deq.ready := io.out.ready
  when (control_fsm_reg === IFetchControlFSMState.NORMAL) {
    io.out.valid              := ifetchBuffer.io.deq.valid
    io.ifetch.req             := req && !io.in.flush && !io.in.stall
    when (io.in.flush && reqQueue.io.deq.valid) {
      control_fsm_reg := IFetchControlFSMState.FLUSH
    }
  } .elsewhen (control_fsm_reg === IFetchControlFSMState.FLUSH) {
    io.out.valid              := false.B
    ifetchBuffer.io.deq.ready := true.B
    when (!reqQueue.io.deq.valid && !io.in.flush) {
      control_fsm_reg := IFetchControlFSMState.NORMAL
      io.ifetch.req := req && !io.in.stall
    }
  }

  when (io.ifetch.req && !io.ifetch.gnt) {
    req_not_gnt_reg := true.B
  } .elsewhen (io.ifetch.req && io.ifetch.gnt) {
    req_not_gnt_reg := false.B
  }
}
