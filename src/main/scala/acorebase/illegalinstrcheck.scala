// SPDX-License-Identifier: Apache-2.0

package acorebase

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.iotesters.PeekPokeTester
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import a_core_common._


/**
  * Checks for illegal instruction.
  * 
  * Inputs:
    illegal_opcode: if Control detected illegal opcode
    rm: Rounding mode from CSR register
    rm_src: Whether ALU is using static or dynamic rounding (static is default)
    f_op: if current operation is a floating-point operation
    f_en: whether F-extension is enabled by misa CSR
  *
  * Outputs:
    illegal_instr: illegal instruction
  */
class IllegalInstrCheck (config: ACoreConfig) extends Module {
  val io = IO(new Bundle {
    val in  = Input(new Bundle {
      val illegal_opcode = Bool()
      val rm = UInt(3.W)
      val rm_src = AluBlockRoundingSrc()
      val f_op = Bool()
    })
    val out = Output(new Bundle {
      val illegal_instr = Bool()
    })
  })

  val illegal_rm = WireDefault(false.B)
  val f_op_when_f_disabled = WireDefault(false.B)

  if (config.extension_set.contains(ISAExtension.F)) {
    // These checks are here, instead of Control, because they use data
    // that may need to be forwarded, and forwarding occurs after Control

    // Checks that dynamic rounding mode is legal if being used
    when (io.in.f_op || (io.in.rm_src === AluBlockRoundingSrc.DRM)) {
      when (VecInit("b111".U, "b101".U, "b110".U).contains(io.in.rm)) {
        illegal_rm := true.B
      }
    }
  }

  io.out.illegal_instr := illegal_rm || io.in.illegal_opcode
}

/** Generates verilog */
object IllegalInstrCheck extends App with OptionParser {
  // Generate verilog
  val annos = Seq(ChiselGeneratorAnnotation(() => new IllegalInstrCheck(
    config = ACoreConfig.default,
  )))
  (new ChiselStage).execute(args, annos)
}

