package acorebase

import chisel3._
import chisel3.util._
import chisel3.experimental._
import a_core_common._

case class DecodeIO(config: ACoreConfig) extends Bundle {
  val in = Flipped(Decoupled(new Bundle {
    val instr_bits    = UInt(32.W)
    val rounding_mode = UInt(3.W)
  }))
  val out = Decoupled(new Bundle {
    val rs1           = UInt(5.W)
    val rs2           = UInt(5.W)
    val rs3           = UInt(5.W)
    val imm           = UInt(32.W)
    val regfile       = new RegFileBlockReadInputs(config.data_width)
    val control       = ControlOutputs(config.data_width)
    val illegal_instr = Bool()
  })
}

/**
  * Instruction Decode stage. Takes the instruction word as an input, and generates
  * necessary control signals for next stages and for fetching data from register file.
  *
  * @param config
  */
class Decode(config: ACoreConfig) extends Module {
  val io = IO(DecodeIO(config))

  val decoder             = Module(new DecoderBlock(config))
  val control             = Module(new Control(config))
  val illegal_instr_check = Module(new IllegalInstrCheck(config))

  val emptyReg            = RegInit(true.B)

  when (io.out.fire && !io.in.fire) {
    emptyReg := true.B
  } .elsewhen(io.in.fire) {
    emptyReg := false.B
  }

  decoder.io.instr_bits             := io.in.bits.instr_bits
  io.out.bits.rs1                   := decoder.io.rs1
  io.out.bits.rs2                   := decoder.io.rs2
  io.out.bits.rs3                   := decoder.io.f_ext.rs3
  io.out.bits.imm                   := decoder.io.imm
  io.out.bits.control               := control.io.out
  io.out.bits.regfile.data.rs1_addr := decoder.io.rs1
  io.out.bits.regfile.data.rs2_addr := decoder.io.rs2
  io.out.bits.regfile.data.rs3_addr := decoder.io.f_ext.rs3
  io.out.bits.regfile.control       := control.io.out.regfile_block_read

  control.io.in.f_ext.fpu_rs2 := decoder.io.rs2
  control.io.in.rd            := decoder.io.rd
  control.io.in.rs1           := decoder.io.rs1
  control.io.in.opcode        := decoder.io.opcode
  control.io.in.funct3        := decoder.io.funct3
  control.io.in.funct7        := decoder.io.funct7
  control.io.in.funct12       := decoder.io.funct12

  illegal_instr_check.io.in.f_op           := control.io.out.alu_block.f_ext.fpu_en
  illegal_instr_check.io.in.rm_src         := control.io.out.alu_block.f_ext.rm_src
  illegal_instr_check.io.in.rm             := io.in.bits.rounding_mode
  illegal_instr_check.io.in.illegal_opcode := control.io.out.illegal_instr
  io.out.bits.illegal_instr                := illegal_instr_check.io.out.illegal_instr

  io.out.valid := io.in.valid
  io.in.ready  := io.out.ready || emptyReg
}
