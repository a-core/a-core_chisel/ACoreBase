// SPDX-License-Identifier: Apache-2.0

package acorebase

import chisel3._

case class BranchPredictorInputs(XLEN: Int) extends Bundle {
  /** Program counter from PCBlock */
  val pc            = UInt(XLEN.W)
  /** Calculated branch result */
  val branch_result = Bool()
  /** Calculated branch target address */
  val branch_target = UInt(XLEN.W)
}

case class BranchPredictorOutputs(XLEN: Int) extends Bundle {
  /** Prediction result (true, false) */
  val predict_branch = Bool()
  /** Predicted target address */
  val pc_predicted   = UInt(XLEN.W)
}

case class BranchPredictorIO(XLEN: Int) extends Bundle {
  val in  = Input(BranchPredictorInputs(XLEN))
  val out = Output(BranchPredictorOutputs(XLEN))
}

abstract class BranchPredictor(XLEN: Int) extends Module {
  val io = IO(BranchPredictorIO(XLEN))
}