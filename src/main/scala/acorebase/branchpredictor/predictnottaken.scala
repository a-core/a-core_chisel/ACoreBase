// SPDX-License-Identifier: Apache-2.0

package acorebase

import chisel3._

/**
  * A very simple branch predictor that predicts that a branch
  * is never taken.
  *
  * @param XLEN
  */
class PredictNotTaken(XLEN: Int) extends BranchPredictor(XLEN: Int) {
  io.out.predict_branch := false.B
  io.out.pc_predicted   := 0.U
}

