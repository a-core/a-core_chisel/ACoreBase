// SPDX-License-Identifier: Apache-2.0

package acorebase

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.experimental.VecLiterals._
import chisel3.experimental.BundleLiterals._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import a_core_common._
import BusType._
import dataclass.data

/**
  * Pipeline register with optional flush.
  * Decoupled (ready/valid) interface.
  * Ready signal is combinationally connected.
  *
  * @param gen
  * @param entries
  * @param pipe
  * @param flow
  * @param useSyncReadMem
  * @param hasFlush
  */
class PipelineReg[T <: Data](
  gen:            T,
  hasFlush:       Boolean = false
) extends Module {
  val io    = IO(new QueueIO(gen, 1, hasFlush))
  io.count := 0.U

  val reg  = Reg(gen)
  val full = RegInit(false.B)
  when (io.deq.fire) {
    full := false.B
  }
  when (io.enq.fire) {
    full := true.B
    reg  := io.enq.bits
  }
  when (io.flush.getOrElse(false.B)) {
    full := false.B
  }
  io.enq.ready := !full || io.deq.ready
  io.deq.valid := full
  io.deq.bits  := reg
}

/**
  * Signals stored in the pipeline register between IF and ID stages
  *
  * @param XLEN
  * @param pc_init PC initialization value
  */
class IFtoID(XLEN: Int, pc_init: String) extends Bundle {
  val instr_bits   = UInt(XLEN.W)
  val pc           = UInt(XLEN.W)
  val trap_cause   = Valid(ExceptionCauses())
}

/**
  * Signals stored in the pipeline register between ID and EX stages
  *
  * @param XLEN
  * @param default_misa Initial value for misa register
  */
class IDtoEX(XLEN: Int, default_misa: Int) extends Bundle {
  val instr_bits      = UInt(XLEN.W)
  val imm             = UInt(XLEN.W)
  val pc              = UInt(XLEN.W)
  val rs1_rdata       = UInt(XLEN.W)
  val rs2_rdata       = UInt(XLEN.W)
  val rs3_rdata       = UInt(XLEN.W)
  val jump            = Bool()
  val mret            = Bool()
  val trap_cause      = Valid(ExceptionCauses())
  val rd_src          = RegFileSrc()
  val muldiv_op       = MultOp()
  val regfile_write_control  = new RegFileBlockWriteInputs(XLEN).control
  val alublock_control       = new ALUBlockInputs(XLEN).control
  val lsu_control            = new LSUControlIO(XLEN)
  val csrblock_control       = new ControlStatusBlockInputs(XLEN).control
  val events                 = new ControlStatusBlockInputs(XLEN).event
}

/**
  * Signals stored in the pipeline register between EX and MEM stages
  *
  * @param XLEN
  */
class EXtoMEM(XLEN: Int) extends Bundle {
  val instr_bits      = UInt(XLEN.W)
  val pc              = UInt(XLEN.W)
  val rs1_rdata       = UInt(XLEN.W)
  val rs2_rdata       = UInt(XLEN.W)
  val lsu_wdata       = UInt(XLEN.W)
  val lsu_wmask       = UInt(4.W)
  val lsu_addr        = UInt(XLEN.W)
  val rd_wdata        = Valid(UInt(XLEN.W))
  val trap_cause      = Valid(ExceptionCauses())
  val rd_src          = RegFileSrc()
  val mul_s1_to_s2           = new Bundle {
    val bits  = (new MultiplierPipelined(XLEN)).Stage1Outputs()
    val valid = Bool()
  }
  val regfile_write_control  = new RegFileBlockWriteInputs(XLEN).control
  val lsu_control            = new LSUControlIO(XLEN)
  val events                 = new ControlStatusBlockInputs(XLEN).event
}

/**
  * Signals stored in the pipeline register between MEM and WB stages
  *
  * @param XLEN
  */
class MEMtoWB(XLEN: Int) extends Bundle {
  val instr_bits            = UInt(XLEN.W)
  val pc                    = UInt(XLEN.W)
  val rd_wdata              = Valid(UInt(XLEN.W))
  val trap_cause            = Valid(ExceptionCauses())
  val regfile_write_control = new RegFileBlockWriteInputs(XLEN).control
  val events                = new ControlStatusBlockInputs(XLEN).event
}
