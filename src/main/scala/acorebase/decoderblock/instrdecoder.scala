// SPDX-License-Identifier: Apache-2.0

// Chisel module InstrDecoder
// Inititally written by Verneri Hirvonen (verneri.hirvonen@aalto.fi), 2021-03-28

package acorebase

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.iotesters.PeekPokeTester
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import a_core_common._


/** IO definitions for InstrDecoder */
class InstrDecoderIO(XLEN: Int = 32) extends Bundle {
  val instr_bits = Input(UInt(XLEN.W))
  val instr_type = Output(InstrType())
  val rs1 = Output(UInt(5.W))
  val rs2 = Output(UInt(5.W))
  val rd = Output(UInt(5.W))
  val opcode = Output(Opcode())
  val funct3 = Output(UInt(3.W))
  val funct7 = Output(UInt(7.W))
  val funct12 = Output(UInt(12.W))
  val instr = Output(Instruction())
  //F-extension
  val rs3 = Output(UInt(5.W))
}


/** Chisel module InstrDecoder */
class InstrDecoder(config: ACoreConfig) extends Module {
  val io = IO(new InstrDecoderIO(XLEN=config.data_width))

  val opcode = Wire(Opcode())
  val funct3 = Wire(UInt(3.W))
  val funct7 = Wire(UInt(7.W))
  val funct12 = Wire(UInt(12.W)) 

  opcode := Opcode(io.instr_bits(6,0))
  funct3 := io.instr_bits(14,12)
  funct7 := io.instr_bits(31,25)
  funct12 := io.instr_bits(31,20)
  io.rs1 := io.instr_bits(19,15)
  io.rs2 := io.instr_bits(24,20)
  io.rd := io.instr_bits(11,7)
  io.opcode := opcode
  io.funct3 := funct3
  io.funct7 := funct7
  io.funct12 := funct12
  io.rs3 := 0.U

  //F-extension
  if (config.extension_set.contains(ISAExtension.F)) { 
    val funct5 = Wire(UInt(5.W))
    funct5 := funct7(6,2)  
    io.rs3 := funct5
  }

  // Instruction decoding implemented as a big blob of combinational logic that set the following outputs:
  //   instruction - decoded instruction id (enum)
  //   instruction_type - instruction type associated with instrution (enum)
  io.instr := Instruction.INVALID
  io.instr_type := InstrType.INVALID
  // LUI
  when (opcode === Opcode.LUI) {
    io.instr_type := InstrType.U
    io.instr := Instruction.LUI
  // AUIPC
  } .elsewhen (opcode === Opcode.AUIPC) {
    io.instr_type := InstrType.U
    io.instr := Instruction.AUIPC
  // JAL
  } .elsewhen (opcode === Opcode.JAL) {
    io.instr_type := InstrType.J
    io.instr := Instruction.JAL
  // JALR
  } .elsewhen (opcode === Opcode.JALR) {
    io.instr_type := InstrType.I
    io.instr := Instruction.JALR
  // BRANCH
  } .elsewhen (opcode === Opcode.BRANCH) {
    io.instr_type := InstrType.B
    when (funct3 === "b000".U) {
      io.instr := Instruction.BEQ
    } .elsewhen (funct3 === "b001".U) {
      io.instr := Instruction.BNE
    } .elsewhen (funct3 === "b100".U) {
      io.instr := Instruction.BLT
    } .elsewhen (funct3 === "b101".U) {
      io.instr := Instruction.BGE
    } .elsewhen (funct3 === "b110".U) {
      io.instr := Instruction.BLTU
    } .elsewhen (funct3 === "b111".U) {
      io.instr := Instruction.BGEU
    } .otherwise {
      // invalid funct3 field for opcode BRANCH
      io.instr := Instruction.INVALID
      io.instr_type := InstrType.INVALID
    }
  // LOAD
  } .elsewhen (opcode === Opcode.LOAD) {
    io.instr_type := InstrType.I
    when (funct3 === "b000".U) {
      io.instr := Instruction.LB
    } .elsewhen (funct3 === "b001".U) {
      io.instr := Instruction.LH
    } .elsewhen (funct3 === "b010".U) {
      io.instr := Instruction.LW
    } .elsewhen (funct3 === "b100".U) {
      io.instr := Instruction.LBU
    } .elsewhen (funct3 === "b101".U) {
      io.instr := Instruction.LHU
    } .otherwise {
      // invalid funct3 field for opcode LOAD
      io.instr := Instruction.INVALID
      io.instr_type := InstrType.INVALID
    }
  // STORE
  } .elsewhen (opcode === Opcode.STORE) {
    io.instr_type := InstrType.S
    when (funct3 === "b000".U) {
      io.instr := Instruction.SB
    } .elsewhen (funct3 === "b001".U) {
      io.instr := Instruction.SH
    } .elsewhen (funct3 === "b010".U) {
      io.instr := Instruction.SW
    } .otherwise {
      // invalid funct3 field for opcode STORE
      io.instr := Instruction.INVALID
      io.instr_type := InstrType.INVALID
    }
  // OP-IMM
  } .elsewhen (opcode === Opcode.OP_IMM) {
    io.instr_type := InstrType.I
    when (funct3 === "b000".U) {
      io.instr := Instruction.ADDI
    } .elsewhen (funct3 === "b010".U) {
      io.instr := Instruction.SLTI
    } .elsewhen (funct3 === "b011".U) {
      io.instr := Instruction.SLTIU
    } .elsewhen (funct3 === "b100".U) {
      io.instr := Instruction.XORI
    } .elsewhen (funct3 === "b110".U) {
      io.instr := Instruction.ORI
    } .elsewhen (funct3 === "b111".U) {
      io.instr := Instruction.ANDI
    } .elsewhen (funct3 === "b001".U && funct7 === "b0000000".U) {
      io.instr := Instruction.SLLI
    } .elsewhen (funct3 === "b101".U && funct7 === "b0000000".U) {
      io.instr := Instruction.SRLI
    } .elsewhen (funct3 === "b101".U && funct7 === "b0100000".U) {
      io.instr := Instruction.SRAI
    } .otherwise {
      // invalid funct3 field for opcode OP-IMM
      io.instr := Instruction.INVALID
      io.instr_type := InstrType.INVALID
    }
  // OP
  } .elsewhen (opcode === Opcode.OP) {
    io.instr_type := InstrType.R
    
    // invalid combination funct3 and funct7 for opcode OP
    io.instr := Instruction.INVALID

    when (funct3 === "b000".U && funct7 === "b0000000".U) {
      io.instr := Instruction.ADD
    } .elsewhen (funct3 === "b000".U && funct7 === "b0100000".U) {
      io.instr := Instruction.SUB
    } .elsewhen (funct3 === "b001".U && funct7 === "b0000000".U) {
      io.instr := Instruction.SLL
    } .elsewhen (funct3 === "b010".U && funct7 === "b0000000".U) {
      io.instr := Instruction.SLT
    } .elsewhen (funct3 === "b011".U && funct7 === "b0000000".U) {
      io.instr := Instruction.SLTU
    } .elsewhen (funct3 === "b100".U && funct7 === "b0000000".U) {
      io.instr := Instruction.XOR
    } .elsewhen (funct3 === "b101".U && funct7 === "b0000000".U) {
      io.instr := Instruction.SRL
    } .elsewhen (funct3 === "b101".U && funct7 === "b0100000".U) {
      io.instr := Instruction.SRA
    } .elsewhen (funct3 === "b110".U && funct7 === "b0000000".U) {
      io.instr := Instruction.OR
    } .elsewhen (funct3 === "b111".U && funct7 === "b0000000".U) {
      io.instr := Instruction.AND
    }    
    // M type extension
    if(config.extension_set.contains(ISAExtension.M)) {
      when (funct3 === "b000".U && funct7 === "b0000001".U) {
        io.instr := Instruction.MUL
      } .elsewhen (funct3 === "b001".U && funct7 === "b0000001".U) {
        io.instr := Instruction.MULH
      } .elsewhen (funct3 === "b010".U && funct7 === "b0000001".U) {
        io.instr := Instruction.MULHSU
      } .elsewhen (funct3 === "b011".U && funct7 === "b0000001".U) {
        io.instr := Instruction.MULHU
      } .elsewhen (funct3 === "b100".U && funct7 === "b0000001".U) {
        io.instr := Instruction.DIV
      } .elsewhen (funct3 === "b101".U && funct7 === "b0000001".U) {
        io.instr := Instruction.DIVU
      } .elsewhen (funct3 === "b110".U && funct7 === "b0000001".U) {
        io.instr := Instruction.REM
      } .elsewhen (funct3 === "b111".U && funct7 === "b0000001".U) {
        io.instr := Instruction.REMU
      }
    }
  // FENCE
  } .elsewhen (opcode === Opcode.MISC_MEM) {
    // TODO: Determine instruction type for FENCE instruction
    io.instr := Instruction.FENCE
  // SYSTEM
  } .elsewhen (opcode === Opcode.SYSTEM) {
    io.instr_type := InstrType.I
    when (funct3 === "b000".U && funct12 === "b000000000000".U) {
      io.instr := Instruction.ECALL
    } .elsewhen (funct3 === "b000".U && funct12 === "b000000000001".U) {
      io.instr := Instruction.EBREAK
    } .elsewhen (funct3 === "b000".U && funct12 === "b001100000010".U) {
      io.instr := Instruction.MRET
    } .otherwise {
      when (funct3 === "b001".U) {
        io.instr := Instruction.CSRRW
      } .elsewhen (funct3 === "b010".U) {
        io.instr := Instruction.CSRRS
      } .elsewhen (funct3 === "b011".U) {
        io.instr := Instruction.CSRRC
      } .elsewhen (funct3 === "b101".U) {
        io.instr := Instruction.CSRRWI
      } .elsewhen (funct3 === "b110".U) {
        io.instr := Instruction.CSRRSI
      } .elsewhen (funct3 === "b111".U) {
        io.instr := Instruction.CSRRCI
      } .otherwise {
        // invalid conbination funct3 for opcode SYSTEM
        io.instr := Instruction.INVALID
        io.instr_type := InstrType.INVALID
      }
    }

  // LOAD_FP
  } .elsewhen (opcode === Opcode.LOAD_FP) {
    io.instr := Instruction.INVALID
    io.instr_type := InstrType.INVALID    

    if(config.extension_set.contains(ISAExtension.F)) {
      io.instr_type := InstrType.I
      when (funct3 === "b010".U) {
        io.instr := Instruction.FLW
      }
    }

  // STORE_FP
  } .elsewhen (opcode === Opcode.STORE_FP) {
    io.instr := Instruction.INVALID
    io.instr_type := InstrType.INVALID    

    if(config.extension_set.contains(ISAExtension.F)) {
      io.instr_type := InstrType.S
      when (funct3 === "b010".U) {
        io.instr := Instruction.FSW
      }
    }

  // OP_FP
  } .elsewhen (opcode === Opcode.OP_FP) {
    io.instr := Instruction.INVALID
    io.instr_type := InstrType.INVALID

    if(config.extension_set.contains(ISAExtension.F)) {
      val fmt = Wire(UInt(2.W))
      fmt := funct7(1,0)

      val rs2 = Wire(UInt(5.W))
      rs2 := io.instr_bits(24,20)

      val rm = Wire(UInt(3.W))
      rm := funct3 

      io.instr_type := InstrType.R

      when (funct7 === "b0000000".U) {
        io.instr := Instruction.FADD_S
      } .elsewhen (funct7 === "b0000100".U) {
        io.instr := Instruction.FSUB_S
      } .elsewhen (funct7 === "b0001000".U) {
        io.instr := Instruction.FMUL_S
      } .elsewhen (funct7 === "b0001100".U) {
        io.instr := Instruction.FDIV_S
      } .elsewhen (funct7 === "b0101100".U && rs2 === "b00000".U) {
        io.instr := Instruction.FSQRT_S
      } .elsewhen (funct7 === "b0010000".U && funct3 === "b000".U) { //rm === funct3
        io.instr := Instruction.FSGNJ_S
      } .elsewhen (funct7 === "b0010000".U && funct3 === "b001".U) {
        io.instr := Instruction.FSGNJN_S
      } .elsewhen (funct7 === "b0010000".U && funct3 === "b010".U) {
        io.instr := Instruction.FSGNJX_S
      } .elsewhen (funct7 === "b0010100".U && funct3 === "b000".U) {
        io.instr := Instruction.FMIN_S
      } .elsewhen (funct7 === "b0010100".U && funct3 === "b001".U) {
        io.instr := Instruction.FMAX_S
      } .elsewhen (funct7 === "b1100000".U && rs2 === "b00000".U) { 
        io.instr := Instruction.FCVT_W_S
      } .elsewhen (funct7 === "b1100000".U && rs2 === "b00001".U) {
        io.instr := Instruction.FCVT_WU_S
      } .elsewhen (funct7 === "b1110000".U && rs2 === "b00000".U && funct3 === "b000".U) {
        io.instr := Instruction.FMV_X_W
      } .elsewhen (funct7 === "b1010000".U && funct3 === "b010".U) {
        io.instr := Instruction.FCMP_FEQ_S
      } .elsewhen (funct7 === "b1010000".U && funct3 === "b001".U) {
        io.instr := Instruction.FCMP_FLT_S
      } .elsewhen (funct7 === "b1010000".U && funct3 === "b000".U) {
        io.instr := Instruction.FCMP_FLE_S
      } .elsewhen (funct7 === "b1110000".U && rs2 === "b00000".U && funct3 === "b001".U) {
        io.instr := Instruction.FCLASS_S
      } .elsewhen (funct7 === "b1101000".U && rs2 === "b00000".U) {
        io.instr := Instruction.FCVT_S_W
      } .elsewhen (funct7 === "b1101000".U && rs2 === "b00001".U) {
        io.instr := Instruction.FCVT_S_WU
      } .elsewhen (funct7 === "b1111000".U && rs2 === "b00000".U && funct3 === "b000".U) {
        io.instr := Instruction.FMV_W_X
      }
    }
    
  // MADD
  } .elsewhen (opcode === Opcode.MADD) {
    io.instr := Instruction.INVALID
    io.instr_type := InstrType.INVALID

    if(config.extension_set.contains(ISAExtension.F)) {
      io.instr_type := InstrType.R
      io.instr := Instruction.FMADD_S
    }

  // MSUB
  } .elsewhen (opcode === Opcode.MSUB) {
    io.instr := Instruction.INVALID
    io.instr_type := InstrType.INVALID

    if(config.extension_set.contains(ISAExtension.F)) {
      io.instr_type := InstrType.R
      io.instr := Instruction.FMSUB_S
    }

  // NMADD
  }  .elsewhen (opcode === Opcode.NMADD) {
    io.instr := Instruction.INVALID
    io.instr_type := InstrType.INVALID

    if(config.extension_set.contains(ISAExtension.F)) {
      io.instr_type := InstrType.R
      io.instr := Instruction.FNMADD_S
    }

  // NMSUB
  }  .elsewhen (opcode === Opcode.NMSUB) {
    io.instr := Instruction.INVALID
    io.instr_type := InstrType.INVALID

    if(config.extension_set.contains(ISAExtension.F)) {
      io.instr_type := InstrType.R
      io.instr := Instruction.FNMSUB_S
    }

  // AMO
  } .elsewhen (opcode === Opcode.AMO) {
    if (config.extension_set.contains(ISAExtension.A)) {
      io.instr_type := InstrType.R // {aq, rl} occupy two lowest bits of funct7
      val funct5 = WireDefault(funct7(6,2))
      when (funct5 === "b00010".U && funct3 === "b010".U) {
        io.instr := Instruction.LR_W
      } .elsewhen (funct5 === "b00011".U && funct3 === "b010".U) {
        io.instr := Instruction.SC_W
      }
    }

  }  .otherwise {
    // invalid opcode
    io.instr := Instruction.INVALID
    io.instr_type := InstrType.INVALID
  }
}

/** Generates verilog */
object InstrDecoder extends App {
  val annos = Seq(ChiselGeneratorAnnotation(() => new InstrDecoder(
    config=ACoreConfig.default
  )))
  (new ChiselStage).execute(args, annos)
}
