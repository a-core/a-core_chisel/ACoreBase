// SPDX-License-Identifier: Apache-2.0

// Chisel module DecoderBlock
// Inititally written by Verneri Hirvonen (verneri.hirvonen@aalto.fi), 2021-03-28

package acorebase

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.iotesters.PeekPokeTester
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import a_core_common._

class DecoderBlockFIO(XLEN: Int = 32) extends Bundle {
  //RV32F extension
  val rs3 = Output(UInt(5.W))
}

/** IO definitions for DecoderBlock */
class DecoderBlockIO(XLEN: Int = 32) extends Bundle {
  val instr_bits = Input(UInt(XLEN.W))
  val imm        = Output(UInt(XLEN.W))
  val instr_type = Output(InstrType())
  val rs1        = Output(UInt(5.W))
  val rs2        = Output(UInt(5.W))
  val rd         = Output(UInt(5.W))
  val opcode     = Output(Opcode())
  val funct3     = Output(UInt(3.W))
  val funct7     = Output(UInt(7.W))
  val funct12    = Output(UInt(12.W))
  val instr      = Output(Instruction())

  //Extensions
  val f_ext      = new DecoderBlockFIO(XLEN)
}

/** Chisel module DecoderBlock */
class DecoderBlock(config: ACoreConfig) extends Module {
  val io = IO(new DecoderBlockIO(XLEN=config.data_width))
  val imm_gen = Module(new ImmGen(config))
  val instr_decoder = Module(new InstrDecoder(config))
  val instr_type = WireDefault(instr_decoder.io.instr_type)

  // inputs
  instr_decoder.io.instr_bits := io.instr_bits
  imm_gen.io.instr_bits := io.instr_bits
  // used to check for RVC type instructions
  val lsb2 = io.instr_bits(1, 0)

  if (config.extension_set.contains(ISAExtension.C)) {

    val instr_decoder_rvc = Module(new InstrDecoderRVC(config))

    when (lsb2 === 3.U) {
      instr_decoder_rvc.io.instr_bits := 0.U

      // instr_decoder outputs
      io.instr_type := instr_type
      io.rs1 := instr_decoder.io.rs1
      io.rs2 := instr_decoder.io.rs2
      io.rd := instr_decoder.io.rd
      io.opcode := instr_decoder.io.opcode
      io.funct3 := instr_decoder.io.funct3
      io.funct7 := instr_decoder.io.funct7
      io.funct12 := instr_decoder.io.funct12
      io.instr := instr_decoder.io.instr

    }.otherwise {
      instr_decoder_rvc.io.instr_bits := io.instr_bits
      instr_decoder.io.instr_bits := 0.U

      instr_type := instr_decoder_rvc.io.instr_type
      io.instr_type := instr_type
      io.rs1 := instr_decoder_rvc.io.rs1
      io.rs2 := instr_decoder_rvc.io.rs2
      io.rd := instr_decoder_rvc.io.rd
      io.opcode := instr_decoder_rvc.io.opcode
      io.funct3 := instr_decoder_rvc.io.funct3
      io.funct7 := instr_decoder_rvc.io.funct7
      io.funct12 := instr_decoder_rvc.io.funct12
      io.instr := instr_decoder_rvc.io.instr
    }
  } else {
      io.instr_type := instr_type
      io.rs1 := instr_decoder.io.rs1
      io.rs2 := instr_decoder.io.rs2
      io.rd := instr_decoder.io.rd
      io.opcode := instr_decoder.io.opcode
      io.funct3 := instr_decoder.io.funct3
      io.funct7 := instr_decoder.io.funct7
      io.funct12 := instr_decoder.io.funct12
      io.instr := instr_decoder.io.instr

  }

  // Initialize extensions with dummy values if not present
  io.f_ext.rs3 := 0.U

  //F
  io.f_ext.rs3 := instr_decoder.io.rs3

  // immediate output mux
  when (instr_type === InstrType.I) {
    io.imm := imm_gen.io.imm_i
  } .elsewhen (instr_type === InstrType.S) {
    io.imm := imm_gen.io.imm_s
  } .elsewhen (instr_type === InstrType.B) {
    io.imm := imm_gen.io.imm_b
  } .elsewhen (instr_type === InstrType.U) {
    io.imm := imm_gen.io.imm_u
  } .elsewhen (instr_type === InstrType.J) {
    io.imm := imm_gen.io.imm_j
  } .otherwise {
    // set immediate to 0 on invalid instr_type
    io.imm := 0.U
  }
  if (config.extension_set.contains(ISAExtension.C)) {
    when (instr_type === InstrType.CI) {
      io.imm := imm_gen.io.imm_ci
    } .elsewhen (instr_type === InstrType.CSS) {
      io.imm := imm_gen.io.imm_css
    } .elsewhen (instr_type === InstrType.CIW) {
      io.imm := imm_gen.io.imm_ciw
    } .elsewhen (instr_type === InstrType.CI16) {
      io.imm := imm_gen.io.imm_ci16
    } .elsewhen (instr_type === InstrType.CIP) {
      io.imm := imm_gen.io.imm_cip
    } .elsewhen (instr_type === InstrType.CL) {
      io.imm := imm_gen.io.imm_cl
    } .elsewhen (instr_type === InstrType.CS) {
      io.imm := imm_gen.io.imm_cs
    } .elsewhen (instr_type === InstrType.CB) {
      io.imm := imm_gen.io.imm_cb
    } .elsewhen (instr_type === InstrType.CJ) {
      io.imm := imm_gen.io.imm_cj
    }
  }
}

/** Generates verilog */
object DecoderBlock extends App {
  val annos = Seq(ChiselGeneratorAnnotation(() => new DecoderBlock(
    config=ACoreConfig.default
  )))
  (new ChiselStage).execute(args, annos)
}
