// SPDX-License-Identifier: Apache-2.0

// Chisel module ImmGen
// Inititally written by Verneri Hirvonen (verneri.hirvonen@aalto.fi), 2021-03-28

package acorebase

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.iotesters.PeekPokeTester
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import a_core_common._


/** IO definitions for ImmGen
  * @param XLEN XLEN from base integer ISA
  */
class ImmGenIO(XLEN: Int = 32) extends Bundle {
  val instr_bits  = Input(UInt(XLEN.W))
  val imm_i = Output(UInt(XLEN.W))
  val imm_s = Output(UInt(XLEN.W))
  val imm_b = Output(UInt(XLEN.W))
  val imm_u = Output(UInt(XLEN.W))
  val imm_j = Output(UInt(XLEN.W))
  val imm_ci = Output(UInt(XLEN.W))
  val imm_css = Output(UInt(XLEN.W))
  val imm_ciw = Output(UInt(XLEN.W))
  val imm_ci16 = Output(UInt(XLEN.W))
  val imm_cip = Output(UInt(XLEN.W))
  val imm_cl = Output(UInt(XLEN.W))
  val imm_cs = Output(UInt(XLEN.W))
  val imm_cb = Output(UInt(XLEN.W))
  val imm_cj = Output(UInt(XLEN.W))
}

/** Generator for Immediate Generator module
  * @param config ACoreConfig configuration class
  */ 
class ImmGen(config: ACoreConfig) extends Module {
  val io = IO(new ImmGenIO(XLEN=config.data_width))

  // extract immediate field using bundle, sign extend, cast to uint
  val i = io.instr_bits.asTypeOf(new InstrTypeI)
  io.imm_i := i.imm_11_0.asSInt.pad(32).asUInt

  // concatenate immediate segments, sign extend, cast to uint
  val s = io.instr_bits.asTypeOf(new InstrTypeS)
  io.imm_s := Cat(s.imm_11_5, s.imm_4_0).asSInt.pad(32).asUInt

  // concatenate immediate segments, sign extend
  val b = io.instr_bits.asTypeOf(new InstrTypeB)
  io.imm_b := Cat(b.imm_12, b.imm_11, b.imm_10_5, b.imm_4_1, false.B).asSInt.pad(32).asUInt

  val u = io.instr_bits.asTypeOf(new InstrTypeU)
  io.imm_u := Cat(u.imm_31_12, 0.U(12.W))
  
  val j = io.instr_bits.asTypeOf(new InstrTypeJ)
  io.imm_j := Cat(j.imm_20, j.imm_19_12, j.imm_11, j.imm_10_1, false.B).asSInt.pad(32).asUInt

  // initialize RVC types to 0.U if c-extension not enabled

  io.imm_ci := 0.U
  io.imm_ci16 := 0.U 
  io.imm_cip := 0.U 
  io.imm_css := 0.U 
  io.imm_ciw := 0.U 
  io.imm_cl := 0.U 
  io.imm_cs := 0.U 
  io.imm_cb := 0.U 
  io.imm_cj := 0.U  

  if (config.extension_set.contains(ISAExtension.C)) {

    val ci = io.instr_bits.asTypeOf(new InstrTypeCI)
    when (ci.funct3 === "b011".U) {
      io.imm_ci := Cat(ci.imm_5, ci.imm_4_0, 0.U(12.W)).asSInt.pad(32).asUInt
    }. otherwise {
      io.imm_ci := Cat(ci.imm_5, ci.imm_4_0).asSInt.pad(32).asUInt
    }

    val ci16 = io.instr_bits.asTypeOf(new InstrTypeCI16)
    io.imm_ci16 := Cat(ci16.imm_9, ci16.imm_8_7, ci16.imm_6, ci16.imm_5, ci16.imm_4, 0.U(4.W)).asSInt.pad(32).asUInt

    val cip = io.instr_bits.asTypeOf((new InstrTypeCIP))
    io.imm_cip := Cat(0.U(24.W), cip.imm_7_6, cip.imm_5, cip.imm_4_2, 0.U(2.W))

    val css = io.instr_bits.asTypeOf(new InstrTypeCSS)
    io.imm_css := Cat(0.U(24.W), css.imm_7_6, css.imm_5_2, 0.U(2.W))

    val ciw = io.instr_bits.asTypeOf(new InstrTypeCIW)
    io.imm_ciw := Cat(0.U(22.W), ciw.imm_9_6, ciw.imm_5_4, ciw.imm_3, ciw.imm_2, 0.U(2.W))

    val cl = io.instr_bits.asTypeOf(new InstrTypeCL)
    io.imm_cl := Cat(0.U(25.W), cl.imm_4, cl.imm_3_1, cl.imm_0, 0.U(2.W))

    val cs = io.instr_bits.asTypeOf(new InstrTypeCS)
    io.imm_cs := Cat(0.U(25.W), cl.imm_4, cl.imm_3_1, cl.imm_0, 0.U(2.W))

    val cb = io.instr_bits.asTypeOf(new InstrTypeCB)
    io.imm_cb := Cat(cb.imm_8, cb.imm_7_6, cb.imm_5, cb.imm_4_3, cb.imm_2_1, false.B).asSInt.pad(32).asUInt

    val cj = io.instr_bits.asTypeOf(new InstrTypeCJ)
    io.imm_cj := Cat(cj.imm_11, cj.imm_10, cj.imm_9_8, cj.imm_7, cj.imm_6, cj.imm_5, cj.imm_4, cj.imm_3_1, false.B).asSInt.pad(32).asUInt
  }
}

/** Generates verilog */
object ImmGen extends App {
  val annos = Seq(ChiselGeneratorAnnotation(() => new ImmGen(
    config=ACoreConfig.default
  )))
  (new ChiselStage).execute(args, annos)
}

