
package acorebase

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.iotesters.PeekPokeTester
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import a_core_common._


/** IO definitions for RVCDecoder */
class InstrDecoderRVCIO(XLEN: Int = 32) extends Bundle {
  val instr_bits = Input(UInt(XLEN.W))
  val instr_type = Output(InstrType())
  val rs1 = Output(UInt(5.W))
  val rs2 = Output(UInt(5.W))
  val rd = Output(UInt(5.W))
  val opcode = Output(Opcode())
  val funct3 = Output(UInt(3.W))
  val funct7 = Output(UInt(7.W))
  val funct12 = Output(UInt(12.W))
  val instr = Output(Instruction())
  //F-extension
  val rs3 = Output(UInt(5.W))
}


//RVC-decoder maps 16-bit compressed instructions to their respective 32-bit instructions

class InstrDecoderRVC(config: ACoreConfig) extends Module {
  val io = IO(new InstrDecoderRVCIO(XLEN=config.data_width))

  val opcode = Wire(UInt(2.W)) 
  val funct2 = Wire(UInt(2.W))
  val funct3 = Wire(UInt(3.W))
  val funct4 = Wire(UInt(4.W))
  val funct6 = Wire(UInt(6.W))
  val funct8 = Wire(UInt(8.W))
  val rs1_5 = Wire(UInt(5.W))
  val rs2_5 = Wire(UInt(5.W))
  val rs1 = Wire(UInt(3.W))
  val rs2 = Wire(UInt(3.W))

  opcode := io.instr_bits(1, 0)
  funct2 := io.instr_bits(11, 10)
  funct3 := io.instr_bits(15, 13)
  funct4 := io.instr_bits(15, 12)
  funct6 := io.instr_bits(15, 10)
  funct8 := Cat(io.instr_bits(15, 10), io.instr_bits(6, 5))
  rs1_5 := io.instr_bits(11, 7)
  rs2_5 := io.instr_bits(6, 2)
  rs1 := io.instr_bits(9, 7)
  rs2 := io.instr_bits(4, 2)

  io.instr := Instruction.INVALID //Initialize io values
  io.instr_type := InstrType.INVALID
  io.funct7 := 0.U
  io.funct12 := 0.U
  io.opcode := Opcode.OP_IMM
  io.funct3 := 0.U
  io.rs1 := 0.U
  io.rs2 := 0.U
  io.rd := 0.U
  io.rs3 := 0.U

  when (VecInit("h0".U(16.W), "hff".U(16.W)).contains(io.instr_bits(15,0))) {
    // All zeros and all ones are illegal instructions
    io.opcode := Opcode.OP_IMM
    io.funct3 := 0.U
    io.rd := 0.U
    io.rs1 := 0.U
    io.instr_type := InstrType.INVALID
    io.instr := Instruction.INVALID
  } .elsewhen (opcode === 0.U) {
    when (funct3 === "b010".U) { //c.lw  
      io.opcode := Opcode.LOAD
      io.funct3 := funct3
      io.rd := rs2 + 8.U  //rs1' valid registers are x8-x15 but for example x8 is represented as "b000" in field rs1'
      io.rs1 := rs1 + 8.U
      io.instr_type := InstrType.CL
      io.instr := Instruction.LW
    }. elsewhen (funct3 === "b110".U) { //c.sw
      io.opcode := Opcode.STORE
      io.funct3 := "b010".U
      io.rs1 := rs1 + 8.U
      io.rs2 := rs2 + 8.U
      io.instr_type := InstrType.CS
      io.instr := Instruction.SW
    }. elsewhen (funct3 === "b000".U) { //c.addi4spn
      io.opcode := Opcode.OP_IMM
      io.rd := rs2 + 8.U
      io.rs1 := 2.U
      io.instr_type := InstrType.CIW
      io.instr := Instruction.ADDI
    }. elsewhen (funct3 === "b011".U) { //c.flw
      if (config.extension_set.contains(ISAExtension.F)) {
        io.opcode := Opcode.LOAD_FP
        io.funct3 := 2.U
        io.rd := rs2 + 8.U
        io.rs1 := rs1 +8.U
        io.instr_type := InstrType.CL
        io.instr := Instruction.FLW
      }
    }. elsewhen (funct3 === "b111".U) { //c.fsw  
      if (config.extension_set.contains(ISAExtension.F)) {
        io.opcode := Opcode.STORE_FP
        io.funct3 := 2.U
        io.rs2 := rs2 + 8.U
        io.rs1 := rs1 +8.U
        io.instr_type := InstrType.CS
        io.instr := Instruction.FSW
      }
    }
  }. elsewhen (opcode === 1.U) {
    when (funct3 === "b010".U) { //c.li
      io.opcode := Opcode.OP_IMM
      io.rd := rs1_5
      io.instr_type := InstrType.CI
      io.instr := Instruction.ADDI
    }. elsewhen (funct3 === "b000".U) { //c.addi
      io.opcode := Opcode.OP_IMM
      io.rd := rs1_5
      io.rs1 := rs1_5
      io.instr_type := InstrType.CI
      io.instr := Instruction.ADDI
    }. elsewhen (funct3 === "b011".U) { 
      when (rs1_5 === "b00010".U) { // c.addi16sp
        io.opcode := Opcode.OP_IMM
        io.rd := rs1_5
        io.rs1 := rs1_5
        io.instr_type := InstrType.CI16
        io.instr := Instruction.ADDI
      }. otherwise { //c.lui
        io.opcode := Opcode.LUI
        io.rd := rs1_5
        io.instr_type := InstrType.CI
        io.instr := Instruction.LUI
      }
    }. elsewhen (funct3 === "b101".U) { //c.j
      io.opcode := Opcode.JAL
      io.instr_type := InstrType.CJ
      io.instr := Instruction.JAL
    }. elsewhen (funct3 === "b001".U) { //c.jal
      io.opcode := Opcode.JAL
      io.rd := 1.U 
      io.instr_type := InstrType.CJ
      io.instr := Instruction.JAL
    }. elsewhen(funct3 === "b110".U) { //c.beqz
      io.opcode := Opcode.BRANCH
      io.rs1 := rs1 + 8.U
      io.instr_type := InstrType.CB
      io.instr := Instruction.BEQ
    }. elsewhen(funct3 === "b111".U) { //c.bnez
      io.opcode := Opcode.BRANCH
      io.funct3 := 1.U
      io.rs1 := rs1 + 8.U
      io.instr_type := InstrType.CB
      io.instr := Instruction.BNE
    }. elsewhen (funct3 === "b100".U) {
      when (funct2 === "b10".U) { //c.andi
        io.funct3 := 7.U
        io.rd := rs1 + 8.U
        io.rs1 := rs1 + 8.U
        io.instr_type := InstrType.CI
        io.instr := Instruction.ANDI
      }. elsewhen(funct2 === "b00".U) { //c.srl1
        io.funct3 := 5.U
        io.rd := rs1 + 8.U
        io.rs1 := rs1 + 8.U
        io.instr_type := InstrType.CI
        io.instr := Instruction.SRLI
      }. elsewhen(funct2 === "b01".U) { //c.srai
        io.funct7 := "b0100000".U
        io.funct3 := 5.U
        io.rd := rs1 + 8.U
        io.rs1 := rs1 + 8.U
        io.instr_type := InstrType.CI
        io.instr := Instruction.SRLI
      }.elsewhen (funct8 === "b10001111".U) { //c.and
        io.opcode := Opcode.OP
        io.funct3 := 7.U
        io.rd := rs1 + 8.U
        io.rs1 := rs1 + 8.U
        io.rs2 := rs2 + 8.U
        io.instr_type := InstrType.CA
        io.instr := Instruction.AND
      }. elsewhen(funct8 === "b10001110".U) { //c.or
        io.opcode := Opcode.OP
        io.funct3 := 6.U
        io.rd := rs1 + 8.U
        io.rs1 := rs1 + 8.U
        io.rs2 := rs2 + 8.U
        io.instr_type := InstrType.CA
        io.instr := Instruction.OR
      }. elsewhen(funct8 === "b10001101".U) { //c.xor
        io.opcode := Opcode.OP
        io.funct3 := 4.U
        io.rd := rs1 + 8.U
        io.rs1 := rs1 + 8.U
        io.rs2 := rs2 + 8.U
        io.instr_type := InstrType.CA
        io.instr := Instruction.XOR
      }. elsewhen(funct8 === "b10001100".U) { //c.sub
        io.opcode := Opcode.OP
        io.funct7 := "b0100000".U
        io.rd := rs1 + 8.U
        io.rs1 := rs1 + 8.U
        io.rs2 := rs2 + 8.U
        io.instr_type := InstrType.CA
        io.instr := Instruction.SUB
      }
    }
  }. elsewhen (opcode === 2.U) { 
    when (funct3 === "b110".U) { //c.swsp
      io.opcode := Opcode.STORE
      io.funct3 := "b010".U
      io.rs1 := 2.U
      io.rs2 := rs2_5
      io.instr_type := InstrType.CSS
      io.instr := Instruction.SW   
    }. elsewhen (funct3 === "b111".U) { //c.fswsp 
      if (config.extension_set.contains(ISAExtension.F)) {
        io.opcode := Opcode.STORE_FP 
        io.funct3 := 2.U
        io.rs1 := 2.U
        io.rs2 := rs2_5
        io.instr_type := InstrType.CSS
        io.instr := Instruction.FLW
      }
    }. elsewhen(funct4 === "b1000".U) { 
      when (rs2_5 === 0.U) { //c.jr
        io.opcode := Opcode.JALR
        io.rs1 := rs1_5
        io.instr_type := InstrType.CI
        io.instr := Instruction.JALR
      } .otherwise { //c.mv
        io.opcode := Opcode.OP
        io.rd := rs1_5
        io.rs2 := rs2_5
        io.instr_type := InstrType.CR
        io.instr := Instruction.ADD
      }
    }. elsewhen (funct4 === "b1001".U && rs2_5 === 0.U) { //c.jalr 
      io.opcode := Opcode.JALR
      io.rd := 1.U
      io.rs1 := rs1_5
      io.instr_type := InstrType.CR
      io.instr := Instruction.JALR
    }. elsewhen (funct4 === "b1001".U) { //c.add
      io.opcode := Opcode.OP
      io.rd := rs1_5
      io.rs1 := rs1_5
      io.rs2 := rs2_5
      io.instr_type := InstrType.CR
      io.instr := Instruction.ADD
    }. elsewhen(funct3 === "b000".U) { //c.slli
      io.funct3 := 1.U
      io.rd := rs1_5
      io.rs1 := rs1_5
      io.instr_type := InstrType.CI
      io.instr := Instruction.SLLI
    }. elsewhen(funct3 === "b010".U) { //c.lwsp
      io.opcode := Opcode.LOAD
      io.funct3 := funct3
      io.rd := rs1_5
      io.rs1 := 2.U
      io.instr_type := InstrType.CIP
      io.instr := Instruction.LW
    }. elsewhen (funct3 === "b011".U) { //c.flwsp 
      if (config.extension_set.contains(ISAExtension.F)) {
        io.opcode := Opcode.LOAD_FP
        io.funct3 := 2.U 
        io.rd := rs1_5
        io.rs1 := 2.U
        io.instr_type := InstrType.CIP
        io.instr := Instruction.FLW
      }
    }
  }
} 



/** Generates verilog */
object InstrDecoderRVC extends App {
  val annos = Seq(ChiselGeneratorAnnotation(() => new InstrDecoderRVC(
    config=ACoreConfig.default
  )))
  (new ChiselStage).execute(args, annos)
}
