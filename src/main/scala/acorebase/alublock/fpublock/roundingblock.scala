// SPDX-License-Identifier: Apache-2.0

// Chisel module RoundingBlock
// Module for Single-precision floating point rounding
package acorebase

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.experimental.BundleLiterals._
import a_core_common._
import dsptools.numbers._

/** IO definitions for ArithmeticBlock */
class RoundingBlockIO(XLEN: Int) extends Bundle {
  val data = new Bundle {
    val in         = Input(UInt(40.W))         // float input
    val fflags_in  = Input(UInt(5.W))          // fflags input
    val out        = Output(UInt(XLEN.W))      // output
    val fflags_out = Output(UInt(5.W))         // fflags output
  }
  val control = new Bundle {
    val roundmode  = Input(RoundingMode())     // rounding operation
    val roundtype  = Input(RoundingType())     // are we rounding int or float or pass
  }
}

/* Module definition for ArithmeticBlock */
class RoundingBlock(XLEN: Int = 32) extends Module {
  val io = IO(new RoundingBlockIO(XLEN=XLEN))

  var ff = WireDefault(VecInit(Seq.fill(5)(0.U(1.W))))

  /**
    * Rounds a 32-bit integer according to RS bits
    *
    * @param sign Sign 
    * @param intVal 31-bit signed or 32-bit unsigned integer
    * @param RS Round and Sticky bits
    * @param isBoolean Whether the number is signed
    * @return Rounded 32-bit integer
    */
  def roundInt(sign: UInt, intVal: UInt, RS: UInt, isSigned: Boolean) : UInt = {
    var roundedInt = WireDefault(0.U(32.W))

    switch(io.control.roundmode) {
      is(RoundingMode.RNE) {
        // Round to Nearest, ties to Even
        when (((RS === "b10".U) || (RS === "b11".U)) && (intVal(0) === 1.U)) {
          roundedInt := intVal + 1.U
        } .otherwise {
          roundedInt := intVal
        }
      }
      is(RoundingMode.RTZ) {
        // Round towards zero
        roundedInt := intVal
      }
      is(RoundingMode.RDN) {
        // Round towards -Inf
        when ((sign === 1.U) && (RS =/= "b00".U)) {
          roundedInt := intVal + 1.U
        } .otherwise {
          roundedInt := intVal
        }
      }
      is(RoundingMode.RUP) {
        // Round towards +Inf
        when ((sign === 0.U && RS =/= "b00".U)) {
          roundedInt := intVal + 1.U
        } .otherwise {
          roundedInt := intVal
        }
      }
      is(RoundingMode.RMM) {
        // Round to Nearest, ties to Max Magnitude
        when ((RS === "b10".U) && ((intVal(0) === 0.U) || (sign === 1.U)) || RS(1) === 0.U ) {
          roundedInt := intVal
        } .otherwise {
          roundedInt := intVal
        }
      }
    }

    if (isSigned)
      Cat(sign, roundedInt(30,0))
    else 
      roundedInt
  }

  /**
    * Rounding using GRS bits, returns combined value
    *
    * @param sign Sign 
    * @param exp Exponent
    * @param mantissa 26 hidden bit, 25-3 mantissa, 2-0 GRS
    * @return Rounded 32-bit float
    */
  def round(sign: UInt, exp: UInt, mantissa: UInt) : UInt = {
    val signBit = WireDefault(sign)
    val exponent = WireDefault(exp)
    val roundedMant = WireDefault(mantissa(27, 3))
    val GRS = WireDefault(mantissa(2, 0))

    switch (io.control.roundmode) {
      is (RoundingMode.RNE) {
        //Round to Nearest, ties to Even
        when ((GRS === "b100".U && mantissa(3) === 0.U) || GRS(2) === 0.U) {
          // round down
          roundedMant := mantissa(27, 3)
        } .otherwise {
          // round up
          roundedMant := mantissa(27, 3) + 1.U

          when (roundedMant(24) === 1.U && exp === "hfe".U) {
            ff(FaultFlag.OF.asUInt()) := 1.U
          }

          // overflow detection
          when (roundedMant(24) === 1.U && exp =/= "hff".U) {
            exponent := exp + 1.U
          } .elsewhen (exp === 0.U && roundedMant(23) === 1.U) {
            exponent := exp + 1.U
          }
        }
      }
      is (RoundingMode.RTZ) {
        //Round towards zero
        roundedMant := mantissa(27, 3)
      }
      is (RoundingMode.RDN) {
        //Round Down, towards -inf
        when (sign === "b1".U && GRS =/= "b000".U) {
          roundedMant := mantissa(27, 3) + 1.U
        } .otherwise {
          roundedMant := mantissa(27, 3)
        }
      }
      is (RoundingMode.RUP) {
        //Round Up, towards +inf
        when (sign === "b0".U && GRS =/= "b000".U) {
          roundedMant := mantissa(27, 3) + 1.U
        } .otherwise {
          roundedMant := mantissa(27, 3)
        }
      }
      is (RoundingMode.RMM) {
        //Round to Nearest, ties to Max Magnitude
        when ((GRS === "b100".U && (mantissa(3) === 0.U || sign === "b1".U)) || GRS(2) === 0.U) {
          // round down
          roundedMant := mantissa(27, 3)
        } .otherwise {
          // round up
          roundedMant := mantissa(27, 3) + 1.U

          // overflow detection
          when (roundedMant(24) === 1.U && exp =/= "b11111111".U) {
            exponent := exp + 1.U
          }
        }
      }
    }
    Cat(signBit, exponent, roundedMant(22, 0))
  }

  //Actual hardware
  when (io.control.roundtype === RoundingType.FLOAT) {
    io.data.out := round(io.data.in(36), io.data.in(35, 28), io.data.in(27, 0))

    // If no other faults and result inexact after rounding raise flag
    when (io.data.in(2, 0) =/= 0.U) {
      ff(FaultFlag.NX.asUInt()) := 1.U
      io.data.fflags_out := io.data.fflags_in | ff.asUInt()
    } .otherwise {
      io.data.fflags_out := io.data.fflags_in
    }

  } .elsewhen(io.control.roundtype === RoundingType.SINT) {
    io.data.out := roundInt(io.data.in(33), io.data.in(32, 2), io.data.in(1, 0), true)
    io.data.fflags_out := io.data.fflags_in

  } .elsewhen(io.control.roundtype === RoundingType.UINT) {
    io.data.out := roundInt(io.data.in(34), io.data.in(33, 2), io.data.in(1, 0), false)
    io.data.fflags_out := io.data.fflags_in

  } .otherwise { 
    //Pass through special cases
    io.data.out := io.data.in(31,0)
    io.data.fflags_out := io.data.fflags_in
 
  }
}
