// SPDX-License-Identifier: Apache-2.0

// Chisel module ArithmeticBlock
// Module for Single-precision floating point arithmetic operations
package acorebase

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.experimental.BundleLiterals._
import a_core_common._
import dsptools.numbers._

/** IO definitions for ArithmeticBlock */
class ArithmeticBlockIO(XLEN: Int) extends Bundle {
  val data = new Bundle {
    val a = Input(UInt(XLEN.W))             // operand A
    val b = Input(UInt(XLEN.W))             // operand B
    val c = Input(UInt(XLEN.W))             // operand C
    val out = Output(UInt(40.W))            // computation result
    val fflags = Output(UInt(5.W))          // data for fcsr ff
  }
  val control = new Bundle {
    val op = Input(FPUOp())                // FPU operation
    val roundtype = Output(RoundingType())  // computation result
  }
}

/* Module definition for ArithmeticBlock */
class ArithmeticBlock(XLEN: Int = 32) extends Module {
  val io = IO(new ArithmeticBlockIO(XLEN=XLEN))
  
  /*
  val nNN = BitPat("b10000000????????????????????????")   //0x
  val pNN = BitPat("b00000000????????????????????????")   //0x 
  val nSN = BitPat("b100000000???????????????????????")   //0x
  val pSN = BitPat("b000000000???????????????????????")   //0x
  val nZero = BitPat("b10000000000000000000000000000000") //0x80000000
  val pZero = BitPat("b00000000000000000000000000000000") //0x00000000 
  val nInf = BitPat("b11111111100000000000000000000000")  //0xff800000
  val pInf = BitPat("b01111111100000000000000000000000")  //0x7f800000
  val sNaN = BitPat("b?111111110??????????????????????")  //0x7fa00000
  val qNaN = BitPat("b?111111111??????????????????????")  //0x7fe00000

  # qNaN 	0x7fe00000
  # sNaN 	0x7fa00000
  # cNaN  0x7fc00000
  */
  
  io.data.fflags := 0.U(5.W) // "b00000"
  io.control.roundtype := RoundingType.FLOAT

  var ff = WireDefault(VecInit(Seq.fill(5)(0.U(1.W))))
  ff := Seq.fill(5)(0.U(1.W))

  io.data.out := 0.U(5.W) // "b00000"

  def add(operA: UInt, operB: UInt, opSign: UInt) : UInt = {
    val tailBits = 26
    val bodyBits = 26
    val signIdx = tailBits+bodyBits-1
    val ovflIdx = tailBits+bodyBits-2
    val hidnIdx = tailBits+bodyBits-3
    val grdbIdx = tailBits-1
    val rndbIdx = tailBits-2
    val stkbIdx = tailBits-3
    val tailStartIdx = tailBits-1
    val mantStartIdx = hidnIdx-1

    val isSubNormalA = {operA(30,23) === 0.U}
    val isSubNormalB = {operB(30,23) === 0.U}
    val isMantZeroA  = {operA(22,0)  === 0.U}
    val isMantZeroB  = {operB(22,0)  === 0.U}

    val signA = operA(31)
    // If exponent is 0 (subnormal number), replace it with 1
    val expA = Mux(isSubNormalA && !isMantZeroA, 1.U(8.W), operA(30, 23))
    // add sign, overflow, hidden bit (if not a subnormal number) and tail
    val fractionA = Cat(0.U(2.W), Mux(isSubNormalA, 0.U, 1.U(1.W)), operA(22, 0), 0.U(tailBits.W))

    val signB = operB(31)
    // If exponent is 0 (subnormal number), replace it with 1
    val expB = Mux(isSubNormalB && !isMantZeroB, 1.U(8.W), operB(30, 23))
    // add sign, overflow, hidden bit (if not a subnormal number) and tail
    val fractionB = Cat(0.U(2.W), Mux(isSubNormalB, 0.U, 1.U(1.W)), operB(22, 0), 0.U(tailBits.W))  

    val tempExpA = WireDefault(expA)
    val tempFractA = WireDefault(fractionA)
    val tempExpB = WireDefault(expB)
    val tempFractB = WireDefault(fractionB)
    val resultSign = WireDefault(0.U(1.W))
    val result = WireDefault(0.U(40.W))
    val shiftOutData = WireDefault(0.U(tailBits.W))
    val stickyBit = WireDefault(0.U(1.W))

    val exA = operA(30,23)
    val exB = operB(30,23)
    val mantA = operA(22,0)
    val mantB = operB(22,0)
    /*
    1. Adjust exponents and align mantissa
	      if expA < expB, shift mantissaA to form mantA * 2^(expA-expB)
        if expA > expB, shift mantissaA to form mantA * 2^(expB-expA)
    2. Add or subtract mantissa
    3. Normalise the result (shift left or right)
    4. check if overflow/underflow
    5. check if zero
    */

     when ((exA === "hff".U) && (mantA > 0.U) && (exB === "hff".U) && (mantB > 0.U)) {  // Check operA and operB
      when(!operA(22) || !operB(22)){
        //sNaN
        ff(FaultFlag.NV.asUInt()) := 1.U // Not valid 
      }
      //qNaN
      io.control.roundtype := RoundingType.SPEC
      result := Cat(operA(31,23), 1.U, operA(21,0))
    }.elsewhen ((exA === "hff".U) && (mantA > 0.U) && ~((exB === "hff".U) && (mantB > 0.U))) {  // Check operA
      when(!operA(22)){
        //sNaN
        ff(FaultFlag.NV.asUInt()) := 1.U // Not valid 
      }
      //qNaN
      io.control.roundtype := RoundingType.SPEC
      result := Cat(operA(31,23), 1.U, operA(21,0))
    } .elsewhen (~((exA === "hff".U) && (mantA > 0.U)) && (exB === "hff".U) && (mantB > 0.U)) { // Check operB
      when(!operB(22)){
        //sNaN
        ff(FaultFlag.NV.asUInt()) := 1.U // Not valid 
      }
      //qNaN
      io.control.roundtype := RoundingType.SPEC
      result := Cat(operB(31,23), 1.U, operB(21,0))
    } .elsewhen (opSign === 0.U && (((exA === "hff".U) && (mantA === 0.U)) || 
                ((exB === "hff".U) && (mantB === 0.U)))) {
      io.control.roundtype := RoundingType.SPEC

      when ((operA === "h7f800000".U && operB === "hff800000".U) || 
                (operA === "hff800000".U && operB === "h7f800000".U)) {
        // mixed infs with add
        ff(FaultFlag.NV.asUInt()) := 1.U // Not valid 
        result := Cat(signA ^ signB, "hff".U, "h400000".U)
      } .elsewhen ((exA === "hff".U) && (mantA === 0.U)) {
        // A infs with add
        result := operA
      } .elsewhen ((exB === "hff".U) && (mantB === 0.U)) {
        // B infs with add
        result := operB
      }
    } .elsewhen (opSign === 1.U && (((exA === "hff".U) && (mantA === 0.U)) || 
                ((exB === "hff".U) && (mantB === 0.U)))) {
      io.control.roundtype := RoundingType.SPEC

      when ((operA === "hff800000".U && operB === "hff800000".U) || 
                (operA === "h7f800000".U && operB === "h7f800000".U)) {
        // same infs with sub
        ff(FaultFlag.NV.asUInt()) := 1.U // Not valid 
        result := "h7fc00000".U
      } .elsewhen ((exA === "hff".U) && (mantA === 0.U)) {
        // A infs with sub
        result := operA
      } .elsewhen ((exB === "hff".U) && (mantB === 0.U)) {
        // B infs with sub
        result := Cat(1.U ^ operB(31), operB(30,0))
      }
    } .elsewhen (opSign === 0.U && ((exA === 0.U && mantA === 0.U) || (exB === 0.U && mantB === 0.U))){
      //zeros with add
      io.control.roundtype := RoundingType.SPEC
      when ((exA === 0.U && mantA === 0.U) && (exB === 0.U && mantB === 0.U)){
        when (operA === "h80000000".U && operB === "h80000000".U){
          // negative zeros
          result := "h80000000".U
        } .otherwise {
          result := "h00000000".U
        }
      } .otherwise {
        when (operA === "h00000000".U || operA === "h80000000".U){
          // negative zero & nmbr
          result := operB
        } .elsewhen (operB === "h00000000".U || operB === "h80000000".U){
          // positive zero & nmbr
          result := operA
        }
      }
    } .elsewhen (opSign === 1.U && ((exA === 0.U && mantA === 0.U) || (exB === 0.U && mantB === 0.U))){
      //zeros with sub
      io.control.roundtype := RoundingType.SPEC
      when (exA === 0.U && mantA === 0.U && exB === 0.U && mantB === 0.U){
        when (operA === "h80000000".U && operB === "h00000000".U){
          // first negative second positive zero
          result := "h80000000".U
        } .otherwise {
          result := "h00000000".U
        }
      } .otherwise {
          when (operA === "h80000000".U || operA === "h00000000".U){
	          // negative zero & nmbr
            result := Cat((1.U ^ signB), operB(30,0))
          } .elsewhen (operB === "h80000000".U || operB === "h00000000".U){
	          // negative zero & nmbr
            result := operA
          }
        }
    } .otherwise {
      // equalizing operand exponents
      val absDiff = WireDefault(0.U(10.W))

      when (expA < expB) {
        absDiff := expB - expA
        tempFractA := fractionA >> absDiff
        tempExpA := expA + absDiff

        when (fractionA > 0.U) {
          when (absDiff <= tailBits) {
            shiftOutData := fractionA(stkbIdx, 0)
          } .otherwise {
            shiftOutData := 1.U  
          }
        } .otherwise {
          shiftOutData := 0.U  
        }
      } .elsewhen (expB < expA) {
        absDiff := expA - expB
        tempFractB := fractionB >> absDiff
        tempExpB := expB + absDiff

        when (fractionB > 0.U) {
          when (absDiff <= tailBits) {
            shiftOutData := fractionB(stkbIdx, 0)
          } .otherwise {
            shiftOutData := 1.U  
          }
        } .otherwise {
          shiftOutData := 0.U  
        }
      }

      //if shifted out any data the result is inexact
      when (shiftOutData.orR) {
        ff(FaultFlag.NX.asUInt()) := 1.U // Inexact
      }
      
      val finalFractA = WireDefault(tempFractA)
      val finalFractB = WireDefault(tempFractB)

      // if negative operands, convert fraction to 2's complement
      when (signA === 1.U) {
        finalFractA := ~tempFractA + 1.U        
      }
      when (signB === ~opSign) {
        finalFractB := ~tempFractB + 1.U
      }

      val addFraction = Wire(UInt((bodyBits+tailBits).W))
      val tempFract = Wire(UInt((bodyBits+tailBits).W))
      addFraction := finalFractA + finalFractB
      tempFract := addFraction
      
      // change of sign if result is negative
      // and convert back to sign-magnitude format
      when (addFraction(signIdx) === 1.U) {
        resultSign := 1.U
        tempFract := ~addFraction + 1.U
      }
      val finalFraction = Wire(UInt((bodyBits+tailBits).W))
      finalFraction := tempFract

      // exponents should be the same value at this point
      val tempExp = Cat("b0".U, tempExpA)   // add 9th bit for overflow
      val finalExp = Wire(UInt(8.W))
      finalExp := tempExpA
      
      when (tempFract > 0.U) { // shifting not required when fraction zero
        // check where the most significant active bit is
        val activeMSB = Wire(UInt(8.W))
        activeMSB := PriorityEncoder(Reverse(tempFract))
        // normalization
        when (isSubNormalA && isSubNormalB) {
          when (tempFract(hidnIdx) === "b1".U) {
            finalExp := tempExpA     
            finalFraction := tempFract
          } .otherwise {
            finalExp := 0.U
            finalFraction := tempFract
          }
        } .elsewhen (tempFract(ovflIdx) === "b1".U) {
          //shift to right
          val overFlowExp = tempExp + 1.U
          
          // overflow detection, flag raised
          when (overFlowExp(8) === "b1".U || overFlowExp === "b011111111".U) {
            finalExp := "b11111111".U
            finalFraction := 0.U
            ff(FaultFlag.OF.asUInt()) := 1.U // Overflow
            ff(FaultFlag.NX.asUInt()) := 1.U // Inexact
          } .otherwise {
            finalExp := tempExpA + 1.U      
            finalFraction := tempFract >> 1
          }
        } .otherwise {
          // shift to left
          val shiftedBits = hidnIdx - (bodyBits + tailBits - activeMSB - 1)
          
          when (tempExpA > shiftedBits) {
            finalFraction := tempFract << shiftedBits
            finalExp := tempExpA - shiftedBits
          } .otherwise {
            when (tempFract =/= 0.U) {
              finalExp := 0.U
              when (operA(30, 23) < operB(30, 23)) {
                finalFraction := tempFract << operA(30, 23)
              } .elsewhen (operB(30, 23) < operA(30, 23)) {
                finalFraction := tempFract << operB(30, 23)
              } .otherwise {
                finalFraction := tempFract << (operA(30, 23) - ~isSubNormalA)
              }
            } .otherwise {
              // underflow
              finalFraction := 0.U
              finalExp := 0.U
              ff(FaultFlag.UF.asUInt()) := 1.U // Underflow
              ff(FaultFlag.NX.asUInt()) := 1.U // Inexact
            }
          }
        }
      }

      stickyBit := (finalFraction(stkbIdx, 0).orR)

      val finalFract = WireDefault(0.U(27.W))
      finalFract := Cat(finalFraction(mantStartIdx + 1, rndbIdx), stickyBit)

      when ((opSign === 0.U) && (signA === 1.U ^ signB === 1.U) && (expA =/= "hff".U) && (expA === expB) && (fractionA === fractionB)){
        result := 0.U
      } .elsewhen ((opSign === 1.U) && (signA === signB) && (expA =/= "hff".U) && (expA === expB) && (fractionA === fractionB)){
        result := 0.U
      } .otherwise {
        // round and compose result
        result := Cat(resultSign, finalExp, Cat(0.U, finalFract))
      }
    }
    result
  }

  def mul(operA: UInt, operB: UInt) : UInt = {
    // multFract: ovfl | hidn | mant (23-bit) | grdb | rndb | tail (21-bit)
    val tailBits = 23
    val bodyBits = 25
    val ovflIdx = tailBits+bodyBits-1
    val hidnIdx = tailBits+bodyBits-2
    val grdbIdx = tailBits-1
    val rndbIdx = tailBits-2
    val stkbIdx = tailBits-3
    val tailStartIdx = tailBits-1
    val mantStartIdx = hidnIdx-1

    val isSubNormalA = {operA(30,23) === 0.U}
    val isSubNormalB = {operB(30,23) === 0.U}
    val isMantZeroA  = {operA(22,0)  === 0.U}
    val isMantZeroB  = {operB(22,0)  === 0.U}


    val signA = operA(31)
    // If exponent is 0 (subnormal number), replace it with 1
    val expA = Mux(isSubNormalA && !isMantZeroA, 1.U(8.W), operA(30, 23))
    // add hidden bit (if not a subnormal number)
    val fractionA = Cat(Mux(isSubNormalA, 0.U, 1.U(1.W)), operA(22, 0))

    val signB = operB(31)
    // If exponent is 0 (subnormal number), replace it with 1
    val expB = Mux(isSubNormalB && !isMantZeroB, 1.U(8.W), operB(30, 23))
    // add hidden bit (if not a subnormal number)
    val fractionB = Cat(Mux(isSubNormalB, 0.U, 1.U(1.W)), operB(22, 0))
  
    val result = WireDefault(0.U(40.W))
    val resultSign = WireDefault(0.U(1.W)) 
    resultSign := signA ^ signB

    /*
    0. check special cases
    1. calculate the sign
    2. add biased exponents
    3. multiply mantissas
    4. normalise (check for underflow/overflow)
    5. round the result
    */

    when ((expA === "hff".U) && (operA(22, 0) > 0.U) && (expB === "hff".U) && (operB(22, 0) > 0.U)) {  
      // Check operA and operB
      when(!operA(22) || !operB(22)){
        //sNaN
        ff(FaultFlag.NV.asUInt()) := 1.U // Not valid 
      }
      //qNaN
      io.control.roundtype := RoundingType.SPEC
      result := Cat(operA(31,23), 1.U, operA(21,0))
    } .elsewhen ((expA === "hff".U) && (operA(22, 0) > 0.U)) {  
      // Check operA
      when(!operA(22)){
        //sNaN
        ff(FaultFlag.NV.asUInt()) := 1.U // Not valid 
      }
      //qNaN
      io.control.roundtype := RoundingType.SPEC
      result := Cat(operA(31,23), 1.U, operA(21,0))
    } .elsewhen ((expB === "hff".U) && (operB(22, 0) > 0.U)) { 
      // Check operB
      when(!operB(22)){
        //sNaN
        ff(FaultFlag.NV.asUInt()) := 1.U // Not valid 
      }
      //qNaN
      io.control.roundtype := RoundingType.SPEC
      result := Cat(operB(31,23), 1.U, operB(21,0))
    } .elsewhen ((operA(30, 0) === 0.U) || (operB(30,0) === 0.U)) { 
      // Check zero
      io.control.roundtype := RoundingType.SPEC
      when ((expA === "hff".U && operA(22, 0) === 0.U) || (expB === "hff".U && operB(22, 0) === 0.U)) {
        // Check zero * inf
        result := "hffc00000".U
        ff(FaultFlag.NV.asUInt()) := 1.U // Not valid 
      } .otherwise {
        // Check zero * number
        result := Cat(resultSign, 0.U(31.W))
      }  
    } .elsewhen ((expA === "hff".U && operA(22, 0) === 0.U) || (expB === "hff".U && operB(22, 0) === 0.U)) {
      // Check operA or operB == inf 
      io.control.roundtype := RoundingType.SPEC
      result := Cat(resultSign, "hff".U, 0.U(23.W))
    } .elsewhen ((expA === "h7f".U && operA(22, 0) === 0.U) || (expB === "h7f".U && operB(22, 0) === 0.U)) {
      // Check operA or operB == 1 
      io.control.roundtype := RoundingType.SPEC
      when ((expA === "h7f".U && operA(22, 0) === 0.U)) {
        // Check A = 1
        result := Cat(resultSign, operB(30, 0)) 
      } .otherwise {
        // Check B = 1 (or both) 
        result := Cat(resultSign, operA(30, 0))
      }
    } .otherwise {
      // Prenormalization of subnormal numbers
      val ldzA = WireDefault(0.U(8.W)) 
      val ldzB = WireDefault(0.U(8.W))
      
      when (isSubNormalA) {
        ldzA := PriorityEncoder(Reverse(fractionA))
      }

      when (isSubNormalB) {
        ldzB := PriorityEncoder(Reverse(fractionB))
      }

      val multExp = expA +& expB // has an offset of +127
      val multFraction = WireDefault(0.U(48.W))
      multFraction := (fractionA << ldzA) * (fractionB << ldzB)

      val tempExp = Wire(UInt(10.W))
      val tempSExp = Wire(SInt(12.W))
      val tempFraction = Wire(UInt((bodyBits+tailBits).W))
      tempExp := multExp
      tempFraction := multFraction

      val shiftOutBit = WireDefault(0.U(1.W))

      when (multFraction(ovflIdx) === "b1".U ||  ~multFraction(hidnIdx, stkbIdx) === 0.U) {
        // shift to right & increment
        tempExp := multExp + 1.U       
        tempFraction := multFraction >> 1

        shiftOutBit := multFraction(0)

        when (shiftOutBit === 1.U){
          ff(FaultFlag.NX.asUInt()) := 1.U // Inexact          
        }
      }
      
      val resultFraction = Wire(UInt(52.W))
      val stickyBit = tempFraction(stkbIdx, 0).orR || shiftOutBit.asBool
  
      tempSExp := tempExp.asSInt - 255.S - ldzA.asSInt - ldzB.asSInt

      resultFraction := Cat(0.U, tempFraction(hidnIdx, rndbIdx), stickyBit, 0.U(24.W))

      //normal sint exponent limits -127 and 127
      when (tempSExp >= 127.S) {
        // overflow
        io.control.roundtype := RoundingType.SPEC
        result := Cat(resultSign, "hff".U, 0.U(23.W))

        ff(FaultFlag.OF.asUInt()) := 1.U // Overflow
        ff(FaultFlag.NX.asUInt()) := 1.U // Inexact
      } .elsewhen(tempSExp < -127.S) {
        // subnormal result
        val ufFraction = Wire(UInt(52.W))
        val ufshift = Wire(SInt(10.W))
        val ufstickyBit = Wire(UInt(1.W))
        ufshift := -127.S - tempSExp //e_min - e
        ufFraction :=  Mux(ufshift(7,0) >= 26.U, 0.U(24.W), resultFraction >> ufshift(7,0))
        ufstickyBit := ufFraction(24, 0).orR
        result := Cat(resultSign, 0.U(8.W), ufFraction(51,25), ufstickyBit)

        // underflow
        when ( ufFraction(26, 0).orR || shiftOutBit.asBool || tempSExp < -149.S || ufshift(7,0) > 32.U) {
          ff(FaultFlag.UF.asUInt()) := 1.U // Underflow
          ff(FaultFlag.NX.asUInt()) := 1.U // Inexact
        }
      } .otherwise {
        // compose result
        val resultExp = WireDefault(0.U(8.W))
        resultExp := tempExp(7,0) - 127.U - ldzA - ldzB
        result := Cat(resultSign, resultExp, resultFraction(51,24))
      }
    }
    result
  }

  def div(operA: UInt, operB: UInt) : UInt = {
    val prefixLen = 2
    val mantLen = 23
    val totalLen = prefixLen + mantLen

    val ovflIdx = 2*totalLen+1
    val hidnIdx = 2*totalLen
    val grdbIdx = 2*totalLen-mantLen-1
    val rndbIdx = 2*totalLen-mantLen-2
    val stkbIdx = 2*totalLen-mantLen-3

    val mantEndIdx = 2*totalLen-mantLen

    val isSubNormalA = {operA(30,23) === 0.U}
    val isSubNormalB = {operB(30,23) === 0.U}
    val isMantZeroA  = {operA(22,0)  === 0.U}
    val isMantZeroB  = {operB(22,0)  === 0.U}

    val signA = operA(31)
    // If exponent is 0 (subnormal number), replace it with 1
    val expA = Mux(isSubNormalA && !isMantZeroA, 1.U(8.W), operA(30, 23))
    // add hidden bit (if not a subnormal number)
    val fractionA = Cat(Mux(isSubNormalA, 0.U, 1.U(1.W)), operA(22, 0))

    val signB = operB(31)
    // If exponent is 0 (subnormal number), replace it with 1
    val expB = Mux(isSubNormalB && !isMantZeroB, 1.U(8.W), operB(30, 23))
    // add hidden bit (if not a subnormal number)
    val fractionB = Cat(Mux(isSubNormalB, 0.U, 1.U(1.W)), operB(22, 0))

    val mantA = operA(22,0)
    val mantB = operB(22,0)

    val result = WireDefault(0.U(40.W))
    val divSign = signA ^ signB

    /*
    1. check for special cases
    2. get sign with XOR
    3. get exponent with sum of exponents
    4. get mantissa with div of mantissas
    5. compose result
    */

    when ((expA === "hff".U) && (operA(22, 0) > 0.U) && (expB === "hff".U) && (operB(22, 0) > 0.U)) {  
      // Check operA and operB
      when(!operA(22) || !operB(22)){
        //sNaN
        ff(FaultFlag.NV.asUInt()) := 1.U // Not valid 
      }
      //qNaN
      io.control.roundtype := RoundingType.SPEC
      result := Cat(operA(31,23), 1.U, operA(21,0))
    } .elsewhen ((expA === "hff".U) && (operA(22, 0) > 0.U)) {  
      // Check operA
      when(!operA(22)){
        //sNaN
        ff(FaultFlag.NV.asUInt()) := 1.U // Not valid 
      }
      //qNaN
      io.control.roundtype := RoundingType.SPEC
      result := Cat(operA(31,23), 1.U, operA(21,0))
    } .elsewhen ((expB === "hff".U) && (operB(22, 0) > 0.U)) { 
      // Check operB
      when(!operB(22)){
        //sNaN
        ff(FaultFlag.NV.asUInt()) := 1.U // Not valid 
      }
      //qNaN
      io.control.roundtype := RoundingType.SPEC
      result := Cat(operB(31,23), 1.U, operB(21,0))
    } .elsewhen ((expA === 0.U && mantA === 0.U) || (expB === 0.U && mantB === 0.U)) {
      io.control.roundtype := RoundingType.SPEC
      when ((expA === 0.U && mantA === 0.U) && (expB === 0.U && mantB === 0.U)) {
        // 0/0 => illegal === qNaN
        result := "hffc00000".U
        ff(FaultFlag.NV.asUInt()) := 1.U // Not valid
      } .elsewhen ((expA === 0.U && mantA === 0.U) && !(expB === 0.U && mantB === 0.U)) {
        // 0/x => +/-0
        result := Cat(signA ^ signB, expA, mantA)
      } .otherwise {
        // x/0 => division by zero
        result := Cat(signA ^ signB, 255.U(8.W), 0.U(23.W))
        when (~(expA === "hff".U && mantA === 0.U)) {
          // inf/0 => inf
          ff(FaultFlag.DZ.asUInt()) := 1.U // Division by zero
        }
      }
    } .elsewhen ((expA === "hff".U && mantA === 0.U) || (expB === "hff".U && mantB === 0.U)) {
      // Check infinities
      io.control.roundtype := RoundingType.SPEC
      when (!(expA === "hff".U && mantA === 0.U) && (expB === "hff".U && mantB === 0.U)) {
        // x/inf => inf
        result := Cat(signA ^ signB, 0.U(8.W), 0.U(23.W))
      } .elsewhen ((expA === "hff".U && mantA === 0.U) && !(expB === "hff".U && mantB === 0.U)) {
        // inf/x => inf
        result :=  Cat(signA ^ signB, expA, mantA)
      } .otherwise {
        // inf/inf => illegal === qNaN
        result :=  "hffc00000".U
        ff(FaultFlag.NV.asUInt()) := 1.U // Not valid
      }
    } .elsewhen (expB === "h7f".U && operB(22, 0) === 0.U) {
      // Check B = 1 (or both) 
      io.control.roundtype := RoundingType.SPEC
      result := Cat(divSign, operA(30, 0))
    } .otherwise {
      var divExp = WireDefault(0.S(12.W))

      // Prenormalization of subnormal numbers
      val ldzA = WireDefault(0.U(8.W)) 
      val ldzB = WireDefault(0.U(8.W))
      
      when (isSubNormalA) {
        ldzA := PriorityEncoder(Reverse(fractionA))
      }

      when (isSubNormalB) {
        ldzB := PriorityEncoder(Reverse(fractionB))
      }

      // The quotient has extra bits to increase accuracy
      val numerator = WireDefault(0.U(totalLen.W))
      val denominator = WireDefault(0.U(totalLen.W))
      val quotient = WireDefault(0.U((2*totalLen+2).W))
      var mant = WireDefault(0.U(24.W))

      numerator := fractionA << ldzA
      denominator := fractionB << ldzB

      // Bits must be shifted like this to receive an integer quotient
      quotient := (numerator << (3*totalLen)) / (denominator << totalLen)

      val isUnderOne = {numerator.asUInt < denominator.asUInt}

      // if the division result is less than 1, subtract exponent by 1
      when (~isUnderOne) {
        divExp := (Cat(0.U(2.W), expA).asSInt - ldzA.asSInt) - (Cat(0.U(2.W), expB).asSInt - ldzB.asSInt)
      } .otherwise {
        divExp := (Cat(0.U(2.W), expA).asSInt - ldzA.asSInt) - (Cat(0.U(2.W), expB).asSInt - ldzB.asSInt) - 1.S
      }

      val tempExp = Wire(SInt(12.W))
      val tempFraction = Wire(UInt((2*totalLen+2).W))
      val shiftOutBit = WireDefault(0.U(1.W))
      val overflowBit = WireDefault(0.U(1.W))
      
      tempExp := divExp
      tempFraction := quotient
      overflowBit := quotient(ovflIdx)

      // mantissa overflow
      when (overflowBit.asBool) {
        // shift to right & increment
        tempExp := divExp + 1.S      
        tempFraction := quotient >> 1
        shiftOutBit := quotient(0)

        when (shiftOutBit === 1.U){
          ff(FaultFlag.NX.asUInt()) := 1.U // Inexact          
        }
      }

      // If answer is less than one, the hidden bit is one bit to the right
      when (~isUnderOne) {
        mant := tempFraction(hidnIdx, mantEndIdx).asUInt
      } .otherwise {
        mant := tempFraction(hidnIdx-1, mantEndIdx-1).asUInt
      }

      // Guard and round bits
      val GR = Mux(~isUnderOne, 
        tempFraction(grdbIdx, rndbIdx),
        tempFraction(grdbIdx-1, rndbIdx-1))

      // Generate the sticky bit which is 1 if any of the bits after the R bit is 1
      val sticky = Mux(~isUnderOne, 
        tempFraction(stkbIdx, 0), 
        tempFraction(stkbIdx-1, 0)).orR || shiftOutBit.asBool

      val resultFraction = WireDefault(0.U(28.W))
      resultFraction := Cat(0.U(1.W), mant, GR, sticky)

      //normal sint exponent limits -127 and 127
      when (divExp > 127.S) {
        // overflow
        io.control.roundtype := RoundingType.SPEC
        result := Cat(divSign, "hff".U, 0.U(23.W))

        ff(FaultFlag.OF.asUInt()) := 1.U // Overflow
        ff(FaultFlag.NX.asUInt()) := 1.U // Inexact
      } .elsewhen(divExp < -126.S) {
        // subnormal result
        val ufFraction = WireDefault(0.U(52.W))
        val ufshift = Wire(SInt(10.W))
        val ufstickyBit = Wire(UInt(1.W))
        ufshift := -126.S - divExp //e_min - e
        ufFraction := Mux(ufshift(7,0) >= 26.U, 0.U(24.W), (Cat(resultFraction, 0.U(24.W))) >> ufshift(7,0))
        ufstickyBit := ufFraction(24, 0).orR
        result := Cat(divSign, 0.U(8.W), ufFraction(51,25), ufstickyBit)

        // underflow
        when ( ufFraction(26, 0).orR || divExp < -149.S || ufshift(7,0) > 32.U) {
          ff(FaultFlag.UF.asUInt()) := 1.U // Underflow
          ff(FaultFlag.NX.asUInt()) := 1.U // Inexact
        }
      } .otherwise {
        // compose result
        val resultExp = WireDefault(0.U(8.W))
        resultExp := divExp(7,0) + 127.U
        result := Cat(divSign, resultExp, resultFraction)
      }
    }
    result
  }

  /**
    * Square root calculation
    * 
    * @param operA operand
    * @param cordicIters number of cordic iterations
    * @param extraCalcBits number of extra bits used for calculations
    * @return 32-bit float number
    */
  def sqrt(operA: UInt, cordicIters: Int = 25, extraCalcBits: Int = 12) : UInt = {
    val signA = operA(31,31)
    val expA  = operA(30,23)
    val mantA = operA(22,0)

    // Adds hidden bit, sign bit, and an extra overflow bit
    val mantissa = Cat(1.U(3.W), operA(22,0)) 

    // Flag that rises when mantissa is all zeros and exponent is odd
    // Skips all for-loops and simply divides exponent by two
    val skipZeros = Mux(operA(22,0) === 0.U && operA(23) === 1.U, true.B, false.B) 

    val result = WireDefault(0.U(40.W))

    /*
    uses cordic algorithm

    0. check for exceptions
    1. set  x(0) = operA + 0.25 
            y(0) = operA - 0.25
    2. calculate 
            x(i+1) = x(i) + y(i) * d(i) * 2^(-i)
            y(i+1) = y(i) + x(i) * d(i) * 2^(-i)
            where d(i) = +1 if y(i) < 0 otherwise d(i) = -1
    4. multiply by cordic gain constant A(N) 
            sqrt(operA) = xN / A(N)
    */

    when (signA === 1.U) {
      io.control.roundtype := RoundingType.SPEC
      when (expA === 0.U && mantA === 0.U) {
          // -0
          result := "h80000000".U
      } .otherwise {
          // rs1 < 0
          result := "h7fc00000".U
      }
      ff(FaultFlag.NV.asUInt()) := 1.U // Not valid
    } .elsewhen (operA === 0.U) {
      // rs1 === +0
      io.control.roundtype := RoundingType.SPEC
      result := 0.U(32.W)
    } .elsewhen (expA === "hff".U) {
      io.control.roundtype := RoundingType.SPEC
      when (mantA =/= 0.U){
        when(!operA(22)){
          // sNaN
          result := "h7fa00000".U
          ff(FaultFlag.NV.asUInt()) := 1.U // Not valid 
        } .otherwise {
          // qNaN
          result := "h7fc00000".U
        }
      } .otherwise {
        // rs1 === inf
        result := "h7f800000".U
      }  
    } .otherwise {
      //inf > rs1 >= 0

      // configurables
      val iterations = cordicIters  // number of cordic iterations
      val extraBits = extraCalcBits // extra bits to be added to increase calculation accuracy

      // pre-calculate how many times an iteration is repeated (i = 3k+1)
      val repeats = {
        var times = 0
        var k = 4
        for (i <- 1 to iterations) {
          if (i == k) {
            k = 3*k + 1 
            times += 1
          }
        }
        times
      }

      val totalIterations = iterations + repeats

      val mantLen = 23  // mantissa length in bits
      val prefixLen = 3 // number of bits added in front of mantissa (hidden bit, sign bit, extra bit)
      val extMantLen = prefixLen+mantLen+extraBits // extended mantissa length for cordic algorithm

      var k = 4 // index for repeating the cordic iteration

      // Vectors for intermediate results
      // All calculations are performed as signed integers
      var nX = WireDefault(VecInit(Seq.fill(iterations+repeats)(0.S(extMantLen.W))))
      var nY = WireDefault(VecInit(Seq.fill(iterations+repeats)(0.S(extMantLen.W))))
      var tmpX = WireDefault(VecInit(Seq.fill(iterations+repeats)(0.S(extMantLen.W))))
      var tmpY = WireDefault(VecInit(Seq.fill(iterations+repeats)(0.S(extMantLen.W))))
      var extMantissa = Cat(mantissa, 0.U(extraBits.W)) // extended mantissa

      var sqrtMant = WireDefault(0.U(mantLen.W))
      var sqrtExp = WireDefault(0.U(8.W))

      // initial value for the cordic algo (0.25)
      val init_val = scala.math.pow(2, extMantLen-prefixLen-2).toLong

      // pre-calculate inverse cordic gain
      val inv_An = { 
        var Ares = 1.0
        var k = 4
        for (i <- 1 to iterations) {
          Ares = Ares * scala.math.sqrt(1.0-scala.math.pow(2, -2*i))
          if (i == k) {
            Ares = Ares * scala.math.sqrt(1.0-scala.math.pow(2, -2*i))
            k = 3*k + 1
          }
        }
        (1.0/Ares * scala.math.pow(2, extMantLen-prefixLen)).round
      } 

      // whether exponent is even or odd
      when (expA % 2.U === 0.U) {
        // if even, the exponent is divided by two and rounded up
        // consequently, the mantissa is divided by two
        when (expA >= 128.U) {
          sqrtExp := 128.U + ((expA - 128.U) >> 1.U) 
        } .otherwise {
          sqrtExp := 128.U - ((128.U - expA) >> 1.U)
        }
        nX(0) := (extMantissa >> 1.U).asSInt() + init_val.S(extMantLen.W)
        nY(0) := (extMantissa >> 1.U).asSInt() - init_val.S(extMantLen.W) 
      } .otherwise {
        // if odd, the exponent is simply divided by two 
        // and the mantissa remains the same
        when (expA >= 127.U) {
          sqrtExp := 127.U + ((expA - 127.U) >> 1.U) 
        } .otherwise {
          sqrtExp := 127.U - ((127.U - expA) >> 1.U)
        }
        nX(0) := extMantissa.asSInt() + init_val.S(extMantLen.W)
        nY(0) := extMantissa.asSInt() - init_val.S(extMantLen.W)
      }

      var shift = 1 // keeps track of how many times the initial value must be bit shifted

      when (!skipZeros) {
        // cordic iteration loop
        for (i <- 1 until iterations + repeats) {

          tmpX(i-1) := nX(i-1) >> shift.U
          tmpY(i-1) := nY(i-1) >> shift.U

          if (i == k) {
            // runs the iteration twice with the same bit shift
            k = 3*k + 1
          } else {
            shift += 1
          }

          when (nY(i-1)(extMantLen-1) === 1.U) {
            // y less than zero
            nX(i) := nX(i-1) + tmpY(i-1)
            nY(i) := nY(i-1) + tmpX(i-1)
          } .otherwise {
            // y more than zero
            nX(i) := nX(i-1) - tmpY(i-1)
            nY(i) := nY(i-1) - tmpX(i-1)
          }
        }

        val inv_An2_hw = WireDefault(inv_An.S(extMantLen.W))

        // multiply by inverse cordic gain
        // this has a length of 2*extMantLen
        // with binary point at 2*mantLen + 2*extraBits - 1
        val mulRes = inv_An2_hw * nX(totalIterations-1) 

        // truncates the extra bits except for three: G, R and an extra bit
        // the remaining value has a length of mantLen+prefixLen+3
        val tempMant = (mulRes >> (2*extraBits + mantLen - 3))(mantLen+prefixLen+2,0)

        // Generates the sticky bit
        // which is 1 if any of the bits after the R bit is 1
        val sticky = mulRes(2*extraBits + mantLen - 3, 0).orR

        var finalMant = WireDefault(0.U((mantLen+prefixLen+2).W))
        var finalExp = WireDefault(0.U(8.W))

        when (tempMant(mantLen+3) === 0.U) {
          // mantissa less than 1, multiply by 2
          finalMant := (tempMant << 1)(mantLen+prefixLen+2,1)
          finalExp := (sqrtExp.asSInt() - 1).asUInt()
        } .otherwise {
          // mantissa more than one
          finalMant := tempMant(mantLen+prefixLen+2,1) 
          finalExp := sqrtExp
        }

        result := Cat(0.U, finalExp, finalMant(mantLen+prefixLen,0), sticky)
      }. otherwise {
        result := Cat(0.U, sqrtExp, 0.U((mantLen + 5).W))
      }
    }  
    result 
  }

  def madd(operA: UInt, operB: UInt, operC: UInt) : UInt = {
    //(a * b) + c
    val ab = WireDefault(0.U(40.W))
    val result = WireDefault(0.U(40.W))

    ab := mul(operA, operB)
    result := add(Cat(ab(36, 28), ab(25, 3)), operC, "b0".U)

    result
  }

  def msub(operA: UInt, operB: UInt, operC: UInt) : UInt = {
    //(a * b) - c
    val ab = WireDefault(0.U(40.W))
    val result = WireDefault(0.U(40.W))

    ab := mul(operA, operB)
    result := add(Cat(ab(36, 28), ab(25, 3)), operC, "b1".U)

    result
  }

  def nmadd(operA: UInt, operB: UInt, operC: UInt) : UInt = {
    //-(a * b) - c
    val ab = WireDefault(0.U(40.W))
    val result = WireDefault(0.U(40.W))

    ab := mul(operA, operB)
    result := add(Cat(ab(36) ^ 1.U, ab(35, 28), ab(25, 3)), operC, "b1".U)

    result
  }

  def nmsub(operA: UInt, operB: UInt, operC: UInt) : UInt = {
    //-(a * b) + c
    val ab = WireDefault(0.U(40.W))
    val result = WireDefault(0.U(40.W))

    ab := mul(operA, operB)
    result := add(Cat(ab(36) ^ 1.U, ab(35, 28), ab(25, 3)), operC, "b0".U)

    result
  }

  switch (io.control.op) {
    is (FPUOp.FADD_S) {
      io.data.out := add(io.data.a, io.data.b, "b0".U)
    }
    is (FPUOp.FSUB_S) {
      io.data.out := add(io.data.a, io.data.b, "b1".U)
    }
    is (FPUOp.FMUL_S) {
      io.data.out := mul(io.data.a, io.data.b)
    }
    is (FPUOp.FDIV_S) {
      io.data.out := div(io.data.a, io.data.b)
    }
    is (FPUOp.FSQRT_S) {
      io.data.out := sqrt(io.data.a)
    }
    is (FPUOp.FMADD_S) {
      io.data.out := madd(io.data.a, io.data.b, io.data.c)
    }
    is (FPUOp.FMSUB_S) {
      io.data.out := msub(io.data.a, io.data.b, io.data.c)
    }
    is (FPUOp.FNMADD_S) {
      io.data.out := nmadd(io.data.a, io.data.b, io.data.c)
    }
    is (FPUOp.FNMSUB_S) {
      io.data.out := nmsub(io.data.a, io.data.b, io.data.c)
    }
  }

  when(ff.asUInt() =/= 0.U) {
    io.data.fflags := ff.asUInt()
  }
}
