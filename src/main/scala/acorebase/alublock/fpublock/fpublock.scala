// SPDX-License-Identifier: Apache-2.0

// Chisel module FPU
// Module for Single-precision floating point calculations
// Inititally written by chisel-blocks-utils initmodule.sh, 2021-07-20
package acorebase

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.experimental.BundleLiterals._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import chisel3.iotesters._
import a_core_common._
import dsptools.{DspTester}

object AluBlockRoundingSrc extends chisel3.ChiselEnum {
  val SRM  = Value(0.U)
  val DRM  = Value(1.U)
}

/** IO definitions for FPUBlock */
class FPUBlockIO(XLEN: Int) extends Bundle {
  val data = new Bundle {
    val a = Input(UInt(XLEN.W))             // operand A
    val b = Input(UInt(XLEN.W))             // operand B
    val c = Input(UInt(XLEN.W))             // operand C
    val out = Output(UInt(XLEN.W))          // computation result
    val fault_data = Output(UInt(5.W))      // data for fcsr FFLAGS
  }
  val control = new Bundle {
    val op = Input(FPUOp())                // FPU operation
    val rm = Input(RoundingMode())          // rounding operation
  }
}


/** Module definition for FPU
  * @param proto type information
  * @param n number of elements in register
  */
class FPUBlock(XLEN: Int = 32) extends Module {
  val io = IO(new FPUBlockIO(XLEN=XLEN))
  val arith_block = Module(new ArithmeticBlock(XLEN=XLEN))
  val round_block = Module(new RoundingBlock(XLEN=XLEN))
  
  val ff = WireDefault(VecInit(Seq.fill(5)(0.U(1.W))))
  ff := Seq.fill(5)(0.U(1.W))  
  io.data.fault_data := 0.U(5.W) // "b00000"
  io.data.out := 0.U

  // connect ios
  io.control.op <> arith_block.io.control.op
  io.data.a <> arith_block.io.data.a
  io.data.b <> arith_block.io.data.b
  io.data.c <> arith_block.io.data.c

  round_block.io.data.in := 0.U
  round_block.io.control.roundmode := io.control.rm
  round_block.io.control.roundtype := RoundingType.SPEC
  round_block.io.data.fflags_in := 0.U

  switch (io.control.op) {
    is (FPUOp.NULL) {
      //FADD.S      rs1 + rs2
      io.data.out                     := 0.U
    }
    is (FPUOp.FADD_S) {
      //FADD.S      rs1 + rs2
      round_block.io.control.roundtype := arith_block.io.control.roundtype  
      round_block.io.data.fflags_in    := arith_block.io.data.fflags
      round_block.io.data.in           := arith_block.io.data.out
      io.data.out                     := round_block.io.data.out
      ff                              := round_block.io.data.fflags_out.asBools() 
    }
    is (FPUOp.FSUB_S) {
      //FSUB.S      rs1 - rs2
      round_block.io.control.roundtype := arith_block.io.control.roundtype 
      round_block.io.data.fflags_in    := arith_block.io.data.fflags
      round_block.io.data.in           := arith_block.io.data.out
      io.data.out                     := round_block.io.data.out
      ff                              := round_block.io.data.fflags_out.asBools()  
    }
    is (FPUOp.FMUL_S) {
      //FMUL.S      rs1 * rs2
      round_block.io.control.roundtype := arith_block.io.control.roundtype 
      round_block.io.data.fflags_in    := arith_block.io.data.fflags
      round_block.io.data.in           := arith_block.io.data.out
      io.data.out                     := round_block.io.data.out
      ff                              := round_block.io.data.fflags_out.asBools() 
    }
    is (FPUOp.FDIV_S) {
      //FDIV.S      rs1 / rs2
      round_block.io.control.roundtype := arith_block.io.control.roundtype 
      round_block.io.data.fflags_in    := arith_block.io.data.fflags
      round_block.io.data.in           := arith_block.io.data.out
      io.data.out                     := round_block.io.data.out
      ff                              := round_block.io.data.fflags_out.asBools()  
    }
    is (FPUOp.FSQRT_S) {
      //FSQRT.S     srqt(rs1)
      round_block.io.control.roundtype := arith_block.io.control.roundtype 
      round_block.io.data.fflags_in    := arith_block.io.data.fflags
      round_block.io.data.in           := arith_block.io.data.out
      io.data.out                     := round_block.io.data.out
      ff                              := round_block.io.data.fflags_out.asBools()  
    }
      is (FPUOp.FMADD_S) {
      //FMADD.S     (rs1 * rs2) + rs3
      round_block.io.control.roundtype := arith_block.io.control.roundtype 
      round_block.io.data.fflags_in    := arith_block.io.data.fflags
      round_block.io.data.in           := arith_block.io.data.out
      io.data.out                     := round_block.io.data.out
      ff                              := round_block.io.data.fflags_out.asBools()  
    }
    is (FPUOp.FMSUB_S) {
      //FMSUB.S     (rs1 * rs2) - rs3
      round_block.io.control.roundtype := arith_block.io.control.roundtype 
      round_block.io.data.fflags_in    := arith_block.io.data.fflags
      round_block.io.data.in           := arith_block.io.data.out
      io.data.out                     := round_block.io.data.out
      ff                              := round_block.io.data.fflags_out.asBools()  
    }
    is (FPUOp.FNMADD_S) {
      //FNMADD.S    -(rs1 * rs2) + rs3
      round_block.io.control.roundtype := arith_block.io.control.roundtype 
      round_block.io.data.fflags_in    := arith_block.io.data.fflags
      round_block.io.data.in           := arith_block.io.data.out
      io.data.out                     := round_block.io.data.out
      ff                              := round_block.io.data.fflags_out.asBools()  
    }
    is (FPUOp.FNMSUB_S) {
      //FNMSUB.S    -(rs1 * rs2) - rs3
      round_block.io.control.roundtype := arith_block.io.control.roundtype 
      round_block.io.data.fflags_in    := arith_block.io.data.fflags
      round_block.io.data.in           := arith_block.io.data.out
      io.data.out                     := round_block.io.data.out
      ff                              := round_block.io.data.fflags_out.asBools()  
    }
    is (FPUOp.FMIN_S) {
      //FMIN.S      MIN(rs1, rs2)

      /*
      0. if signA === NaN => B
         if signB === NaN => A
         if signA === NaN && signB === NaN	=> cNaN
      1. if signA > signB => A
         if signA < signB	=> B
         if signA === signB && signA === 1.U (negative values) {
          if signA === signB && exponentA > exponentB => A
          if signA === signB && exponentA < exponentB => B
          if signA === signB && exponentA === exponentB && exponentA > exponentB	=> A
          if signA === signB && exponentA === exponentB && exponentA < exponentB	=> B
          if signA === signB && exponentA === exponentB && mantissaA === mantissaB	=> A
        } else {
          if signA === signB && exponentA < exponentB => A
          if signA === signB && exponentA > exponentB => B
          if signA === signB && exponentA === exponentB && exponentA < exponentB	=> A
          if signA === signB && exponentA === exponentB && exponentA > exponentB	=> B
          if signA === signB && exponentA === exponentB && mantissaA === mantissaB	=> A
        } 
      */

      val signA = io.data.a(31,31)
      val expA = io.data.a(30,23)
      val mantA = io.data.a(22,0)

      val signB = io.data.b(31,31)
      val expB = io.data.b(30,23)
      val mantB = io.data.b(22,0)

      val cNaN = "h7fc00000".U
      val hasNaN = WireDefault(false.B)

      when((expA === "hff".U && mantA > 0.U) || (expB === "hff".U && mantB > 0.U)) {
        when(expA === "hff".U && expB === "hff".U) {
          when(!io.data.a(22) || !io.data.b(22)) {
            ff(FaultFlag.NV.asUInt()) := 1.U //sNaN
          } 
          io.data.out := cNaN
        } .elsewhen(expB === "hff".U) {
          when(!io.data.b(22)) {
            ff(FaultFlag.NV.asUInt()) := 1.U //sNaN
          } 
          io.data.out := io.data.a
        } .elsewhen(expA === "hff".U) {
          when(!io.data.a(22)) {
            ff(FaultFlag.NV.asUInt()) := 1.U //sNaN
          }
          io.data.out := io.data.b
        }
        hasNaN := true.B
      }

      when(~hasNaN) {
        when(io.data.a === "hff800000".U){
	        io.data.out := io.data.a
        } .elsewhen(io.data.b === "hff800000".U){
	        io.data.out := io.data.b
        } .elsewhen(expA === 0.U && expB === 0.U && mantA === 0.U && mantB === 0.U) {
          when(signA > signB) {
            io.data.out := io.data.a
          } .otherwise {
            io.data.out := io.data.b
          }
        }.elsewhen(signA === 1.U && signA === signB) {
          when(expA === expB) {
            when(mantA === mantB || mantA > mantB) {
              io.data.out := io.data.a
            } .otherwise {
              io.data.out := io.data.b
            }
          } .elsewhen(expA > expB) {
            io.data.out := io.data.a
          } .otherwise {
            io.data.out := io.data.b
          }
        } .elsewhen(signA === 0.U && signA === signB) {
          when(expA === expB) {
            when(mantA === mantB || mantA < mantB) {
              io.data.out := io.data.a
            } .otherwise {
              io.data.out := io.data.b
            }
          } .elsewhen(expA < expB) {
            io.data.out := io.data.a
          } .otherwise {
            io.data.out := io.data.b
          }
        } .elsewhen(signA > signB) {
          io.data.out := io.data.a
        } .otherwise {
          io.data.out := io.data.b
        }
      }
    }
    is (FPUOp.FMAX_S) {
      //FMAX.S      MAX(rs1, rs2)

      /*
      0. if signA === NaN => B
         if signB === NaN => A
         if signA === NaN && signB === NaN => cNaN
      1. if signA < signB => A
         if signA > signB => B
         if signA === signB && signA === 1.U (negative values) {
          if signA === signB && exponentA < exponentB => A
          if signA === signB && exponentA > exponentB => B
          if signA === signB && exponentA === exponentB && exponentA < exponentB => A
          if signA === signB && exponentA === exponentB && exponentA > exponentB => B
          if signA === signB && exponentA === exponentB && mantissaA === mantissaB => A
        } else {
          if signA === signB && exponentA > exponentB => A
          if signA === signB && exponentA < exponentB => B
          if signA === signB && exponentA === exponentB && exponentA > exponentB => A
          if signA === signB && exponentA === exponentB && exponentA < exponentB => B
          if signA === signB && exponentA === exponentB && mantissaA === mantissaB => A
        } 
      */

      val signA = io.data.a(31,31)
      val expA = io.data.a(30,23)
      val mantA = io.data.a(22,0)

      val signB = io.data.b(31,31)
      val expB = io.data.b(30,23)
      val mantB = io.data.b(22,0)

      val cNaN = "h7fc00000".U
      val hasNaN = WireDefault(false.B)

      when((expA === "hff".U && mantA > 0.U) || (expB === "hff".U && mantB > 0.U)) {
        when(expA === "hff".U && expB === "hff".U) {
          when(!io.data.a(22) || !io.data.b(22)) {
            ff(FaultFlag.NV.asUInt()) := 1.U //sNaN
          } 
          io.data.out := cNaN
        } .elsewhen(expB === "hff".U) {
          when(!io.data.b(22)) {
            ff(FaultFlag.NV.asUInt()) := 1.U //sNaN
          } 
          io.data.out := io.data.a
        } .elsewhen(expA === "hff".U) {
          when(!io.data.a(22)) {
            ff(FaultFlag.NV.asUInt()) := 1.U //sNaN
          }
          io.data.out := io.data.b
        }
        hasNaN := true.B
      }

      when(~hasNaN) {
        when(io.data.a === "hff800000".U){
	        io.data.out := io.data.a
        } .elsewhen(io.data.b === "hff800000".U){
	        io.data.out := io.data.b
        } .elsewhen(expA === 0.U && expB === 0.U && mantA === 0.U && mantB === 0.U) {
          when(signA < signB) {
            io.data.out := io.data.a
          } .otherwise {
            io.data.out := io.data.b
          }
        } .elsewhen(signA === 1.U && signA === signB) {
          when(expA === expB) {
            when(mantA === mantB || mantA < mantB) {
              io.data.out := io.data.a
            } .otherwise {
              io.data.out := io.data.b
            }
          } .elsewhen(expA < expB) {
            io.data.out := io.data.a
          } .otherwise {
            io.data.out := io.data.b
          }
        } .elsewhen(signA === 0.U && signA === signB) {
          when(expA === expB) {
            when(mantA === mantB || mantA > mantB) {
              io.data.out := io.data.a
            } .otherwise {
              io.data.out := io.data.b
            }
          } .elsewhen(expA > expB) {
            io.data.out := io.data.a
          } .otherwise {
            io.data.out := io.data.b
          }
        } .elsewhen(signA < signB) {
          io.data.out := io.data.a
        } .otherwise {
          io.data.out := io.data.b
        }
      }
    }
    is (FPUOp.FCVT_W_S) { 
      //FCVT.W.S      FPU    ->   32 int

      /*
      "If the rounded result is not representable in the destination format, 
      it is clipped to the nearest value and the invalid flag is set."
      */

      val sign = WireDefault(0.U(1.W))
      sign :=  io.data.a(31)
      val exp = Cat(0.U, io.data.a(30,23))

      val mant = Wire(UInt(24.W))

      // If exponent is 0, then don't add the hidden bit
      mant := Cat(Mux(exp === 0.U, 0.U(1.W), 1.U(1.W)), io.data.a(22,0))

      val expBias = WireDefault(0.S(10.W))
      expBias := exp.asSInt - 127.S

      val shiftedBits = WireDefault(0.U(32.W))

      val result = WireDefault(0.U(32.W))

      when (expBias > 30.S) {
        // Float is larger than the maximum signed integer
        // -> Clip to largest possible value and set invalid op flag
        // This handles also +-Inf and NaN cases
        ff(FaultFlag.NV.asUInt()) := 1.U
        // Positive stuff and negative NaN result in 2^31 - 1, all others in -2^31
        io.data.out := Mux(sign === 0.U || (sign === 1.U && exp === "hff".U && mant(22,0) =/= 0.U), "h7fffffff".U, "h80000000".U)
      } .otherwise {
        when (expBias >= 23.S) {
          // Resulting int is larger than 23 bits -> no rounding needed and bits shift left
          shiftedBits := mant << expBias.asUInt - 23.U
          result := shiftedBits
        } .otherwise {
          // Resulting int is less than 23 bits so it needs rounding and bits shift right
          shiftedBits := mant >> 23.U - expBias.asUInt
          val roundBit = mant(23.U - expBias.asUInt - 1.U)
          val stickyGenBits = Wire(UInt(24.W))
          stickyGenBits := mant << (expBias.asUInt + 2.U)
          val stickyBit = stickyGenBits.orR
          val integer = WireDefault(0.U(31.W)) 
          integer := shiftedBits.pad(31)

          val outbits = WireDefault(0.U(32.W))

          when (expBias < 0.S) {
            outbits := io.data.a(22,0)
          } .otherwise {
            outbits := io.data.a(22,0) << expBias.asUInt
          }

          when (outbits =/= 0.U ) {
            ff(FaultFlag.NX.asUInt()) := 1.U
          }

          round_block.io.control.roundtype := RoundingType.SINT
          round_block.io.data.in := Cat(sign, integer, Cat(roundBit, stickyBit))
          result := round_block.io.data.out
        }
        // Convert to 2's complement if necessary
        io.data.out := Mux(sign === 0.U, result, Cat(result(31), ~result(30,0)) + 1.U)
      }

    }
    is (FPUOp.FCVT_S_W) {
      //FCVT.S.W      32 int  ->   FPU

      /*
      0. If the input is -2^32, send a fixed result as the algorithm
        would not directly work for that case
      1. Take sign from FPU 31 bit
      2. Calculate bias from leftmost bit
      3. Calculcate exponent
        exponent = 127 + bias
      4. Normalize Int for mantissa
        mantissa = int << bias (or int >> bias)
      5. If bias is larger than 23, generate GRS bits and round the result
      6. Compose result from sign, exponent & mantissa
      */

      val result = WireDefault(0.U(32.W))
      when (io.data.a === "h80000000".U) {
        result := "hcf000000".U
      } .elsewhen (io.data.a === "h00000000".U) {
        result := "h00000000".U
      } .otherwise {
        val sign = io.data.a(31)

        // Convert from 2's to 1's complement
        val intA = Mux(sign === 0.U, io.data.a(30,0), ~io.data.a(30,0) + 1.U)

        // check where the most significant active bit is
        val biasCount = WireDefault(0.U)
        val shiftedBits = WireDefault(intA)
        
        // Gets added to 127
        biasCount := 30.U - PriorityEncoder(Reverse(intA))

        val exp = biasCount + 127.U(8.W)

        // normalization
        when (biasCount > 23.U) {
          //shift to right
          shiftedBits := intA >> (biasCount - 23.U)
          val mant = shiftedBits(22,0)
          val guardBit = intA(biasCount - 23.U - 1.U)
          // if bias == 24, there is no round bit -> replace it with 1
          val roundBit = Mux(biasCount === 24.U, 0.U, intA(biasCount - 23.U - 2.U))
          val stickyGenBits = Wire(UInt(31.W))
          stickyGenBits := intA << (30.U - biasCount + 23.U + 2.U)
          val stickyBit = stickyGenBits.orR

          round_block.io.control.roundtype := RoundingType.FLOAT
          round_block.io.data.in := Cat(sign, exp, Cat(0.U, 1.U, mant, guardBit, roundBit, stickyBit))
          result := round_block.io.data.out
        } .otherwise {
          // shift to left
          shiftedBits := intA << (23.U - biasCount)
          val mant = shiftedBits(22,0)
          result := Cat(sign, exp, mant)
        }
      }

      io.data.out := result
    }
    is (FPUOp.FCVT_WU_S) {
      //FCVT.WU.S     FPU    ->   32 Uint

      /*
      1. Calculcate exponent bias
        expBias = exp - 127
      2. Shift mantissa with exponent bias
      3. Compose result from mantissa 
      4. Round if exponent is larger than 23
      */

        val sign = WireDefault(0.U(1.W))
      sign :=  io.data.a(31)
      val exp = Cat(0.U, io.data.a(30,23))

      val mant = Wire(UInt(24.W))
      mant := Cat(1.U(1.W), io.data.a(22,0))

      val expBias = WireDefault(0.S(10.W))
      expBias := exp.asSInt - 127.S

      val shiftedBits = WireDefault(0.U(32.W))

      when (sign === 1.U && ((expBias < -1.S && io.control.rm === RoundingMode.RNE) || ((expBias < 0.S) && io.control.rm === RoundingMode.RTZ))) {
        // All negative values are zero except for NaN
        ff(FaultFlag.NX.asUInt()) := 1.U
        io.data.out := Fill(32, 0.U)
      } .elsewhen (sign === 1.U && ~(exp === "hff".U && mant(22,0) > 0.U)) {
        // All negative values are zero except for NaN

        ff(FaultFlag.NV.asUInt()) := 1.U
        io.data.out := Fill(32, 0.U)
      } .elsewhen (expBias > 31.S) {
        // Float is larger than the maximum unsigned integer
        // -> Clip to largest possible value and set invalid op flag
        // This handles also +Inf and NaN cases
        ff(FaultFlag.NV.asUInt()) := 1.U
        io.data.out := Fill(32, 1.U)
      } .elsewhen (expBias >= 23.S) {
        // Resulting int is larger than 23 bits -> no rounding needed and bits shift left
        shiftedBits := mant << expBias.asUInt - 23.U
        io.data.out := shiftedBits
      } .otherwise {
        // Resulting int is less than 23 bits so it needs rounding and bits shift right
        shiftedBits := mant >> 23.U - expBias.asUInt
        val roundBit = mant(23.U - expBias.asUInt - 1.U)
        val stickyGenBits = Wire(UInt(24.W))
        stickyGenBits := mant << (expBias.asUInt + 2.U)
        val stickyBit = stickyGenBits.orR

        val integer = WireDefault(0.U(31.W)) 
        integer := shiftedBits.pad(31)

        val outbits = WireDefault(0.U(32.W))

        when (expBias < 0.S) {
          outbits := io.data.a(22,0)
        } .otherwise {
          outbits := io.data.a(22,0) << expBias.asUInt
        }

        when (outbits =/= 0.U ) {
          ff(FaultFlag.NX.asUInt()) := 1.U
        }
        
        round_block.io.control.roundtype := RoundingType.UINT
        round_block.io.data.in := Cat(sign, integer, Cat(roundBit, stickyBit))
        io.data.out := round_block.io.data.out
      }



    }
    is (FPUOp.FCVT_S_WU) {
      //FCVT.S.WU     32 Uint ->   FPU

      /*
      1. presume sign as 0 (UInt >= +0)
      2. calculate bias from leftmost bit
      3. calculcate exponent
		    exponent = 127 + bias
      4. normalize Int for mantissa
		    mantissa = int << bias (or int >> bias)
      5. compose result from sign, exponent & mantissa
      */

      val intA = io.data.a(31,0)      

      // check where the most significant active bit is
      val biasCount = WireDefault(0.U)
      val shiftedBits = WireDefault(intA)
      val result = WireDefault(0.U(32.W))
      when (io.data.a === "h00000000".U) {
        result := "h00000000".U
      } .otherwise {
        // Gets added to 127
        biasCount := 31.U - PriorityEncoder(Reverse(intA))

        val sign = 0.U
        val exp = biasCount + 127.U(8.W)

        // normalization
        when (biasCount >= 23.U) {
          //shift to right
          shiftedBits := intA >> (biasCount - 23.U)
          val mant = shiftedBits(22,0)
          val guardBit = intA(biasCount - 23.U - 1.U)
          // if bias == 24, there is no round bit -> replace it with 1
          val roundBit = Mux(biasCount === 24.U, 0.U, intA(biasCount - 23.U - 2.U))
          val stickyGenBits = Wire(UInt(31.W))
          stickyGenBits := intA << (30.U - biasCount + 23.U + 2.U)
          val stickyBit = stickyGenBits.orR

          round_block.io.control.roundtype := RoundingType.FLOAT
          round_block.io.data.in := Cat(sign, exp, Cat(0.U, 1.U, mant, guardBit, roundBit, stickyBit))
          result := round_block.io.data.out
        } .otherwise {
          // shift to left
          shiftedBits := intA << (23.U - biasCount)
          val mant = shiftedBits(22,0)
          result := Cat(sign, exp, mant)
        }
      }
      io.data.out := result
    }
    is (FPUOp.FSGNJ_S) {
      //FSGNJ.S       rs1 with rs2's sign bit 
      val sign = io.data.b(31)
      val body = io.data.a(30,0)
      val result = Cat(sign, body)
      io.data.out := result
    }
    is (FPUOp.FSGNJN_S) {
      //FSGNJN.S      rs1 with opposite of r2's sign bit
      val sign = io.data.b(31)
      val body = io.data.a(30,0)
      val result = Cat(~sign, body)
      io.data.out := result
    }
    is (FPUOp.FSGNJX_S) {
      //FSGNJX.S      rs1 with XOR of rs1 & rs2 sign bits
      val sign = io.data.a(31) ^ io.data.b(31)
      val body = io.data.a(30,0)
      val result = Cat(sign, body)
      io.data.out := result
    }
    is (FPUOp.FMV_X_W) {
      //FMV.X.W       raw FPU value into Integer regs
      io.data.out := io.data.a
    }
    is (FPUOp.FMV_W_X) {
      //FMV.W.X       raw Integer value into FPU regs
      io.data.out := io.data.a
    }
    is (FPUOp.FCMP_FEQ_S) {
      //FCMP.FEQ.S    rs1 == rs2

      /*
      0. check for NaNs
      1. check for zeros (+0 == -0)
      2. if signA === signB && exponentA === exponentB && mantissaA === mantissaB	=> 1 / true
	       else => 0 / false
      */

      val signA = io.data.a(31)
      val expA = io.data.a(30,23)
      val mantA = io.data.a(22,0)

      val signB = io.data.b(31)
      val expB = io.data.b(30,23)
      val mantB = io.data.b(22,0)

      when ((expA === "hff".U) && (mantA > 0.U) && (expB === "hff".U) && (mantB > 0.U)) {  
        when(!io.data.a(22) || !io.data.b(22)){
          ff(FaultFlag.NV.asUInt()) := 1.U // Not valid 
        }
        io.data.out := 0.U(32.W)
      } .elsewhen(expA === "hff".U && mantA > 0.U) {
        when(!io.data.a(22)) {
          ff(FaultFlag.NV.asUInt()) := 1.U //sNaN
        }
        io.data.out := 0.U(32.W)
      } .elsewhen(expB === "hff".U && mantB > 0.U) {
        when(!io.data.b(22)) {
          ff(FaultFlag.NV.asUInt()) := 1.U //sNaN
        }
        io.data.out := 0.U(32.W)
      } .elsewhen(expA === 0.U && expB === 0.U && mantA === 0.U && mantB === 0.U) {
        io.data.out := 1.U(32.W)
      } .otherwise {
        when(signA === signB) {
          when(expA === expB) {
            when(mantA === mantB) {
              io.data.out := 1.U(32.W)
            } .otherwise{
              io.data.out := 0.U(32.W)
            }
          } .otherwise{
            io.data.out := 0.U(32.W)
          }
        } .otherwise{
          io.data.out := 0.U(32.W)
        }
      }
    }
    is (FPUOp.FCMP_FLT_S) {
      //FCMP.FLT.S    rs1 < rs2

      /*
      0. check for NaNs and infs
      1. if signA > signB => 1 / true
         if signA === signB && exponentA < exponentB => 1 / true
         if signA === signB && exponentA === exponentB && mantissaA < mantissaB => 1 / true
	       else => 0 / false
      */

      val signA = io.data.a(31)
      val expA = io.data.a(30,23)
      val mantA = io.data.a(22,0)

      val signB = io.data.b(31)
      val expB = io.data.b(30,23)
      val mantB = io.data.b(22,0)

      when((expA === "hff".U && mantA > 0.U) || (expB === "hff".U && mantB > 0.U)) {
        // NaNs result in zeros and an invalid op flag
        io.data.out := 0.U(32.W)
        ff(FaultFlag.NV.asUInt()) := 1.U //NaN
      } .elsewhen(expA === 0.U && mantA === 0.U && expB === 0.U && mantB === 0.U) {
        // -0 < +0 == false
        io.data.out := 0.U(32.W) 
      } .otherwise {
        when(signA === signB) {
          when (signA === 0.U && signB === 0.U) {
          // both are positive numbers
            when(expA === expB) {
              when(mantA === mantB) {
                io.data.out := 0.U(32.W)
              } .otherwise{
                io.data.out := mantA < mantB
              }
            } .otherwise {
              io.data.out := expA < expB
            }
          } .otherwise {
            // if sign for both is negative, the comparisons are reversed
            when(expA === expB) {
              when(mantA === mantB) {
                io.data.out := 0.U(32.W)
              } .otherwise{
                io.data.out := mantA > mantB
              }
            } .otherwise {
              io.data.out := expA > expB
            }
          }
        } .otherwise {
          io.data.out := signA > signB
        }
      }
    }
    is (FPUOp.FCMP_FLE_S) {
      //FCMP.FLE.S    rs1 <= rs2

      /*
      0. check for NaNs or infs
      1. if signA > signB => 1 / true
         if signA === signB && exponentA < exponentB => 1 / true
         if signA === signB && exponentA === exponentB && mantissaA < mantissaB => 1 / true
         if signA === signB && exponentA === exponentB && mantissaA === mantissaB => 1 / true
	       else => 0 / false
      */

      val signA = io.data.a(31,31)
      val expA = io.data.a(30,23)
      val mantA = io.data.a(22,0)

      val signB = io.data.b(31,31)
      val expB = io.data.b(30,23)
      val mantB = io.data.b(22,0)

      when ((expA === "hff".U && mantA > 0.U) || (expB === "hff".U && mantB > 0.U)) {
        // NaNs result in zeros and an invalid op flag
        io.data.out := 0.U(32.W)
        ff(FaultFlag.NV.asUInt()) := 1.U //NaN
      } .elsewhen(expA === 0.U && mantA === 0.U && expB === 0.U && mantB === 0.U) {
        io.data.out := 1.U(32.W) 
      } .otherwise {
        when(signA === signB) {
          when (signA === 0.U) {
          // both are positive numbers
            when(expA === expB) {
              when(mantA === mantB) {
                io.data.out := 1.U(32.W)
              } .otherwise{
                io.data.out := mantA < mantB
              }
            } .otherwise {
              io.data.out := expA < expB
            }
          } .otherwise {
            // if sign for both is negative, the comparisons are reversed
            when(expA === expB) {
              when(mantA === mantB) {
                io.data.out := 1.U(32.W)
              } .otherwise{
                io.data.out := mantA > mantB
              }
            } .otherwise {
              io.data.out := expA > expB
            }
          }
        } .otherwise {
          io.data.out := signA > signB
        }
      }
    }
    is (FPUOp.FCLASS_S) {
      //FCLASS.S      Identify type of the FPU
      val flagBits = WireDefault(0.U(10.W))
      val result = WireDefault(0.U(32.W))      

      /*  
      set bit in rd  		meaning		
      0 		= 	rs1 is -inf
      1 		= 	rs1 is a negative normal number      
      2 		= 	rs1 is a negative subnormal number
      3 		= 	rs1 is -0
      4 		= 	rs1 is +0
      5 		= 	rs1 is a positive normal number      
      6 		= 	rs1 is a positive subnormal number
      7 		= 	rs1 is +inf
      8 		= 	rs1 is a signaling NaN
      9 		= 	rs1 is a quiet NaN

      type			pattern
      nNN 		= 	"b10000000????????????????????????"
      pNN 		= 	"b00000000????????????????????????" 
      nSN 		= 	"b100000000???????????????????????"
      pSN 		= 	"b000000000???????????????????????"
      nZero 	= 	"b10000000000000000000000000000000" //0x80000000
      pZero 	= 	"b00000000000000000000000000000000" //0x00000000 
      nInf 		= 	"b11111111100000000000000000000000" //0xff800000
      pInf 		= 	"b01111111100000000000000000000000" //0x7f800000
      sNaN 		= 	"b?111111110??????????????????????" //0x7fc00000
      qNaN 		= 	"b?111111111??????????????????????" //0x7fc00000
      */

      val signA = io.data.a(31,31)
      val expA = io.data.a(30,23)
      val mantA = io.data.a(22,0)

      when(signA === 1.U) {
        when(expA === 0.U) {
          when(mantA === 0.U) {
            result := flagBits.bitSet(3.U, true.B) //nZero
          } .otherwise {
            result := flagBits.bitSet(2.U, true.B) //nSN
          }
        } .elsewhen(expA === "hff".U) {
          when(mantA === 0.U) {
            result := flagBits.bitSet(0.U, true.B) //-Inf
          } .elsewhen(!io.data.a(22)) {
            result := flagBits.bitSet(8.U, true.B) //sNaN
          } .otherwise {
            result := flagBits.bitSet(9.U, true.B) //qNaN
          }
        } .otherwise {
          result := flagBits.bitSet(1.U, true.B) //nNN
        }
      } .elsewhen(signA === 0.U) {
        when(expA === 0.U) {
          when(mantA === 0.U) {
            result := flagBits.bitSet(4.U, true.B) //pZero
          } .otherwise {
            result := flagBits.bitSet(5.U, true.B) //pSN
          }
        } .elsewhen(expA === "hff".U) {
          when(mantA === 0.U) {
            result := flagBits.bitSet(7.U, true.B) //Inf
          } .elsewhen(!io.data.a(22)) {
            result := flagBits.bitSet(8.U, true.B) //sNaN
          } .otherwise {
            result := flagBits.bitSet(9.U, true.B) //qNaN
          }
        } .otherwise{
          result := flagBits.bitSet(6.U, true.B) //pNN
        }
      }
      io.data.out := result.pad(32)
    }
  }

  when (ff.asUInt() =/= 0.U) {
    io.data.fault_data := ff.asUInt()
  }
}


/** This gives you verilog */
object FPUBlock extends App with OptionParser {
  // Generate verilog
  val annos = Seq(ChiselGeneratorAnnotation(() => new FPUBlock(XLEN=32)))
  (new ChiselStage).execute(args ++ Array("--target-dir", "verilog"), annos)
}

