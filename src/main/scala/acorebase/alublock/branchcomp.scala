// SPDX-License-Identifier: Apache-2.0
// Inititally written by Verneri Hirvonen (verneri.hirvonen@aalto.fi), 2021-03-28

package acorebase

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import a_core_common._

/**
* Branch Comparator
* Uses ALU result to determine if branch is taken or not
* Also computes branch target
*
* @param XLEN
*/
class BranchComp(XLEN: Int = 32) extends Module {
  val io = IO(new Bundle {
    val alu_result    = Input(UInt(XLEN.W))
    val alu_zero      = Input(Bool())
    val funct3        = Input(UInt(3.W))
    val opcode        = Input(Opcode())
    val imm           = Input(UInt(XLEN.W))
    val pc            = Input(UInt(XLEN.W))
    val branch_taken  = Output(Bool())
    val branch_target = Output(UInt(XLEN.W))
  })
  
  io.branch_taken  := false.B
  io.branch_target := 0.U

  when (io.opcode === Opcode.BRANCH) {
    when (io.funct3 === "b000".U) {
      // BEQ, ALUOp = SUB
      io.branch_taken := io.alu_zero
    } .elsewhen (io.funct3 === "b001".U) {
      // BNE, ALUOp = SUB
      io.branch_taken := !io.alu_zero
    } .elsewhen (io.funct3 === "b100".U) {
      // BLT, ALUOp = LT
      io.branch_taken := !io.alu_zero
    } .elsewhen (io.funct3 === "b101".U) {
      // BGE, ALUOp = LT
      io.branch_taken := io.alu_zero
    } .elsewhen (io.funct3 === "b110".U) {
      // BLTU, ALUOp = LTU
      io.branch_taken := !io.alu_zero
    } .elsewhen (io.funct3 === "b111".U) {
      // BGEU, ALUOp = LTU
      io.branch_taken := io.alu_zero
    }

    io.branch_target := io.pc + io.imm
  }
}