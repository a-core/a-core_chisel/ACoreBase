// SPDX-License-Identifier: Apache-2.0

/* 
The Pipelined Parametric Division Block (designed with Chisel)
=============================================================

The general features of the pipeline (multiple parallel) division system are as follows:

- Parametrically designed. It has two general parameters:
    @XLEN: Width of input and output data
    @DIVCOUNT: How many division blocks are generated (notice it can equal maximum XLEN+2)
- The router system sets enable signals of the all division blocks. All division blocks catch input signals with their enable 
signals (@division_en inputs).
- The router waits for results in a FIFO principle. Once the busy flag of the awaited division block goes low and valid high,
the result is propagated and block_num shows which block finished.
- Normal division takes XLEN+3 clock cycles, special cases take 3 clock cycles. Anyhow, the results are provided in the same
order that they were given by the user.

The general features of the division block design are as follows:

- Parametrically designed. For example, if the parameter n is defined as 32 in the module, 32-bit division takes place.
- Designed as multi-clock. The processing time will take n+3 clock cycles. (n: divider operation bit width)
- In order to prevent possible errors that may occur in the Divider, defined an error signal (logic high/true in cases such 
as dividing the divisor by 0 and overflow).
- Designed the working logic according to an algorithm I found (described below).
- The advantage of this algorithm over other division operations is that it has a single shift register. Thus, the 
utilization values to be low.

The division algorithm works following:

1- Set the left half (MSB side) of the remainder to 0 and put the dividend on the right half (LSB side). Then make a left shift.
2- Subtract the divisor from the left half (MSB side) of the Remainder. If this result is less than zero, shift left the remainder 
and go to the next step.
3- If this result is greater than or equal to zero, write this result on the left half of the remainder and shift the remainder 
to the left. Then set the rightmost bit of the remainder to 1.
4- If you come to the last repeat (when N=WIDTH+1), take the right half of the remainder and transfer it to the quotient output. 
Shift the left half of the remainder to the right and transfer it to the remainder output.

About divider module inteface:
--Inputs:
@dividend: dividend
@divisor: divisor
@sign_en: must be activated for signal process
@division_en: must be enabled during division block operation
@clear: Resets all blocks to a known state
        
--Outputs:
@quotient: quotient
@remainder: remainder
@division_error: division error (e.g. dividing by 0 etc.)
@result_valid: if result valid, it will be set high locgic/true(as Boolean)
@division_busy: if all division blocks are busy, it will be set high locgic/true(as Boolean)
@block_num: gives block number that ensures the outputs in same clock cycle period (one-hot)

Since the design is parametric, the bit width is adjusted according to the parameters entered in dividend, divisor, quotient and 
remainder. Due to the advantage of Chisel, the module I designed can be used as a generator.

When dividend and divisor inputs are entered, divider_en_i input must be set to 1 or true for one clock cycle.
In case the operation generates an error, the output of division_error will be logic high. Valid will also be high.
When the process is complete, the result_valid output will be logic high and the results will be ready.

@author: Omer Karslioglu (omer.karslioglu@aalto.fi), 10.09.22
Modified by Aleksi Korsman (aleksi.korsman@aalto.fi), 17.11.22
*/

package acorebase

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import a_core_common._


class DivisionWrapper(val XLEN: Int = 32, val DIVCOUNT: Int = 34) extends Module{
    val in=IO(new Bundle {
        // Inputs:
        val dividend        = Input(UInt(XLEN.W))           // dividend
        val divisor         = Input(UInt(XLEN.W))           // divisor
        val sign_en         = Input(Bool())
        val division_en     = Input(Bool())                 // must be enabled when giving the dividend and divisor
        val clear           = Input(Bool())
    })
    val out=IO(new Bundle {    
        // Outputs:
        val quotient        = Output(UInt(XLEN.W))          // quotient
        val remainder       = Output(UInt(XLEN.W))          // remainder
        val division_error  = Output(Bool())                // division error (dividing by 0)
        val result_valid    = Output(Bool())                // if result valid
        val division_busy   = Output(Bool())                // division busy flag
        val block_num       = Output(UInt(DIVCOUNT.W))      // block order: BE CAREFUL THAT DIVCOUT = XLEN + 2

        val break1          = Output(UInt(DIVCOUNT.W))
    })

    // Registers:
    // One-hot encoding for the division block that is enabled next
    val division_en_r       = RegInit(1.U(DIVCOUNT.W))
    // Holds the index (in one-hot encoding) of the block that the router waits for
    val block_wait_r        = RegInit(1.U(DIVCOUNT.W)) 

    // Wires:
    val division_en_w       = Wire(UInt(DIVCOUNT.W))
    val busy_w              = Wire(UInt(DIVCOUNT.W))
    val result_valid_w      = Wire(UInt(DIVCOUNT.W))
    val block_wait_w_v      = Wire(Vec(DIVCOUNT, Bool()))
    // One-hot encoding for which block should be cleared
    val clear_w             = Wire(UInt(DIVCOUNT.W))
   
    // Sub Modules:
    val DivisionTopBlock_m = (0 until DIVCOUNT).map(x => Module(new DivisionTopBlock(WIDTH = XLEN)))
    for(i <- 0 until DIVCOUNT){ // Connections
        DivisionTopBlock_m(i).in.dividend             := in.dividend
        DivisionTopBlock_m(i).in.divisor              := in.divisor
        DivisionTopBlock_m(i).in.sign_en              := in.sign_en
        DivisionTopBlock_m(i).in.division_en          := in.division_en && division_en_w(i)
        DivisionTopBlock_m(i).in.clear                := clear_w(i)
    }

    // Assignments:
    // makes all division blocks output_result_valid signal(1.w) to one wire names (DIVCOUNT.W) - As using Cat(...) funftion to combine them:
    var i = 0
    result_valid_w          := (VecInit.fill (DIVCOUNT) {val res = DivisionTopBlock_m(i).out.result_valid; i += 1; res}).asUInt 

    // makes all division blocks division_busy signal(1.w) to one wire name (DIVCOUNT.W) - As using Cat(...) funftion to combine them
    var j = 0
    busy_w                  := (VecInit.fill (DIVCOUNT) {val res = DivisionTopBlock_m(j).out.division_busy; j += 1; res}).asUInt

    // to determine block number that represents current outputs
    out.block_num           :=  block_wait_r

    clear_w := 0.U

    // Output selector (encoder and mux system in general the schematic): 
    // Holds information which block the router is waiting for
    block_wait_w_v   := VecInit(block_wait_r.asBools)

    val cases_for_valid = block_wait_w_v.zipWithIndex.map{case (x, index) =>
        x -> DivisionTopBlock_m(index).out.result_valid    
    }

    out.result_valid := MuxCase(0.U, cases_for_valid)
    
    val cases_for_quotient  = block_wait_w_v.zipWithIndex.map{case (x, index) =>
        x -> DivisionTopBlock_m(index).out.quotient
    }
    out.quotient            :=  MuxCase(0.U, cases_for_quotient)

    val cases_for_remainder         = block_wait_w_v.zipWithIndex.map{case (x, index) =>
        x -> DivisionTopBlock_m(index).out.remainder
    }
    out.remainder           :=  MuxCase(0.U, cases_for_remainder)

    val cases_for_division_error    = block_wait_w_v.zipWithIndex.map{case (x, index) =>
        x -> DivisionTopBlock_m(index).out.division_error
    }
    out.division_error      :=  MuxCase(0.U, cases_for_division_error)    

    // Division block selection for current inputs according to busy_w to set enable signals:
    when((division_en_r & busy_w).orR){   // if next block is busy
        out.division_busy := true.B
        division_en_w     := 0.U
    }.otherwise{
        out.division_busy := false.B
        division_en_w     := division_en_r
    }
    
    // Shift division_en_r to the left
    when(in.division_en && !out.division_busy){
        // Wrap gracefully from MSB to LSB
        when(division_en_r(DIVCOUNT-1) === 1.U) {
            division_en_r := 1.U
        } .otherwise {
            division_en_r := division_en_r << 1
        }
    }

    // Start waiting for the next block when result is valid
    when(out.result_valid) {
        block_wait_r := Mux(block_wait_r(DIVCOUNT-1), 1.U, block_wait_r << 1)
        when (!in.division_en) {
            clear_w := block_wait_r
        }
    }

    // Clear all blocks
    when (in.clear) {
        clear_w := Fill(DIVCOUNT, 1.U)
    }

    out.break1 := busy_w
}


/**
  * Division Top module
  * This initiates one division block
  * Produces a result for integer division and remainder
  * Usage: Toggle division_en when providing inputs, edge triggered
  *
  * @param WIDTH Width of input and output data (Int)
  * @param useCustomDivBlock Use custom FSM based division (Boolean)
  */
class DivisionTopBlock(val WIDTH: Int = 32, val useCustomDivBlock: Boolean = true) extends Module{
    val in=IO(new Bundle {
        // Inputs:
        val dividend        = Input(UInt(WIDTH.W))      
        val divisor         = Input(UInt(WIDTH.W))      
        // Signed division
        val sign_en         = Input(Bool())              
        // must be enabled when giving the dividend and divisor
        val division_en     = Input(Bool())             
        // Reset the FSM to a known state
        val clear           = Input(Bool())
    })
    val out=IO(new Bundle {    
        // Outputs:
        val quotient        = Output(UInt(WIDTH.W))     
        val remainder       = Output(UInt(WIDTH.W))     
        // Thrown for div-by-0 and overflow cases
        val division_error  = Output(Bool())            
        val result_valid    = Output(Bool())    
        val division_busy   = Output(Bool())    
    })

    // Temporary wires for outputs
    val quotient        = WireDefault(0.U(WIDTH.W))
    val remainder       = WireDefault(0.U(WIDTH.W))
    val division_error  = WireDefault(false.B)       
    val result_valid    = WireDefault(false.B)       
    val division_busy   = WireDefault(false.B)       


    if (useCustomDivBlock) {
        // Wires:
        val quotient_sign_w                         = Wire(Bool())
        val remainder_sign_w                        = Wire(Bool())

        // Sub Modules:

        // Divison Module:
        val DivisionProcess_m                       = Module(new DivisionProcess(n = WIDTH+1))
                    
        // Sign Converter Modules:
        val SignConverterBlock1_m                   = Module(new SignConverterBlock(n = WIDTH)) // for dividend
        val SignConverterBlock2_m                   = Module(new SignConverterBlock(n = WIDTH)) // for divisor
        val SignConverterBlock3_m                   = Module(new SignConverterBlock(n = WIDTH)) // for quotient
        val SignConverterBlock4_m                   = Module(new SignConverterBlock(n = WIDTH)) // for remainder

        // Assignments:
        quotient_sign_w                             := (in.dividend(WIDTH-1) ^ in.divisor(WIDTH-1)) && in.sign_en
        remainder_sign_w                            := in.dividend(WIDTH-1) && in.sign_en

        // Connections:

        // Sign inputs:
        DivisionProcess_m.in.quotient_sign          := quotient_sign_w 
        DivisionProcess_m.in.remainder_sign         := remainder_sign_w
        DivisionProcess_m.in.divisor_sign           := in.divisor(WIDTH-1) && in.sign_en

        // for dividend: 
        SignConverterBlock1_m.in.in                 := in.dividend
        SignConverterBlock1_m.in.sign_enable        := in.dividend(WIDTH-1) && in.sign_en
        DivisionProcess_m.in.dividend               := SignConverterBlock1_m.out.out

        // for divisor:
        SignConverterBlock2_m.in.in                 := in.divisor
        SignConverterBlock2_m.in.sign_enable        := in.divisor(WIDTH-1) && in.sign_en
        DivisionProcess_m.in.divisor                := SignConverterBlock2_m.out.out

        // for quotient:
        SignConverterBlock3_m.in.in                 := DivisionProcess_m.out.quotient
        SignConverterBlock3_m.in.sign_enable        := DivisionProcess_m.out.quotient_sign

        // for remainder:
        SignConverterBlock4_m.in.in                 := DivisionProcess_m.out.remainder
        SignConverterBlock4_m.in.sign_enable        := DivisionProcess_m.out.remainder_sign

        // enable:
        DivisionProcess_m.in.division_en            := in.division_en
        DivisionProcess_m.in.clear                  := in.clear

        quotient                                := SignConverterBlock3_m.out.out
        remainder                               := Mux((DivisionProcess_m.out.remainder === 0.U), 0.U, SignConverterBlock4_m.out.out)
        division_error                          := DivisionProcess_m.out.division_error
        result_valid                            := DivisionProcess_m.out.result_valid
        division_busy                           := DivisionProcess_m.out.division_busy
    } else {
        // This has not been tested for a while
        division_busy := false.B
        when (in.division_en) {
            // Division by zero
            when (in.divisor === 0.U) {
                out.quotient       := Fill(WIDTH, 1.U)
                out.remainder      := in.dividend 
                out.division_error := true.B
                out.result_valid   := true.B
            // Overflow
            } .elsewhen (in.sign_en && 
                        (in.dividend === Cat(1.U, 0.U((WIDTH-1).W))) && 
                        (in.divisor === Fill(WIDTH, 1.U))) {

                out.quotient       := Cat(1.U, 0.U((WIDTH-1).W))
                out.remainder      := 0.U
                out.division_error := true.B
                out.result_valid   := true.B
            } .otherwise {
                val sign_a = in.dividend(WIDTH-1) && in.sign_en
                val sign_b = in.divisor(WIDTH-1) && in.sign_en
                val operand_a = Mux(sign_a, (in.dividend ^ Fill(WIDTH, 1.U)) + 1.U, in.dividend)
                val operand_b = Mux(sign_b, (in.divisor ^ Fill(WIDTH, 1.U)) + 1.U, in.divisor)
                val div_result = operand_a / operand_b
                val rem_result = operand_a % operand_b
                val sign_out = sign_a ^ sign_b
                when(rem_result === 0.U) {
                    remainder := 0.U
                } .elsewhen (sign_a) {
                    remainder := (rem_result ^ Fill(WIDTH, 1.U)) + 1.U
                } .otherwise {
                    remainder := rem_result
                }
                quotient := Mux(sign_out, (div_result ^ Fill(WIDTH, 1.U)) + 1.U, div_result)
                division_error := false.B
                result_valid := true.B
            }
        }
    }

    out.division_busy  := division_busy
    out.quotient       := quotient
    out.remainder      := remainder
    out.division_error := division_error
    out.result_valid   := result_valid
}


/**
  * Division module that uses a Finite-State Machine
  * This module should be used with DivisionTopBlock that handles the special cases (div-by-zero and overflow)
  * Usage: Toggle division_en when providing inputs, edge triggered
  *
  * @param n Numeber of bits used in shift registers. Should be one higher than size of input data.
  */
class DivisionProcess(val n: Int = 32) extends Module{
    // Inputs:
    val in=IO(new Bundle {
        val quotient_sign   = Input(Bool())
        val remainder_sign  = Input (Bool())
        val divisor_sign    = Input(Bool())
        val dividend        = Input(UInt((n-1).W))          // dividend
        val divisor         = Input(UInt((n-1).W))          // divisor
        val division_en     = Input(Bool())             // must be enabled when giving the dividend and divisor
        val clear           = Input(Bool())
    })

    // Outputs:
    val out=IO(new Bundle {
        val quotient_sign   = Output(Bool())
        val remainder_sign  = Output(Bool())
        val quotient        = Output(UInt((n-1).W))     // quotient
        val remainder       = Output(UInt((n-1).W))     // remainder
        val result_valid    = Output(Bool())            // if result valid
        val division_error  = Output(Bool())            // division error (dividing by 0)
        val division_busy   = Output(Bool())            // division busy flag
    })
    
    // Registers:
    // counter for states
    val counter_r                   = RegInit(0.U(((math.log(n)/math.log(2)).toInt + 1).W))   
    // shift register
    val remainder_r                 = RegInit(0.U((n*2).W)) 
    val result_valid_flag_r         = RegInit(0.U(1.W))
    val quotient_sign_r             = RegInit(0.U(1.W))
    val remainder_sign_r            = RegInit(0.U(1.W))
    val dividend_r                  = RegInit(0.U((n-1).W))
    val divisor_r                   = RegInit(0.U((n-1).W))
    val divisor_sign_r              = RegInit(false.B)
    val division_error_r            = RegInit(0.U(1.W))
    val busy_r                      = RegInit(0.U(1.W))      
    val en_r                        = RegInit(0.U(1.W))
    
    // Wires:
    val sub_result_w                = Wire(UInt(n.W))        
    val division_en_edge            = !en_r && in.division_en
    
    // Assignments:
    sub_result_w                    := remainder_r(((n*2)-1), n) - divisor_r  
    out.remainder_sign              := remainder_sign_r 
    out.quotient_sign               := quotient_sign_r
    out.quotient                    := remainder_r((n-1), 0)
    out.remainder                   := Cat(0.U, remainder_r(((n*2)-1), n+1))
    out.result_valid                := result_valid_flag_r
    out.division_error              := division_error_r
    out.division_busy               := busy_r            
    
    // States:
    when (in.clear) {
        counter_r               := 0.U
        remainder_r             := 0.U
        result_valid_flag_r     := 0.U
        quotient_sign_r         := 0.U
        remainder_sign_r        := 0.U
        dividend_r              := 0.U
        divisor_r               := 0.U
        divisor_sign_r          := 0.U
        division_error_r        := 0.U
        busy_r                  := 0.U
        en_r                    := 0.U
    } .elsewhen ((!en_r && !division_en_edge)) {                           
        // Hold output values until next division enable
        counter_r               := 0.U
        busy_r                  := 0.U
        en_r                    := 0.U
    }.elsewhen(counter_r === 0.U && division_en_edge){                     
        // Start division
        dividend_r              := in.dividend
        divisor_r               := in.divisor
        divisor_sign_r          := in.divisor_sign
        quotient_sign_r         := in.quotient_sign                        
        remainder_sign_r        := in.remainder_sign                       
        counter_r               := 1.U
        result_valid_flag_r     := 0.U
        division_error_r        := 0.U
        busy_r                  := 1.U
        en_r                    := 1.U
    }.elsewhen(counter_r === 1.U){
        // Handle special cases
        when (divisor_r === 0.U) {
            remainder_r         := Cat(dividend_r(n-2,0), 0.U, Fill(n, 1.U))
            division_error_r    := 1.U
            result_valid_flag_r := 1.U
            quotient_sign_r     := 0.U // To not convert in sign converter
            busy_r              := 0.U
            counter_r           := 0.U
            en_r                := 0.U
        } .elsewhen (dividend_r === Cat(1.U, 0.U((n-2).W)) && (divisor_r === 1.U) && divisor_sign_r) {
            remainder_r         := Cat(0.U(n.W), 1.U, 0.U((n-2).W))
            division_error_r    := 1.U
            result_valid_flag_r := 1.U
            quotient_sign_r     := 0.U // To not convert in sign converter
            busy_r              := 0.U
            counter_r           := 0.U
            en_r                := 0.U
        // Start processing division
        } .otherwise {
            counter_r               := counter_r + 1.U
            result_valid_flag_r     := 0.U
            division_error_r        := 0.U
            remainder_r             := Cat(0.U((n-1).W), dividend_r, 0.U(1.W))
            busy_r                  := 1.U
        }
    }.elsewhen(counter_r < ((n+2).U)){         
        // General process
        result_valid_flag_r     := 0.U
        division_error_r        := 0.U
        // If sub result is positive, shift one and concatenate
        when(sub_result_w(n-1) === 0.U){                      
            remainder_r             := Cat(sub_result_w, remainder_r((n-1), 0), 1.U)
        // If negative, shift left
        }.otherwise{              
            remainder_r             :=  remainder_r<<1 | 0.U
        }
        // Final step when counter reaches this value
        when(counter_r === ((n+1).U)){   
            result_valid_flag_r     := 1.U
            counter_r               := 0.U 
            busy_r                  := 0.U
            en_r                    := 0.U
        // Otherwise, increment counter
        }.otherwise{
            counter_r               := counter_r + 1.U
            busy_r                  := 1.U
        }
    // Undefined state
    }.otherwise{                         
        remainder_r             := 0.U
        result_valid_flag_r     := 0.U
        division_error_r        := 1.U
        quotient_sign_r         := 0.U
        remainder_sign_r        := 0.U
        counter_r               := 0.U
        busy_r                  := 1.U 
        en_r := 0.U
    }
}


// Signed converter for division process
class SignConverterBlock(val n: Int = 32) extends Module{
    // Inputs:
    val in = IO(new Bundle {
        val in              = Input(UInt(n.W))
        val sign_enable     = Input(Bool())
    })
    // Outputs:
    val out = IO(new Bundle {
        val out             = Output(UInt(n.W))
    })
    
    val interWire_w         = Wire(UInt(n.W))

    val bitmask = BigDecimal(math.pow(2,n)).setScale(0, BigDecimal.RoundingMode.FLOOR).toBigInt - 1

    // Assignments:
    interWire_w             := (in.in ^ bitmask.U) + in.sign_enable
    
    // should return interWire_w, since sign_enable_i is true
    out.out                 := Mux(in.sign_enable, interWire_w, in.in)
}


/** Generates verilog output */
object DivisionTopBlock extends App {
  val annos = Seq(ChiselGeneratorAnnotation(() => new DivisionTopBlock(
    WIDTH=32
  )))
  (new ChiselStage).execute(args, annos)
}