// SPDX-License-Identifier: Apache-2.0

package acorebase

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.experimental.BundleLiterals._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import chisel3.iotesters._
import a_core_common._


/** IO structure for the ALU module. */
class ALUIO(XLEN: Int = 32) extends Bundle {
  val op = Input(ALUOp())                 // ALU operation
  val a = Input(UInt(XLEN.W))             // operand A
  val b = Input(UInt(XLEN.W))             // operand B
  val out = Output(UInt(XLEN.W))          // computation result
  val zero = Output(Bool())               // true if output is zero
}

/** Generator for an Arithmetic Logic Unit
  *
  * @param XLEN Base ISA register width. Corresponds to ALU operand width.
  */
class ALU(val XLEN: Int = 32) extends Module {
  val io = IO(new ALUIO(XLEN=XLEN))

  io.out := DontCare

  switch (io.op) {
    is (ALUOp.ADD) {
      io.out := io.a + io.b
    }
    is (ALUOp.SUB) {
      io.out := io.a - io.b
    }
    is (ALUOp.AND) {
      io.out := io.a & io.b
    }
    is (ALUOp.OR) {
      io.out := io.a | io.b
    }
    is (ALUOp.XOR) {
      io.out := io.a ^ io.b
    }
    is (ALUOp.LT) {
      io.out := io.a.asSInt < io.b.asSInt
    }
    is (ALUOp.LTU) {
      io.out := io.a < io.b
    }
    is (ALUOp.SLL) {
      io.out := io.a << io.b(4,0)
    }
    is (ALUOp.SRL) {
      io.out := io.a >> io.b(4,0)
    }
    is (ALUOp.SRA) {
      io.out := (io.a.asSInt >> io.b(4,0)).asUInt
    }
    is (ALUOp.ADD_JALR) {
      io.out := (io.a + io.b) & (~1.U(XLEN.W))
    }
    is (ALUOp.FWD_A) {
      io.out := io.a
    }
  }

  io.zero := io.out === 0.U
}

/** Generates verilog output */
object ALU extends App {
  val annos = Seq(ChiselGeneratorAnnotation(() => new ALU(
    XLEN=32
  )))
  (new ChiselStage).execute(args, annos)
}

