// SPDX-License-Identifier: Apache-2.0

// Pipelined Multiplier module
// Inititally written by Aleksi Korsman, 2023-12-08

package acorebase

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import a_core_common._
import scala.math.{pow, BigInt, BigDecimal}

/** Pipelined multiplier with two stages.
  * The multiplier performs a 33x33 bit signed multiplication, so that it can perform 32x32 bit signed/unsigned
  * multiplication by just selecting the MSB accordingly.
  *
  * @param XLEN
  */
class MultiplierPipelined(XLEN: Int) {

  case class Stage1Inputs() extends Bundle {
    val operand_a = UInt(XLEN.W)
    val operand_b = UInt(XLEN.W)
    val op        = MultOp()
  }

  case class Stage1Outputs() extends Bundle {
    val result = SInt(((XLEN + 1) * 2).W)
    val op     = MultOp()
  }

  case class Stage2Outputs() extends Bundle {
    val result = UInt(XLEN.W)
  }

  /** First stage
    *
    * @param XLEN
    */
  class Stage1 extends Module {

    val io = IO(new Bundle {
      val in  = Input(Stage1Inputs())
      val out = ValidIO(Output(Stage1Outputs()))
    })

    override val desiredName = "Multiplier_Stage1"

    val a_sign = WireDefault(false.B)
    val b_sign = WireDefault(false.B)
    val valid  = WireDefault(false.B)

    when(io.in.op === MultOp.MUL) {
      valid  := true.B
      a_sign := false.B
      b_sign := false.B
    }.elsewhen(io.in.op === MultOp.MULH) {
      valid  := true.B
      a_sign := io.in.operand_a.head(1)
      b_sign := io.in.operand_b.head(1)
    }.elsewhen(io.in.op === MultOp.MULHU) {
      valid  := true.B
      a_sign := false.B
      b_sign := false.B
    }.elsewhen(io.in.op === MultOp.MULHSU) {
      valid  := true.B
      a_sign := io.in.operand_a.head(1)
      b_sign := false.B
    }

    val operand_a = Mux(valid, Cat(a_sign, io.in.operand_a).asSInt, 0.S)
    val operand_b = Mux(valid, Cat(b_sign, io.in.operand_b).asSInt, 0.S)

    io.out.bits.result := operand_a * operand_b
    io.out.valid       := valid
    io.out.bits.op     := io.in.op

  }

  /** Second stage
    *
    * @param XLEN
    */
  class Stage2 extends Module {

    override val desiredName = "Multiplier_Stage2"

    val io = IO(new Bundle {
      val in  = Input(Valid(Stage1Outputs()))
      val out = Output(Valid(Stage2Outputs()))
    })

    io.out.bits.result := 0.U
    io.out.valid       := io.in.valid

    when(io.in.bits.op === MultOp.MUL) {
      io.out.bits.result := io.in.bits.result(31, 0).asUInt
    }.elsewhen(io.in.bits.op.isOneOf(MultOp.MULH, MultOp.MULHU, MultOp.MULHSU)) {
      io.out.bits.result := io.in.bits.result(63, 32).asUInt
    }

  }

}
