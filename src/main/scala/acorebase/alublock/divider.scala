// SPDX-License-Identifier: Apache-2.0

// Divider wrapper module
// Inititally written by Aleksi Korsman, 2023-12-11

package acorebase

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import a_core_common._
import scala.math.{pow, BigInt, BigDecimal}

case class DividerInputs(XLEN: Int) extends Bundle {
  val operand_a = UInt(XLEN.W)
  val operand_b = UInt(XLEN.W)
  val clear     = Bool()
  val op        = MultOp()
}

case class DividerOutputs(XLEN: Int) extends Bundle {
  val result = UInt(XLEN.W)
}

/** Wrapper module for DivisionTopBlock. Division takes 30+ clock cycles to complete. This module will keep
  * `io.out.valid` low until the division is complete.
  *
  * @param XLEN
  */
class Divider(XLEN: Int) extends Module {

  val io = IO(new Bundle {

    val in = Flipped(DecoupledIO(DividerInputs(XLEN)))

    val out = ValidIO(DividerOutputs(XLEN))

  })

  val divider = Module(new DivisionTopBlock(WIDTH = XLEN, useCustomDivBlock = true))

  divider.in.clear       := false.B
  divider.in.dividend    := 0.U
  divider.in.divisor     := 0.U
  divider.in.sign_en     := false.B
  divider.in.division_en := false.B
  divider.in.clear       := divider.out.result_valid || io.in.bits.clear

  io.in.ready        := !divider.out.division_busy
  io.out.valid       := divider.out.result_valid
  io.out.bits.result := divider.out.quotient

  when(io.in.bits.op === MultOp.DIV) {
    divider.in.dividend    := io.in.bits.operand_a
    divider.in.divisor     := io.in.bits.operand_b
    divider.in.sign_en     := true.B
    divider.in.division_en := true.B
    io.out.bits.result     := divider.out.quotient
    io.out.valid           := divider.out.result_valid
  }.elsewhen(io.in.bits.op === MultOp.DIVU) {
    divider.in.dividend    := io.in.bits.operand_a
    divider.in.divisor     := io.in.bits.operand_b
    divider.in.sign_en     := false.B
    divider.in.division_en := true.B
    io.out.bits.result     := divider.out.quotient
    io.out.valid           := divider.out.result_valid
  }.elsewhen(io.in.bits.op === MultOp.REM) {
    divider.in.dividend    := io.in.bits.operand_a
    divider.in.divisor     := io.in.bits.operand_b
    divider.in.sign_en     := true.B
    divider.in.division_en := true.B
    io.out.bits.result     := divider.out.remainder
    io.out.valid           := divider.out.result_valid
  }.elsewhen(io.in.bits.op === MultOp.REMU) {
    divider.in.dividend    := io.in.bits.operand_a
    divider.in.divisor     := io.in.bits.operand_b
    divider.in.sign_en     := false.B
    divider.in.division_en := true.B
    io.out.bits.result     := divider.out.remainder
    io.out.valid           := divider.out.result_valid
  }

}
