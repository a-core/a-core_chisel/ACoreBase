// SPDX-License-Identifier: Apache-2.0

// Chisel module ACoreBase
// Inititally written by Verneri Hirvonen (verneri.hirvonen@aalto.fi), 2020-12-28
// Modified by Aleksi Korsman (aleksi.korsman@aalto.fi), 2025-0218

package acorebase

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.experimental.BundleLiterals._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import a_core_common._
import amba.axi4l._
import BusType._

/** ACoreBase IO
  * @param config
  *   ACore Config
  */
case class ACoreBaseIO(config: ACoreConfig) extends Bundle {
  // Instruction fetch AXI4-Lite port
  val ifetch = Flipped(new AXI4LIO(config.addr_width, config.data_width))
  // Core enable
  val core_en = Input(Bool())
  // Core has encountered a trap
  val core_fault = Output(Bool())
  // AXI4-Lite port for data memory
  val dmem = Flipped(new AXI4LIO(config.addr_width, config.data_width))
  // Debug interface
  val debug = if (config.debug) Some(DebugIO()) else None
  // HPM freq enable signal
  val load_indicator = Output(Bool())
}

/** A-Core Generator
  * @param config
  *   ACoreConfig configuration class
  * @param enable_rvfi
  *   Enable RV formal interface.
  */
class ACoreBase(
    config: ACoreConfig  = ACoreConfig.default,
    enable_rvfi: Boolean = false,
) extends Module {

  val io = IO(ACoreBaseIO(config))
  // RISC-V formal verification interface
  val rvfi  = if (enable_rvfi) Some(IO(RVFIIO(XLEN = config.data_width))) else None
  // Don't optimize IO ports away
  dontTouch(io)

  // ===================================================================================================
  // ================================== Code modules ===================================================
  // ===================================================================================================

  val ifetch              = Module(new IFetch            (config))
  val decode              = Module(new Decode            (config))
  val execute             = Module(new Execute           (config))
  val mem_access          = Module(new MemAccess         (config))
  val regfile_block       = Module(new RegFileBlock      (config))
  val ppl_ctrl            = Module(new PipelineController(config))
  val lsu                 = Module(new LSU               (config))

  // Convert req/gnt to AXI4L
  val ifetch_if = Module(new AXI4LMasterInterface(config.addr_width, config.data_width))

  // Pipeline registers
  val if_id_reg  = Module(new PipelineReg(new IFtoID (config.data_width, config.pc_init),      hasFlush = true))
  val id_ex_reg  = Module(new PipelineReg(new IDtoEX (config.data_width, config.default_misa), hasFlush = true))
  val ex_mem_reg = Module(new PipelineReg(new EXtoMEM(config.data_width),                      hasFlush = true))
  val mem_wb_reg = Module(new PipelineReg(new MEMtoWB(config.data_width),                      hasFlush = true))

  // Indicates that the processor has encountered an exception sometime in the past
  val fault_reg = RegInit(false.B)

  // Trap detected at write-back stage
  val trap   = Wire(Bool())

  // Forwarding data from write-back stage
  val wb_fwd = Wire(Valid(ForwardBundle(config.data_width)))

  // Debug instruction text
  val debug_instr_text = if (config.debug) Some(Module(new DebugInstrText)) else None

  // ===================================================================================================
  // ================================== Connections ====================================================
  // ===================================================================================================

  // Saves if there is a trap during execution
  when(io.core_en && trap) {
    fault_reg := true.B
  }

  io.core_fault := fault_reg

  // HPM load indicator
  io.load_indicator := execute.io.csr_out.load.load_indicator

  // Pipeline controller init signals
  ppl_ctrl.io.in.rs1_addr.getElements.foreach(_ := 0.U)
  ppl_ctrl.io.in.rs2_addr.getElements.foreach(_ := 0.U)
  ppl_ctrl.io.in.rs3_addr.getElements.foreach(_ := 0.U)
  ppl_ctrl.io.in.rs1_select.getElements.foreach(_ := RegFileSelect.INT)
  ppl_ctrl.io.in.rs2_select.getElements.foreach(_ := RegFileSelect.INT)
  ppl_ctrl.io.in.rs3_select.getElements.foreach(_ := RegFileSelect.INT)
  ppl_ctrl.io.in.forward.getElements.collect{
    case v: Valid[_] => {v.bits := {
      val a = Wire(ForwardBundle(config.data_width))
      a.data.bits := 0.U
      a.data.valid := false.B
      a.rd_addr := 0.U
      a.wr_reg_select := RegFileSelect.NONE
      a
    }; v.valid := false.B}
  }
  ppl_ctrl.io.in.ret.getElements.foreach(_ := false.B)
  ppl_ctrl.io.in.csr_wr_en.getElements.foreach(_ := false.B)
  ppl_ctrl.io.in.csr_rd_en.getElements.foreach(_ := false.B)

  // Instruction and data memory fetch ports
  io.ifetch          <> ifetch_if.io.axi4l
  io.dmem            <> lsu.io.lsu_port
  mem_access.io.dmem <> lsu.io.core

  // ===================================================================================================
  // ================================== Instruction fetch ==============================================
  // ===================================================================================================
  ifetch.io.out.ready              := if_id_reg.io.enq.ready
  ifetch.io.in.branch.valid        := execute.io.out.bits.alu.data.branch_taken && ex_mem_reg.io.enq.fire
  ifetch.io.in.branch.bits         := execute.io.out.bits.alu.data.branch_target
  ifetch.io.in.jump.valid          := id_ex_reg.io.deq.bits.jump && ex_mem_reg.io.enq.fire
  ifetch.io.in.jump.bits           := execute.io.out.bits.alu.data.result
  ifetch.io.in.mret.valid          := id_ex_reg.io.deq.bits.mret && ex_mem_reg.io.enq.fire
  ifetch.io.in.mret.bits           := execute.io.csr_out.trap.mepc
  ifetch.io.in.trap.valid          := trap
  ifetch.io.in.trap.bits           := execute.io.csr_out.trap.mtvec
  ifetch.io.in.flush               := ppl_ctrl.io.out.flush.ifetch
  ifetch.io.in.stall               := ppl_ctrl.io.out.stall.ifetch

  ifetch_if.io.clk_rst.ACLK        := clock
  ifetch_if.io.clk_rst.ARESETn     := !reset.asBool
  ifetch_if.io.core.read           <> ifetch.io.ifetch
  ifetch_if.io.core.write.req      := false.B
  ifetch_if.io.core.write.data     := 0.U
  ifetch_if.io.core.write.mask     := 0.U
  ifetch_if.io.core.write.addr     := 0.U

  if_id_reg.io.enq.bits.instr_bits := ifetch.io.out.bits.instr_bits
  if_id_reg.io.enq.bits.pc         := ifetch.io.out.bits.pc
  if_id_reg.io.enq.bits.trap_cause := ifetch.io.out.bits.trap_cause
  if_id_reg.io.enq.valid           := ifetch.io.out.valid
  if_id_reg.io.flush.get           := ppl_ctrl.io.out.flush.ifetch

  // ===================================================================================================
  // ================================== Instruction decode =============================================
  // ===================================================================================================
  if_id_reg.io.deq.ready           := decode.io.in.ready && !ppl_ctrl.io.out.stall.decode
  decode.io.in.valid               := if_id_reg.io.deq.valid
  decode.io.out.ready              := id_ex_reg.io.enq.ready && !ppl_ctrl.io.out.stall.decode
  id_ex_reg.io.enq.valid           := decode.io.out.valid && !ppl_ctrl.io.out.stall.decode

  decode.io.in.bits.instr_bits      := if_id_reg.io.deq.bits.instr_bits  
  decode.io.in.bits.rounding_mode   := execute.io.csr_out.data.rm
  
  regfile_block.io.readIn.data.rs1_addr := decode.io.out.bits.rs1
  regfile_block.io.readIn.data.rs2_addr := decode.io.out.bits.rs2
  regfile_block.io.readIn.data.rs3_addr := decode.io.out.bits.rs3
  regfile_block.io.readIn.control       := decode.io.out.bits.control.regfile_block_read

  // Register data is forwarded in case of data hazards, see PipelineController
  id_ex_reg.io.enq.bits.rs1_rdata := Mux(
    ppl_ctrl.io.out.forward_vec(0),
    ppl_ctrl.io.out.rs_forward_data_vec(0),
    regfile_block.io.readOut.data.rs1_rdata
  )
  id_ex_reg.io.enq.bits.rs2_rdata := Mux(
    ppl_ctrl.io.out.forward_vec(1),
    ppl_ctrl.io.out.rs_forward_data_vec(1),
    regfile_block.io.readOut.data.rs2_rdata
  )
  id_ex_reg.io.enq.bits.rs3_rdata := Mux(
    ppl_ctrl.io.out.forward_vec(2),
    ppl_ctrl.io.out.rs_forward_data_vec(2),
    regfile_block.io.readOut.data.rs3_rdata
  )
  id_ex_reg.io.enq.bits.instr_bits            := if_id_reg.io.deq.bits.instr_bits  
  id_ex_reg.io.enq.bits.imm                   := decode.io.out.bits.imm
  id_ex_reg.io.enq.bits.pc                    := if_id_reg.io.deq.bits.pc
  id_ex_reg.io.enq.bits.mret                  := decode.io.out.bits.control.mret
  id_ex_reg.io.enq.bits.trap_cause            := if_id_reg.io.deq.bits.trap_cause
  id_ex_reg.io.enq.bits.jump                  := decode.io.out.bits.control.jump
  id_ex_reg.io.enq.bits.rd_src                := decode.io.out.bits.control.rd_src
  id_ex_reg.io.enq.bits.regfile_write_control := decode.io.out.bits.control.regfile_block_write
  id_ex_reg.io.enq.bits.alublock_control      := decode.io.out.bits.control.alu_block
  id_ex_reg.io.enq.bits.lsu_control           := decode.io.out.bits.control.lsu
  id_ex_reg.io.enq.bits.csrblock_control      := decode.io.out.bits.control.csr_block
  id_ex_reg.io.enq.bits.muldiv_op             := decode.io.out.bits.control.muldiv_op
  id_ex_reg.io.enq.bits.events                := decode.io.out.bits.control.csr_block_event

  // Check for exceptions
  id_ex_reg.io.enq.bits.trap_cause := if_id_reg.io.deq.bits.trap_cause
  when (decode.io.out.bits.illegal_instr) {
    id_ex_reg.io.enq.bits.trap_cause.bits := ExceptionCauses.ILLEGAL_INSTR
    id_ex_reg.io.enq.bits.trap_cause.valid := true.B
  } .elsewhen (decode.io.out.bits.control.breakpoint) {
    id_ex_reg.io.enq.bits.trap_cause.bits := ExceptionCauses.BREAKPOINT
    id_ex_reg.io.enq.bits.trap_cause.valid := true.B
  }

  // ===================================================================================================
  // ================================== Execute ========================================================
  // ===================================================================================================
  id_ex_reg.io.deq.ready           := execute.io.in.ready  && !ppl_ctrl.io.out.stall.execute
  execute.io.in.valid              := id_ex_reg.io.deq.valid
  execute.io.out.ready             := ex_mem_reg.io.enq.ready && !ppl_ctrl.io.out.stall.execute
  ex_mem_reg.io.enq.valid          := execute.io.out.valid && !ppl_ctrl.io.out.stall.execute
  id_ex_reg.io.flush.get           := ppl_ctrl.io.out.flush.execute

  execute.io.in.bits.trap_cause                 := id_ex_reg.io.deq.bits.trap_cause
  execute.io.in.bits.alu_ctrl                   := id_ex_reg.io.deq.bits.alublock_control
  execute.io.in.bits.mul_op                     := id_ex_reg.io.deq.bits.muldiv_op
  execute.io.in.bits.imm                        := id_ex_reg.io.deq.bits.imm
  execute.io.in.bits.rd_src                     := id_ex_reg.io.deq.bits.rd_src
  execute.io.in.bits.wr_reg_select              := id_ex_reg.io.deq.bits.regfile_write_control.wr_reg_select
  execute.io.in.bits.pc                         := id_ex_reg.io.deq.bits.pc
  execute.io.in.bits.rs1_rdata                  := id_ex_reg.io.deq.bits.rs1_rdata
  execute.io.in.bits.rs2_rdata                  := id_ex_reg.io.deq.bits.rs2_rdata
  execute.io.in.bits.rs3_rdata                  := id_ex_reg.io.deq.bits.rs3_rdata
  execute.io.in.bits.instr_bits                 := id_ex_reg.io.deq.bits.instr_bits
  execute.io.in.bits.lsu_control                := id_ex_reg.io.deq.bits.lsu_control
  execute.io.in.bits.csr_ctrl                   := id_ex_reg.io.deq.bits.csrblock_control
  execute.io.ctrl_in.flush                      := ppl_ctrl.io.out.flush.execute

  ex_mem_reg.io.enq.bits.instr_bits            := id_ex_reg.io.deq.bits.instr_bits
  ex_mem_reg.io.enq.bits.pc                    := id_ex_reg.io.deq.bits.pc
  ex_mem_reg.io.enq.bits.rd_wdata              := execute.io.out.bits.rd_wdata
  ex_mem_reg.io.enq.bits.mul_s1_to_s2          := execute.io.out.bits.mul
  ex_mem_reg.io.enq.bits.lsu_wmask             := execute.io.out.bits.lsu_wmask
  ex_mem_reg.io.enq.bits.lsu_control           := id_ex_reg.io.deq.bits.lsu_control
  ex_mem_reg.io.enq.bits.lsu_wdata             := execute.io.out.bits.lsu_wdata
  ex_mem_reg.io.enq.bits.lsu_addr              := execute.io.out.bits.lsu_addr
  ex_mem_reg.io.enq.bits.rd_src                := id_ex_reg.io.deq.bits.rd_src
  ex_mem_reg.io.enq.bits.rs1_rdata             := id_ex_reg.io.deq.bits.rs1_rdata
  ex_mem_reg.io.enq.bits.rs2_rdata             := id_ex_reg.io.deq.bits.rs2_rdata
  ex_mem_reg.io.enq.bits.events                := id_ex_reg.io.deq.bits.events
  ex_mem_reg.io.enq.bits.trap_cause            := execute.io.out.bits.trap_cause
  ex_mem_reg.io.enq.bits.regfile_write_control := id_ex_reg.io.deq.bits.regfile_write_control
  ex_mem_reg.io.enq.bits.events                := id_ex_reg.io.deq.bits.events

  // ===================================================================================================
  // ================================== Memory Access ==================================================
  // ===================================================================================================
  ex_mem_reg.io.deq.ready           := mem_access.io.in.ready
  mem_access.io.in.valid            := ex_mem_reg.io.deq.valid
  mem_access.io.out.ready           := mem_wb_reg.io.enq.ready
  mem_wb_reg.io.enq.valid           := mem_access.io.out.valid
  ex_mem_reg.io.flush.get           := ppl_ctrl.io.out.flush.memaccess1

  mem_access.io.in.bits.lsu.wdata             := ex_mem_reg.io.deq.bits.lsu_wdata
  mem_access.io.in.bits.lsu.wmask             := ex_mem_reg.io.deq.bits.lsu_wmask
  mem_access.io.in.bits.lsu.addr              := ex_mem_reg.io.deq.bits.lsu_addr
  mem_access.io.in.bits.lsu.control           := ex_mem_reg.io.deq.bits.lsu_control
  mem_access.io.in.bits.mul                   := ex_mem_reg.io.deq.bits.mul_s1_to_s2
  mem_access.io.in.bits.rd_src                := ex_mem_reg.io.deq.bits.rd_src
  mem_access.io.in.bits.instr_bits            := ex_mem_reg.io.deq.bits.instr_bits  
  mem_access.io.in.bits.pc                    := ex_mem_reg.io.deq.bits.pc
  mem_access.io.in.bits.trap_cause            := ex_mem_reg.io.deq.bits.trap_cause
  mem_access.io.in.bits.rd_wdata              := ex_mem_reg.io.deq.bits.rd_wdata
  mem_access.io.in.bits.regfile_write_control := ex_mem_reg.io.deq.bits.regfile_write_control
  mem_access.io.in.bits.events                := ex_mem_reg.io.deq.bits.events
  mem_access.io.ctrl_in.stall                 := ppl_ctrl.io.out.stall.memaccess1
  mem_access.io.ctrl_in.flush                 := ppl_ctrl.io.out.flush.memaccess1

  mem_wb_reg.io.enq.bits.instr_bits            := mem_access.io.out.bits.instr_bits
  mem_wb_reg.io.enq.bits.pc                    := mem_access.io.out.bits.pc
  mem_wb_reg.io.enq.bits.rd_wdata              := mem_access.io.out.bits.rd_wdata
  mem_wb_reg.io.enq.bits.trap_cause            := mem_access.io.out.bits.trap_cause
  mem_wb_reg.io.enq.bits.events                := mem_access.io.out.bits.events
  mem_wb_reg.io.enq.bits.regfile_write_control := mem_access.io.out.bits.regfile_write_control

  // ===================================================================================================
  // ================================== Write Back =====================================================
  // ===================================================================================================
  // There is nothing that could stall Write-Back
  mem_wb_reg.io.deq.ready := true.B
  mem_wb_reg.io.flush.get := ppl_ctrl.io.out.flush.writeback

  trap                      := mem_wb_reg.io.deq.bits.trap_cause.valid && mem_wb_reg.io.deq.valid
  wb_fwd.valid              := mem_wb_reg.io.deq.valid
  wb_fwd.bits.data          := mem_wb_reg.io.deq.bits.rd_wdata
  wb_fwd.bits.rd_addr       := riscv_util.getRd(mem_wb_reg.io.deq.bits.instr_bits)
  wb_fwd.bits.wr_reg_select := mem_wb_reg.io.deq.bits.regfile_write_control.wr_reg_select

  regfile_block.io.writeIn.data.rd_wdata         := mem_wb_reg.io.deq.bits.rd_wdata.bits
  regfile_block.io.writeIn.data.rd_waddr         := riscv_util.getRd(mem_wb_reg.io.deq.bits.instr_bits)

  execute.io.csr_trap_in.mcause     := mem_wb_reg.io.deq.bits.trap_cause.bits.asUInt
  execute.io.csr_trap_in.mepc       := mem_wb_reg.io.deq.bits.pc
  execute.io.csr_trap_in.trap       := mem_wb_reg.io.deq.bits.trap_cause.valid && mem_wb_reg.io.deq.valid
  execute.io.csr_event_in.fflags_en := false.B // TODO 
  execute.io.csr_event_in.instret   := mem_wb_reg.io.deq.valid && !trap

  // Modify state only when values are valid and there was no trap
  when (mem_wb_reg.io.deq.valid && !trap) {
    execute.io.csr_event_in                        := mem_wb_reg.io.deq.bits.events
    regfile_block.io.writeIn.control.wr_reg_select := mem_wb_reg.io.deq.bits.regfile_write_control.wr_reg_select
  } .otherwise {
    execute.io.csr_event_in.getElements.foreach(_ := false.B)
    regfile_block.io.writeIn.control.wr_reg_select := RegFileSelect.NONE
  }

  // Core enable signal - if this is false, all stages are stalled
  ppl_ctrl.io.in.core_en := io.core_en

  // Pipeline Controller connections for register forwarding
  ppl_ctrl.io.in.rs1_addr.decode           := decode.io.out.bits.rs1
  ppl_ctrl.io.in.rs2_addr.decode           := decode.io.out.bits.rs2
  ppl_ctrl.io.in.rs3_addr.decode           := decode.io.out.bits.rs3
  ppl_ctrl.io.in.rs1_select.decode         := decode.io.out.bits.control.regfile_block_read.rs1_out_src
  ppl_ctrl.io.in.rs2_select.decode         := decode.io.out.bits.control.regfile_block_read.rs2_out_src
  ppl_ctrl.io.in.rs3_select.decode         := RegFileSelect.FLOAT
  ppl_ctrl.io.in.forward.execute.bits      := execute.io.out.bits.forward
  ppl_ctrl.io.in.forward.execute.valid     := execute.io.out.valid
  ppl_ctrl.io.in.forward.memaccess0.bits   := mem_access.io.forward_0.bits
  ppl_ctrl.io.in.forward.memaccess0.valid  := mem_access.io.forward_0.valid
  ppl_ctrl.io.in.forward.memaccess1.bits   := mem_access.io.forward_1.bits
  ppl_ctrl.io.in.forward.memaccess1.valid  := mem_access.io.forward_1.valid
  ppl_ctrl.io.in.forward.writeback.bits    := wb_fwd.bits
  ppl_ctrl.io.in.forward.writeback.valid   := wb_fwd.valid
  ppl_ctrl.io.in.branch                    := execute.io.out.bits.alu.data.branch_taken && ex_mem_reg.io.enq.fire
  ppl_ctrl.io.in.jump                      := id_ex_reg.io.deq.bits.jump && ex_mem_reg.io.enq.fire
  ppl_ctrl.io.in.trap                      := trap
  ppl_ctrl.io.in.mret                      := id_ex_reg.io.deq.bits.mret && ex_mem_reg.io.enq.fire
  ppl_ctrl.io.in.csr_wr_en.execute         := id_ex_reg.io.deq.bits.csrblock_control.write_en
  ppl_ctrl.io.in.csr_rd_en.execute         := id_ex_reg.io.deq.bits.csrblock_control.read_en
  ppl_ctrl.io.in.ret.execute               := execute.io.out.valid
  ppl_ctrl.io.in.ret.memaccess0            := mem_access.io.in.valid
  ppl_ctrl.io.in.ret.memaccess1            := mem_access.io.out.valid
  ppl_ctrl.io.in.ret.writeback             := mem_wb_reg.io.deq.valid
  

  // Debug connections
  if (config.debug) {
    debug_instr_text.get.io.instr := Instruction.ADD //decode.io.decoder.instr
    io.debug.get.instruction_text := debug_instr_text.get.io.instr_text
  }


  // ===================================== RISC-V Formal Interface ======================================

  // TODO - this requires propagating a lot of signals, that are not otherwise
  // needed, to WB stage
  // Create a bundle in pipelineregs that clearly indicates that these signals are
  // needed only for rvfi, and maybe even instantiate them only when a parameter is set
  /*
  if (enable_rvfi) {
    rvfi.get.valid  := mem_wb_reg.ret
    rvfi.get.order  := execute.io.csr_out.data.minstret
    rvfi.get.insn   := mem_wb_reg.instr_bits  
    rvfi.get.trap   := mem_wb_reg.trap_cause.valid
    rvfi.get.halt   := 0.U
    rvfi.get.intr   := 0.U      // Do we need this?
    rvfi.get.mode   := 3.U      // machine mode
    rvfi.get.ixl    := 1.U      // 32-bit

    val regfile_wr_en = mem_wb_reg.regfile_write_control.wr_reg_select.isOneOf(Seq(RegFileSelect.INT, RegFileSelect.FLOAT))
    rvfi.get.rs1_addr   := riscv_util.getRs1(mem_wb_reg.instr_bits)
    rvfi.get.rs2_addr   := riscv_util.getRs2(mem_wb_reg.instr_bits)
    rvfi.get.rd_addr    := Mux(
      regfile_wr_en, 
      riscv_util.getRd(mem_wb_reg.instr_bits  ),
      0.U
    )
    rvfi.get.rd_wdata   := Mux(regfile_wr_en, mem_wb_reg.rd_wdata, 0.U)
    rvfi.get.rs1_rdata  := mem_wb_reg.rs1_rdata
    rvfi.get.rs2_rdata  := mem_wb_reg.rs2_rdata

    rvfi.get.pc_rdata := mem_wb_reg.pc
    rvfi.get.pc_wdata := mem_wb_reg.pc + 4.U

    rvfi.get.mem_addr   := mem_wb_reg.alu_data.result
    rvfi.get.mem_rmask  := "b0000".U
    val funct3 = riscv_util.getFunct3(mem_wb_reg.instr_bits)
    when (mem_wb_reg.lsu_control.req_info.ren) {
      when (funct3 === 0.U) {
        rvfi.get.mem_rmask  := "b0001".U
      } .elsewhen(funct3 === 1.U) {
        rvfi.get.mem_rmask  := "b0011".U
      } .elsewhen(funct3 === 2.U) {
        rvfi.get.mem_rmask  := "b1111".U
      }
    }
    rvfi.get.mem_wmask  := "b0000".U
    when (mem_wb_reg.lsu_control.req_info.wen) {
      when (funct3 === 0.U) {
        rvfi.get.mem_wmask  := "b0001".U
      } .elsewhen(funct3 === 1.U) {
        rvfi.get.mem_wmask  := "b0011".U
      } .elsewhen(funct3 === 2.U) {
        rvfi.get.mem_wmask  := "b1111".U
      }
    }
    rvfi.get.mem_rdata  := mem_wb_reg.rd_wdata
    rvfi.get.mem_wdata  := mem_wb_reg.rs2_rdata
  }
*/
}

/** Generates verilog */
object ACoreBase extends App with OptionParser {

  // Parse command-line arguments
  val default_opts: Map[String, String] = Map(
    "-core_config" -> "",
    "-isa_string"  -> "",
    "-debug"       -> "",
    "-rvfi"        -> "false"
  )

  val help = Seq(
    ("-core_config", "String",  "A-Core configuration YAML file."),
    ("-isa_string",  "String",  "Manually set isa_string (e.g. \"rv32imfc\")."),
    ("-debug",       "Boolean", "Enable debug signals."),
    ("-rvfi",        "Boolean", "Enable RISC-V Formal Interface. Default \"false\"")
  )

  val (options, arguments) = getopts(default_opts, args.toList, help)

  val core_config_file = options("-core_config")

  val core_config: ACoreConfig = core_config_file match {
    case "" => ACoreConfig.default
    case c  => ACoreConfig.loadFromFile(c)
  }

  val isa_string = options("-isa_string")

  if (isa_string != "") {
    core_config.extension_set = ACoreConfig.parseExtensionSet(isa_string)
  }

  val debug = options("-debug")
  if (debug != "") {
    core_config.debug = debug.toBoolean
  }

  core_config.validate()

  println(core_config)

  // Generate verilog
  val annos = Seq(ChiselGeneratorAnnotation(() =>
    new ACoreBase(
      config = core_config,
      enable_rvfi = options("-rvfi").toBoolean
    )
  ))

  (new ChiselStage).execute(arguments.toArray, annos)
}
