// SPDX-License-Identifier: Apache-2.0

// Initially written by Aleksi Korsman (aleksi.korsman@aalto.fi), 2023-03-21
package acorebase

import chisel3._
import chisel3.util._
import chisel3.experimental._
import a_core_common._
import amba.common._

/**
  * Cache bypass module. Checks whether cached data can be used for a memory address.
  *
  * @param memory_map Memory map
  * @param config ACoreConfig
  */
class CacheBypass (memory_map: Map[MemoryRange, _], config: ACoreConfig) extends Module {
  val io = IO(new Bundle {
    val addr = Input(UInt(config.addr_width.W))
    val cached = Output(Bool())
  })

  io.cached := false.B

  for (mappings <- memory_map) {
    val range = mappings._1
    when (io.addr >= range.begin.U && io.addr <= range.end.U) {
      io.cached := range.cached.asBool
    }
  }

}

