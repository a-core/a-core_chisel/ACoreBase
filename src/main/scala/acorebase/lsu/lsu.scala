// SPDX-License-Identifier: Apache-2.0

// Initially written by Aleksi Korsman (aleksi.korsman@aalto.fi), 2023-03-21
package acorebase

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.experimental.dataview._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import a_core_common._
import amba.axi4l._
import amba.common._
import acorebase._
import BusType._

case class LSUIO(config: ACoreConfig) extends Bundle with LSUPort {
  val lsu_port = config.bus_type match {
    case BusType.AXI4L => 
      AXI4L_LSUPort(config.addr_width, config.data_width).lsu_port
    case BusType.WishboneB4 =>
      ???
    case _ =>
      ???
  }
  val core = MemoryInterface(config.addr_width, config.data_width)
}

class LSU (config: ACoreConfig, formal: Boolean = false) extends Module {

  val io = IO(LSUIO(config))

  val lsu = config.bus_type match {
    case BusType.AXI4L =>
      Module(new LSU_AXI4L(config))
    case BusType.WishboneB4 =>
      ???
    case _ =>
      ???
  }

  lsu.io.ACLK    := clock
  lsu.io.ARESETn := !(reset.asBool)

  if (config.dCache.option == CacheOption.NONE) {
    lsu.io.core <> io.core
    io.lsu_port <> lsu.io.lsu_port
  } else if (config.dCache.option == CacheOption.ALWAYS_ON) {
    ???
    /*
    val cache = Module(new DataCache(memory_map, config))

    cache.io.mem.mem_if <> lsu.io.core

    io.core.gnt         := cache.io.core.mem_if.gnt
    io.core.wr_fault    := cache.io.core.mem_if.wr_fault
    io.core.rd_fault    := cache.io.core.mem_if.rd_fault
    io.core.fault       := cache.io.core.mem_if.fault
    io.core.rdata.valid := cache.io.core.mem_if.rdata.valid

    cache.io.core.mem_if.req := (io.core.req && !(rd_misalig || wr_misalig))
    cache.io.core.mem_if.atomic_req := io.core.atomic_req

    cache.io.core.mem_if.info.wdata       := masking_unit.io.out.mem.wdata
    cache.io.core.mem_if.info.wmask       := masking_unit.io.out.mem.wmask
    cache.io.core.mem_if.info.wen         := io.core.info.wen
    cache.io.core.mem_if.info.ren         := io.core.info.ren
    cache.io.core.mem_if.info.addr        := io.core.info.addr
    cache.io.core.mem_if.info.op_width    := MemOpWidth.WORD
    cache.io.core.mem_if.info.op_unsigned := false.B

    lsu.io.core.req        := cache.io.mem.mem_if.req
    lsu.io.core.info.ren   := cache.io.mem.mem_if.info.ren
    lsu.io.core.info.wen   := cache.io.mem.mem_if.info.wen
    lsu.io.core.info.addr  := cache.io.mem.mem_if.info.addr
    lsu.io.core.info.wdata := cache.io.mem.mem_if.info.wdata
    lsu.io.core.info.wmask := cache.io.mem.mem_if.info.wmask

    masking_unit.io.in.rdata := cache.io.core.mem_if.rdata.bits
    */
  }

  if (formal) {
    val formal_module = Module(new LSUFormal(config))
    val last_info = RegEnable(io.core.info, io.core.gnt)
    val port_type = new AXI4LIO(config.addr_width, config.data_width)
    formal_module.io.core                  := io.core
    formal_module.io.last_info             := last_info
    formal_module.io.axi4l_aclk            := clock
    formal_module.io.axi4l_aresetn         := !reset.asBool
    formal_module.io.axi4l.viewAs[AXI4LIO] := io.lsu_port.asTypeOf(port_type)
  }

}

/* AXI4-Lite Manager Formal Properties */
class LSUFormal(config: ACoreConfig) extends BlackBox with HasBlackBoxResource {
  val io = IO(new Bundle {
    val core          = Input(MemoryInterface(config.addr_width, config.data_width))
    val last_info     = Input(MemRequest(config.addr_width, config.data_width))
    val axi4l_aclk    = Input(Clock())
    val axi4l_aresetn = Input(Bool())
    val axi4l         = Input(new VerilogAXI4LIO(config.addr_width, config.data_width))
  })
  addResource("/LSUFormal.v")
}

/** This gives you verilog */
object LSU extends App with OptionParser {
  val defaultOpts: Map[String, String] = Map(
    "-addr_width"->"32",
    "-data_width"->"32",
    "-formal"->"false",
  )
  val help = Seq(
    ("-addr_width", "Int", "AXI4-Lite address bus width."),
    ("-data_width", "Int", "AXI4-Lite data bus width."),
    ("-formal", "Boolean", "Include formal assertions in generated verilog."),
  )
  // Parse command-line arguments
  val (options, arguments) = getopts(defaultOpts, args.toList, help)
  printopts(options, arguments)

  val addr_width = options("-addr_width").toInt
  val data_width = options("-data_width").toInt
  val formal = options("-formal").toBoolean
  val config = ACoreConfig.default
  config.addr_width = addr_width
  config.data_width = data_width
  config.dCache.option = CacheOption.NONE
  val annos = Seq(ChiselGeneratorAnnotation(() => new LSU(config, formal)))

  (new ChiselStage).execute(arguments.toArray, annos)
}