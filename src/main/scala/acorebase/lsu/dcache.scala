// SPDX-License-Identifier: Apache-2.0

// Chisel module Data cache
// Initially written by Aleksi Korsman (aleksi.korsman@aalto.fi), 2023-03-21

package acorebase

import chisel3._
import chisel3.util._
import chisel3.experimental._
import amba.common._
import a_core_common._

object CCState extends chisel3.ChiselEnum{
    val IDLE, COMPARE, WRITE, READ_BYPASS, ALLOCATE = Value
}
import CCState._


case class DataCacheCore(config: ACoreConfig) extends Bundle {
  val mem_if = MemoryInterface(config.addr_width, config.data_width)
}
case class DataCacheMemory(config: ACoreConfig) extends Bundle {
  val mem_if = MemoryInterface(config.addr_width, config.data_width)
}

case class DataCacheIO(config: ACoreConfig) extends Bundle {
  val core = DataCacheCore(config)
  val mem = Flipped(DataCacheMemory(config))
}

class DataCache(memory_map: Map[MemoryRange, _], config: ACoreConfig) extends Module {
  val io = IO(DataCacheIO(config))
  ???

  /*
  val cache_bypass_block = Module(new CacheBypass(memory_map, config))
  val mem_req_block = Module(new SeqMemRequest(config))


  io.core.mem_if.gnt := false.B
  io.core.mem_if.rd_fault := false.B
  io.core.mem_if.rd_misalig := false.B
  io.core.mem_if.wr_fault := false.B
  io.core.mem_if.wr_misalig := false.B
  io.core.mem_if.fault := false.B
  io.core.mem_if.rdata.bits := 0.U
  io.core.mem_if.rdata.valid := false.B
  io.mem.mem_if.req := false.B
  io.mem.mem_if.info.wmask := "b1111".U
  io.mem.mem_if.info.addr := 0.U
  io.mem.mem_if.info.ren := false.B
  io.mem.mem_if.info.wen := false.B
  io.mem.mem_if.info.wdata := 0.U
  io.mem.mem_if.info.op_unsigned := false.B
  io.mem.mem_if.info.op_width := MemOpWidth.WORD
  io.mem.mem_if.atomic_req := false.B // AXI side doesn't care about atomic ops yet

  /* Addres for reading from cache */
  val cacheReadAddr = io.core.mem_if.info.addr(config.addr_width-1, log2Ceil(config.dCache.line_depth) + 2)
  /** Store address to a register */
  val storeReadAddr = WireDefault(false.B)
  /* Register for storing read address */
  val readAddrReg = RegEnable(io.core.mem_if.info.addr, storeReadAddr)
  /* Cache address from register */
  val cacheReadAddrReg = readAddrReg(config.addr_width-1, log2Ceil(config.dCache.line_depth) + 2)
  /* Tag from register */
  val tagReg = readAddrReg(config.addr_width-1, log2Ceil(config.dCache.depth)+log2Ceil(config.dCache.line_depth)+2)
  /* Tag index from register */
  val tagIdxReg = readAddrReg(log2Ceil(config.dCache.depth)+log2Ceil(config.dCache.line_depth)+1,2+log2Ceil(config.dCache.line_depth))
  /* Cache line read index */
  val readIdx = {if (config.dCache.line_depth == 1) 0.U 
                 else io.core.mem_if.info.addr(log2Ceil(config.dCache.line_depth)+1, 2)}
  /* Register for storing cache line read index */
  val readIdxReg = RegEnable(readIdx, storeReadAddr)

  mem_req_block.io.start := false.B
  mem_req_block.io.gnt := io.mem.mem_if.gnt
  mem_req_block.io.cache_addr := cacheReadAddrReg
  mem_req_block.io.rdata <> io.mem.mem_if.rdata


  val state = RegInit(CCState.COMPARE)

  val cacheSize = config.dCache.depth
  val lineSize = config.dCache.line_depth

  val tagMemoryWidth = 32 // TODO: calculate from input parameters. Round to nearest pow2.
  val tagLowIdx = 0
  val tagHighIdx = config.addr_width - log2Ceil(config.dCache.depth) - log2Ceil(config.dCache.line_depth) - 3

  val validBits = RegInit(VecInit.fill(cacheSize) {false.B})
  val reservedBits = RegInit(VecInit.fill(cacheSize) {0.U(lineSize.W)})

  val tagMemory = config.dCache.tag_memory_type match {
    case MemoryType.ASYNC =>
      Mem(cacheSize, UInt(tagMemoryWidth.W))
    case MemoryType.SYNC =>
      SyncReadMem(cacheSize, UInt(tagMemoryWidth.W))
    case _ =>
      ???
  }

  val cacheWidth = config.data_width
  val cacheMemory = config.dCache.cache_memory_type match {
    case MemoryType.ASYNC =>
      Seq.fill(config.dCache.line_depth)(Mem(cacheSize, UInt(cacheWidth.W)))
    case MemoryType.SYNC =>
      Seq.fill(config.dCache.line_depth)(SyncReadMem(cacheSize, UInt(cacheWidth.W)))
    case _ =>
      ???
  }

  val tagIdx = io.core.mem_if.info.addr(log2Ceil(config.dCache.depth)+log2Ceil(config.dCache.line_depth)+1,2+log2Ceil(config.dCache.line_depth))
  val tag    = io.core.mem_if.info.addr(config.addr_width-1, log2Ceil(config.dCache.depth)+log2Ceil(config.dCache.line_depth)+2)

  val tagWriteEn = WireDefault(false.B)

  val tagMemOut = tagMemory.read(tagIdx)

  when (tagWriteEn) {
    tagMemory.write(tagIdxReg, tagReg)
    validBits(tagIdxReg) := true.B
  }

  val readTag   = tagMemOut(tagHighIdx, tagLowIdx)
  val validBit  = validBits(tagIdx)

  val tagMatch = (readTag === tag) && validBit

  /** Cache hit (only for read requests on cached memory ranges) */
  val hit = WireDefault(false.B)
  dontTouch(hit)

  cache_bypass_block.io.addr := io.core.mem_if.info.addr

  val readReq = io.core.mem_if.req && io.core.mem_if.info.ren // && io.core.mem_if.gnt

  val rdata = VecInit.fill(config.dCache.line_depth){0.U(config.data_width.W)}
  val cacheWen = WireDefault(false.B)
  val cacheWdata = WireDefault(0.U)
  val cacheWidx = WireDefault(0.U)

  for (i <- (0 until config.dCache.line_depth)) {
    rdata(i) := cacheMemory(i).read(cacheReadAddr)

    when (cacheWen && cacheWidx === i.U) {
      cacheMemory(i).write(cacheReadAddrReg, cacheWdata)
    }
  }

  /** Select signals for next state */
  val stateSelect = VecInit(readReq,
                            tagMatch,
                            cache_bypass_block.io.cached)
  /* Mux for selecting next state during COMPARE state */
  val stateMux = MuxCase(CCState.COMPARE, 
                         Seq((stateSelect(0) === 0.U)
                                -> state,
                            ((stateSelect(0) === 1.U) && (stateSelect(1) === 0.U) && (stateSelect(2) === 1.U)) 
                                -> CCState.ALLOCATE,
                            ((stateSelect(0) === 1.U) && (stateSelect(2) === 0.U))                             
                                -> CCState.READ_BYPASS))


  /** Start signal for memory request block. True by default */
  val seqMemReqStartReg = Reg(Bool())
  seqMemReqStartReg := true.B

  io.core.mem_if.fault    := io.mem.mem_if.fault
  io.core.mem_if.rd_fault := io.mem.mem_if.rd_fault
  io.core.mem_if.wr_fault := io.mem.mem_if.wr_fault

  when (io.mem.mem_if.fault) {
    mem_req_block.reset := true.B
  }

  val rdataNext = WireDefault(0.U)
  val hitNext = WireDefault(false.B)

  io.core.mem_if.rdata.bits   := rdata(readIdxReg)
  io.core.mem_if.rdata.valid  := RegNext(hitNext)

  when (state === CCState.COMPARE) {
    // TODO: separate between SYNC and ASYNC cache mem
    hitNext            := hit
    io.core.mem_if.gnt := io.core.mem_if.req
    storeReadAddr := true.B
    hit := tagMatch && validBit && io.core.mem_if.req && io.core.mem_if.info.ren && cache_bypass_block.io.cached
    state := stateMux
  } .elsewhen (state === CCState.ALLOCATE) {
    // If busy, send the start signal again
    seqMemReqStartReg := seqMemReqStartReg && mem_req_block.io.busy
    mem_req_block.io.start := seqMemReqStartReg

    // Connect memory request signals to memory interface
    io.mem.mem_if.req := mem_req_block.io.req
    io.mem.mem_if.info.ren := mem_req_block.io.ren
    io.mem.mem_if.info.addr := mem_req_block.io.addr

    // Signals for writing to cache
    cacheWdata := mem_req_block.io.cache_wdata
    cacheWen   := mem_req_block.io.cache_wen
    cacheWidx  := mem_req_block.io.cache_widx

    // Provide data when available
    when ((cacheWidx === readIdxReg) && cacheWen) {
      io.core.mem_if.rdata.bits := mem_req_block.io.cache_wdata
      io.core.mem_if.rdata.valid := true.B
    }

    when ((cacheWidx === ((config.dCache.line_depth-1)).U) && cacheWen) {
      state := CCState.COMPARE
      tagWriteEn := true.B
    }

  } .elsewhen (state === CCState.READ_BYPASS) {
    io.mem.mem_if.req := true.B
    io.mem.mem_if.info.ren := true.B
    io.mem.mem_if.info.addr := Cat(cacheReadAddrReg, readIdxReg, 0.U(2.W))
    
    io.core.mem_if.rdata <> io.mem.mem_if.rdata
    when (io.mem.mem_if.rdata.valid) {
      state := CCState.COMPARE
    }
  }

  val writeReq = io.core.mem_if.info.wen && io.core.mem_if.req

  if (config.dCache.write_policy == WritePolicy.WRITE_THROUGH) {
    when (writeReq && state === CCState.COMPARE) {
      io.mem.mem_if.info <> io.core.mem_if.info
      io.mem.mem_if.req  := io.core.mem_if.req
      io.core.mem_if.gnt := io.mem.mem_if.gnt
      when (tagMatch) {
        validBit := false.B
      }
    }

    if (config.extension_set.contains(ISAExtension.A)) {
      dontTouch(reservedBits)

      val atomicReadReq = readReq && io.core.mem_if.atomic_req
      when (atomicReadReq && io.core.mem_if.gnt) {
        // clear old reservation on other cache lines
        for (i <- (0 until cacheSize)) {
          reservedBits(i) := 0.U(lineSize.W)
        }
        // set reserved bit for current atomic load
        reservedBits(tagIdx) := 1.U << readIdx
      }

      val atomicWriteReq = writeReq && io.core.mem_if.atomic_req
      val atomicWriteReqReg = RegNext(atomicWriteReq)

      // check if corresponding reserved bit is set
      val atomicWriteGnt = reservedBits(tagIdx)(readIdx)
      
      // Block requests to memory when reserved bit is not set
      when (atomicWriteReq && !atomicWriteGnt) {
        io.mem.mem_if.req := false.B
      }
      
      val storeCondStatus = WireDefault(UInt(32.W), !atomicWriteGnt) // zero = success
      val storeCondStatusReg = RegNext(storeCondStatus)
      
      // Normal store to reserved address clears reservation
      when (writeReq && tagMatch) {
        reservedBits(tagIdx) := reservedBits(tagIdx) & ~(1.U << readIdx)
      }

      when (atomicWriteReq && state === CCState.COMPARE) {
        // Immediately acknowledge failing conditional stores
        io.core.mem_if.gnt := Mux(!atomicWriteGnt, true.B, io.mem.mem_if.gnt)

        // SC.W clears all previous reservations
        for (i <- (0 until cacheSize)) {
          reservedBits(i) := 0.U(lineSize.W)
        }
      }

      // Return store condition through read data bus
      val writeGntReg = RegNext(io.core.mem_if.gnt)
      when (writeGntReg && atomicWriteReqReg) {
        io.core.mem_if.rdata.bits := storeCondStatusReg
        io.core.mem_if.rdata.valid := true.B
      }
    }
  }
  */

}

