// SPDX-License-Identifier: Apache-2.0

// Initially written by Verneri Hirvonen (verneri.hirvonen@aalto.fi), 2022-2-02-25
// Modified by Aleksi Korsman (aleksi.korsman@aalto.fi), 2023-03-21
package acorebase

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import a_core_common._
import acorebase._
import amba.axi4l._
import scala.math.log

case class AXI4L_LSUPort(addr_width: Int, data_width: Int) extends Bundle with LSUPort {
  val lsu_port = Flipped(new AXI4LIO(addr_width, data_width))
}

case class AXI4L_LSUIO(config: ACoreConfig) extends Bundle with LSUPort {
  // AXI Clock and Reset
  val ACLK    = Input(Clock())
  // FIXME: AXI should have an async reset
  //        https://gitlab.com/a-core/a-core_chisel/amba/-/issues/1
  // val ARESETn = Input(AsyncReset())
  val ARESETn = Input(Bool())
  val lsu_port = AXI4L_LSUPort(config.addr_width, config.data_width).lsu_port
  val core = MemoryInterface(config.addr_width, config.data_width)
}

/** Load Store Unit that uses the AXI4-Lite protocol
  * @param addr_width AXI4-Lite address bus width.
  * @param data_width AXI4-Lite data bus width.
  * @param formal Include BlackBoxed verilog formal properties.
  */
class LSU_AXI4L(config: ACoreConfig) extends RawModule {
  val io = IO(new AXI4L_LSUIO(config))

  val axi4lm = Module(new AXI4LMasterInterface(config.addr_width, config.data_width, with_wresp = true))
  axi4lm.io.axi4l <> io.lsu_port
  axi4lm.io.clk_rst.ARESETn := io.ARESETn
  axi4lm.io.clk_rst.ACLK := io.ACLK

  io.core.gnt := axi4lm.io.core.read.gnt || axi4lm.io.core.write.gnt
  io.core.rresp.bits.fault := axi4lm.io.core.read.fault
  io.core.rresp.bits.data  := axi4lm.io.core.read.data.bits
  io.core.rresp.valid      := axi4lm.io.core.read.data.valid
  io.core.wresp.bits.fault := axi4lm.io.core.write.fault
  io.core.wresp.valid      := axi4lm.io.core.write.resp.get
  axi4lm.io.core.read.req := io.core.req && !io.core.info.wen
  axi4lm.io.core.write.req := io.core.req && io.core.info.wen
  axi4lm.io.core.write.mask := io.core.info.wmask
  axi4lm.io.core.write.addr := Cat(io.core.info.addr(config.addr_width-1, 2), 0.U(2.W))
  axi4lm.io.core.write.data := io.core.info.wdata
  axi4lm.io.core.read.addr := io.core.info.addr

  // AXI4-Lite interface
  dontTouch(io.lsu_port)


  def log2(x: Int): Int = {
    (log(x)/log(2)).toInt
  }
}

/* AXI4-Lite Manager Formal Properties */
class LSU_AXI4LFormal(addr_width: Int, data_width: Int) extends BlackBox(
  Map("ADDR_WIDTH" -> addr_width, "DATA_WIDTH" -> data_width)
) with HasBlackBoxResource {
  val io = IO(new Bundle {
    // Core Interface
    val req         = Input(Bool())
    val gnt         = Input(Bool())
    val ren         = Input(Bool())
    val wen         = Input(Bool())
    val op_width    = Input(UInt(2.W)) 
    val op_unsigned = Input(Bool())
    val addr        = Input(UInt(addr_width.W))
    val wdata       = Input(UInt(data_width.W))
    val wmask       = Input(UInt((data_width/8).W))
    val rdata_ready = Input(Bool())
    val rdata_valid = Input(Bool())
    val rdata_bits  = Input(UInt(data_width.W))
    val fault       = Input(Bool())
    val rd_misalig  = Input(Bool())
    val wr_misalig  = Input(Bool())

    // last request
    val last_ren         = Input(Bool())
    val last_wen         = Input(Bool())
    val last_op_width    = Input(UInt(2.W)) 
    val last_op_unsigned = Input(Bool())
    val last_addr        = Input(UInt(addr_width.W))
    val last_wdata       = Input(UInt(data_width.W))

    // Clock and Reset
    val ACLK      = Input(Clock())
    val ARESETn   = Input(Bool())
    // Read Address Channel
    val ARREADY   = Input(Bool())
    val ARVALID   = Input(Bool())
    val ARADDR    = Input(UInt(addr_width.W))
    val ARPROT    = Input(UInt(3.W))
    // Read Data Channel
    val RREADY    = Input(Bool())
    val RVALID    = Input(Bool())
    val RDATA     = Input(UInt(data_width.W))
    val RRESP     = Input(UInt(2.W))
    // Write Address Channel
    val AWREADY   = Input(Bool())
    val AWVALID   = Input(Bool())
    val AWADDR    = Input(UInt(addr_width.W))
    val AWPROT    = Input(UInt(3.W))
    // Write Data Channel
    val WREADY    = Input(Bool())
    val WVALID    = Input(Bool())
    val WDATA     = Input(UInt(data_width.W))
    val WSTRB     = Input(UInt((data_width/8).W))
    // Write Response Channel
    val BREADY    = Input(Bool())
    val BVALID    = Input(Bool())
    val BRESP     = Input(UInt(2.W))
  })
  addResource("/LSU_AXI4LFormal.v")
}


