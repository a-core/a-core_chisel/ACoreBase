package acorebase

import chisel3._
import chisel3.util._
import chisel3.experimental._
import a_core_common._

case class MemRequest(addr_width: Int, data_width: Int) extends Bundle {
  /** Write enable. Otherwise a read. */
  val wen         = Bool()
  /** Operation width (byte, halfword, word) */
  val wmask       = UInt(4.W)
  /** Write data */
  val wdata       = UInt(data_width.W)
  /** Unsigned operation (determines if padding is 1's or 0's) */
  val op_unsigned = Bool()
  /** Read/Write address */
  val addr        = UInt(addr_width.W)
  /** Atomic request (LR/SC) */
  val atomic      = Bool()
}

/** Memory interface (core -> cache)*/
case class MemoryInterface(addr_width: Int, data_width: Int) extends Bundle {
  /** Request */
  val req  = Input(Bool())
  /** Request granted */
  val gnt  = Output(Bool())
  /** Bundle for information for a memory request */
  val info  = Input(new MemRequest(addr_width, data_width))
  /** Read data with valid interface */
  val rresp = Valid(new Bundle {
    val data = UInt(data_width.W)
    val fault = Bool()
  })
  /** Write response */
  val wresp = Valid(new Bundle {
    val fault = Bool()
  })
}

case class MemAccessIO(config: ACoreConfig) extends Bundle {
  val dmem = Flipped(MemoryInterface(config.addr_width, config.data_width))
  val in = Flipped(Decoupled((new Bundle {
    val lsu = new Bundle {
      val addr    = UInt(config.addr_width.W)
      val wdata   = UInt(config.data_width.W)
      val wmask   = UInt(4.W)
      val control = new LSUControlIO(config.data_width)
    }
    val mul                   = Valid((new MultiplierPipelined(config.data_width).Stage1Outputs()))
    val rd_src                = RegFileSrc()
    val regfile_write_control = new RegFileBlockWriteInputs(config.data_width).control
    val events                = new ControlStatusBlockInputs(config.data_width).event
    val trap_cause            = Valid(ExceptionCauses())
    val instr_bits            = UInt(32.W)
    val pc                    = UInt(config.addr_width.W)
    val rd_wdata              = Valid(UInt(config.data_width.W))
  })))
  val out = Decoupled(new Bundle {
    val trap_cause            = Valid(ExceptionCauses())
    val rd_wdata              = Valid(UInt(config.data_width.W))
    val instr_bits            = UInt(32.W)
    val pc                    = UInt(config.addr_width.W)
    val regfile_write_control = new RegFileBlockWriteInputs(config.data_width).control
    val events                = new ControlStatusBlockInputs(config.data_width).event
  })
  val ctrl_in = Input(new Bundle {
    val stall = Bool()
    val flush = Bool()
  })
  val forward_0 = Valid(ForwardBundle(config.data_width))
  val forward_1 = Valid(ForwardBundle(config.data_width))
}

/**
  * Memory Access stage. This stage has the following:
    - Data memory access (read and write)
    - Pipelined multiplier stage 2
  *
  * Similar to IFetch, this stage has a controller to ensure all 
  * memory transactions are received in case of a flush.
  * The memory fetch is sequential and thus this stage takes at least 2 cycles.
  * 
  * @param config
  */
class MemAccess(config: ACoreConfig) extends Module {
  val io = IO(MemAccessIO(config))

  // Second stage of multiplier
  val mul = new MultiplierPipelined(config.data_width)
  val mul_s2 = Module(new mul.Stage2)

  object MemAccessControlFSMState extends chisel3.ChiselEnum {
    val NORMAL = Value
    val FLUSH  = Value
  }
  val control_fsm_reg = RegInit(MemAccessControlFSMState.NORMAL)

  // Fault bit that asserts when the first misaligned/fault is detected
  // Stalls new requests to this stage until a flush is performed
  val fault          = WireDefault(false.B)
  val faultSticky    = WireDefault(false.B)
  val faultStickyReg = RegInit(false.B)

  val memRdata       = WireDefault(0.U(config.data_width.W))
  val req_reg        = RegInit(false.B)
  val req_not_gnt    = io.dmem.req && !io.dmem.gnt

  // Data needed to store for memory accesses
  // Using a Queue for the ready-valid interface
  // In truth, this cannot be deeper than 1,
  // because we must stop a new request if the previous one faults
  val memReqHolder = Module(new Queue(new Bundle {
    val addr_lsb              = UInt(2.W)
    val op_width              = MemOpWidth()
    val op_unsigned           = Bool()
    val req                   = Bool()
    val instr_bits            = UInt(32.W)
    val pc                    = UInt(config.addr_width.W)
    val regfile_write_control = chiselTypeOf(io.in.bits.regfile_write_control)
    val rd_wdata              = Valid(UInt(config.data_width.W))
    val rd_src                = RegFileSrc()
    val events                = chiselTypeOf(io.in.bits.events)
    val trap_cause            = Valid(ExceptionCauses())
  }, 1, pipe = true))

  // Memory request if
  val req = ((io.in.bits.lsu.control.ren || io.in.bits.lsu.control.wen) // read or write enable
              && !io.in.bits.trap_cause.valid                           // this instr hasn't trapped
              && !faultSticky                                           // previous instr hasn't trapped
              && memReqHolder.io.enq.ready                              // request FIFO is ready
              && io.in.valid) || req_reg                                // input data is valid

  memReqHolder.io.enq.bits.instr_bits            := io.in.bits.instr_bits
  memReqHolder.io.enq.bits.pc                    := io.in.bits.pc
  memReqHolder.io.enq.bits.regfile_write_control := io.in.bits.regfile_write_control
  memReqHolder.io.enq.bits.events                := io.in.bits.events
  memReqHolder.io.enq.bits.addr_lsb              := io.in.bits.lsu.addr(1,0)
  memReqHolder.io.enq.bits.req                   := req
  memReqHolder.io.enq.bits.op_unsigned           := riscv_util.getFunct3(io.in.bits.instr_bits)(2)
  memReqHolder.io.enq.bits.op_width              := MemOpWidth(riscv_util.getFunct3(io.in.bits.instr_bits)(1,0))
  memReqHolder.io.enq.bits.rd_wdata.bits         := Mux(io.in.bits.rd_src === RegFileSrc.MULT_RESULT, mul_s2.io.out.bits.result, io.in.bits.rd_wdata.bits)
  memReqHolder.io.enq.bits.rd_wdata.valid        := Mux(io.in.bits.rd_src === RegFileSrc.MULT_RESULT, mul_s2.io.out.valid, io.in.bits.rd_wdata.valid)
  memReqHolder.io.enq.bits.rd_src                := io.in.bits.rd_src
  memReqHolder.io.enq.bits.trap_cause            := io.in.bits.trap_cause
  memReqHolder.io.enq.valid                      := Mux(req, io.dmem.req && io.dmem.gnt, true.B) && io.in.valid
  memReqHolder.io.deq.ready                      := Mux(memReqHolder.io.deq.bits.req, io.dmem.rresp.valid || io.dmem.wresp.valid, true.B)

  io.dmem.info.wmask       := io.in.bits.lsu.wmask
  io.dmem.info.wdata       := io.in.bits.lsu.wdata
  io.dmem.info.op_unsigned := riscv_util.getFunct3(io.in.bits.instr_bits)(2)
  // We send a 32-bit aligned address to LSU. The write data and mask have been aligned and calulated in Execute.
  // The read data is aligned below based on the op width.
  io.dmem.info.addr        := Cat(io.in.bits.lsu.addr >> 2, 0.U(2.W))
  io.dmem.info.wen         := io.in.bits.lsu.control.wen && !io.in.bits.lsu.control.ren
  io.dmem.info.atomic      := io.in.bits.lsu.control.atomic

  fault          := (io.dmem.rresp.valid && io.dmem.rresp.bits.fault) ||
                    (io.dmem.wresp.valid && io.dmem.wresp.bits.fault)
  faultStickyReg := Mux(io.ctrl_in.flush, false.B, fault)
  faultSticky    := fault || faultStickyReg

  mul_s2.io.in := io.in.bits.mul

  io.out.bits.trap_cause            := memReqHolder.io.deq.bits.trap_cause
  io.out.bits.instr_bits            := memReqHolder.io.deq.bits.instr_bits
  io.out.bits.pc                    := memReqHolder.io.deq.bits.pc
  io.out.bits.events                := memReqHolder.io.deq.bits.events
  io.out.bits.rd_wdata.bits         := Mux(memReqHolder.io.deq.bits.rd_src === RegFileSrc.MEM_DATA, memRdata, memReqHolder.io.deq.bits.rd_wdata.bits)
  io.out.bits.rd_wdata.valid        := Mux(memReqHolder.io.deq.bits.rd_src === RegFileSrc.MEM_DATA, io.dmem.rresp.valid, memReqHolder.io.deq.bits.rd_wdata.valid)
  io.out.bits.regfile_write_control := memReqHolder.io.deq.bits.regfile_write_control

  when (io.dmem.wresp.valid && io.dmem.wresp.bits.fault) {
    io.out.bits.trap_cause.valid := true.B
    io.out.bits.trap_cause.bits := ExceptionCauses.STORE_ACCESS_FAULT
  } .elsewhen (io.dmem.rresp.valid && io.dmem.rresp.bits.fault) {
    io.out.bits.trap_cause.valid := true.B
    io.out.bits.trap_cause.bits := ExceptionCauses.LOAD_ACCESS_FAULT
  }

  // Align incoming data depending on op width
  val rdata = WireDefault(0.U(config.data_width.W))
  memRdata := io.dmem.rresp.bits.data
  when (memReqHolder.io.deq.bits.op_width === MemOpWidth.BYTE) {
    switch(memReqHolder.io.deq.bits.addr_lsb) {
      is(0.U) { rdata := io.dmem.rresp.bits.data(7,0) }
      is(1.U) { rdata := io.dmem.rresp.bits.data(15,8) }
      is(2.U) { rdata := io.dmem.rresp.bits.data(23,16) }
      is(3.U) { rdata := io.dmem.rresp.bits.data(31,24) }
    }
    memRdata := 
      Mux(
        memReqHolder.io.deq.bits.op_unsigned, 
        rdata, 
        rdata(7,0).asSInt.pad(config.data_width).asUInt
      )
  } .elsewhen(memReqHolder.io.deq.bits.op_width === MemOpWidth.HWORD) {
    switch(memReqHolder.io.deq.bits.addr_lsb) {
      is(0.U) { rdata := io.dmem.rresp.bits.data(15,0) }
      is(1.U) { rdata := DontCare }
      is(2.U) { rdata := io.dmem.rresp.bits.data(31,16) }
      is(3.U) { rdata := DontCare }
    }
    memRdata := 
      Mux(
        memReqHolder.io.deq.bits.op_unsigned, 
        rdata, 
        rdata(15,0).asSInt.pad(config.data_width).asUInt
      )
  }

  // Data forwarding information
  // Data can be forwarded from both sides of the request holder
  io.forward_0.bits.data          := memReqHolder.io.enq.bits.rd_wdata
  io.forward_0.bits.rd_addr       := riscv_util.getRd(io.in.bits.instr_bits)
  io.forward_0.bits.wr_reg_select := io.in.bits.regfile_write_control.wr_reg_select
  io.forward_0.valid              := io.in.valid
  io.forward_1.bits.data          := io.out.bits.rd_wdata
  io.forward_1.bits.rd_addr       := riscv_util.getRd(memReqHolder.io.deq.bits.instr_bits)
  io.forward_1.bits.wr_reg_select := io.out.bits.regfile_write_control.wr_reg_select
  io.forward_1.valid              := memReqHolder.io.deq.valid

  // Memory access controller
  // Ensures all requests are received and ignored during a flush
  io.dmem.req  := false.B
  io.out.valid := false.B
  io.in.ready  := memReqHolder.io.enq.ready && !req_not_gnt
  when (control_fsm_reg === MemAccessControlFSMState.NORMAL) {
    io.out.valid := memReqHolder.io.deq.fire
    io.dmem.req  := req
    when (io.ctrl_in.flush && (memReqHolder.io.deq.valid || req_not_gnt)) {
      control_fsm_reg := MemAccessControlFSMState.FLUSH
      req_reg         := req_not_gnt
    }
  } .elsewhen (control_fsm_reg === MemAccessControlFSMState.FLUSH) {
    req_reg := req_not_gnt
    when (!memReqHolder.io.deq.valid && !req_reg && !io.ctrl_in.flush) {
      control_fsm_reg := MemAccessControlFSMState.NORMAL
      io.dmem.req     := req
    }
  }

}
