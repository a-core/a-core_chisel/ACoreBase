// SPDX-License-Identifier: Apache-2.0

// Chisel module Control
// Inititally written by Verneri Hirvonen (verneri.hirvonen@aalto.fi), 2021-03-25

package acorebase

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.iotesters.PeekPokeTester
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import a_core_common._

class ControlFIO(XLEN: Int = 32) extends Bundle {
  //RV32F extension-specific inputs
  val fpu_rs2 = Input(UInt(5.W))
}

case class LSUControlIO(XLEN: Int = 32) extends Bundle {
  val ren = Bool()
  val wen = Bool()
  val atomic = Bool()
}

case class ControlInputs(XLEN: Int = 32) extends Bundle {
  // Inputs
  val opcode        = Opcode()
  val funct3        = UInt(3.W)
  val funct7        = UInt(7.W)
  val funct12       = UInt(12.W)
  val rd            = UInt(5.W)
  val rs1           = UInt(5.W)
  val f_ext         = new ControlFIO(XLEN)
}

case class ControlOutputs(data_width: Int) extends Bundle {
  val branch           = Bool()
  val jump             = Bool()
  val mret             = Bool()
  val illegal_instr    = Bool()
  val breakpoint       = Bool()
  val rd_src           = RegFileSrc()

  // Block-specific Outputs
  val alu_block           = new ALUBlockInputs(data_width).control
  val muldiv_op           = MultOp()
  val regfile_block_read  = new RegFileBlockReadInputs(data_width).control
  val regfile_block_write = new RegFileBlockWriteInputs(data_width).control
  val lsu                 = new LSUControlIO(data_width)
  val csr_block           = new ControlStatusBlockInputs(data_width).control
  val csr_block_event     = new ControlStatusBlockInputs(data_width).event
}

/** IO definitions for Control */
class ControlIO(config: ACoreConfig) extends Bundle {

  val in = Input(ControlInputs(config.data_width))
  val out = Output(ControlOutputs(config.data_width))

}

/** Chisel module Control */
class Control(config: ACoreConfig) extends Module {
  val io = IO(new ControlIO(config))
  //======================================= Initializations =========================================
  io.out.illegal_instr := false.B
  io.out.breakpoint := false.B
  io.out.rd_src := RegFileSrc.IMM
  io.out.alu_block.operand_a_src := AluOperandSrc.IMM
  io.out.alu_block.operand_b_src := AluOperandSrc.IMM
  io.out.alu_block.block_result_src := AluBlockResultSrc.ALU_OUT
  io.out.alu_block.operation := ALUOp.ADD
  io.out.alu_block.f_ext.fpu_en := false.B
  io.out.regfile_block_write.wr_reg_select := RegFileSelect.NONE
  io.out.regfile_block_read.rs1_out_src := RegFileSelect.NONE
  io.out.regfile_block_read.rs2_out_src := RegFileSelect.NONE
  io.out.lsu.ren := false.B
  io.out.lsu.wen := false.B
  io.out.lsu.atomic := false.B
  io.out.jump := false.B
  io.out.branch := false.B
  io.out.mret := false.B

  // Initialize extensions
  io.out.muldiv_op                     := MultOp.NULL
  io.out.alu_block.f_ext.rm_src        := AluBlockRoundingSrc.SRM
  io.out.alu_block.f_ext.fpu_op        := FPUOp.NULL
  io.out.csr_block.csr_op              := CSROp.NULL
  io.out.csr_block.read_en             := false.B
  io.out.csr_block.write_en            := false.B    
  io.out.csr_block_event.m_op          := false.B
  io.out.csr_block_event.f_op          := false.B
  io.out.csr_block_event.b_op          := false.B
  io.out.csr_block_event.j_op          := false.B
  io.out.csr_block_event.fflags_en     := false.B
  io.out.csr_block_event.instret       := true.B

  //======================================= OPCODE specific logic =========================================

  when (io.in.opcode === Opcode.LUI) {
    io.out.regfile_block_write.wr_reg_select := RegFileSelect.INT
    io.out.rd_src                            := RegFileSrc.IMM
  } .elsewhen (io.in.opcode === Opcode.AUIPC) {
    io.out.regfile_block_write.wr_reg_select := RegFileSelect.INT
    io.out.rd_src                            := RegFileSrc.ALU_RESULT
    io.out.alu_block.operation               := ALUOp.ADD
    io.out.alu_block.operand_a_src           := AluOperandSrc.PC
    io.out.alu_block.operand_b_src           := AluOperandSrc.IMM

  } .elsewhen (io.in.opcode === Opcode.JAL) {
    io.out.jump                              := true.B
    io.out.regfile_block_write.wr_reg_select := RegFileSelect.INT
    io.out.rd_src                            := RegFileSrc.PC_4
    io.out.alu_block.operation               := ALUOp.ADD
    io.out.alu_block.operand_a_src           := AluOperandSrc.PC
    io.out.alu_block.operand_b_src           := AluOperandSrc.IMM
    io.out.csr_block_event.j_op              := true.B
  } .elsewhen (io.in.opcode === Opcode.JALR) {
    io.out.jump                              := true.B
    io.out.regfile_block_write.wr_reg_select := RegFileSelect.INT
    io.out.rd_src                            := RegFileSrc.PC_4
    io.out.regfile_block_read.rs1_out_src    := RegFileSelect.INT
    io.out.alu_block.operation               := ALUOp.ADD_JALR
    io.out.alu_block.operand_a_src           := AluOperandSrc.RS1
    io.out.alu_block.operand_b_src           := AluOperandSrc.IMM
    io.out.csr_block_event.j_op              := true.B
  } .elsewhen (io.in.opcode === Opcode.BRANCH) {
    io.out.branch                         := true.B
    io.out.regfile_block_read.rs1_out_src := RegFileSelect.INT
    io.out.regfile_block_read.rs2_out_src := RegFileSelect.INT
    io.out.alu_block.operand_a_src        := AluOperandSrc.RS1
    io.out.alu_block.operand_b_src        := AluOperandSrc.RS2
    // BEQ
    when (io.in.funct3 === "b000".U) {
      io.out.alu_block.operation  := ALUOp.SUB
      io.out.csr_block_event.b_op := true.B
    // BNE
    } .elsewhen (io.in.funct3 === "b001".U) {
      io.out.alu_block.operation  := ALUOp.SUB
      io.out.csr_block_event.b_op := true.B
    // BLT
    } .elsewhen (io.in.funct3 === "b100".U) {
      io.out.alu_block.operation  := ALUOp.LT
      io.out.csr_block_event.b_op := true.B
    // BGE
    } .elsewhen (io.in.funct3 === "b101".U) {
      io.out.alu_block.operation  := ALUOp.LT
      io.out.csr_block_event.b_op := true.B
    // BLTU
    } .elsewhen (io.in.funct3 === "b110".U) {
      io.out.alu_block.operation  := ALUOp.LTU
      io.out.csr_block_event.b_op := true.B
    // BGEU
    } .elsewhen (io.in.funct3 === "b111".U) {
      io.out.alu_block.operation  := ALUOp.LTU
      io.out.csr_block_event.b_op := true.B
    } .otherwise {
      io.out.illegal_instr := true.B
    }
  } .elsewhen (io.in.opcode === Opcode.LOAD) {

    io.out.regfile_block_write.wr_reg_select := RegFileSelect.INT
    io.out.regfile_block_read.rs1_out_src    := RegFileSelect.INT
    io.out.rd_src                            := RegFileSrc.MEM_DATA
    io.out.alu_block.operation               := ALUOp.ADD
    io.out.alu_block.operand_a_src           := AluOperandSrc.RS1
    io.out.alu_block.operand_b_src           := AluOperandSrc.IMM
    io.out.lsu.ren                  := true.B
  } .elsewhen (io.in.opcode === Opcode.STORE) {
    io.out.regfile_block_read.rs1_out_src    := RegFileSelect.INT
    io.out.regfile_block_read.rs2_out_src    := RegFileSelect.INT
    io.out.alu_block.operation               := ALUOp.ADD
    io.out.alu_block.operand_a_src           := AluOperandSrc.RS1
    io.out.alu_block.operand_b_src           := AluOperandSrc.IMM
    io.out.lsu.wen                  := true.B
  } .elsewhen (io.in.opcode === Opcode.AMO) {
    if (config.extension_set.contains(ISAExtension.A)) {
      val funct5 = WireDefault(io.in.funct7(6,2))
      // LR.W
      when (funct5 === "b00010".U && io.in.funct3 === "b010".U) {
        io.out.regfile_block_write.wr_reg_select := RegFileSelect.INT
        io.out.regfile_block_read.rs1_out_src    := RegFileSelect.INT
        io.out.rd_src                            := RegFileSrc.MEM_DATA
        io.out.lsu.ren                           := true.B
        io.out.lsu.atomic                        := true.B
        io.out.alu_block.operation               := ALUOp.FWD_A
        io.out.alu_block.operand_a_src           := AluOperandSrc.RS1
      // SC.W
      } .elsewhen (funct5 === "b00011".U && io.in.funct3 === "b010".U) {
        io.out.regfile_block_write.wr_reg_select := RegFileSelect.INT
        io.out.regfile_block_read.rs1_out_src    := RegFileSelect.INT
        io.out.regfile_block_read.rs2_out_src    := RegFileSelect.INT
        io.out.rd_src                            := RegFileSrc.MEM_DATA // LSU returns SC.W condition through read bus
        io.out.lsu.wen                           := true.B
        io.out.lsu.atomic                        := true.B
        io.out.alu_block.operation               := ALUOp.ADD
        io.out.alu_block.operand_a_src           := AluOperandSrc.RS1

      } .otherwise {
        io.out.illegal_instr := true.B
      }
    } else {
      io.out.illegal_instr := true.B
    }

  } .elsewhen (io.in.opcode === Opcode.OP_IMM) {
    io.out.regfile_block_write.wr_reg_select := RegFileSelect.INT
    io.out.regfile_block_read.rs1_out_src    := RegFileSelect.INT
    io.out.rd_src                            := RegFileSrc.ALU_RESULT
    io.out.alu_block.operand_a_src           := AluOperandSrc.RS1
    io.out.alu_block.operand_b_src           := AluOperandSrc.IMM
    when (io.in.funct3 === "b000".U) {
      io.out.alu_block.operation := ALUOp.ADD
    } .elsewhen (io.in.funct3 === "b010".U) {
      io.out.alu_block.operation := ALUOp.LT
    } .elsewhen (io.in.funct3 === "b011".U) {
      io.out.alu_block.operation := ALUOp.LTU
    } .elsewhen (io.in.funct3 === "b100".U) {
      io.out.alu_block.operation := ALUOp.XOR
    } .elsewhen (io.in.funct3 === "b110".U) {
      io.out.alu_block.operation := ALUOp.OR
    } .elsewhen (io.in.funct3 === "b111".U) {
      io.out.alu_block.operation := ALUOp.AND
    } .elsewhen (io.in.funct3 === "b001".U && io.in.funct7 === "b0000000".U) {
      io.out.alu_block.operation := ALUOp.SLL
    } .elsewhen (io.in.funct3 === "b101".U && io.in.funct7 === "b0000000".U) {
      io.out.alu_block.operation := ALUOp.SRL
    } .elsewhen (io.in.funct3 === "b101".U && io.in.funct7 === "b0100000".U) {
      io.out.alu_block.operation := ALUOp.SRA
    } .otherwise {
      io.out.illegal_instr := true.B
    }
  } .elsewhen (io.in.opcode === Opcode.OP) {
    io.out.regfile_block_write.wr_reg_select := RegFileSelect.INT
    io.out.regfile_block_read.rs1_out_src    := RegFileSelect.INT
    io.out.regfile_block_read.rs2_out_src    := RegFileSelect.INT
    io.out.rd_src                            := RegFileSrc.ALU_RESULT
    io.out.alu_block.operand_a_src           := AluOperandSrc.RS1
    io.out.alu_block.operand_b_src           := AluOperandSrc.RS2
    when (io.in.funct3 === "b000".U && io.in.funct7 === "b0000000".U) {
      io.out.alu_block.operation := ALUOp.ADD
    } .elsewhen (io.in.funct3 === "b000".U && io.in.funct7 === "b0100000".U) {
      io.out.alu_block.operation := ALUOp.SUB
    } .elsewhen (io.in.funct3 === "b001".U && io.in.funct7 === "b0000000".U) {
      io.out.alu_block.operation := ALUOp.SLL
    } .elsewhen (io.in.funct3 === "b010".U && io.in.funct7 === "b0000000".U) {
      io.out.alu_block.operation := ALUOp.LT
    } .elsewhen (io.in.funct3 === "b011".U && io.in.funct7 === "b0000000".U) {
      io.out.alu_block.operation := ALUOp.LTU
    } .elsewhen (io.in.funct3 === "b100".U && io.in.funct7 === "b0000000".U) {
      io.out.alu_block.operation := ALUOp.XOR
    } .elsewhen (io.in.funct3 === "b101".U && io.in.funct7 === "b0000000".U) {
      io.out.alu_block.operation := ALUOp.SRL
    } .elsewhen (io.in.funct3 === "b101".U && io.in.funct7 === "b0100000".U) {
      io.out.alu_block.operation := ALUOp.SRA
    } .elsewhen (io.in.funct3 === "b110".U && io.in.funct7 === "b0000000".U) {
      io.out.alu_block.operation := ALUOp.OR
    } .elsewhen (io.in.funct3 === "b111".U && io.in.funct7 === "b0000000".U) {
      io.out.alu_block.operation := ALUOp.AND
    } .otherwise {
      if (config.extension_set.contains(ISAExtension.M)) {
        when (io.in.funct7 === "b0000001".U) {
          io.out.alu_block.block_result_src := AluBlockResultSrc.MULT_OUT
          when (io.in.funct3 === "b000".U && io.in.funct7 === "b0000001".U) {
            io.out.rd_src    := RegFileSrc.MULT_RESULT
            io.out.muldiv_op := MultOp.MUL
            io.out.csr_block_event.m_op := true.B
          } .elsewhen (io.in.funct3 === "b001".U && io.in.funct7 === "b0000001".U) {
            io.out.rd_src    := RegFileSrc.MULT_RESULT
            io.out.muldiv_op := MultOp.MULH
            io.out.csr_block_event.m_op := true.B
          } .elsewhen (io.in.funct3 === "b010".U && io.in.funct7 === "b0000001".U) {
            io.out.rd_src    := RegFileSrc.MULT_RESULT
            io.out.muldiv_op := MultOp.MULHSU
            io.out.csr_block_event.m_op := true.B
          } .elsewhen (io.in.funct3 === "b011".U && io.in.funct7 === "b0000001".U) {
            io.out.rd_src    := RegFileSrc.MULT_RESULT
            io.out.muldiv_op := MultOp.MULHU
            io.out.csr_block_event.m_op := true.B
          } .elsewhen (io.in.funct3 === "b100".U && io.in.funct7 === "b0000001".U) {
            io.out.rd_src    := RegFileSrc.DIV_RESULT
            io.out.muldiv_op := MultOp.DIV
            io.out.csr_block_event.m_op := true.B
          } .elsewhen (io.in.funct3 === "b101".U && io.in.funct7 === "b0000001".U) {
            io.out.rd_src    := RegFileSrc.DIV_RESULT
            io.out.muldiv_op := MultOp.DIVU
            io.out.csr_block_event.m_op := true.B
          } .elsewhen (io.in.funct3 === "b110".U && io.in.funct7 === "b0000001".U) {
            io.out.rd_src    := RegFileSrc.DIV_RESULT
            io.out.muldiv_op := MultOp.REM
            io.out.csr_block_event.m_op := true.B
          } .elsewhen (io.in.funct3 === "b111".U && io.in.funct7 === "b0000001".U) {
            io.out.rd_src    := RegFileSrc.DIV_RESULT
            io.out.muldiv_op := MultOp.REMU
            io.out.csr_block_event.m_op := true.B
          } .otherwise {
            io.out.illegal_instr := true.B
          }
        } .otherwise {
          io.out.illegal_instr := true.B
        }
      } else {
        io.out.illegal_instr := true.B
      }
    }

  } .elsewhen (io.in.opcode === Opcode.LOAD_FP) {
    if (config.extension_set.contains(ISAExtension.F)) {
      io.out.rd_src                            := RegFileSrc.MEM_DATA
      io.out.regfile_block_read.rs1_out_src    := RegFileSelect.INT
      io.out.regfile_block_write.wr_reg_select := RegFileSelect.FLOAT
      io.out.alu_block.f_ext.fpu_en            := true.B
      io.out.alu_block.f_ext.fpu_op            := FPUOp.FADD_S
      io.out.alu_block.operand_a_src           := AluOperandSrc.RS1
      io.out.alu_block.operand_b_src           := AluOperandSrc.IMM
      io.out.lsu.ren                           := true.B
      io.out.csr_block_event.f_op              := true.B
    } else {
      io.out.illegal_instr := true.B
    }
  } .elsewhen (io.in.opcode === Opcode.STORE_FP) {
    if (config.extension_set.contains(ISAExtension.F)) {
      io.out.regfile_block_write.wr_reg_select := RegFileSelect.INT
      io.out.regfile_block_read.rs1_out_src    := RegFileSelect.INT
      io.out.regfile_block_read.rs2_out_src    := RegFileSelect.FLOAT
      io.out.alu_block.f_ext.fpu_en            := true.B
      io.out.alu_block.f_ext.fpu_op            := FPUOp.FADD_S
      io.out.alu_block.operand_a_src           := AluOperandSrc.RS1
      io.out.alu_block.operand_b_src           := AluOperandSrc.IMM
      io.out.lsu.wen                           := true.B
      io.out.csr_block_event.f_op              := true.B
    } else {
      io.out.illegal_instr := true.B
    }
  } .elsewhen (io.in.opcode === Opcode.OP_FP) {
    if (config.extension_set.contains(ISAExtension.F)) {
      io.out.regfile_block_write.wr_reg_select := RegFileSelect.FLOAT
      io.out.regfile_block_read.rs1_out_src    := RegFileSelect.FLOAT
      io.out.regfile_block_read.rs2_out_src    := RegFileSelect.FLOAT
      io.out.rd_src                            := RegFileSrc.ALU_RESULT
      io.out.alu_block.f_ext.fpu_en            := true.B
      io.out.alu_block.block_result_src        := AluBlockResultSrc.SPFP_OUT
      io.out.alu_block.operand_a_src           := AluOperandSrc.RS1
      io.out.alu_block.operand_b_src           := AluOperandSrc.RS2
      io.out.csr_block_event.f_op              := true.B
      // Decode rounding mode
      when( io.in.funct3 === "b111".U ) {
        io.out.alu_block.f_ext.rm_src := AluBlockRoundingSrc.DRM
      } .otherwise {
        io.out.alu_block.f_ext.rm_src := AluBlockRoundingSrc.SRM
      }
      when ( io.in.funct7 === "b0000000".U) {
        io.out.alu_block.f_ext.fpu_op    := FPUOp.FADD_S
        io.out.csr_block_event.fflags_en := true.B
      } .elsewhen ( io.in.funct7 === "b0000100".U ) {
        io.out.alu_block.f_ext.fpu_op    := FPUOp.FSUB_S
        io.out.csr_block_event.fflags_en := true.B
      } .elsewhen ( io.in.funct7 === "b0001000".U ) {
        io.out.alu_block.f_ext.fpu_op    := FPUOp.FMUL_S
        io.out.csr_block_event.fflags_en := true.B
      } .elsewhen ( io.in.funct7 === "b0001100".U ) {
        io.out.alu_block.f_ext.fpu_op    := FPUOp.FDIV_S
        io.out.csr_block_event.fflags_en := true.B
      } .elsewhen ( io.in.f_ext.fpu_rs2 === "b00000".U && io.in.funct7 === "b0101100".U ) {
        io.out.alu_block.f_ext.fpu_op    := FPUOp.FSQRT_S
        io.out.csr_block_event.fflags_en := true.B
      } .elsewhen ( io.in.funct3 === "b000".U && io.in.funct7 === "b0010000".U) {
        io.out.alu_block.f_ext.fpu_op    := FPUOp.FSGNJ_S
        io.out.csr_block_event.fflags_en := true.B
      } .elsewhen ( io.in.funct3 === "b001".U && io.in.funct7 === "b0010000".U) {
        io.out.alu_block.f_ext.fpu_op    := FPUOp.FSGNJN_S
        io.out.csr_block_event.fflags_en := true.B
      } .elsewhen ( io.in.funct3 === "b010".U && io.in.funct7 === "b0010000".U) {
        io.out.alu_block.f_ext.fpu_op    := FPUOp.FSGNJX_S
        io.out.csr_block_event.fflags_en := true.B
      } .elsewhen ( io.in.funct3 === "b000".U && io.in.funct7 === "b0010100".U) {
        io.out.alu_block.f_ext.fpu_op    := FPUOp.FMIN_S
        io.out.csr_block_event.fflags_en := true.B
      } .elsewhen ( io.in.funct3 === "b001".U && io.in.funct7 === "b0010100".U) {
        io.out.alu_block.f_ext.fpu_op    := FPUOp.FMAX_S
        io.out.csr_block_event.fflags_en := true.B
      } .elsewhen ( io.in.f_ext.fpu_rs2 === "b00000".U && io.in.funct7 === "b1100000".U ) {
        io.out.alu_block.f_ext.fpu_op            := FPUOp.FCVT_W_S
        io.out.csr_block_event.fflags_en         := true.B
        io.out.regfile_block_write.wr_reg_select := RegFileSelect.INT
        io.out.regfile_block_read.rs1_out_src    := RegFileSelect.FLOAT
        io.out.regfile_block_read.rs2_out_src    := RegFileSelect.FLOAT
      } .elsewhen ( io.in.f_ext.fpu_rs2 === "b00001".U && io.in.funct7 === "b1100000".U ) {
        io.out.alu_block.f_ext.fpu_op            := FPUOp.FCVT_WU_S
        io.out.csr_block_event.fflags_en         := true.B
        io.out.regfile_block_write.wr_reg_select := RegFileSelect.INT
        io.out.regfile_block_read.rs1_out_src    := RegFileSelect.FLOAT
        io.out.regfile_block_read.rs2_out_src    := RegFileSelect.FLOAT
      } .elsewhen ( io.in.f_ext.fpu_rs2 === "b00000".U && io.in.funct7 === "b1110000".U && io.in.funct3 === "b000".U ) {
        io.out.alu_block.f_ext.fpu_op            := FPUOp.FMV_X_W
        io.out.regfile_block_write.wr_reg_select := RegFileSelect.INT
        io.out.regfile_block_read.rs1_out_src    := RegFileSelect.FLOAT
        io.out.regfile_block_read.rs2_out_src    := RegFileSelect.FLOAT
      } .elsewhen ( io.in.funct3 === "b010".U &&  io.in.funct7 === "b1010000".U ) {
        io.out.alu_block.f_ext.fpu_op            := FPUOp.FCMP_FEQ_S
        io.out.regfile_block_write.wr_reg_select := RegFileSelect.INT
        io.out.csr_block_event.fflags_en         := true.B
      } .elsewhen ( io.in.funct3 === "b001".U &&  io.in.funct7 === "b1010000".U ) {
        io.out.alu_block.f_ext.fpu_op            := FPUOp.FCMP_FLT_S
        io.out.regfile_block_write.wr_reg_select := RegFileSelect.INT
        io.out.csr_block_event.fflags_en         := true.B
      } .elsewhen ( io.in.funct3 === "b000".U &&  io.in.funct7 === "b1010000".U ) {
        io.out.alu_block.f_ext.fpu_op            := FPUOp.FCMP_FLE_S
        io.out.regfile_block_write.wr_reg_select := RegFileSelect.INT
        io.out.csr_block_event.fflags_en         := true.B
      } .elsewhen ( io.in.f_ext.fpu_rs2 === "b00000".U && io.in.funct7 === "b1110000".U && io.in.funct3 === "b001".U ) {
        io.out.alu_block.f_ext.fpu_op            := FPUOp.FCLASS_S
        io.out.regfile_block_write.wr_reg_select := RegFileSelect.INT
        io.out.csr_block_event.fflags_en         := true.B
      } .elsewhen ( io.in.f_ext.fpu_rs2 === "b00000".U && io.in.funct7 === "b1101000".U ) {
        io.out.alu_block.f_ext.fpu_op            := FPUOp.FCVT_S_W
        io.out.regfile_block_write.wr_reg_select := RegFileSelect.FLOAT
        io.out.regfile_block_read.rs1_out_src    := RegFileSelect.INT
        io.out.regfile_block_read.rs2_out_src    := RegFileSelect.INT
        io.out.csr_block_event.fflags_en         := true.B
      } .elsewhen ( io.in.f_ext.fpu_rs2 === "b00001".U && io.in.funct7 === "b1101000".U ) {
        io.out.alu_block.f_ext.fpu_op            := FPUOp.FCVT_S_WU
        io.out.regfile_block_write.wr_reg_select := RegFileSelect.FLOAT
        io.out.regfile_block_read.rs1_out_src    := RegFileSelect.INT
        io.out.regfile_block_read.rs2_out_src    := RegFileSelect.INT
        io.out.csr_block_event.fflags_en         := true.B
      } .elsewhen ( io.in.f_ext.fpu_rs2 === "b00000".U && io.in.funct7 === "b1111000".U  && io.in.funct3 === "b000".U ) {
        io.out.alu_block.f_ext.fpu_op            := FPUOp.FMV_W_X
        io.out.regfile_block_write.wr_reg_select := RegFileSelect.FLOAT
        io.out.regfile_block_read.rs1_out_src    := RegFileSelect.INT
        io.out.regfile_block_read.rs2_out_src    := RegFileSelect.INT
      } .otherwise {
        io.out.illegal_instr := true.B
      }
    } else {
      io.out.illegal_instr := true.B
    }
  } .elsewhen (io.in.opcode === Opcode.MADD) {
    if (config.extension_set.contains(ISAExtension.F)) {
      io.out.rd_src                            := RegFileSrc.ALU_RESULT
      io.out.regfile_block_write.wr_reg_select := RegFileSelect.FLOAT
      io.out.regfile_block_read.rs1_out_src    := RegFileSelect.FLOAT
      io.out.regfile_block_read.rs2_out_src    := RegFileSelect.FLOAT
      io.out.alu_block.f_ext.fpu_en            := true.B
      io.out.alu_block.block_result_src        := AluBlockResultSrc.SPFP_OUT
      io.out.alu_block.operand_a_src           := AluOperandSrc.RS1
      io.out.alu_block.operand_b_src           := AluOperandSrc.RS2
      io.out.csr_block_event.fflags_en         := true.B
      io.out.csr_block_event.f_op              := true.B
      // Decode rounding mode
      when( io.in.funct3 === "b111".U ) {
        io.out.alu_block.f_ext.rm_src := AluBlockRoundingSrc.DRM
      } .otherwise {
        io.out.alu_block.f_ext.rm_src := AluBlockRoundingSrc.SRM
      }
      when (io.in.funct7(1,0) === "b00".U) {
        io.out.alu_block.f_ext.fpu_op    := FPUOp.FMADD_S
        io.out.csr_block_event.fflags_en := true.B
      } .otherwise {
        io.out.illegal_instr := true.B
      }
    } else {
      io.out.illegal_instr := true.B
    }
  }.elsewhen (io.in.opcode === Opcode.MSUB) {
    if (config.extension_set.contains(ISAExtension.F)) {
      io.out.rd_src                            := RegFileSrc.ALU_RESULT
      io.out.regfile_block_write.wr_reg_select := RegFileSelect.FLOAT
      io.out.regfile_block_read.rs1_out_src    := RegFileSelect.FLOAT
      io.out.regfile_block_read.rs2_out_src    := RegFileSelect.FLOAT
      io.out.alu_block.f_ext.fpu_en            := true.B
      io.out.csr_block_event.fflags_en         := true.B
      io.out.alu_block.block_result_src        := AluBlockResultSrc.SPFP_OUT
      io.out.alu_block.operand_a_src           := AluOperandSrc.RS1
      io.out.alu_block.operand_b_src           := AluOperandSrc.RS2
      io.out.csr_block_event.f_op              := true.B
      // Decode rounding mode
      when( io.in.funct3 === "b111".U ) {
        io.out.alu_block.f_ext.rm_src := AluBlockRoundingSrc.DRM
      } .otherwise {
        io.out.alu_block.f_ext.rm_src := AluBlockRoundingSrc.SRM
      }
      when (io.in.funct7(1,0) === "b00".U) {
        io.out.alu_block.f_ext.fpu_op := FPUOp.FMSUB_S
        io.out.csr_block.read_en := true.B
      } .otherwise {
        io.out.illegal_instr := true.B
      }
    } else {
      io.out.illegal_instr := true.B
    }

  }.elsewhen (io.in.opcode === Opcode.NMADD) {
    if (config.extension_set.contains(ISAExtension.F)) {
      io.out.rd_src  := RegFileSrc.ALU_RESULT
      io.out.regfile_block_write.wr_reg_select := RegFileSelect.FLOAT
      io.out.regfile_block_read.rs1_out_src    := RegFileSelect.FLOAT
      io.out.regfile_block_read.rs2_out_src    := RegFileSelect.FLOAT
      io.out.alu_block.f_ext.fpu_en            := true.B
      io.out.alu_block.block_result_src        := AluBlockResultSrc.SPFP_OUT
      io.out.alu_block.operand_a_src           := AluOperandSrc.RS1
      io.out.alu_block.operand_b_src           := AluOperandSrc.RS2
      io.out.csr_block_event.f_op              := true.B
      when( io.in.funct3 === "b111".U ) {
        io.out.alu_block.f_ext.rm_src := AluBlockRoundingSrc.DRM
      } .otherwise {
        io.out.alu_block.f_ext.rm_src := AluBlockRoundingSrc.SRM
      }
      when (io.in.funct7(1,0) === "b00".U) {
        io.out.alu_block.f_ext.fpu_op := FPUOp.FNMADD_S
        io.out.csr_block_event.fflags_en := true.B
      } .otherwise {
        io.out.illegal_instr := true.B
      }
    } else {
      io.out.illegal_instr := true.B
    }
  }.elsewhen (io.in.opcode === Opcode.NMSUB) {
    if (config.extension_set.contains(ISAExtension.F)) {
      io.out.rd_src  := RegFileSrc.ALU_RESULT
      io.out.regfile_block_write.wr_reg_select := RegFileSelect.FLOAT
      io.out.regfile_block_read.rs1_out_src    := RegFileSelect.FLOAT
      io.out.regfile_block_read.rs2_out_src    := RegFileSelect.FLOAT
      io.out.alu_block.f_ext.fpu_en            := true.B
      io.out.alu_block.block_result_src        := AluBlockResultSrc.SPFP_OUT
      io.out.alu_block.operand_a_src           := AluOperandSrc.RS1
      io.out.alu_block.operand_b_src           := AluOperandSrc.RS2
      io.out.csr_block_event.f_op              := true.B
      when( io.in.funct3 === "b111".U ) {
        io.out.alu_block.f_ext.rm_src := AluBlockRoundingSrc.DRM
      } .otherwise {
        io.out.alu_block.f_ext.rm_src := AluBlockRoundingSrc.SRM
      }
      when (io.in.funct7(1,0) === "b00".U) {
        io.out.alu_block.f_ext.fpu_op    := FPUOp.FNMSUB_S
        io.out.csr_block_event.fflags_en := true.B
      } .otherwise {
        io.out.illegal_instr := true.B
      }
    } else {
      io.out.illegal_instr := true.B
    }
  } .elsewhen (io.in.opcode === Opcode.MISC_MEM) {
    // FENCE
    when (io.in.funct3 === "b000".U) {
      // Implement as NOP
      // Drive control signals with values that don't change the processor state
      io.out.rd_src                  := RegFileSrc.ALU_RESULT
      io.out.alu_block.operand_a_src := AluOperandSrc.RS1
      io.out.alu_block.operand_b_src := AluOperandSrc.IMM
      io.out.alu_block.operation     := ALUOp.ADD
    } .otherwise {
      // invalid funct3 field for MISC_MEM opcode
      io.out.illegal_instr := true.B
    }
  } .elsewhen (io.in.opcode === Opcode.SYSTEM) {
    when (io.in.funct12 === "b001100000010".U) {
      // MRET
      io.out.mret := true.B
    } .elsewhen ( io.in.funct3 === "b000".U) {
      // Implement the ECALL/EBREAK instructions with a single SYSTEM hardware instruction that always traps
      io.out.breakpoint := true.B
    } .otherwise {
      io.out.regfile_block_write.wr_reg_select := RegFileSelect.INT
      io.out.rd_src                            := RegFileSrc.CSR_RESULT
      io.out.csr_block.read_en                 := true.B
      io.out.csr_block.write_en                := true.B
      when ( io.in.funct3 === "b001".U ) {
      io.out.regfile_block_read.rs1_out_src := RegFileSelect.INT
        io.out.csr_block.csr_op             := CSROp.CSRRW
        io.out.csr_block.read_en            := !(io.in.rd === 0.U)
      } .elsewhen ( io.in.funct3 === "b010".U ) {
      io.out.regfile_block_read.rs1_out_src := RegFileSelect.INT
        io.out.csr_block.csr_op             := CSROp.CSRRS
        io.out.csr_block.write_en           := !(io.in.rs1 === 0.U)
      } .elsewhen ( io.in.funct3 === "b011".U ) {
        io.out.regfile_block_read.rs1_out_src := RegFileSelect.INT
        io.out.csr_block.csr_op               := CSROp.CSRRC
        io.out.csr_block.write_en             := !(io.in.rs1 === 0.U)
      } .elsewhen ( io.in.funct3 === "b101".U ) {
        io.out.csr_block.csr_op  := CSROp.CSRRWI
        io.out.csr_block.read_en := !(io.in.rd === 0.U)
      } .elsewhen ( io.in.funct3 === "b110".U ) {
        io.out.csr_block.csr_op   := CSROp.CSRRSI
        io.out.csr_block.write_en := !(io.in.rs1 === 0.U)
      } .elsewhen ( io.in.funct3 === "b111".U ) {
        io.out.csr_block.csr_op   := CSROp.CSRRCI
        io.out.csr_block.write_en := !(io.in.rs1 === 0.U)
      } .otherwise {
        io.out.illegal_instr := true.B
      }
    }
  } .otherwise {
    // trap on unknown instructions
    io.out.illegal_instr := true.B
  }
}

/** Generates verilog */
object Control extends App {
  val annos = Seq(ChiselGeneratorAnnotation(() => new Control(
    config = ACoreConfig.default
  )))
  (new ChiselStage).execute(args, annos)
}
