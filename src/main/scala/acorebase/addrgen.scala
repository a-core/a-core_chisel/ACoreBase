package acorebase

import chisel3._
import chisel3.util._
import chisel3.experimental._
import a_core_common._

case class AddrGenIO(config: ACoreConfig) extends Bundle {
  val in = Input(new Bundle {
    val jump   = Valid(UInt(32.W))
    val branch = Valid(UInt(32.W))
    val trap   = Valid(UInt(32.W))
    val mret   = Valid(UInt(32.W))
    val grant  = Bool()
  })
  val out = Output(new Bundle {
    val pc   = UInt(config.addr_width.W)
  })
}

class AddrGen(config: ACoreConfig) extends Module {

  val io = IO(AddrGenIO(config))

  // Program counter registers and wires
  val pc_reg = RegInit(config.pc_init.U(32.W))
  val pc_4     = pc_reg + 4.U

  io.out.pc   := pc_reg

  // Mux for PC reg
  when (io.in.trap.valid) {
    pc_reg := io.in.trap.bits
  } .elsewhen (io.in.jump.valid) {
    pc_reg := io.in.jump.bits
  } .elsewhen(io.in.branch.valid) {
    pc_reg := io.in.branch.bits
  } .elsewhen(io.in.mret.valid) {
    pc_reg := io.in.mret.bits
  } .elsewhen(io.in.grant) {
    pc_reg := pc_4
  }

}