// SPDX-License-Identifier: Apache-2.0
// Inititally written by Verneri Hirvonen (verneri.hirvonen@aalto.fi) 2021-03-25

package acorebase

import chisel3._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import chisel3.util._
import chisel3.experimental._
import chisel3.iotesters.PeekPokeTester
import a_core_common._

object RegFileSrc extends chisel3.ChiselEnum {
  val ALU_RESULT = Value(0.U)
  val MEM_DATA = Value(1.U)
  val PC_4 = Value(2.U)
  val IMM = Value(3.U)
  val CSR_RESULT = Value(4.U)
  val MULT_RESULT = Value(5.U)
  val DIV_RESULT = Value(6.U)
}

object RegFileSelect extends chisel3.ChiselEnum {
  val NONE = Value(0.U)
  val INT = Value(1.U)
  val FLOAT = Value(2.U)
}

/**
  * Inputs for register write operation
  *
  * @param XLEN
  */
class RegFileBlockWriteInputs(XLEN: Int = 32) extends Bundle {
  // Data path signals
  val data = new Bundle {
    // Destination register address
    val rd_waddr = UInt(5.W)
    // Write data
    val rd_wdata = UInt(XLEN.W)
  }

  // Control signals from Control
  val control = new Bundle {
    // Select register file to write to
    val wr_reg_select	= RegFileSelect()
  }
}

/**
  * Inputs for register read operation
  *
  * @param XLEN
  */
class RegFileBlockReadInputs(XLEN: Int = 32) extends Bundle {
  // Data path signals
  val data = new Bundle {
    // Read ports
    val rs1_addr = UInt(5.W)
    val rs2_addr = UInt(5.W)
    val rs3_addr = UInt(5.W)

  }

  // Control signals from Control
  val control = new Bundle {
    // Select register file to read from
    val rs1_out_src = RegFileSelect()
    val rs2_out_src = RegFileSelect()
   
 }
}

/**
  * Outputs from register read operation
  *
  * @param XLEN
  */
class RegFileBlockReadOutputs(XLEN: Int = 32) extends Bundle {
  // Data path signals
  val data = new Bundle {
    // Data from read port A and B
    val rs1_rdata = UInt(XLEN.W)
    val rs2_rdata = UInt(XLEN.W)
    val rs3_rdata = UInt(XLEN.W)
  }
}

/** Chisel module RegFileBlock */
class RegFileBlock(config: ACoreConfig) extends Module {

  val io = IO(new Bundle {

    // separate read and write interfaces (can be used simultaneously)
    val readIn  = Input(new RegFileBlockReadInputs(config.data_width))
    val readOut = Output(new RegFileBlockReadOutputs(config.data_width))
    val writeIn = Input(new RegFileBlockWriteInputs(config.data_width))

    // debug outputs
    val debug_write_data = if (config.debug) Some(Output(UInt(config.data_width.W))) else None

  })

  // Integer register file
  val regfile = Module(new IntRegFile(XLEN=config.data_width))
  // Float register file
  val float_regfile = if (config.extension_set.contains(ISAExtension.F)) 
                        Some(Module(new FloatRegFile(FLEN=config.data_width))) else None
  
  // Initializations 
  regfile.io.write_en := false.B
  regfile.io.write_addr := 0.U(5.W)
  regfile.io.write_data := 0.U(config.data_width.W)
  
  if (config.extension_set.contains(ISAExtension.F)){
    float_regfile.get.io.write_en := false.B
    float_regfile.get.io.write_addr := 0.U(5.W)
    float_regfile.get.io.write_data := 0.U(config.data_width.W)
  }

  // Wires for writing to destination register (rd)
  val rd_wdata = WireDefault(0.U(config.data_width.W))
  val rd_waddr = io.writeIn.data.rd_waddr
  
  // Read ports for integer regfile
  regfile.io.read_a_addr := io.readIn.data.rs1_addr
  regfile.io.read_b_addr := io.readIn.data.rs2_addr

  // Write ports for integer regfile
  regfile.io.write_addr := rd_waddr
  regfile.io.write_en   := io.writeIn.control.wr_reg_select === RegFileSelect.INT
  regfile.io.write_data := rd_wdata

  // Initialize extensions with dummy values
  io.readOut.data.rs3_rdata := 0.U

  // Read and write ports for float regfile
  if (config.extension_set.contains(ISAExtension.F)) {
    float_regfile.get.io.read_a_addr := io.readIn.data.rs1_addr
    float_regfile.get.io.read_b_addr := io.readIn.data.rs2_addr
    float_regfile.get.io.read_c_addr := io.readIn.data.rs3_addr
    float_regfile.get.io.write_addr := rd_waddr
    float_regfile.get.io.write_en := io.writeIn.control.wr_reg_select === RegFileSelect.FLOAT
    float_regfile.get.io.write_data := rd_wdata
  }

  // write data mux (set write data to 0 if write address is x0)
  when ( !((rd_waddr === 0.U) && (io.writeIn.control.wr_reg_select === RegFileSelect.INT)) ) {
    rd_wdata := io.writeIn.data.rd_wdata
  }

  if (config.extension_set.contains(ISAExtension.F)) {

    // output path selection mux
    when (io.readIn.control.rs1_out_src === RegFileSelect.FLOAT) {
      io.readOut.data.rs1_rdata := float_regfile.get.io.read_a_data
    } .otherwise {
      io.readOut.data.rs1_rdata := regfile.io.read_a_data
    }
    when (io.readIn.control.rs2_out_src === RegFileSelect.FLOAT) {
      io.readOut.data.rs2_rdata := float_regfile.get.io.read_b_data
    } .otherwise {
      io.readOut.data.rs2_rdata := regfile.io.read_b_data
    }
    io.readOut.data.rs3_rdata := float_regfile.get.io.read_c_data
  } else {
    // No mux - all data goes to int regfile regardless of out_src
    regfile.io.write_data := rd_wdata
    io.readOut.data.rs1_rdata := regfile.io.read_a_data
    io.readOut.data.rs2_rdata := regfile.io.read_b_data  
  }

  // debug outputs
  if (config.debug) {
    io.debug_write_data.get := rd_wdata
  }

}


/** Generates verilog */
object RegFileBlock extends App {
  val annos = Seq(ChiselGeneratorAnnotation(() => new RegFileBlock(
    config=ACoreConfig.default
  )))
  (new ChiselStage).execute(args, annos)
}


