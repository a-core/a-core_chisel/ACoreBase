// SPDX-License-Identifier: Apache-2.0

package acorebase

import chisel3._
import chisel3.experimental._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import chisel3.iotesters.PeekPokeTester

class FloatRegFileIO(dataWidth: Int) extends Bundle {
  // Read port A
  val read_a_addr = Input(UInt(5.W))
  val read_a_data = Output(UInt(dataWidth.W))

  // Read port B
  val read_b_addr = Input(UInt(5.W))
  val read_b_data = Output(UInt(dataWidth.W))
    
  // Read port C
  val read_c_addr = Input(UInt(5.W))
  val read_c_data = Output(UInt(dataWidth.W))

  // Write port
  val write_en = Input(Bool())
  val write_addr = Input(UInt(5.W))
  val write_data = Input(UInt(dataWidth.W))
}

class FloatRegFile(val FLEN: Int = 32) extends Module {
  val io = IO(new FloatRegFileIO(dataWidth=FLEN)) 
  val f = RegInit(VecInit(Seq.fill(32)(0.U(FLEN.W))))

  // connect read ports to registers
  io.read_a_data := f(io.read_a_addr)
  io.read_b_data := f(io.read_b_addr)
  io.read_c_data := f(io.read_c_addr)

  when (io.write_en) {
    f(io.write_addr) := io.write_data
  }
}

//This gives you verilog
object FloatRegFile extends App {
    val annos = Seq(ChiselGeneratorAnnotation(() => new FloatRegFile(
        FLEN=32
    )))
    (new ChiselStage).execute(args, annos)
}
