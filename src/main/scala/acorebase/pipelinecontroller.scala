// SPDX-License-Identifier: Apache-2.0

package acorebase

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.experimental.BundleLiterals._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import a_core_common._

/**
  * Bundle that contains the pipeline stage names with parametrizable data type
  */
class PipelineBundle[T <: Data](datatype: => T) extends Bundle {
  val ifetch     = datatype
  val decode     = datatype
  val execute    = datatype
  val memaccess0 = datatype
  val memaccess1 = datatype
  val writeback  = datatype
}

/**
  * Bundle for data needed from stages that can provide register data
  *
  * @param config
  */
case class ForwardBundle(data_width: Int) extends Bundle {
  val data          = Valid(UInt(data_width.W))
  val rd_addr       = UInt(5.W)
  val wr_reg_select = RegFileSelect()
}

class PipelineControllerInputs(XLEN: Int) extends Bundle {
  
  // Core enable signal
  val core_en = Bool()
  // Control detected jump instruction
  val jump = Bool()
  // ALU detected that branch was taken
  val branch = Bool()
  // Trap Controller detected a trap
  val trap = Bool()
  // Control detected mret instruction
  val mret = Bool()
  // Register data hazard signals
  // Source addresses
  val rs1_addr       = new PipelineBundle(UInt(5.W))
  val rs2_addr       = new PipelineBundle(UInt(5.W))
  val rs3_addr       = new PipelineBundle(UInt(5.W))
  // Source regfile (int, float)
  val rs1_select     = new PipelineBundle(RegFileSelect())
  val rs2_select     = new PipelineBundle(RegFileSelect())
  val rs3_select     = new PipelineBundle(RegFileSelect())
  // Data from possible register forwarding sources
  val forward        = new PipelineBundle(Valid(ForwardBundle(XLEN)))
  // Instruction retired
  val ret            = new PipelineBundle(Bool())
  // CSR Write enable
  val csr_wr_en      = new PipelineBundle(Bool())
  // CSR Read enable
  val csr_rd_en      = new PipelineBundle(Bool())
}

class PipelineControllerOutputs(XLEN: Int) extends Bundle {
  // Flushes a pipeline stage
  val flush = new PipelineBundle(Bool())
  // Stalls a pipeline stage
  val stall = new PipelineBundle(Bool())
  // Signal to forward rs1,rs2,rs3
  val forward_vec         = Vec(3, Bool())
  // Data for forwarding rs1,rs2,rs3
  val rs_forward_data_vec = Vec(3, UInt(XLEN.W))
}

class PipelineController (config: ACoreConfig) extends Module {
  val io = IO(new Bundle {
    val in = Input(new PipelineControllerInputs(config.data_width))
    val out = Output(new PipelineControllerOutputs(config.data_width))
  })

  // Initializes all signals as false
  io.out.flush.getElements.foreach(_ := false.B)
  io.out.stall.getElements.foreach(_ := false.B)
  io.out.forward_vec.getElements.foreach(_ := 0.U)
  io.out.rs_forward_data_vec.getElements.foreach(_ := 0.U)

  // If core is not enabled, stall all signals
  when (!io.in.core_en) {
    io.out.stall.getElements.foreach(_ := true.B)
  }

  // Control hazards

  // For jump, branch, mret, the signal is triggered when receiving a handshake from the next stage.
  // Therefore, we can flush also the stage that causes the event.

  // Jumps - jump flag comes from Execute
  // IFetch and Decode and Execute are flushed

  // Branches - branch flag comes from Execute from ALU's branch_taken signal
  // IFetch and Decode and Execute are flushed

  // Traps
  // If trap is detected, the full pipeline is flushed

  // Mret
  // Ifetch and Decode and Execute are flushed

  // Flush signal for traps
  val trap_flush      = WireDefault(false.B)
  // Flush signal for mret
  val mret_flush      = WireDefault(false.B)
  // Flush signal for branches
  val branch_flush    = WireDefault(false.B)
  // Flush signal for jumps
  val jump_flush      = WireDefault(false.B)

  // Flush signal generation
  io.out.flush.ifetch     := (trap_flush || jump_flush ||
                               branch_flush || mret_flush)
  io.out.flush.decode     := (trap_flush || branch_flush || jump_flush || mret_flush)
  io.out.flush.execute    := (trap_flush || branch_flush || jump_flush || mret_flush)
  io.out.flush.memaccess1 := (trap_flush)
  io.out.flush.writeback  := (trap_flush)

  // Priority mux for the case when we receive multiple simultaneously
  when (io.in.trap) {
    trap_flush   := true.B
  } .elsewhen (io.in.branch) {
    branch_flush := true.B
  } .elsewhen (io.in.jump) {
    jump_flush   := true.B
  } .elsewhen (io.in.mret) {
    mret_flush   := true.B
  }

  // Data hazards

  // Register hazard - register read is the same as register written
  // Decode stage is stalled when hazard detected but data cannot yet be forwarded

  val fwd_vec = VecInit(io.in.forward.execute, io.in.forward.memaccess0, io.in.forward.memaccess1, io.in.forward.writeback)

  var i = 0 // initialized for for-loops
  var j = 0

  // rs sources - rs1, rs2, rs3
  val rs_addr_vec = VecInit(io.in.rs1_addr.decode, io.in.rs2_addr.decode, io.in.rs3_addr.decode)
  // rs source register file (none, int, float)
  val rs_select_vec = VecInit(Seq[RegFileSelect.Type](io.in.rs1_select.decode, io.in.rs2_select.decode, io.in.rs3_select.decode))

  // Generates the same hardware for checking hazards for rs1, rs2, and rs3
  for (i <- 0 until rs_addr_vec.length) {
    // Check if rs is zero - do not forward if it is
    val rs_zero = isZero(rs_addr_vec(i), rs_select_vec(i))

    // Check which instruction stages hold the rs address in rd and have write enable
    val rs_hazard_vec = VecInit.fill(fwd_vec.length) {false.B}
    for (j <- 0 until fwd_vec.length) {
      rs_hazard_vec(j) := checkHazard(rs_addr_vec(i), rs_select_vec(i), fwd_vec(j)) && !rs_zero
    }

    // Flag that a hazard is present
    val rs_hazard_flag = rs_hazard_vec.contains(true.B)
    // Index for the first instruction stage that has a hazard
    val rs_hazard_idx = rs_hazard_vec.indexWhere(_ === true.B)

    io.out.forward_vec(i) := rs_hazard_flag

    // If hazard, send the data to be forwarded from correct source
    when (rs_hazard_flag) {
      when (fwd_vec(rs_hazard_idx).bits.data.valid) {
        io.out.rs_forward_data_vec(i) := fwd_vec(rs_hazard_idx).bits.data.bits
      } .otherwise {
        io.out.stall.decode := !io.out.flush.decode
        io.out.rs_forward_data_vec(i) := 0.U
      }
    }
  }

  // Stall CSR read and write until pipeline is empty
  // This needs to be done for two reasons:
  // 1. E.g. minstret is incremented when an instruction reaches WB stage. Therefore we
  //    need to wait until the preceding instructions finish to get the correct value
  // 2. A preceding instruction might cause a trap - we cannot write to a register
  //    before knowing that it won't be the case
  val ret_vec = VecInit(io.in.ret.memaccess0, io.in.ret.memaccess1, io.in.ret.writeback)
  val ret_exists = ret_vec.asUInt.orR
  val csr_wr_en = io.in.csr_wr_en.execute
  val csr_rd_en = io.in.csr_rd_en.execute

  when (csr_wr_en || csr_rd_en) {
    when (ret_exists) {
      io.out.stall.execute := true.B
    }
  }


  /**
    * Returns if source register is the zeroth register in Integer register file
    * If F-extension is not toggled, this check is not needed
    */
  def isZero(rs: UInt, rs_select: RegFileSelect.Type) : Bool = {
    if (config.extension_set.contains(ISAExtension.F)) {
      (rs === 0.U) && (rs_select === RegFileSelect.INT)
    }
    else {
      (rs === 0.U)
    }
  }

  /**
    * Returns if rd and rs addresses match AND they point to the same register file AND 
    * write enable is on for rd
    */
  def checkHazard(rs: UInt,
                  rs_select: RegFileSelect.Type,
                  fwd_data: Valid[ForwardBundle]
                  ) : Bool = {
    val addr_match = (fwd_data.bits.rd_addr === rs)
    var and_list = Seq[Bool](addr_match)
    val rf_select_match = (fwd_data.bits.wr_reg_select === rs_select) && rs_select =/= RegFileSelect.NONE
    and_list = and_list :+ rf_select_match :+ fwd_data.valid
    val result = VecInit(and_list).asUInt.andR
    result
  }

}
