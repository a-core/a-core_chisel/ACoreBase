// SPDX-License-Identifier: Apache-2.0

package acorebase

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.iotesters.PeekPokeTester
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import a_core_common._
import scala.math.{ pow }

// An IO with the same input and output, and a valid signal for the input
class CSRRegIO[+T <: Data](gen: T) extends Bundle {
  val in    = Flipped(Valid(gen))
  val out   = Output(gen)
}
object CSRRegIO {
  def apply[T <: Data](gen: T): CSRRegIO[T] = new CSRRegIO(gen)
}

/** IO definitions for ControlStatusRegs */
case class ControlStatusRegsIO(XLEN: Int) extends Bundle{

  val event = new Bundle {
    val m_op       = Input(Bool())
    val f_op       = Input(Bool())
    val b_op       = Input(Bool())
    val j_op       = Input(Bool())
    val instret    = Input(Bool())
    val fflags_en  = Input(Bool())
  }
  val trap       = Input(Bool())

  // Inputs from trap controller
  val mepc       = Input(UInt(XLEN.W))
  val mcause     = Input(UInt(XLEN.W))

  // Fault port
  val fflags     = Input(UInt(5.W))

  // Write port
  val read_en    = Input(Bool())
  val write_en   = Input(Bool())
  val csr_waddr  = Input(UInt(12.W))
  val write_data = Input(UInt(32.W))

  // Read port
  val read_data = Output(UInt(32.W))
  val csr_raddr = Input(UInt(12.W))
  
  // Outputs
  val rm             = Output(UInt(3.W))     // Dynamic Rounding Mode
  val mtvec          = Output(UInt(XLEN.W))
  val mie            = Output(Bool())        // Global interrupt enable
  val minstret       = Output(UInt(64.W))
  val mepc_out       = Output(UInt(XLEN.W))
  val load_indicator = Output(Bool())  // Indicates that "heavy" computing happens
  val illegal_instr  = Output(Bool())
}

/** Module definition for ControlStatusRegs
  * @param config ACoreConfig configuration class
  */
class ControlStatusRegs(config: ACoreConfig) extends Module {
    val io = IO(ControlStatusRegsIO(XLEN=config.data_width))

    val perf_mons = Module(new PerfMonitors(config))
    perf_mons.io.mcountinhibit.in.bits    := 0.U
    perf_mons.io.mcountinhibit.in.valid   := false.B
    perf_mons.io.mcycle.in.bits           := 0.U
    perf_mons.io.mcycle.in.valid          := false.B
    perf_mons.io.minstret.in.bits         := 0.U
    perf_mons.io.minstret.in.valid        := false.B
    perf_mons.io.muldivthreshold.in.bits  := 0.U
    perf_mons.io.muldivthreshold.in.valid := false.B
    perf_mons.io.muldivcycles.in.bits     := 0.U
    perf_mons.io.muldivcycles.in.valid    := false.B
    perf_mons.io.mhpmevent.foreach  (f => {f.in.bits := 0.U; f.in.valid := false.B})
    perf_mons.io.mhpmcounter.foreach(f => {f.in.bits := 0.U; f.in.valid := false.B})
    perf_mons.io.m_op    := io.event.m_op
    perf_mons.io.f_op    := io.event.f_op
    perf_mons.io.b_op    := io.event.b_op
    perf_mons.io.j_op    := io.event.j_op
    perf_mons.io.instret := io.event.instret

    io.load_indicator := perf_mons.io.load_indicator
    io.minstret       := perf_mons.io.minstret.out
    io.illegal_instr  := false.B

    val frm = RegInit(0.U(3.W))
    val fflags = RegInit(0.U(5.W))
    
    // Trap specific CSRs
    val mcause = RegInit(0.U(config.data_width.W))
    val mtvec  = RegInit(0.U(config.data_width.W))
    val mepc   = RegInit(0.U(config.data_width.W))

    // mstatus has 64-bits
    // For RV32, it is separated into two registers
    val mstatus = RegInit(0.U(config.data_width.W))
    val mstatush = RegInit(0.U(32.W))

    // mscratch is free to read and write
    // typically swapped with a user register upon entry to 
    // an M-mode trap handler
    val mscratch = RegInit(0.U(config.data_width.W))


    /** nonstandard CSRs **/
    // simulation stop request
    val mstopsim = RegInit(0.U(2.W))

    // misa register tells what extensions are in use
    // Default misa value is generated in a_core_common
    val misaReg = RegInit(config.default_misa.asUInt) 

    io.read_data := 0.U

    io.mtvec :=  mtvec
    io.mie := mstatus(3)
    io.mepc_out := mepc
    io.rm := frm

    when (io.trap) {
      // Trap overrides everything
      mcause := io.mcause
      mepc   := io.mepc
    }

    when (io.write_en) {
      when (io.csr_waddr === "h001".U) {
        fflags := io.write_data(4,0)
      } .elsewhen (io.csr_waddr === "h002".U) {
        frm := io.write_data(2,0)
      } .elsewhen (io.csr_waddr === "h003".U) {
        fflags := io.write_data(4,0)
        frm := io.write_data(7,5)
      } .elsewhen (io.csr_waddr === "hBC0".U) {
        mstopsim := io.write_data(1,0)
      } .elsewhen (io.csr_waddr === "hBC1".U) {
        perf_mons.io.muldivthreshold.in.bits  := io.write_data
        perf_mons.io.muldivthreshold.in.valid := true.B
      } .elsewhen (io.csr_waddr === "hBC2".U) {
        perf_mons.io.muldivcycles.in.bits  := io.write_data
        perf_mons.io.muldivcycles.in.valid := true.B
      } .elsewhen (io.csr_waddr === "h300".U) {
        mstatus := Cat(
          0.U(24.W),
          io.write_data(7),
          0.U(3.W),
          io.write_data(3),
          0.U(3.W)
        )
      } .elsewhen (io.csr_waddr === "h305".U) {
        mtvec := io.write_data
      } .elsewhen (io.csr_waddr === "h310".U) {
        mstatush := 0.U
      } .elsewhen (io.csr_waddr === "h320".U) {
        perf_mons.io.mcountinhibit.in.bits  := io.write_data
        perf_mons.io.mcountinhibit.in.valid := true.B
      } .elsewhen (io.csr_waddr === "h340".U) {
        mscratch := io.write_data
      } .elsewhen (io.csr_waddr === "h341".U) {
        when (!io.trap) {
          mepc := Cat(io.write_data(31,2), 0.U(2.W))
        }
      } .elsewhen (io.csr_waddr === "h342".U) {
        when (!io.trap) {
          mcause := io.write_data(3,0)
        }
      // mhpmcounter 
      } .elsewhen ((io.csr_waddr >= "hB03".U) && (io.csr_waddr <= "hB1F".U)) {
        if (config.data_width == 32) {
          perf_mons.io.mhpmcounter(io.csr_waddr(4,0)).in.bits  :=
            Cat(perf_mons.io.mhpmcounter(io.csr_waddr(4,0)).out(63,32), io.write_data)
          perf_mons.io.mhpmcounter(io.csr_waddr(4,0)).in.valid := true.B
        } else {
          perf_mons.io.mhpmcounter(io.csr_waddr(4,0)).in.bits := io.write_data
        }
      // mhpmcounterh
      } .elsewhen (io.csr_waddr >= "hB83".U && io.csr_waddr <= "hB9F".U) {
        // FIXME: raise illegal instruction exception with XLEN=64
        if (config.data_width == 32) {
          perf_mons.io.mhpmcounter(io.csr_waddr(4,0)).in.bits  :=
            Cat(io.write_data, perf_mons.io.mhpmcounter(io.csr_waddr(4,0)).out(31,0))
          perf_mons.io.mhpmcounter(io.csr_waddr(4,0)).in.valid := true.B
        } else {
          io.illegal_instr := true.B
        }
      // mhpmevent  
      } .elsewhen(io.csr_waddr >= "h323".U && io.csr_waddr <= "h33F".U){
        perf_mons.io.mhpmevent(io.csr_waddr(4,0)).in.bits  := io.write_data
        perf_mons.io.mhpmevent(io.csr_waddr(4,0)).in.valid := true.B
      } .otherwise {
        io.illegal_instr := true.B
      }
    } .elsewhen(io.event.fflags_en) {
      fflags := fflags | io.fflags
    }

    when (io.csr_raddr === "h001".U) {
      io.read_data := fflags.pad(32)
    } .elsewhen (io.csr_raddr === "h002".U) {
      io.read_data := frm.pad(32)
    } .elsewhen (io.csr_raddr === "h003".U) {
      io.read_data := Cat(frm, fflags).pad(32)
    // misa
    } .elsewhen (io.csr_raddr === "h301".U) {
      io.read_data := misaReg
    // mcycle
    } .elsewhen (io.csr_raddr === "hB00".U) {
      if (config.data_width == 32) {
        io.read_data := perf_mons.io.mcycle.out(31,0)
      } else {
        io.read_data := perf_mons.io.mcycle.out(63,0)
      }
    // minstret
    } .elsewhen (io.csr_raddr === "hB02".U) {
      if (config.data_width == 32) {
        io.read_data := perf_mons.io.minstret.out(31,0)
      } else {          
        io.read_data := perf_mons.io.minstret.out(63,0)
      }
    // mcycleh
    } .elsewhen (io.csr_raddr === "hB80".U) {
      // FIXME: raise illegal instruction exception with XLEN=64
      if (config.data_width == 32) {
        io.read_data := perf_mons.io.mcycle.out(63,32)
      }
    // minstreth
    } .elsewhen (io.csr_raddr === "hB82".U) {
      // FIXME: raise illegal instruction exception with XLEN=64
      if (config.data_width == 32) {
        io.read_data := perf_mons.io.minstret.out(63,32)
      }
    // mhpmcounter 
    } .elsewhen (io.csr_raddr >= "hB03".U && io.csr_raddr <= "hB1F".U) {
      if (config.data_width == 32) {
        io.read_data := perf_mons.io.mhpmcounter(io.csr_raddr(4,0)).out(31,0)
      } else {
        io.read_data := perf_mons.io.mhpmcounter(io.csr_raddr(4,0)).out(63,0) 
      }
    // mhpmcounterh
    } .elsewhen (io.csr_raddr >= "hB83".U && io.csr_raddr <= "hB9F".U) {
      if (config.data_width == 32) {
        io.read_data := perf_mons.io.mhpmcounter(io.csr_raddr(4,0)).out(63,32)
      } else {
        io.illegal_instr := io.read_en
      }
    // mhpmevent  
    } .elsewhen(io.csr_raddr >= "h323".U && io.csr_raddr <= "h33F".U){
      io.read_data := perf_mons.io.mhpmevent(io.csr_raddr(4,0)).out
    // mstopsim
    } .elsewhen (io.csr_raddr === "hBC0".U) {
      io.read_data := mstopsim.pad(32)
    // muldivthreshold
    } .elsewhen (io.csr_raddr === "hBC1".U) {
      io.read_data := perf_mons.io.muldivthreshold.out
    // muldivcycles
    } .elsewhen (io.csr_raddr === "hBC2".U) {
      io.read_data := perf_mons.io.muldivcycles.out
    // mstatus
    } .elsewhen (io.csr_raddr === "h300".U) {
      io.read_data := Cat(
        0.U(24.W),
        mstatus(7),
        0.U(3.W),
        mstatus(3),
        0.U(3.W)
      )
    // mtvec
    } .elsewhen (io.csr_raddr === "h305".U) {
      io.read_data := mtvec
    // mstatush
    } .elsewhen (io.csr_raddr === "h310".U) {
      if (config.data_width == 32) io.read_data := 0.U(32.W)
    // mcountinhibit
    } .elsewhen (io.csr_raddr === "h320".U) {
      io.read_data := perf_mons.io.mcountinhibit.out
    // mscratch
    } .elsewhen (io.csr_raddr === "h340".U) {
      io.read_data := mscratch
    // mepc_in
    } .elsewhen (io.csr_raddr === "h341".U) {
      io.read_data := Cat(mepc(31,2), 0.U(2.W))
    // mcause
    } .elsewhen (io.csr_raddr === "h342".U) {
      io.read_data := mcause(3,0)
    } .otherwise {
      io.illegal_instr := io.read_en
    }
}

/** This gives you verilog */
object ControlStatusRegs extends App with OptionParser {
  // Generate verilog
  val annos = Seq(ChiselGeneratorAnnotation(() => new ControlStatusRegs(config=ACoreConfig.default)))
  (new ChiselStage).execute(args ++ Array("--target-dir", "verilog"), annos)
}
