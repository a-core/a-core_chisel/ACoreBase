package acorebase

import chisel3._
import chisel3.util._
import chisel3.experimental._
import a_core_common._

case class PerfMonitorsIO() extends Bundle {
    // Event enable inputs
    val m_op = Input(Bool())
    val f_op = Input(Bool())
    val b_op = Input(Bool())
    val j_op = Input(Bool())

    // Instruction retired
    val instret = Input(Bool())

    // Standard CSRs
    val mcountinhibit = CSRRegIO(UInt(32.W))
    val mcycle = CSRRegIO(UInt(64.W))
    val minstret = CSRRegIO(UInt(64.W))
    val mhpmevent = Vec(32, CSRRegIO(UInt(32.W)))
    val mhpmcounter = Vec(32, CSRRegIO(UInt(64.W)))

    // PMU signals
    // Indicates that "heavy" computing happens
    val load_indicator = Output(Bool())
    val muldivthreshold = CSRRegIO(UInt(32.W))
    val muldivcycles = CSRRegIO(UInt(32.W))
}

class PerfMonitors(config: ACoreConfig) extends Module {
    val io = IO(PerfMonitorsIO())

    // MCOUNTINHIBIT
    // machine counter-inhibit CSR, res increment when corresponding bit is clear(0) 
    // By default, mcycle and minstret are enabled, the rest disabled
    val mcountinhibit = RegInit((VecInit.tabulate(32) { x => {
      if (0 <= x && x < 3 ) {
        0.U
      } else {
        1.U
      }
    } }))

    io.mcountinhibit.out := mcountinhibit.asUInt
    when (io.mcountinhibit.in.valid) {
      mcountinhibit(0) := io.mcountinhibit.in.bits(0)
      mcountinhibit(2) := io.mcountinhibit.in.bits(2)
      for (i <- config.perf_monitors.hpm_counters) {
        mcountinhibit(i) := io.mcountinhibit.in.bits(i)
      }
    }

    // MCYCLE
    // elapsed clock cycles
    val mcycle = RegInit(0.U(64.W))
    when (!mcountinhibit(0)) {
      mcycle := mcycle + 1.U
    }

    io.mcycle.out := mcycle

    // MINSTRET
    // number of retired (completed) instructions
    val minstret = RegInit(0.U(64.W))
    when (io.instret && !mcountinhibit(2)) {
      minstret := minstret + 1.U
    }

    io.minstret.out := minstret

    // MHPMEVENT 3-31
    // XLEN-bit event selectors. Indicate which event triggers the counter. 0 =  "no event", 1 = "event"
    val mhpmeventVec = RegInit(VecInit.fill(32) { 0.U(config.data_width.W) })
    // Connect all to output but generate write logic to only enabled ones
    io.mhpmevent.zip(mhpmeventVec).foreach{case (a, b) => a.out := b}
    for (i <- config.perf_monitors.hpm_counters) {
      when (io.mhpmevent(i).in.valid) {
        mhpmeventVec(i) := io.mhpmevent(i).in.bits(0) // Only 0th bit is used for now
      }
    }

    // MHPMCOUNTER 3-31
    // 64-bit event counters 
    val mhpmcounterVec = RegInit(VecInit(Seq.fill(32)(0.U(64.W))))
    // Connect all to output but generate write logic to only enabled ones
    // Indices 0-2 not used here
    io.mhpmcounter.zip(mhpmcounterVec).foreach{case (a, b) => a.out := b}
    for (i <- config.perf_monitors.hpm_counters) {
      if (i == 3) {
        when (io.m_op && !mcountinhibit(i)) {
          mhpmcounterVec(i) := mhpmcounterVec(i) + 1.U
        }
      }
      if (i == 4) {
        when (io.f_op && !mcountinhibit(i)) {
          mhpmcounterVec(i) := mhpmcounterVec(i) + 1.U
        }
      }
      if (i == 5) {
        when (io.b_op && !mcountinhibit(i)) {
          mhpmcounterVec(i) := mhpmcounterVec(i) + 1.U
        }
      }
      if (i == 6) {
        when (io.j_op && !mcountinhibit(i)) {
          mhpmcounterVec(i) := mhpmcounterVec(i) + 1.U
        }
      }

      when (io.mhpmcounter(i).in.valid) {
        mhpmcounterVec(i) := io.mhpmcounter(i).in.bits
      }
    }

    /** nonstandard CSR **/
    // PMU stuff
    // custom csr reg. How many mul/div operations should occur before flag is sent
    val muldivthreshold = RegInit(0.U(32.W))
    io.muldivthreshold.out := muldivthreshold
    if (config.perf_monitors.use_load_indicator) {
      when (io.muldivthreshold.in.valid) {
        muldivthreshold := io.muldivthreshold.in.bits
      }
    }
    // custom csr reg. Contains "length" of the cycle window. Only one flag can be sent per window
    val muldivcycles = RegInit(0.U(32.W))
    io.muldivcycles.out := muldivcycles
    if (config.perf_monitors.use_load_indicator) {
      when (io.muldivcycles.in.valid) {
        muldivcycles := io.muldivcycles.in.bits
      }
    }

    // couter and freq flag for mul/div operations 
    if (config.perf_monitors.use_load_indicator && config.perf_monitors.hpm_counters.contains(3)) {
      // give flag when x(muldivthreshold) amount of operations happen inside cycle window y(muldivcycles)
      val muldivop = RegInit(0.U(16.W))
      val muldivcycles_counter = RegInit(0.U(16.W))
      when (!mcountinhibit(3)) {
        muldivcycles_counter := muldivcycles_counter + 1.U
      }
      when (io.m_op) {
        muldivop := muldivop + 1.U
      }
      // Send flag when X amount of operations happen
      when (muldivop === muldivthreshold && muldivthreshold =/= 0.U) {
        io.load_indicator := true.B
        muldivop := muldivop + 1.U
      } .otherwise {
        io.load_indicator := false.B
      }
      // Reset counters when window y is reached
      when (muldivcycles_counter === muldivcycles) {
        muldivcycles_counter := 0.U
        muldivop := 0.U
      }
    } else {
      io.load_indicator := false.B
    }

}