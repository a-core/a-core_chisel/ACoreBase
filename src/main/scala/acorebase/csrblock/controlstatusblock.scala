// SPDX-License-Identifier: Apache-2.0

// Chisel module ControlStatusBlock
// Module for CS register file
// Inititally written by chisel-blocks-utils initmodule.sh, 2021-07-20
package acorebase

import chisel3.experimental._
import chisel3._
import chisel3.util._
import chisel3.iotesters.PeekPokeTester
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import a_core_common._

class ControlStatusBlockInputs(XLEN: Int = 32) extends Bundle {

  val data = new Bundle {
    val csr_raddr   = UInt(12.W)
    val csr_waddr   = UInt(12.W)
    val rs1         = UInt(5.W)
    val rs1_rdata   = UInt(XLEN.W)
    val fflags      = UInt(5.W)
  }

  val control = new Bundle {
    val csr_op     = CSROp()
    val read_en    = Bool()
    val write_en   = Bool()
  }

  val event = Output(ControlStatusRegsIO(XLEN).event)

  val trap = new Bundle {
    val mcause     = UInt(XLEN.W)
    val mepc       = UInt(XLEN.W)
    val trap       = Bool()
  }
}

class ControlStatusBlockOutputs(XLEN: Int = 32) extends Bundle {
  val data = new Bundle {
    val csr_rdata = UInt(XLEN.W)
    val rm        = UInt(3.W) // Dynamic Rounding Mode
    val minstret  = UInt(64.W)
  }

  val trap = new Bundle {
    val mtvec     = UInt(XLEN.W)
    val mepc      = UInt(XLEN.W)
  }
  val load = new Bundle {
    val load_indicator = Bool()
  }
}


/** Module definition for ControlStatusBlock
  * @param config ACoreConfig configuration class
  */
class ControlStatusBlock(config: ACoreConfig) extends Module {

  val io = IO(new Bundle {
    val in  = Input(new ControlStatusBlockInputs(config.data_width))
    val out = Output(new ControlStatusBlockOutputs(config.data_width))
  })

  val csregs = Module(new ControlStatusRegs(config))
  val csrex  = Module(new CSREx(config))


  csregs.io.mcause  := io.in.trap.mcause
  csregs.io.mepc    := io.in.trap.mepc
  csregs.io.trap    := io.in.trap.trap

  csregs.io.csr_waddr  := io.in.data.csr_waddr
  csregs.io.csr_raddr  := io.in.data.csr_raddr
  csregs.io.fflags     := io.in.data.fflags
  csregs.io.read_en    := io.in.control.read_en
  csregs.io.write_en   := io.in.control.write_en
  csregs.io.write_data := csrex.io.out.data.csr_wdata
  
  csregs.io.event <> io.in.event

  csrex.io.in.data.csr_rdata := csregs.io.read_data
  csrex.io.in.data.rs1       := io.in.data.rs1
  csrex.io.in.data.rs1_rdata := io.in.data.rs1_rdata
  csrex.io.in.control.csr_op := io.in.control.csr_op

  io.out.load.load_indicator := csregs.io.load_indicator
  io.out.data.csr_rdata      := 0.U
  io.out.data.rm             := csregs.io.rm
  io.out.data.csr_rdata      := csregs.io.read_data
  io.out.trap.mtvec          := csregs.io.mtvec
  io.out.trap.mepc           := csregs.io.mepc_out
  io.out.data.minstret       := csregs.io.minstret

}

/** This gives you verilog */
object ControlStatusBlock extends App with OptionParser {
  // Generate verilog
  val annos = Seq(ChiselGeneratorAnnotation(() => new ControlStatusBlock(config=ACoreConfig.default)))
  (new ChiselStage).execute(args ++ Array("--target-dir", "verilog"), annos)
}
