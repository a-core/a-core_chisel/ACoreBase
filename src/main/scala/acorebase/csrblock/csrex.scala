// SPDX-License-Identifier: Apache-2.0

// Chisel module CSREx
// Module for executing a CSR operation during Execute stage
package acorebase

import chisel3.experimental._
import chisel3._
import chisel3.util._
import chisel3.iotesters.PeekPokeTester
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import a_core_common._

class CSRExInputs(XLEN: Int = 32) extends Bundle {
  val data = new Bundle {
    val rs1_rdata = UInt(XLEN.W) // Data from rs1
    val rs1       = UInt(5.W)    // rs1 field in instruction
    val csr_rdata = UInt(XLEN.W) // Data read from CSR
  }

  val control = new Bundle {
    val csr_op   = CSROp()       // CSR operation
  }
}

class CSRExOutputs(XLEN: Int = 32) extends Bundle {
  val data = new Bundle {
    val csr_wdata = UInt(XLEN.W)  // CSR write data
  }
}


/** Performs CSR operations 
  * @param config ACoreConfig configuration class
  */
class CSREx(config: ACoreConfig) extends Module {

  val io = IO(new Bundle {
    val in = Input(new CSRExInputs(config.data_width))
    val out = Output(new CSRExOutputs(config.data_width))
  })

  // zero-extend immediate in rs1
  val uimm = io.in.data.rs1.pad(config.data_width)

  // Generating wdata depending on CSR operation
  // No need to do anything if write enable is false
  when(io.in.control.csr_op === CSROp.CSRRW) {
    io.out.data.csr_wdata := io.in.data.rs1_rdata
  } .elsewhen(io.in.control.csr_op === CSROp.CSRRS) {
    io.out.data.csr_wdata := io.in.data.csr_rdata | io.in.data.rs1_rdata
  } .elsewhen(io.in.control.csr_op === CSROp.CSRRC) {
    io.out.data.csr_wdata := io.in.data.csr_rdata & (~io.in.data.rs1_rdata)
  } .elsewhen(io.in.control.csr_op === CSROp.CSRRWI) {
    io.out.data.csr_wdata := uimm
  } .elsewhen(io.in.control.csr_op === CSROp.CSRRSI) {
    io.out.data.csr_wdata := io.in.data.csr_rdata | uimm
  } .elsewhen(io.in.control.csr_op === CSROp.CSRRCI) {
    io.out.data.csr_wdata := io.in.data.csr_rdata & (~uimm)
  } .otherwise {
    io.out.data.csr_wdata := io.in.data.csr_rdata
  }

}

/** This gives you verilog */
object CSREx extends App with OptionParser {
  // Generate verilog
  val annos = Seq(ChiselGeneratorAnnotation(() => new CSREx(config=ACoreConfig.default)))
  (new ChiselStage).execute(args ++ Array("--target-dir", "verilog"), annos)
}
