// SPDX-License-Identifier: Apache-2.0

package acorebase

import chisel3._
import chisel3.util._
import chisel3.experimental._
import a_core_common._


trait Debug {

  /** encode a string UInt as ASCII */
  def ascii_bits(message: String): Bits = {
    ("h" + message.map(c => "%02x".format(c.toByte)).mkString).U
  }

  /**
   * Parses instrction name from instruction type
   * For example: Instruction.LUI -> "LUI"
   * @param instr
   * @return
   */
  def toSimpleString(instr: Instruction.Type): String = {
    val pattern = """\((?:\d*=(\w.*))\).*""".r
    val ret = pattern.findAllIn(instr.toString()).matchData.next().group(1)
    ret
  }

}

/** Debug IO definitions */
case class DebugIO() extends Bundle {
  val instruction_text = Output(Bits(100.W))
}

class DebugInstrText extends Module with Debug {

  val io = IO(new Bundle {
    val instr      = Input(Instruction())
    val instr_text = Output(Bits(100.W))
  })
  io.instr_text := ascii_bits("xxxxxxxx")
  Instruction.all.foreach(instr => {
    when (io.instr === instr) { io.instr_text := ascii_bits(toSimpleString(instr))}
  })
}

/** RISC-V Formal Interface
  * @see
  *   Specification https://github.com/YosysHQ/riscv-formal/blob/main/docs/rvfi.md TODO: Add outputs for optional RVFI
  *   features when necessary
  */
case class RVFIIO(XLEN: Int) extends Bundle {
  // Instruction metadata
  val valid = Output(Bool())
  val order = Output(UInt(64.W))
  val insn  = Output(UInt(32.W))
  val trap  = Output(Bool())
  val halt  = Output(Bool())
  val intr  = Output(Bool())
  val mode  = Output(UInt(2.W))
  val ixl   = Output(UInt(2.W))

  // Integer register read/write
  val rs1_addr  = Output(UInt(5.W))
  val rs2_addr  = Output(UInt(5.W))
  val rs1_rdata = Output(UInt(XLEN.W))
  val rs2_rdata = Output(UInt(XLEN.W))
  val rd_addr   = Output(UInt(5.W))
  val rd_wdata  = Output(UInt(XLEN.W))

  // Program counter
  val pc_rdata = Output(UInt(XLEN.W))
  val pc_wdata = Output(UInt(XLEN.W))

  // Memory access
  val mem_addr  = Output(UInt(XLEN.W))
  val mem_rmask = Output(UInt((XLEN / 8).W))
  val mem_wmask = Output(UInt((XLEN / 8).W))
  val mem_rdata = Output(UInt(XLEN.W))
  val mem_wdata = Output(UInt(XLEN.W))
}
