package acorebase

import chisel3._
import chisel3.util._
import chisel3.experimental._
import a_core_common._

case class ExecuteIO(config: ACoreConfig) extends Bundle {
  val in = Flipped(Decoupled((new Bundle {
    val instr_bits    = UInt(config.data_width.W)
    val alu_ctrl      = new ALUBlockInputs(config.data_width).control
    val mul_op        = MultOp()
    val csr_ctrl      = new ControlStatusBlockInputs(config.data_width).control
    val rd_src        = RegFileSrc()
    val wr_reg_select = RegFileSelect()
    val imm           = UInt(config.data_width.W)
    val pc            = UInt(config.addr_width.W)
    val rs1_rdata     = UInt(config.data_width.W)
    val rs2_rdata     = UInt(config.data_width.W)
    val rs3_rdata     = UInt(config.data_width.W)
    val trap_cause    = Valid(ExceptionCauses())
    val lsu_control   = new LSUControlIO(config.data_width)
  })))
  val out = Decoupled(new Bundle {
    val mul         = Valid((new MultiplierPipelined(config.data_width)).Stage1Outputs())
    val alu         = new ALUBlockOutputs(config.data_width)
    val rd_wdata    = Valid(UInt(config.data_width.W))
    val lsu_wdata   = UInt(config.data_width.W)
    val lsu_addr    = UInt(config.addr_width.W)
    val lsu_wmask   = UInt(4.W)
    val trap_cause  = Valid(ExceptionCauses())
    val forward     = Output(ForwardBundle(config.data_width))
  })
  val ctrl_in = Input(new Bundle {
    val flush = Bool()
  })
  val csr_out = Output(new ControlStatusBlockOutputs(config.data_width))
  val csr_event_in = Input(new ControlStatusBlockInputs(config.data_width).event)
  val csr_trap_in = Input(new ControlStatusBlockInputs(config.data_width).trap)
}

/**
  * Execute stage. Following things occur here:
    - Integer arithmetic-logic unit (ALU)
    - Load-store Unit (LSU) write data alignment and mask generation
    - Divider for integer division operations. This op takes 36 clock cycles.
    - First stage of a pipelined multiplier
    - Control-status register (CSR) read-modify-write
  *
  * @param config
  */
class Execute(config: ACoreConfig) extends Module {
  val io = IO(ExecuteIO(config))

  val mul          = new MultiplierPipelined(config.data_width)
  val mul_s1       = Module(new mul.Stage1)
  val alu_block    = Module(new ALUBlock(config))
  val divider      = Module(new Divider(XLEN = config.data_width))
  val csreg_block  = Module(new ControlStatusBlock(config))

  val emptyReg     = RegInit(true.B)

  val stageValid   = WireDefault(true.B)
  val rd_misalig   = WireDefault(false.B)
  val wr_misalig   = WireDefault(false.B)
  val lsu_op_width = MemOpWidth(riscv_util.getFunct3(io.in.bits.instr_bits)(1, 0))
  val lsu_addr     = alu_block.io.out.data.result
  val lsu_data     = io.in.bits.rs2_rdata
  val numLanes     = config.data_width / 8

  // Stage is "empty" and thus ready to receive data
  when (io.out.fire && !io.in.fire) {
    emptyReg := true.B
  } .elsewhen(io.in.fire) {
    emptyReg := false.B
  }

  alu_block.io.in.data.pc                := io.in.bits.pc
  alu_block.io.in.data.imm               := io.in.bits.imm
  alu_block.io.in.data.read_a_data       := io.in.bits.rs1_rdata
  alu_block.io.in.data.read_b_data       := io.in.bits.rs2_rdata
  alu_block.io.in.data.funct3            := riscv_util.getFunct3(io.in.bits.instr_bits)
  alu_block.io.in.data.opcode            := riscv_util.getOpcode(io.in.bits.instr_bits)
  alu_block.io.in.data.clear             := io.ctrl_in.flush
  alu_block.io.in.data.valid             := io.in.valid
  alu_block.io.in.data.f_ext.read_c_data := io.in.bits.rs3_rdata
  alu_block.io.in.data.f_ext.static_rm   := riscv_util.getFunct3(io.in.bits.instr_bits)
  alu_block.io.in.data.f_ext.dynamic_rm  := csreg_block.io.out.data.rm
  alu_block.io.in.control                := io.in.bits.alu_ctrl

  mul_s1.io.in.operand_a             := io.in.bits.rs1_rdata
  mul_s1.io.in.operand_b             := io.in.bits.rs2_rdata
  mul_s1.io.in.op                    := io.in.bits.mul_op
  divider.io.in.bits.op              := io.in.bits.mul_op
  divider.io.in.bits.operand_a       := io.in.bits.rs1_rdata
  divider.io.in.bits.operand_b       := io.in.bits.rs2_rdata
  divider.io.in.valid                := io.in.valid
  divider.io.in.bits.clear           := io.ctrl_in.flush
  csreg_block.io.in.data.csr_raddr   := riscv_util.getFunct12(io.in.bits.instr_bits)
  csreg_block.io.in.data.csr_waddr   := riscv_util.getFunct12(io.in.bits.instr_bits)
  csreg_block.io.in.data.rs1         := riscv_util.getRs1(io.in.bits.instr_bits)
  csreg_block.io.in.data.rs1_rdata   := io.in.bits.rs1_rdata
  // TODO: this will store fflags immediately after alu op so that there is no stalling between
  // consecutive F ops
  // However, a trap in some situation would cause incorrect fflags to be stored
  // Solution would be to store fflags at WB and provide forwarding
  // This should be done when new FPU is integrated
  csreg_block.io.in.data.fflags      := alu_block.io.out.data.f_ext.fault_data
  csreg_block.io.in.control.csr_op   := io.in.bits.csr_ctrl.csr_op
  csreg_block.io.in.control.read_en  := io.in.bits.csr_ctrl.read_en  && io.in.valid && !io.ctrl_in.flush
  // Write only at output handshake
  csreg_block.io.in.control.write_en := io.in.bits.csr_ctrl.write_en && io.in.valid && !io.ctrl_in.flush && io.out.fire
  csreg_block.io.in.event            := io.csr_event_in
  csreg_block.io.in.trap             := io.csr_trap_in

  when (!io.in.valid) {
    mul_s1.io.in.op                    := MultOp.NULL
    divider.io.in.bits.op              := MultOp.NULL
  }

  io.csr_out                 := csreg_block.io.out
  io.out.bits.mul            := mul_s1.io.out
  io.out.bits.alu            := alu_block.io.out
  io.in.ready                := (io.out.ready && stageValid) || emptyReg
  io.out.bits.rd_wdata.valid := false.B
  io.out.bits.rd_wdata.bits  := 0.U
  io.out.valid               := stageValid && io.in.valid

  // Multiplex the correct result and valid signal
  switch (io.in.bits.rd_src) {
    is (RegFileSrc.ALU_RESULT) {
      stageValid                 := alu_block.io.out.data.valid
      io.out.bits.rd_wdata.bits  := alu_block.io.out.data.result
      io.out.bits.rd_wdata.valid := alu_block.io.out.data.valid
    }
    is (RegFileSrc.DIV_RESULT) {
      stageValid                 := divider.io.out.valid
      io.out.bits.rd_wdata.bits  := divider.io.out.bits.result
      io.out.bits.rd_wdata.valid := divider.io.out.valid
    }
    is (RegFileSrc.CSR_RESULT) {
      stageValid                 := io.in.valid
      io.out.bits.rd_wdata.bits  := csreg_block.io.out.data.csr_rdata
      io.out.bits.rd_wdata.valid := true.B
    }
    is (RegFileSrc.IMM)        {
      stageValid                 := io.in.valid
      io.out.bits.rd_wdata.bits  := io.in.bits.imm
      io.out.bits.rd_wdata.valid := true.B
    }
    is (RegFileSrc.PC_4)       {
      stageValid                 := io.in.valid
      io.out.bits.rd_wdata.bits  := io.in.bits.pc + 4.U
      io.out.bits.rd_wdata.valid := true.B
    }
  }

  // Calculate whether address is misaligned
  when (lsu_op_width === MemOpWidth.HWORD 
        && lsu_addr(0) === 1.U) {
    rd_misalig := io.in.bits.lsu_control.ren
    wr_misalig := io.in.bits.lsu_control.wen
  } .elsewhen(lsu_op_width.isOneOf(Seq(MemOpWidth.WORD, MemOpWidth.DWORD)) 
              && alu_block.io.out.data.result(1,0) =/= 0.U) {
    rd_misalig := io.in.bits.lsu_control.ren
    wr_misalig := io.in.bits.lsu_control.wen
  }

  // Calculate write mask and data
  io.out.bits.lsu_wdata := 0.U
  io.out.bits.lsu_wmask := 0.U
  io.out.bits.lsu_addr  := lsu_addr
  when (lsu_op_width === MemOpWidth.BYTE) {
    for (i <- 0 until numLanes) {
      when (lsu_addr(log2Up(numLanes)-1, 0) === i.U) {
        io.out.bits.lsu_wdata := lsu_data(7,0) << i*8
        io.out.bits.lsu_wmask := 1.U << i
      }
    }
  } .elsewhen(lsu_op_width === MemOpWidth.HWORD) {
    for (i <- 0 until numLanes/2) {
      when (lsu_addr(log2Up(numLanes)-1,0) === (2*i).U) {
        io.out.bits.lsu_wdata := lsu_data(15,0) << i*2*8
        io.out.bits.lsu_wmask := "b11".U << i*2
      }
    }
  } .elsewhen(lsu_op_width === MemOpWidth.WORD) {
    io.out.bits.lsu_wdata := lsu_data
    io.out.bits.lsu_wmask := "b1111".U
  }

  // Check for traps
  io.out.bits.trap_cause := io.in.bits.trap_cause
  when (alu_block.io.out.data.exception) {
    io.out.bits.trap_cause.bits  := ExceptionCauses.INSTR_ADDR_MISALIG
    io.out.bits.trap_cause.valid := true.B
  } .elsewhen (rd_misalig) {
    io.out.bits.trap_cause.bits  := ExceptionCauses.LOAD_ADDR_MISALIG
    io.out.bits.trap_cause.valid := true.B
  } .elsewhen (wr_misalig) {
    io.out.bits.trap_cause.bits  := ExceptionCauses.STORE_ADDR_MISALIG
    io.out.bits.trap_cause.valid := true.B
  }

  // Data Forwarding information
  io.out.bits.forward.data          := io.out.bits.rd_wdata
  io.out.bits.forward.rd_addr       := riscv_util.getRd(io.in.bits.instr_bits)
  io.out.bits.forward.wr_reg_select := io.in.bits.wr_reg_select

}