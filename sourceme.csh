set SBYPATH="/prog/riscv/SymbiYosys/bin"
set YOSYSPATH="/prog/riscv/yosys/bin"
set BTORPATH="/prog/riscv/boolector/bin"

set TOOLPATHS="$SBYPATH $YOSYSPATH $BTORPATH"

foreach TOOLPATH ($TOOLPATHS)
    if ( "${PATH}" !~ *"${TOOLPATH}"* ) then
        echo "Adding ${TOOLPATH} to path"
        setenv PATH ${TOOLPATH}:${PATH}
    else
        echo "${TOOLPATH} already in path"
    endif
end

unset SBYPATH
unset YOSYSPATH
unset TOOLPATHS
 
