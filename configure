#!/usr/bin/env bash
#############################################################################
# This is a Chisel RTL generator configuration script file. 
# The purpose is to control the compilation and testing
# in parametrized manner from this single file
# Created by Marko Kosunen on 24.03.2015
# Last modification by Marko Kosunen, marko.kosunen@aalto.fi, 11.12.2018 20:27
#############################################################################
##Function to display help with -h argument and to control 
##The configuration from the commnad line
help_f()
{
    echo -e "CONFIGURE Release 1.0 (04.04.2016)"
    echo -e "configure-configuration script for Chisel RTL generation"
    echo -e "Written by Marko "Pikkis" Kosunen"
    echo -e -n "\n"
    echo -e "SYNOPSIS"
    echo -e "  configure [OPTIONS] "
    echo -e "DESCRIPTION"
    echo -e "  Producess all configurations and Makefile for the Chisel RTL generation"
    echo -e -n "\n"
    echo -e "OPTIONS"
    echo -e "  -h"
    echo -e "      Show this help."
}

while getopts h opt
do
  case "$opt" in
    h) help_f; exit 0;;
    \?) help_f;;
  esac
done


#$FIRRTL #Sets up THESDKHOME from gloabl setup file
#. ../../TheSDK.config
ROOTPATH=`pwd`
VERILOGDIR=verilog
VERILOGPATH=${ROOTPATH}/${VERILOGDIR}
SCALAPATH=${ROOTPATH}/src/main/scala
FIRRTL_JAR="$ROOTPATH/rocket-chip/firrtl/utils/bin/firrtl.jar"

VPATHDIRS=`find ${SCALAPATH}/* -type d -exec basename '{}' ';'`
MODULES="ACoreBase LSU_AXI4L"
TEST_MODULES="ALUBlockSpec ALUSpec MultiplierBlockSpec DivisionBlockSpec PCBlockSpec RegFileSpec"
FLOAT_TEST_MODULES="FPUBlockSpec_AutoGen FPUBlockSpec_Manual FPUBlockSpec_TestFloat"
FORMAL_MODULES="LSU"
PACKAGE="acorebase"

############################# MAKEFILE   ####################################
CURRENTFILE="${ROOTPATH}/Makefile"
echo "Creating ${CURRENTFILE} for modules:"
for i in ${MODULES}; do
    echo "- $i"
done
cat <<EOF > ${CURRENTFILE}
#Directories
VERILOGDIR = ${VERILOGDIR}
VERILOGPATH = ${VERILOGPATH}
SCALAPATH = ${SCALAPATH}
#DEPDIR :=.depdir
#\$(shell mkdir -p \$(DEPDIR) >/dev/null)
\$(shell mkdir -p \$(VERILOGPATH) >/dev/null)
MODULES= ${MODULES}
TEST_MODULES=${TEST_MODULES}
FORMAL_MODULES = ${FORMAL_MODULES}
PACKAGE=${PACKAGE}

TARGETS = \$(foreach name,\$(MODULES), \$(VERILOGDIR)/\$(name).v)
TEST_TARGETS = \$(foreach name,\$(TEST_MODULES), test_\$(name))
FLOAT_TEST_TARGETS = \$(foreach name,\$(FLOAT_TEST_MODULES), test_\$(name))

#Commands
SBT=sbt -J-Xmx16G -J-Xss8M

# Default parameters
core_config ?= ""
isa_string ?= ""
debug ?= ""
rvfi ?= false

MAINPARAMS = -core_config \$(core_config) \\
			-isa_string \$(isa_string) \\
			-debug \$(debug) \\
			-rvfi \$(rvfi)

TOUCH=touch -r
`for i in ${VPATHDIRS}; do
    echo vpath %.scala \\$\(SCALAPATH\)/$i
done`
.PHONY: all test help doc clean \$(MODULES)


all: \$(TARGETS)

# Test all in TEST_TARGETS
test: \
`
echo ""
echo -en "\t\\$(SBT) 'testOnly"
for i in ${TEST_MODULES}; do
    echo -n " ${PACKAGE}.$i"
done
echo "'"
`

# Test all in FLOAT_TEST_TARGETS
test_f: \
`
echo ""
echo -en "\t\\$(SBT) 'testOnly"
for i in ${FLOAT_TEST_MODULES}; do
    echo -n " ${PACKAGE}.$i"
done
echo "'"
`

# Recipes for individual modules
`for i in ${MODULES}; do
    echo -e "$i: \\$(VERILOGDIR)/$i.v"
done`

# Test recipes for all test modules
`for i in ${TEST_MODULES}; do
	echo ".PHONY: test_$i"
    echo "test_$i:"
	echo -e "\t\\$(SBT) 'testOnly ${PACKAGE}.$i'"
done`
`for i in ${FLOAT_TEST_MODULES}; do
	echo ".PHONY: test_$i"
    echo "test_$i:"
	echo -e "\t\\$(SBT) 'testOnly ${PACKAGE}.$i'"
done`

`for i in ${FORMAL_MODULES}; do
    echo "bmc_$i: ${VERILOGDIR}/${i}Formal.v"
    echo -e "\tsby -f formal/$i.sby bmc"
done`

`for i in ${FORMAL_MODULES}; do
    echo "cover_$i: ${VERILOGDIR}/${i}Formal.v"
    echo -e "\tsby -f formal/$i.sby cover"
done`

`for i in ${FORMAL_MODULES}; do
    echo "prove_$i: ${VERILOGDIR}/${i}Formal.v"
    echo -e "\tsby -f formal/$i.sby prove"
done`


`for i in ${FORMAL_MODULES}; do
	echo ".PHONY: ${VERILOGDIR}/${i}Formal.v"
    echo "${VERILOGDIR}/${i}Formal.v: src/main/resources/${i}Formal.v"
    echo -e "\t\\$(SBT) 'runMain \\$(PACKAGE).${i} -formal true -td ${VERILOGDIR}'"
done`

clean:
	rm -f \$(VERILOGPATH)/*.v
	rm -f \$(VERILOGPATH)/*.anno
	rm -f \$(VERILOGPATH)/*.fir
	rm -rf \$(VERILOGPATH)
	#rm -rf \$(DEPDIR)

MMOD ?=
memmap:
	cp \$(VERILOGPATH)/\$(MMOD).v  \$(VERILOGPATH)/\$(MMOD)_unmapped.orig.v
	\$(SBT) 'runMain \$(MMOD).\$(MMOD) -td \$(VERILOGPATH) --infer-rw \$(MMOD) --repl-seq-mem -c:\$(MMOD):-o:\$(VERILOGPATH)/\$(MMOD).conf'

# Generate cleanup recipes for individual modules
`for i in ${MODULES}; do
	echo .PHONY: clean_$i
	echo clean_$i: 
	echo -e "\trm -f \\$(VERILOGPATH)/$i.v"
	echo -e "\trm -f \\$(VERILOGPATH)/$i.anno"
	echo -e "\trm -f \\$(VERILOGPATH)/$i.fir"
    echo -e "\trm -f \\$(VERILOGPATH)/${i}_memmapped.conf"
    echo -e "\trm -f \\$(VERILOGPATH)/${i}_memmapped.v"
done`

help:
	@echo "configured modules are:";
	@for i in \$(MODULES) ; do \\
	   echo \$\$i; \\
	done
	@echo "\ntestable modules are:";
	@for i in \$(TEST_MODULES) ; do \\
	   echo \$\$i; \\
	done


doc:
	sbt doc
EOF
##################Hereafter some files you should not need to modify ################################

